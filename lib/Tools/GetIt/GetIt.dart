// import 'package:cineprize/ServiceLayer/Auth/Rest/CurrentUser/CurrentUserService.dart';
// import 'package:cineprize/ServiceLayer/Management/StateManagement.dart';
// import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
// import 'package:get_it/get_it.dart';
//
// final GetIt getIt = GetIt.instance;
//
// void setupGetIt() {
//   // ServiceLayer
//   //Not : [Provider Package kullandığım için bu nesneler ApplicationLayer'da Provider objesinin içinde kullanılmalıdır.]
//   getIt.registerLazySingleton<StateManagement>(() => StateManagement());
//   getIt.registerLazySingleton<UserManagement>(() => UserManagement());
//   getIt.registerLazySingleton<CurrentUserService>(() => CurrentUserService());
// }
