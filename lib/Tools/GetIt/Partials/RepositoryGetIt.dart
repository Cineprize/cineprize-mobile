// import 'package:cineprize/RepositoryLayer/Auth/CurrentUser/CurrentUserRepository.dart';
// import 'package:cineprize/RepositoryLayer/Auth/Logout/LogoutRepository.dart';
// import 'package:cineprize/RepositoryLayer/Auth/ManualAccount/ManualAccountLoginRepository.dart';
// import 'package:cineprize/RepositoryLayer/Auth/ManualAccount/ManualAccountRegisterRepository.dart';
// import 'package:cineprize/RepositoryLayer/CRUD/Category/CategoryReadRepository.dart';
// import 'package:cineprize/RepositoryLayer/CRUD/Channel/CategoryReadRepository.dart';
// import 'package:cineprize/RepositoryLayer/CRUD/Channel/ChannelFavoriteRepository.dart';
// import 'package:cineprize/RepositoryLayer/CRUD/Channel/ChannelVideosReadRepository.dart';
// import 'package:cineprize/RepositoryLayer/CRUD/Comment/CommentRepository.dart';
// import 'package:cineprize/RepositoryLayer/CRUD/Coupon/EnterCouponRepository.dart';
// import 'package:cineprize/RepositoryLayer/CRUD/FollowWin/FollowWinRepository.dart';
// import 'package:cineprize/RepositoryLayer/CRUD/Giveaway/GiveawayJoinRepository.dart';
// import 'package:cineprize/RepositoryLayer/CRUD/Giveaway/GiveawayReadRepository.dart';
// import 'package:cineprize/RepositoryLayer/CRUD/Profile/ProfileUpdateRepository.dart';
// import 'package:cineprize/RepositoryLayer/CRUD/Question/QuestionReadRepository.dart';
// import 'package:cineprize/RepositoryLayer/CRUD/Question/QuestionReplyRepository.dart';
// import 'package:cineprize/RepositoryLayer/CRUD/Question/QuestionViewRepository.dart';
// import 'package:cineprize/RepositoryLayer/CRUD/Shop/BuyProductRepository.dart';
// import 'package:cineprize/RepositoryLayer/CRUD/Shop/ProductReadRepository.dart';
// import 'package:cineprize/RepositoryLayer/CRUD/Slider/SliderReadRepository.dart';
// import 'package:cineprize/RepositoryLayer/CRUD/Surprise/BuySurpriseRepository.dart';
// import 'package:cineprize/RepositoryLayer/CRUD/Surprise/SurpriseReadRepository.dart';
// import 'package:cineprize/RepositoryLayer/CRUD/Video/TrendReadRepository.dart';
// import 'package:cineprize/RepositoryLayer/CRUD/Video/VideoLikeDislikeRepository.dart';
// import 'package:cineprize/RepositoryLayer/CRUD/Video/VideoReadRepository.dart';
// import 'package:cineprize/RepositoryLayer/CRUD/Video/VideoSetViewRepository.dart';
// import 'package:cineprize/RepositoryLayer/Storage/StorageUploadRepository.dart';
// import 'package:cineprize/Tools/GetIt/GetIt.dart';
//
// void repositoryGetIt() {
//   // RepositoryLayer
//   getIt.registerLazySingleton<ManualAccountLoginRepository>(() => ManualAccountLoginRepository());
//   getIt.registerLazySingleton<ManualAccountRegisterRepository>(() => ManualAccountRegisterRepository());
//   getIt.registerLazySingleton<LogoutRepository>(() => LogoutRepository());
//   // getIt.registerLazySingleton<CurrentUserRepository>(() => CurrentUserRepository());
//   // ************************************************************************ //
//
//   // getIt.registerLazySingleton<StorageUploadRepository>(() => StorageUploadRepository());
//
//   getIt.registerLazySingleton<ProfileUpdateRepository>(() => ProfileUpdateRepository());
//
//   getIt.registerLazySingleton<EnterCouponRepository>(() => EnterCouponRepository());
//
//   getIt.registerLazySingleton<SliderReadRepository>(() => SliderReadRepository());
//
//   getIt.registerLazySingleton<CategoryReadRepository>(() => CategoryReadRepository());
//
//   getIt.registerLazySingleton<VideoReadRepository>(() => VideoReadRepository());
//
//   getIt.registerLazySingleton<ChannelVideosReadRepository>(() => ChannelVideosReadRepository());
//
//   getIt.registerLazySingleton<ChannelFavoriteRepository>(() => ChannelFavoriteRepository());
//
//   getIt.registerLazySingleton<ProductReadRepository>(() => ProductReadRepository());
//
//   getIt.registerLazySingleton<BuyProductRepository>(() => BuyProductRepository());
//
//   getIt.registerLazySingleton<VideoLikeDislikeRepository>(() => VideoLikeDislikeRepository());
//
//   getIt.registerLazySingleton<SurpriseReadRepository>(() => SurpriseReadRepository());
//
//   getIt.registerLazySingleton<BuySurpriseRepository>(() => BuySurpriseRepository());
//
//   getIt.registerLazySingleton<VideoSetViewRepository>(() => VideoSetViewRepository());
//
//   getIt.registerLazySingleton<TrendReadRepository>(() => TrendReadRepository());
//
//   getIt.registerLazySingleton<FollowWinRepository>(() => FollowWinRepository());
//
//   getIt.registerLazySingleton<CategoryToChannelReadRepository>(() => CategoryToChannelReadRepository());
//
//   getIt.registerLazySingleton<QuestionReadRepository>(() => QuestionReadRepository());
//
//   getIt.registerLazySingleton<QuestionViewRepository>(() => QuestionViewRepository());
//
//   getIt.registerLazySingleton<QuestionReplyRepository>(() => QuestionReplyRepository());
//
//   getIt.registerLazySingleton<GiveawayReadRepository>(() => GiveawayReadRepository());
//
//   getIt.registerLazySingleton<GiveawayJoinRepository>(() => GiveawayJoinRepository());
//
//   //Comment Repository Get Id
//   getIt.registerLazySingleton<CommentRepository>(() => CommentRepository());
// }
