// import 'package:cineprize/AbstractLayer/Auth/Logout/LogoutBase.dart';
// import 'package:cineprize/AbstractLayer/Auth/ManualAccount/ManualAccountLoginBase.dart';
// import 'package:cineprize/AbstractLayer/Auth/ManualAccount/ManualAccountRegisterBase.dart';
// import 'package:cineprize/AbstractLayer/CRUD/Category/CategoryReadBase.dart';
// import 'package:cineprize/AbstractLayer/CRUD/Channel/CategoryToChannelReadBase.dart';
// import 'package:cineprize/AbstractLayer/CRUD/Channel/ChannelFavoriteBase.dart';
// import 'package:cineprize/AbstractLayer/CRUD/Channel/ChannelVideosReadBase.dart';
// import 'package:cineprize/AbstractLayer/CRUD/Comment/CommentBase.dart';
// import 'package:cineprize/AbstractLayer/CRUD/Coupon/EnterCouponBase.dart';
// import 'package:cineprize/AbstractLayer/CRUD/FollowWin/FollowWinBase.dart';
// import 'package:cineprize/AbstractLayer/CRUD/Giveaway/GiveawayJoinBase.dart';
// import 'package:cineprize/AbstractLayer/CRUD/Giveaway/GiveawayReadBase.dart';
// import 'package:cineprize/AbstractLayer/CRUD/Question/QuestionReadBase.dart';
// import 'package:cineprize/AbstractLayer/CRUD/Question/QuestionReplyBase.dart';
// import 'package:cineprize/AbstractLayer/CRUD/Question/QuestionViewBase.dart';
// import 'package:cineprize/AbstractLayer/CRUD/Shop/BuyProductBase.dart';
// import 'package:cineprize/AbstractLayer/CRUD/Shop/ProductReadBase.dart';
// import 'package:cineprize/AbstractLayer/CRUD/Slider/SliderReadBase.dart';
// import 'package:cineprize/AbstractLayer/CRUD/Surprise/BuySurpriseBase.dart';
// import 'package:cineprize/AbstractLayer/CRUD/Surprise/SurpriseReadBase.dart';
// import 'package:cineprize/AbstractLayer/CRUD/Video/TrendReadBase.dart';
// import 'package:cineprize/AbstractLayer/CRUD/Video/VideoLikeDislikeBase.dart';
// import 'package:cineprize/AbstractLayer/CRUD/Video/VideoReadBase.dart';
// import 'package:cineprize/AbstractLayer/CRUD/Video/VideoSetViewBase.dart';
// import 'package:cineprize/DataAccessLayer/Auth/Rest/Logout/LogoutRest.dart';
// import 'package:cineprize/DataAccessLayer/Auth/Rest/ManualAccountLoginRest.dart';
// import 'package:cineprize/DataAccessLayer/Auth/Rest/ManualAccountRegisterRest.dart';
// import 'package:cineprize/DataAccessLayer/CRUD/Rest/Category/CategoryReadRest.dart';
// import 'package:cineprize/DataAccessLayer/CRUD/Rest/Channel/CategoryToChannelReadRest.dart';
// import 'package:cineprize/DataAccessLayer/CRUD/Rest/Channel/ChannelFavoriteRest.dart';
// import 'package:cineprize/DataAccessLayer/CRUD/Rest/Channel/ChannelVideosReadRest.dart';
// import 'package:cineprize/DataAccessLayer/CRUD/Rest/Comment/CommentRest.dart';
// import 'package:cineprize/DataAccessLayer/CRUD/Rest/Coupon/EnterCouponRest.dart';
// import 'package:cineprize/DataAccessLayer/CRUD/Rest/FollowWin/FollowWinRest.dart';
// import 'package:cineprize/DataAccessLayer/CRUD/Rest/Giveaway/GiveawayJoinRest.dart';
// import 'package:cineprize/DataAccessLayer/CRUD/Rest/Giveaway/GiveawayReadRest.dart';
// import 'package:cineprize/DataAccessLayer/CRUD/Rest/Question/QuestionReadRest.dart';
// import 'package:cineprize/DataAccessLayer/CRUD/Rest/Question/QuestionReplyRest.dart';
// import 'package:cineprize/DataAccessLayer/CRUD/Rest/Question/QuestionViewRest.dart';
// import 'package:cineprize/DataAccessLayer/CRUD/Rest/Shop/BuyProductRest.dart';
// import 'package:cineprize/DataAccessLayer/CRUD/Rest/Shop/ProductReadRest.dart';
// import 'package:cineprize/DataAccessLayer/CRUD/Rest/Slider/SliderReadRest.dart';
// import 'package:cineprize/DataAccessLayer/CRUD/Rest/Surprise/BuySurpriseRest.dart';
// import 'package:cineprize/DataAccessLayer/CRUD/Rest/Surprise/SurpriseReadRest.dart';
// import 'package:cineprize/DataAccessLayer/CRUD/Rest/Video/TrendReadRest.dart';
// import 'package:cineprize/DataAccessLayer/CRUD/Rest/Video/VideoLikeDislikeRest.dart';
// import 'package:cineprize/DataAccessLayer/CRUD/Rest/Video/VideoReadRest.dart';
// import 'package:cineprize/DataAccessLayer/CRUD/Rest/Video/VideoSetViewRest.dart';
// import 'package:cineprize/Tools/GetIt/GetIt.dart';
//
// void dataAccessGetIt() {
//   // DataAccessLayer
//   getIt.registerLazySingleton<ManualAccountLoginBase>(() => ManualAccountLoginRest());
//   getIt.registerLazySingleton<ManualAccountRegisterBase>(() => ManualAccountRegisterRest());
//   getIt.registerLazySingleton<LogoutBase>(() => LogoutRest());
//   // getIt.registerLazySingleton<CurrentUserBase>(() => CurrentUserFirebase());
//   // ************************************************************************ //
//
//   // getIt.registerLazySingleton<StorageUploadBase>(() => StorageUploadFirebase());
//
//   // getIt.registerLazySingleton<ProfileUpdateBase>(() => ProfileUpdateRest());
//
//   getIt.registerLazySingleton<EnterCouponBase>(() => EnterCouponRest());
//
//   getIt.registerLazySingleton<SliderReadBase>(() => SliderReadRest());
//
//   getIt.registerLazySingleton<CategoryReadBase>(() => CategoryReadRest());
//
//   getIt.registerLazySingleton<VideoReadBase>(() => VideoReadRest());
//
//   getIt.registerLazySingleton<ChannelVideosReadBase>(() => ChannelVideosReadRest());
//
//   getIt.registerLazySingleton<ChannelFavoriteBase>(() => ChannelFavoriteRest());
//
//   getIt.registerLazySingleton<ProductReadBase>(() => ProductReadRest());
//
//   getIt.registerLazySingleton<BuyProductBase>(() => BuyProductRest());
//
//   getIt.registerLazySingleton<VideoLikeDislikeBase>(() => VideoLikeDislikeRest());
//
//   getIt.registerLazySingleton<SurpriseReadBase>(() => SurpriseReadRest());
//
//   getIt.registerLazySingleton<BuySurpriseBase>(() => BuySurpriseRest());
//
//   getIt.registerLazySingleton<TrendReadBase>(() => TrendReadRest());
//
//   getIt.registerLazySingleton<FollowWinBase>(() => FollowWinRest());
//
//   getIt.registerLazySingleton<CategoryToChannelReadBase>(() => CategoryToChannelReadRest());
//
//   getIt.registerLazySingleton<QuestionReadBase>(() => QuestionReadRest());
//
//   getIt.registerLazySingleton<QuestionViewBase>(() => QuestionViewRest());
//
//   getIt.registerLazySingleton<QuestionReplyBase>(() => QuestionReplyRest());
//
//   getIt.registerLazySingleton<VideoSetViewBase>(() => VideoSetViewRest());
//
//   getIt.registerLazySingleton<GiveawayReadBase>(() => GiveawayReadRest());
//
//   getIt.registerLazySingleton<GiveawayJoinBase>(() => GiveawayJoinRest());
//
//   // Comment Data Get Its
//   getIt.registerLazySingleton<CommentBase>(() => CommentRest());
// }
