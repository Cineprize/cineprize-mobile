import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MySharedPreferences {
  static Future<int?> getInt({required String key}) async {
    final prefs = await SharedPreferences.getInstance();
    final value = prefs.getInt(key);
    return value;
  }

  static Future<bool> setInt({required String key, required int value}) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.setInt(key, value);
  }

  static Future<double?> getDouble({required String key}) async {
    final prefs = await SharedPreferences.getInstance();
    final value = prefs.getDouble(key);
    return value;
  }

  static Future<bool> setDouble(
      {required String key, required int value}) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.setInt(key, value);
  }

  static Future<String?> getString({required String key}) async {
    final prefs = await SharedPreferences.getInstance();
    final value = prefs.getString(key);
    return value;
  }

  static Future<bool> setString(
      {required String key, required String value}) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.setString(key, value);
  }
}
