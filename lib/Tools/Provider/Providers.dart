import 'package:cineprize/ServiceLayer/Auth/Rest/CurrentUser/CurrentUserService.dart';
import 'package:cineprize/ServiceLayer/Management/StateManagement.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Providers {
  final Widget child;

  Providers({required this.child});

  Widget include() {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<CurrentUserService>(
            create: (_) => CurrentUserService.include),
        ChangeNotifierProvider<StateManagement>(
            create: (_) => StateManagement.include),
        ChangeNotifierProvider<UserManagement>(
            create: (_) => UserManagement.include),
      ],
      child: this.child,
    );
  }
}
