// TODO:: android reklam için yorum satırından çıkar
import 'package:google_mobile_ads/google_mobile_ads.dart';

import 'AdHelper.dart';

class AdmobInitialize {
  static AdmobInitialize? _instance;

  static AdmobInitialize get instance {
    if (_instance != null) return _instance!;

    _instance = AdmobInitialize.init();
    return _instance!;
  }

  AdmobInitialize.init() {
    MobileAds.instance.initialize();
  }

  Function? rewardedAd({required RewardedAdLoadCallback listener}) {
    RewardedAd.load(
      adUnitId: AdHelper.rewardedAdUnitId,
      request: AdRequest(),
      rewardedAdLoadCallback: listener,
    );
  }
}
