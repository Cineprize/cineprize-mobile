import 'dart:io';

class AdHelper {
  static String get rewardedAdUnitId {
    if (Platform.isAndroid) {
      /*      return 'ca-app-pub-3940256099942544/5224354917'; */
      return 'ca-app-pub-6371419771126701/6064867167';
    } else if (Platform.isIOS) {
      return 'ca-app-pub-6371419771126701/3326142270';
    } else {
      throw new UnsupportedError('Desteklenmeyen Platform');
    }
  }
}
