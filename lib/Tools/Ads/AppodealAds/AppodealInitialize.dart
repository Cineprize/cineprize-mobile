/* import 'package:appodeal_flutter/appodeal_flutter.dart';
import 'package:cineprize/Models/User.dart';
import 'package:cineprize/ServiceLayer/Management/StateManagement.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:cineprize/Tools/Ads/AndroidAdsTypeEnum.dart';
import 'package:cineprize/Tools/Ads/IOSAdsTypeEnum.dart';

AndroidAdsTypeEnum androidAdsTypeEnum = AndroidAdsTypeEnum.ExtraGains;
IOSAdsTypeEnum iosAdsTypeEnum = IOSAdsTypeEnum.VideoQuestion;
Future callBackFunction;

class AppodealInitialize {
  static const androidAppKey = 'df8b8db7df2a01596380d584149fac12d3a16205b848dc1a';
  static const iosAppKey = 'be6ad1dcb962bf49a0ead8f1d6550f938b458d8f2ff7710e';

  static void start(UserManagement userManagement, StateManagement stateManagement) {
    User user = userManagement.user;

    //Appodeal.requestIOSTrackingAuthorization();

    // Set the app keys (Uygulama anahtarlarını ayarlayın)
    Appodeal.setAppKeys(androidAppKey: androidAppKey, iosAppKey: iosAppKey);

    // Geçiş reklamı, etkinliği tetikledi
    Appodeal.setInterstitialCallback(
      (event) => print('Interstitial ad triggered the event $event'),
    );

    /* Appodeal.setRewardCallback(
      (String event) async {
        //print('Reward ad triggered the event $event'),

        // android ekstra kazançlar
        if (Platform.isAndroid) {
          // Ödül reklam, etkinliği tetikledi
          if (event == 'onRewardedVideoClosed') {
            switch (androidAdsTypeEnum) {
              case AndroidAdsTypeEnum.ExtraGains:
                print('Aaa oldu');
                final String url = '$baseApiUrl/ad-watched';

                var response = await http.post(url,
                    headers: {'Authorization': 'Bearer ${user.token}'});

                Map<String, dynamic> responseJson = jsonDecode(response.body);

                if (responseJson['success']) {
                  User newUser = User.fromMap(responseJson['data']);

                  user.coin = newUser.coin;
                  user.gold = newUser.gold;
                  user.usedGold = newUser.usedGold;
                  stateManagement.state = ViewStateEnum.Idle;
                  return MyInfoDialog(loginNavigatorContext,
                      title: 'Tebrikler!',
                      description: responseJson['message'],
                      buttonText: 'Tamam',
                      alertType: AlertType.success);
                } else {
                  stateManagement.state = ViewStateEnum.Idle;
                }
                break;
              case AndroidAdsTypeEnum.VideoQuestion:
                await callBackFunction;
                break;
            }
          }
        } else if (Platform.isIOS) {
          if (event == 'onRewardedVideoClosed') {
            switch (androidAdsTypeEnum) {
              case AndroidAdsTypeEnum.ExtraGains:
                final String url = '$baseApiUrl/ad-watched';

                var response = await http.post(url,
                    headers: {'Authorization': 'Bearer ${user.token}'});

                Map<String, dynamic> responseJson = jsonDecode(response.body);

                if (responseJson['success']) {
                  User newUser = User.fromMap(responseJson['data']);

                  user.coin = newUser.coin;
                  user.gold = newUser.gold;
                  user.usedGold = newUser.usedGold;

                  stateManagement.state = ViewStateEnum.Idle;
                  return MyInfoDialog(loginNavigatorContext,
                      title: 'Tebrikler!',
                      description: responseJson['message'],
                      buttonText: 'Tamam',
                      alertType: AlertType.success);
                } else {
                  stateManagement.state = ViewStateEnum.Idle;
                }
                break;
              case AndroidAdsTypeEnum.VideoQuestion:
                await callBackFunction;
                break;
            }
          }
        }
      },
    ); */

    Appodeal.setAutoCache(AdType.REWARD, true);

    // Yetki verildikten veya verildikten sonra Appodeal'ı başlatın
    Appodeal.initialize(
      hasConsent: true,
      adTypes: [
        AdType.REWARD,
      ],
      testMode: false,
    );
  }
}
 */
