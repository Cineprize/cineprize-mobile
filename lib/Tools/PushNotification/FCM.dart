import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class FCM {
  final FirebaseMessaging _fcm = FirebaseMessaging.instance;

  /* /// Create a [AndroidNotificationChannel] for heads up notifications
  const AndroidNotificationChannel channel = AndroidNotificationChannel(
    'high_importance_channel', // id
    'High Importance Notifications', // title
    'This channel is used for important notifications.', // description
    importance: Importance.high,
  );

  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin(); */

  /* Future<void> _firebaseMessagingBackgroundHandler(
      RemoteMessage message) async {
    // If you're going to use other Firebase services in the background, such as Firestore,
    // make sure you call `initializeApp` before using other Firebase services.
    await Firebase.initializeApp();
    print('Handling a background message ${message.messageId}');
  }
 */
  Future initialize() async {
    await _fcm.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );

    /*  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler); */

    /* await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel); */

    /* await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );
 */
    /* _fcm.requestNotificationPermissions();
    _fcm.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('onMessage ==== $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('onMessage ==== $message');
      },
      onResume: (Map<String, dynamic> message) async {
        print('onMessage ==== $message');
      },
    );
 */
    void _serialiseAndNavigate(Map<String, dynamic> message) {
      var notificationData = message['data'];
    }
  }
}
