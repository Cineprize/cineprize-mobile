import 'package:cineprize/Models/User.dart';

abstract class CurrentUserBase {
  Future<User> getCurrentUser();
}
