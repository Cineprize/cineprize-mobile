import 'package:cineprize/Models/Response.dart';

abstract class PhoneVerificationBase {
  Future<Response> sendCode({
    required String phone,
  });

  Future<Response> verifyCode({
    required String phone,
    required String code,
  });

  Future<Response> checkCode();
}
