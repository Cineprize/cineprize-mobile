import 'package:cineprize/Models/Response.dart';
import 'package:cineprize/Models/User.dart';

abstract class AuthBase {
  Future<void> logout();

  Future<Response> forgetPassword({required String email});

  Future<User> login({required String id, required String password});

  Future<User> register(
      {required String userName,
      required String email,
      required String password});

  Future<User> registerDetail(
      {required DateTime birthDate,
      required String gender,
      required String refer,
      required String canEarn,
      required String userName,
      required String password});
}
