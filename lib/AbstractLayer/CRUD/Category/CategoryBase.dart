import 'package:cineprize/Models/Response.dart';

abstract class CategoryBase {
  Future<Response> read();
}
