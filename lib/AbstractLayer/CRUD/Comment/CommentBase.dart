import 'package:cineprize/Models/Response.dart';

abstract class CommentBase {
  Future<Response> add({required String content, required String videoID});

  Future<Response> delete({required String commentID});

  Future<Response> like({required String commentID});

  Future<Response> complain({required String commentID});

  Future<Response> read({required String videoID, int page});

  Future<Response> ban({required String commentID});

  Future<Response> block({required String commentID});
}
