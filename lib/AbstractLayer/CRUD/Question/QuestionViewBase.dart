import 'package:flutter/cupertino.dart';

abstract class QuestionViewBase {
  Future<bool> view(String question_id);
}
