import 'package:flutter/cupertino.dart';

abstract class QuestionReplyBase {
  Future<Map<String, dynamic>> reply(
      {required String questionID, required String answer});
}
