import 'package:cineprize/Models/Response.dart';
import 'package:flutter/cupertino.dart';

abstract class GuessBase {
  Future<Response> score({required authUser});

  Future<Map<String, dynamic>> status({required authUser});

  Future<Response> bet({required String bet, required authUser});

  Future<Response> questions({required authUser});

  Future<Response> completed(
      {required Map<String, bool> answers, required authUser});
}
