import 'package:cineprize/Models/User.dart';
import 'package:flutter/material.dart';

abstract class ChannelFavoriteBase {
  Future<bool> isFavorite({required String channel_id});

  Future<Map<String, dynamic>> addOrRemove({required String channel_id});
}
