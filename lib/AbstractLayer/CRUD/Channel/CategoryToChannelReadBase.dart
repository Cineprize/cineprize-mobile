import 'package:cineprize/Models/User.dart';

abstract class CategoryToChannelReadBase {
  Future<List<User>> read({required String category_id});
}
