import 'package:cineprize/Models/Video.dart';

abstract class ChannelVideosReadBase {
  Future<List<Video>> lastVideos({required String channel_id});

  Future<List<Video>> videos({required String channel_id, String page});
}
