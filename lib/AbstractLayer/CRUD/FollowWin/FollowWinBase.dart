import 'package:cineprize/Models/User.dart';
import 'package:flutter/cupertino.dart';

abstract class FollowWinBase {
  Future<Map<String, dynamic>> follow(
      {required String type, required User authUser});
}
