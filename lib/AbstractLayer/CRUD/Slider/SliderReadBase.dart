import 'package:cineprize/Models/Slider.dart';

abstract class SliderReadBase {
  Future<List<Slider>> read();
}
