import 'package:cineprize/CommonLayer/Video/VideoQueryType.dart';
import 'package:cineprize/Models/CategoryWithVideo.dart';
import 'package:cineprize/Models/Video.dart';

abstract class VideoReadBase {
  Future<List<Video>> read({VideoQueryType videoQueryType});

  Future<List<CategoryWithVideo>> categoryWithVideoHome();

  Future<List<CategoryWithVideo>> categoryWithVideoHomeOther();
}
