abstract class VideoLikeDislikeBase {
  Future<bool> like({required String videoID});

  Future<bool> dislike({required String videoID});
}
