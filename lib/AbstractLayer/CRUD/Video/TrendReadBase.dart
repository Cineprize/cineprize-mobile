import 'package:cineprize/Models/Video.dart';

abstract class TrendReadBase {
  Future<List<Video>> read();
}
