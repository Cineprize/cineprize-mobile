import 'package:cineprize/Models/VideoDetail.dart';

abstract class VideoDetailBase {
  Future<VideoDetail> get({required String videoId});
}
