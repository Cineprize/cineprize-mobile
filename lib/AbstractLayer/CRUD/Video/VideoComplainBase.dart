import 'package:cineprize/Models/Response.dart';
import 'package:flutter/cupertino.dart';

abstract class VideoComplainBase {
  Future<Response> complain({required String videoID, required String reason});
}
