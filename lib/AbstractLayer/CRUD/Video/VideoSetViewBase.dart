abstract class VideoSetViewBase {
  Future<List<int>> set({required String videoID, required int maxCoin});
}
