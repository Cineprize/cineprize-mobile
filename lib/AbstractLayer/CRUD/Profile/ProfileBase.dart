import 'package:cineprize/Models/User.dart';
import 'package:flutter/cupertino.dart';

abstract class ProfileBase {
  Future<User> update({
    required Map<String, dynamic> data,
    bool isSetState = true,
  });
}
