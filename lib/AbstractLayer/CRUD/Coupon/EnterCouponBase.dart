import 'package:cineprize/Models/User.dart';
import 'package:flutter/cupertino.dart';

abstract class EnterCouponBase {
  Future<Map<String, dynamic>> enter(
      {required String code, required User authUser});
}
