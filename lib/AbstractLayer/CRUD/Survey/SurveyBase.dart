import 'package:flutter/cupertino.dart';

abstract class SurveyBase {
  Future<Map<String, dynamic>> get({required authUser});

  Future<Map<String, dynamic>> choose(
      {required String survey_id,
      required String survey_option_id,
      required authUser});
}
