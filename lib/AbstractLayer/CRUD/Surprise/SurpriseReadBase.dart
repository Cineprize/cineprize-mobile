import 'package:cineprize/Models/Surprise.dart';

abstract class SurpriseReadBase {
  Future<List<Surprise>> read();
}
