import 'package:flutter/cupertino.dart';

abstract class BuySurpriseBase {
  Future<Map<String, dynamic>> buy(
      {required String surprise_id, required int bid, required authUser});
}
