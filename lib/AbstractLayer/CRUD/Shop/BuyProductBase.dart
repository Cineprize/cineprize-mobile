import 'package:cineprize/Models/Product.dart';
import 'package:cineprize/Models/User.dart';
import 'package:flutter/cupertino.dart';

abstract class BuyProductBase {
  Future<Map<String, dynamic>> buy(
      {required Product product, required User authUser});
}
