abstract class SaleReadBase {
  Future<Map<String, dynamic>> read();
}
