import 'package:cineprize/Models/Product.dart';

abstract class ProductReadBase {
  Future<List<Product>> read();
}
