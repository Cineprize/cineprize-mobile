import 'package:cineprize/Models/Giveaway.dart';

abstract class GiveawayReadBase {
  Future<Giveaway> read();
}
