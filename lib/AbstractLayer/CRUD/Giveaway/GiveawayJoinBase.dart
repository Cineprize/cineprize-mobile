import 'package:flutter/material.dart';

abstract class GiveawayJoinBase {
  Future<Map<String, dynamic>> join(String giveawayID);
}
