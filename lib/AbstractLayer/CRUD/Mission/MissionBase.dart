import 'package:cineprize/Models/Mission.dart';
import 'package:cineprize/Models/Response.dart';

abstract class MissionBase {
  Future<Response> read();

  Future<Response> click({required String missionId});
}
