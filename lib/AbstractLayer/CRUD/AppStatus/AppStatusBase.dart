import 'package:cineprize/Models/Response.dart';

abstract class AppStatusBase {
  Future<Response> control(
      {required String platform, required String app_build_number});
}
