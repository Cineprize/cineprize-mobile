import 'dart:io';

import 'package:flutter/cupertino.dart';

abstract class StorageUploadBase {
  Future<String> upload(
      {required String uid,
      required String type,
      required File file,
      required String lastFile});
}
