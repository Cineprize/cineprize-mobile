import 'dart:convert';
import 'dart:io';

import 'package:cineprize/AbstractLayer/Auth/AuthBase.dart';
import 'package:cineprize/ApplicationLayer/Auth/AuthManagement/LoginNavigator.dart';
import 'package:cineprize/DataAccessLayer/DataAccessLayerVariable.dart';
import 'package:cineprize/Models/Response.dart';
import 'package:cineprize/Models/User.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class AuthRest implements AuthBase {
  Future<void> logout() async {
    final url = Uri.parse('$baseApiUrl/logout');

    await http.post(url, headers: {
      'Authorization': 'Bearer ${UserManagement.include.user.token}',
      'Accept': 'application/json'
    });
  }

  @override
  Future<Response> forgetPassword({required String email}) async {
    final url = Uri.parse('$baseApiUrl/forgot-password');

    var response = await http.post(url, body: {
      'email': email,
    }, headers: {
      'Accept': 'application/json',
    });

    Map<String, dynamic> responseJson = jsonDecode(response.body);

    return Response(
        success: responseJson['errors'] == null,
        message: responseJson['errors'] == null
            ? responseJson['message']
            : responseJson['errors']['email'][0]);
  }

  @override
  Future<User> login({required String id, required String password}) async {
    final url = Uri.parse('$baseApiUrl/login');

    String deviceId = await getId();
    var response = await http.post(url, body: {
      'email': id.toString(),
      'password': password.toString(),
      'deviceID': deviceId.toString(),
    });
    Map<String, dynamic> responseJson = jsonDecode(response.body);
    User user;
    bool success = responseJson['success'];
    if (success) {
      user = User.fromMap(responseJson['data']);
    } else {
      user = User.error(errorMessage: responseJson['message'][0].toString());
    }
    return user;
  }

  @override
  Future<User> register(
      {required String userName,
      required String email,
      required String password}) async {
    var client = new http.Client();
    String deviceId = await getId();
    final url = Uri.parse('$baseApiUrl/register');

    var response = await client.post(url, body: {
      'userName': userName,
      'email': email,
      'password': password,
      'deviceID': deviceId.toString(),
    });

    Map<String, dynamic> responseParsed = jsonDecode(response.body.toString());

    bool success = responseParsed['success'];

    client.close();
    print(responseParsed.toString());

    if (success) {
      User user = User.fromMap(responseParsed['data']);
      print("user1:" + user.toString());
      return user;
    } else {
      if (responseParsed['message'][0] != null) {
        User user =
            User.error(errorMessage: responseParsed['message'][0].toString());
        return user;
      }
      if (responseParsed['message']['userName'].toString() != 'null') {
        User user = User.error(
            errorMessage: responseParsed['message']['userName'][0].toString());
        return user;
      }
      if (responseParsed['message']['email'].toString() != 'null') {
        User user = User.error(
            errorMessage: responseParsed['message']['email'][0].toString());
        return user;
      }
      if (responseParsed['message']['password'].toString() != 'null') {
        User user = User.error(
            errorMessage: responseParsed['message']['password'][0].toString());
        return user;
      }
      User user = User.error(
          errorMessage: 'Kayıt sırasında bir hata oluştu!'.toString());
      return user;
    }
  }

  @override
  Future<User> registerDetail({
    required DateTime birthDate,
    required String gender,
    required String refer,
    required String canEarn,
    required String userName,
    required String password,
  }) async {
    var client = new http.Client();

    String deviceId = await getId();

    String platform = Platform.isAndroid ? 'android' : 'ios';
    final url = Uri.parse('$baseApiUrl/register-detail');

    var response = await client.post(url, body: {
      'birthDate': birthDate.toString(),
      'gender': gender,
      'refer': refer,
      'canEarn': canEarn,
      'deviceID': deviceId.toString(),
      'systemOS': platform,
    }, headers: {
      'Authorization': 'Bearer ${UserManagement.include.user.token}',
      'Accept': 'application/json'
    });

    Map<String, dynamic> responseParsed = jsonDecode(response.body.toString());

    bool success = responseParsed['success'];

    User user;
    client.close();

    if (success) {
      user = User.fromMap(responseParsed['data']);
      return user;
    } else {
      if (responseParsed['message'][0] != null) {
        user =
            User.error(errorMessage: responseParsed['message'][0].toString());
        return user;
      }
      if (responseParsed['message']['refer'] != null) {
        user = User.error(
            errorMessage: responseParsed['message']['refer'][0].toString());
        return user;
      }
      if (responseParsed['message']['gender'] != null) {
        user = User.error(
            errorMessage: responseParsed['message']['gender'][0].toString());
        return user;
      }
      if (responseParsed['message']['birthDate'] != null) {
        user = User.error(
            errorMessage: responseParsed['message']['birthDate'][0].toString());
        return user;
      }
      user = User.error(
          errorMessage: 'Kayıt sırasında bir hata oluştu!'.toString());
      return user;
    }
  }
}
