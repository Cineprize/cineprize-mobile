import 'dart:convert';

import 'package:cineprize/AbstractLayer/Auth/PhoneVerificationBase.dart';
import 'package:cineprize/DataAccessLayer/DataAccessLayerVariable.dart';
import 'package:cineprize/Models/Response.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class PhoneVerificationRest implements PhoneVerificationBase {
  @override
  Future<Response> sendCode({required String phone}) async {
    final url = Uri.parse('$baseApiUrl/send-phone-code');
    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };
    var response = await http.post(url,
        body: {
          'phone': phone,
        },
        headers: headers);
    Map<String, dynamic> responseJson = jsonDecode(response.body);
    return Response(
        success: responseJson['success'],
        message: responseJson['message'],
        data: {
          'remainingTime': responseJson['remaining_time'] != null
              ? int.parse(responseJson['remaining_time'].toString())
              : null
        });
  }

  @override
  Future<Response> verifyCode(
      {required String phone, required String code}) async {
    final url = Uri.parse('$baseApiUrl/verify-phone-code');
    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };
    var response = await http.post(url,
        body: {'phone': phone, 'code': code}, headers: headers);
    Map<String, dynamic> responseJson = jsonDecode(response.body);
    return Response(
      success: responseJson['success'].toString() == 'true',
      message: responseJson['message'],
    );
  }

  @override
  Future<Response> checkCode() async {
    final url = Uri.parse('$baseApiUrl/check-phone-code');
    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };

    var response = await http.get(url, headers: headers);
    Map<String, dynamic> responseJson = jsonDecode(response.body);
    return Response(
      success: responseJson['success'].toString() == 'true',
      message: responseJson['message'],
      data: {
        'remainingTime': responseJson['remaining_time'] != null
            ? int.parse(responseJson['remaining_time'].toString())
            : null,
        'phone': responseJson['phone'] != null
            ? responseJson['phone'].toString()
            : null
      },
    );
  }
}
