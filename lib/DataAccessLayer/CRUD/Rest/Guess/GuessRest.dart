import 'dart:convert';

import 'package:cineprize/AbstractLayer/CRUD/Guess/GuessBase.dart';
import 'package:cineprize/DataAccessLayer/DataAccessLayerVariable.dart';
import 'package:cineprize/Models/Response.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class GuessRest implements GuessBase {
  @override
  Future<Response> score({required authUser}) async {
    final url = Uri.parse('$baseApiUrl/guess/score');
    Map<String, String> headers = {'Authorization': 'Bearer ${authUser.token}'};

    var res = await http.get(url, headers: headers);

    Map<String, dynamic> responseJson = jsonDecode(res.body);

    return new Response(
        success: responseJson['success'], message: responseJson['message']);
  }

  @override
  Future<Map<String, dynamic>> status({required authUser}) async {
    final url = Uri.parse('$baseApiUrl/guess/status');
    Map<String, String> headers = {'Authorization': 'Bearer ${authUser.token}'};

    var res = await http.get(url, headers: headers);

    Map<String, dynamic> responseJson = jsonDecode(res.body);

    String message = responseJson['message'];
    bool success = responseJson['success'];
    String status = responseJson['status'];

    return {'message': message, 'success': success, 'status': status};
  }

  @override
  Future<Response> bet({required String bet, authUser}) async {
    final url = Uri.parse('$baseApiUrl/guess/bet');
    Map<String, String> headers = {'Authorization': 'Bearer ${authUser.token}'};

    var res = await http.post(url, headers: headers, body: {'bet': bet});

    Map<String, dynamic> responseJson = jsonDecode(res.body);

    String message = responseJson['message'];
    bool success = responseJson['success'];
    Map<String, dynamic> data = responseJson['data'];

    return new Response(success: success, message: message, data: data);
  }

  @override
  Future<Response> questions({authUser}) async {
    final url = Uri.parse('$baseApiUrl/guess/questions');
    Map<String, String> headers = {'Authorization': 'Bearer ${authUser.token}'};

    var res = await http.get(url, headers: headers);

    Map<String, dynamic> responseJson = jsonDecode(res.body);

    final Response response = new Response(
        success: responseJson['success'],
        message: responseJson['message'],
        data: responseJson['data']);

    return response;
  }

  @override
  Future<Response> completed(
      {required Map<String, bool> answers, authUser}) async {
    final url = Uri.parse('$baseApiUrl/guess/completed');
    Map<String, String> headers = {'Authorization': 'Bearer ${authUser.token}'};

    var res = await http
        .post(url, headers: headers, body: {'answers': json.encode(answers)});

    Map<String, dynamic> responseJson = jsonDecode(res.body);

    String message = responseJson['message'];
    bool success = responseJson['success'];

    return new Response(success: success, message: message);
  }
}
