import 'dart:convert';

import 'package:cineprize/AbstractLayer/CRUD/Category/CategoryBase.dart';
import 'package:cineprize/DataAccessLayer/DataAccessLayerVariable.dart';
import 'package:cineprize/Models/Category.dart';
import 'package:cineprize/Models/Response.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:http/http.dart' as http;

class CategoryRest implements CategoryBase {
  @override
  Future<Response> read() async {
    final url = Uri.parse('$baseApiUrl/category');

    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };

    var response = await http.get(url, headers: headers);

    Map<String, dynamic> responseJson = jsonDecode(response.body);
    List<Category> categories = [];
    if (responseJson['success']) {
      for (int i = 0; i < responseJson['data'].length; i++) {
        categories.add(new Category.fromMap(
          responseJson['data'][i],
        ));
      }
    }
    return Response(
      success: responseJson['success'],
      message: responseJson['message'],
      data: categories,
    );
  }
}
