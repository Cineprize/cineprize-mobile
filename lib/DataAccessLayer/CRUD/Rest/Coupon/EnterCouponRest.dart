import 'dart:convert';

import 'package:cineprize/AbstractLayer/CRUD/Coupon/EnterCouponBase.dart';
import 'package:cineprize/DataAccessLayer/DataAccessLayerVariable.dart';
import 'package:cineprize/Models/User.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:http/http.dart' as http;

class EnterCouponRest implements EnterCouponBase {
  @override
  Future<Map<String, dynamic>> enter(
      {required String code, required User authUser}) async {
    final url = Uri.parse('$baseApiUrl/coupon-enter/$code');
    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };
    var response = await http.get(url, headers: headers);

    Map<String, dynamic> responseJson = jsonDecode(response.body);

    bool success = responseJson['success'];
    String message = responseJson['message'];
    late User user;

    if (responseJson['data'] != null) {
      user = User.fromMap(responseJson['data']);
    }

    return {'message': message, 'success': success, 'data': user};
  }
}
