import 'dart:convert';

import 'package:cineprize/AbstractLayer/CRUD/Channel/ChannelFavoriteBase.dart';
import 'package:cineprize/DataAccessLayer/DataAccessLayerVariable.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:http/http.dart' as http;

class ChannelFavoriteRest implements ChannelFavoriteBase {
  @override
  Future<bool> isFavorite({required String channel_id}) async {
    final url = Uri.parse('$baseApiUrl/channel/is-favorite/$channel_id');
    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };

    var response = await http.get(url, headers: headers);

    Map<String, dynamic> responseJson = jsonDecode(response.body);

    return responseJson['isFavorite'];
  }

  @override
  Future<Map<String, dynamic>> addOrRemove({required String channel_id}) async {
    final url = Uri.parse('$baseApiUrl/channel/favorite');

    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };

    var response = await http
        .post(url, headers: headers, body: {'channel_id': channel_id});

    Map<String, dynamic> responseJson = jsonDecode(response.body);

    String message = responseJson['message'];
    bool success = responseJson['success'];
    bool isFavorite = responseJson['isFavorite'];

    return {'message': message, 'success': success, 'isFavorite': isFavorite};
  }
}
