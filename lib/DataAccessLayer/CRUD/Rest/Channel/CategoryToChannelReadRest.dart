import 'dart:convert';

import 'package:cineprize/AbstractLayer/CRUD/Channel/CategoryToChannelReadBase.dart';
import 'package:cineprize/DataAccessLayer/DataAccessLayerVariable.dart';
import 'package:cineprize/Models/User.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class CategoryToChannelReadRest implements CategoryToChannelReadBase {
  @override
  Future<List<User>> read({required String category_id}) async {
    User user = UserManagement.include.user;

    final url = Uri.parse('$baseApiUrl/channel/$category_id');
    Map<String, String> headers = {'Authorization': 'Bearer ${user.token}'};

    var response = await http.get(url, headers: headers);

    Map<String, dynamic> responseJson = jsonDecode(response.body);

    bool success = responseJson['success'];

    List<User> channels = [];

    for (int i = 0; i < responseJson['data'].length; i++) {
      channels.add(new User.fromMap(responseJson['data'][i]));
    }

    return channels;
  }
}
