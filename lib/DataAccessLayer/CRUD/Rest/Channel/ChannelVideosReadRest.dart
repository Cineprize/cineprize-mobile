import 'dart:convert';

import 'package:cineprize/AbstractLayer/CRUD/Channel/ChannelVideosReadBase.dart';
import 'package:cineprize/DataAccessLayer/DataAccessLayerVariable.dart';
import 'package:cineprize/Models/Video.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:http/http.dart' as http;

class ChannelVideosReadRest implements ChannelVideosReadBase {
  @override
  Future<List<Video>> lastVideos({required String channel_id}) async {
    final url = Uri.parse('$baseApiUrl/channel-last-videos/$channel_id');
    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };

    var response = await http.get(url, headers: headers);

    Map<String, dynamic> responseJson = jsonDecode(response.body);

    List<Video> videos = [];

    for (int i = 0; i < responseJson['data'].length; i++) {
      videos.add(Video.fromMap(responseJson['data'][i]));
    }

    return videos;
  }

  @override
  Future<List<Video>> videos(
      {required String channel_id, String page = '0'}) async {
    final url = Uri.parse('$baseApiUrl/channel-videos');
    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };

    var response = await http.post(url, headers: headers, body: {
      'channel_id': channel_id /*, 'page': page*/
    });

    Map<String, dynamic> responseJson = jsonDecode(response.body);

    List<Video> videos = [];

    for (int i = 0; i < responseJson['data'].length; i++) {
      videos.add(Video.fromMap(responseJson['data'][i]));
    }

    return videos;
  }
}
