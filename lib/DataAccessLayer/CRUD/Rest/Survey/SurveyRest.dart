import 'dart:convert';

import 'package:cineprize/AbstractLayer/CRUD/Survey/SurveyBase.dart';
import 'package:cineprize/DataAccessLayer/DataAccessLayerVariable.dart';
import 'package:cineprize/Models/Survey.dart';
import 'package:http/http.dart' as http;

class SurveyRest implements SurveyBase {
  @override
  Future<Map<String, dynamic>> get({required authUser}) async {
    Survey survey = new Survey();

    final url = Uri.parse('$baseApiUrl/survey');
    Map<String, String> headers = {'Authorization': 'Bearer ${authUser.token}'};

    var response = await http.get(url, headers: headers);

    Map<String, dynamic> responseJson = jsonDecode(response.body);

    String message = responseJson['message'];
    bool success = responseJson['success'];

    if (success) {
      survey = Survey.fromMap(
          responseJson['data'], responseJson['data']['id'].toString());
    }

    return {'message': message, 'success': success, 'data': survey};
  }

  @override
  Future<Map<String, dynamic>> choose(
      {required String survey_id,
      required String survey_option_id,
      authUser}) async {
    final url = Uri.parse('$baseApiUrl/survey/choose');
    Map<String, String> headers = {'Authorization': 'Bearer ${authUser.token}'};

    var response = await http.post(url,
        headers: headers,
        body: {'survey_id': survey_id, 'survey_option_id': survey_option_id});

    Map<String, dynamic> responseJson = jsonDecode(response.body);

    String message = responseJson['message'];
    bool success = responseJson['success'];
    Map<String, dynamic> data = responseJson['data'];

    return {'message': message, 'success': success, 'data': data};
  }
}
