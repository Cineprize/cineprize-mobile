import 'dart:convert';

import 'package:cineprize/AbstractLayer/CRUD/Video/VideoLikeDislikeBase.dart';
import 'package:cineprize/DataAccessLayer/DataAccessLayerVariable.dart';
import 'package:cineprize/Models/Video.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:http/http.dart' as http;

class VideoLikeDislikeRest implements VideoLikeDislikeBase {
  @override
  Future<bool> dislike({required String videoID}) async {
    final url = Uri.parse('$baseApiUrl/dislike');
    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };

    var response =
        await http.post(url, body: {'video_id': videoID}, headers: headers);

    Map<String, dynamic> responseJson = jsonDecode(response.body);

    /* bool success = responseJson['success'];
    Video video;
    if (success) {
      video = new Video.fromMap(responseJson['data'], true, false);
    } else {
      video = new Video.error(errorMessage: responseJson['message']);
    } */
    return responseJson['success'] as bool;
  }

  @override
  Future<bool> like({required String videoID}) async {
    final url = Uri.parse('$baseApiUrl/like');
    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };
    var response =
        await http.post(url, body: {'video_id': videoID}, headers: headers);
    Map<String, dynamic> responseJson = jsonDecode(response.body);
    return responseJson['success'] as bool;
  }
}
