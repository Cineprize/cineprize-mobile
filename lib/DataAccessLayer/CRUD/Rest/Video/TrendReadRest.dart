import 'dart:convert';

import 'package:cineprize/AbstractLayer/CRUD/Video/TrendReadBase.dart';
import 'package:cineprize/DataAccessLayer/DataAccessLayerVariable.dart';
import 'package:cineprize/Models/Video.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:http/http.dart' as http;

class TrendReadRest implements TrendReadBase {
  @override
  Future<List<Video>> read() async {
    final url = Uri.parse('$baseApiUrl/trend');

    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };

    var response = await http.get(url, headers: headers);

    Map<String, dynamic> responseJson = jsonDecode(response.body);

    List<Video> videos = [];

    for (int i = 0; i < responseJson['data'].length; i++) {
      videos.add(Video.fromMap(responseJson['data'][i]));
    }

    return videos;
  }
}
