import 'dart:convert';

import 'package:cineprize/AbstractLayer/CRUD/Video/VideoDetailBase.dart';
import 'package:cineprize/DataAccessLayer/DataAccessLayerVariable.dart';
import 'package:cineprize/Models/Comment.dart';
import 'package:cineprize/Models/Question.dart';
import 'package:cineprize/Models/VideoDetail.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:http/http.dart' as http;

class VideoDetailRest implements VideoDetailBase {
  @override
  Future<VideoDetail> get({required String videoId}) async {
    final url = Uri.parse('$baseApiUrl/videoDetail');
    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };

    var response = await http.post(
      url,
      body: {
        'video_id': videoId,
      },
      headers: headers,
    );

    Map<String, dynamic> responseJson = jsonDecode(response.body);

    List<Question> questions = [];

    if (responseJson['questions'] != null) {
      for (int i = 0; i < responseJson['questions'].length; i++) {
        questions.add(Question.fromMap(responseJson['questions'][i]));
      }
    }

    List<Comment> comments = [];

    if (responseJson['lastComments'] != null) {
      for (int i = 0; i < responseJson['lastComments'].length; i++) {
        comments.add(Comment.fromMap(responseJson['lastComments'][i]));
      }
    }

    return VideoDetail.fromMap(responseJson, questions, comments);
  }
}
