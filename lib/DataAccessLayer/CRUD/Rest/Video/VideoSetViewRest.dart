import 'dart:convert';

import 'package:cineprize/AbstractLayer/CRUD/Video/VideoSetViewBase.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:http/http.dart' as http;

import '../../../DataAccessLayerVariable.dart';

class VideoSetViewRest implements VideoSetViewBase {
  @override
  Future<List<int>> set({required String videoID, required int maxCoin}) async {
    final url = Uri.parse('$baseApiUrl/view/viewed');
    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };

    var response = await http.post(url,
        body: {
          'video_id': videoID.toString(),
          'maxCoin': maxCoin.toString(),
        },
        headers: headers);
    Map<String, dynamic> responseJson = jsonDecode(response.body);
    bool success = responseJson['success'];

    if (success) {
      List<int> ints = [];
      ints.add(responseJson['coin']);
      ints.add(responseJson['gold']);
      return ints;
    }
    return [0, 0];
  }
}
