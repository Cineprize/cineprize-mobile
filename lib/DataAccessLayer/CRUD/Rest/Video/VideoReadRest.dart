import 'dart:convert';

import 'package:cineprize/AbstractLayer/CRUD/Video/VideoReadBase.dart';
import 'package:cineprize/CommonLayer/Video/VideoQueryType.dart';
import 'package:cineprize/DataAccessLayer/DataAccessLayerVariable.dart';
import 'package:cineprize/Models/CategoryWithVideo.dart';
import 'package:cineprize/Models/Video.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class VideoReadRest implements VideoReadBase {
  @override
  Future<List<Video>> read({VideoQueryType? videoQueryType}) async {
    final url = Uri.parse('$baseApiUrl/video/news');
    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };

    /*  if (videoQueryType == VideoQueryType.News) {
      url = '$baseApiUrl/video/news';
    }
 */
    var response = await http.get(url, headers: headers);

    Map<String, dynamic> responseJson = jsonDecode(response.body);

    List<Video> videos = [];

    for (int i = 0; i < responseJson['data'].length; i++) {
      videos.add(Video.fromMap(responseJson['data'][i]));
    }

    return videos;
  }

  @override
  Future<List<CategoryWithVideo>> categoryWithVideoHome() async {
    final url = Uri.parse('$baseApiUrl/video/home/category');
    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };

    var response = await http.get(url, headers: headers);

    Map<String, dynamic> responseJson = jsonDecode(response.body);

    List<CategoryWithVideo> categoryWithVideos = [];

    for (int i = 0; i < responseJson['data'].length; i++) {
      categoryWithVideos
          .add(CategoryWithVideo.fromMap(responseJson['data'][i]));
    }

    return categoryWithVideos;
  }

  @override
  Future<List<CategoryWithVideo>> categoryWithVideoHomeOther() async {
    final url = Uri.parse('$baseApiUrl/video/home/others');
    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };

    var response = await http.get(url, headers: headers);

    Map<String, dynamic> responseJson = jsonDecode(response.body);

    List<CategoryWithVideo> categoryWithVideos = [];

    for (int i = 0; i < responseJson['data'].length; i++) {
      categoryWithVideos.add(CategoryWithVideo.fromMap(
        responseJson['data'][i],
      ));
    }

    return categoryWithVideos;
  }
}
