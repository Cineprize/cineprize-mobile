import 'dart:convert';

import 'package:cineprize/AbstractLayer/CRUD/Video/VideoComplainBase.dart';
import 'package:cineprize/DataAccessLayer/DataAccessLayerVariable.dart';
import 'package:cineprize/Models/Response.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:http/http.dart' as http;

class VideoComplainRest implements VideoComplainBase {
  @override
  Future<Response> complain(
      {required String videoID, required String reason}) async {
    final url = Uri.parse('$baseApiUrl/video/complain/$videoID');
    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };

    var response = await http.post(url,
        body: {'reason': reason.toString()}, headers: headers);

    Map<String, dynamic> responseJson = jsonDecode(response.body);

    return Response(
      success: responseJson['success'],
      message: responseJson['message'].toString(),
      data: null,
    );
  }
}
