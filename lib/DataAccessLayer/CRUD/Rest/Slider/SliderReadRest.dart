import 'dart:convert';

import 'package:cineprize/AbstractLayer/CRUD/Slider/SliderReadBase.dart';
import 'package:cineprize/DataAccessLayer/DataAccessLayerVariable.dart';
import 'package:cineprize/Models/Slider.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:http/http.dart' as http;

class SliderReadRest implements SliderReadBase {
  @override
  Future<List<Slider>> read() async {
    // Firestore sliders koleksiyon nesnesi

    List<Slider> sliders = [];
    final url = Uri.parse('$baseApiUrl/slider');
    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };

    var response = await http.get(url, headers: headers);

    Map<String, dynamic> responseJson = jsonDecode(response.body);

    for (int i = 0; i < (responseJson['data'] ?? []).length; i++) {
      sliders.add(Slider.fromMap(
        responseJson['data'][i],
      ));
    }

    return sliders;
  }
}
