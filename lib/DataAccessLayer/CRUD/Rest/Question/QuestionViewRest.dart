import 'dart:convert';

import 'package:cineprize/AbstractLayer/CRUD/Question/QuestionViewBase.dart';
import 'package:cineprize/DataAccessLayer/DataAccessLayerVariable.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:http/http.dart' as http;

class QuestionViewRest implements QuestionViewBase {
  @override
  Future<bool> view(String question_id) async {
    final url = Uri.parse(
        '$baseApiUrl/question/viewed/$question_id/${UserManagement.include.user.uid}');
    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };

    var response = await http.get(url, headers: headers);

    Map<String, dynamic> responseJson = jsonDecode(response.body);

    bool success = responseJson['success'];

    return success;
  }
}
