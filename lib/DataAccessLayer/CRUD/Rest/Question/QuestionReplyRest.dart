import 'dart:convert';

import 'package:cineprize/AbstractLayer/CRUD/Question/QuestionReplyBase.dart';
import 'package:cineprize/DataAccessLayer/DataAccessLayerVariable.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:http/http.dart' as http;

class QuestionReplyRest implements QuestionReplyBase {
  @override
  Future<Map<String, dynamic>> reply(
      {required String questionID, required String answer}) async {
    final url = Uri.parse('$baseApiUrl/question/reply');
    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };

    var res = await http.post(url, headers: headers, body: {
      'question_id': questionID,
      'reply': answer,
    });

    Map<String, dynamic> responseJson = jsonDecode(res.body);

    if (responseJson['success']) {
      UserManagement.include.user.answerCount += 1;
    }
    return responseJson;
  }
}
