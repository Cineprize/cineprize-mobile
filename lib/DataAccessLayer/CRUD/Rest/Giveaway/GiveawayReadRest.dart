import 'dart:convert';

import 'package:cineprize/AbstractLayer/CRUD/Giveaway/GiveawayReadBase.dart';
import 'package:cineprize/DataAccessLayer/DataAccessLayerVariable.dart';
import 'package:cineprize/Models/Giveaway.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:http/http.dart' as http;

class GiveawayReadRest implements GiveawayReadBase {
  @override
  Future<Giveaway> read() async {
    final url = Uri.parse('$baseApiUrl/giveaway');
    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };

    var response = await http.get(url, headers: headers);

    Map<String, dynamic> responseJson = jsonDecode(response.body);

    Giveaway video = Giveaway.fromMap(responseJson['data']);

    return video;
  }
}
