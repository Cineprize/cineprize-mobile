import 'dart:convert';

import 'package:cineprize/AbstractLayer/CRUD/Giveaway/GiveawayJoinBase.dart';
import 'package:cineprize/DataAccessLayer/DataAccessLayerVariable.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class GiveawayJoinRest implements GiveawayJoinBase {
  @override
  Future<Map<String, dynamic>> join(String giveawayID) async {
    final url = Uri.parse('$baseApiUrl/giveaway/$giveawayID');
    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };

    var response = await http.get(url, headers: headers);

    Map<String, dynamic> responseJson = jsonDecode(response.body);
    return responseJson;
  }
}
