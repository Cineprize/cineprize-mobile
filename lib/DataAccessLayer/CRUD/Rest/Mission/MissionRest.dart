import 'dart:convert';

import 'package:cineprize/AbstractLayer/CRUD/Mission/MissionBase.dart';
import 'package:cineprize/DataAccessLayer/DataAccessLayerVariable.dart';
import 'package:cineprize/Models/Mission.dart';
import 'package:cineprize/Models/Response.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:http/http.dart' as http;

class MissionRest implements MissionBase {
  @override
  Future<Response> read() async {
    final url = Uri.parse('$baseApiUrl/mission');
    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };

    var response = await http.get(url, headers: headers);

    Map<String, dynamic> responseJson = jsonDecode(response.body);
    List<Mission> missions = [];
    for (int i = 0; i < (responseJson['data'] ?? []).length; i++) {
      missions.add(Mission.fromMap(responseJson['data'][i]));
    }

    return Response(
      success: responseJson['success'],
      message: responseJson['message'],
      data: missions,
    );
  }

  @override
  Future<Response> click({required String missionId}) async {
    final url = Uri.parse('$baseApiUrl/mission/clicked');
    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };

    var response =
        await http.post(url, body: {'mission_id': missionId}, headers: headers);

    Map<String, dynamic> responseJson = jsonDecode(response.body);
    return Response(
        success: responseJson['success'],
        message: responseJson['message'],
        data: responseJson['data'] ?? []);
  }
}
