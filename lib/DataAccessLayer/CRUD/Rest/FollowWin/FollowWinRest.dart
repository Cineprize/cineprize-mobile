import 'dart:convert';

import 'package:cineprize/AbstractLayer/CRUD/FollowWin/FollowWinBase.dart';
import 'package:cineprize/DataAccessLayer/DataAccessLayerVariable.dart';
import 'package:cineprize/Models/User.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:http/http.dart' as http;

class FollowWinRest implements FollowWinBase {
  @override
  Future<Map<String, dynamic>> follow(
      {required String type, required User authUser}) async {
    final url = Uri.parse(
        '$baseApiUrl/follow-win/${UserManagement.include.user.uid}/$type');

    Map<String, String> headers = {'Authorization': 'Bearer ${authUser.token}'};

    var response = await http.post(url, headers: headers);

    Map<String, dynamic> responseJson = jsonDecode(response.body);

    User user = User.fromMap(responseJson['data']);
    bool success = responseJson['success'];

    return {'data': user, 'success': success};
  }
}
