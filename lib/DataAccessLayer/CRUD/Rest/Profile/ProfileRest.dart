import 'dart:convert';
import 'dart:io';

import 'package:cineprize/AbstractLayer/CRUD/Profile/ProfileBase.dart';
import 'package:cineprize/DataAccessLayer/DataAccessLayerVariable.dart';
import 'package:cineprize/Models/User.dart' as UserModel;
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:http/http.dart' as http;
// ignore: implementation_imports
import 'package:http_parser/src/media_type.dart';

class ProfileRest implements ProfileBase {
  @override
  Future<UserModel.User> update({
    required Map<String, dynamic> data,
    bool isSetState = true,
  }) async {
    var client = new http.Client();

    var url;

    if (data['imagePath'] != null) {
      url = Uri.parse('$baseApiUrl/user/photo');
      Map<String, String> headers = {
        'Authorization': 'Bearer ${UserManagement.include.user.token}',
        'Content-type': 'multipart/form-data'
      };
      var request = http.MultipartRequest('POST', url);

      request.files.add(
        http.MultipartFile(
          'profileImage',
          File(data['imagePath']).readAsBytes().asStream(),
          File(data['imagePath']).lengthSync(),
          filename: data['imagePath'].split('/').last,
          contentType: MediaType('image', 'jpeg'),
        ),
      );
      request.headers.addAll(headers);
      await request.send();
    }

    url = Uri.parse('$baseApiUrl/user');
    Map<String, String> newData = new Map<String, String>();
    data.forEach((key, value) {
      newData.addAll({key.toString(): value.toString()});
    });

    var response = await client.post(
      url,
      body: newData,
      headers: {
        'Authorization': 'Bearer ${UserManagement.include.user.token}',
      },
    );

    Map<String, dynamic> responseParsed = jsonDecode(response.body.toString());

    client.close();

    bool success = responseParsed['success'];

    if (success) {
      UserModel.User user = UserModel.User.fromMap(responseParsed['data']);

      return user;
    } else {
      UserModel.User user = UserModel.User.error(
          errorMessage: responseParsed['message'][0].toString());
      return user;
    }
  }
}
