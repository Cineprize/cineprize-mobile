import 'dart:convert';

import 'package:cineprize/AbstractLayer/CRUD/Shop/SaleReadBase.dart';
import 'package:cineprize/DataAccessLayer/DataAccessLayerVariable.dart';
import 'package:cineprize/Models/Sale.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:http/http.dart' as http;

class SaleReadRest implements SaleReadBase {
  @override
  Future<Map<String, dynamic>> read() async {
    List<Sale> sales = [];

    final url = Uri.parse('$baseApiUrl/shop/sales');
    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };

    var response = await http.post(url, headers: headers);

    Map<String, dynamic> responseJson = jsonDecode(response.body);

    for (int i = 0; i < (responseJson['data'] ?? []).length; i++) {
      sales.add(Sale.fromMap(responseJson['data'][i]));
    }

    return {
      'success': responseJson['success'],
      'message': responseJson['message'],
      'data': sales
    };
  }
}
