import 'dart:convert';
import 'dart:io';

import 'package:cineprize/AbstractLayer/CRUD/Shop/BuyProductBase.dart';
import 'package:cineprize/DataAccessLayer/DataAccessLayerVariable.dart';
import 'package:cineprize/Models/Product.dart';
import 'package:cineprize/Models/User.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class BuyProductRest implements BuyProductBase {
  @override
  Future<Map<String, dynamic>> buy(
      {required Product product, required User authUser}) async {
    final url = Uri.parse('$baseApiUrl/shop/buy');
    Map<String, String> headers = {'Authorization': 'Bearer ${authUser.token}'};
    var response = await http.post(url, headers: headers, body: {
      'product_id': product.uid,
      'platform': Platform.isIOS ? '1' : '0',
    });

    Map<String, dynamic> responseJson = jsonDecode(response.body);

    String message = responseJson['message'];
    bool success = responseJson['success'];

    return {'message': message, 'success': success};
  }
}
