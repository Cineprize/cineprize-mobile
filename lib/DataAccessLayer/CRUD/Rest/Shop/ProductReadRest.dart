import 'dart:convert';
import 'dart:io';

import 'package:cineprize/AbstractLayer/CRUD/Shop/ProductReadBase.dart';
import 'package:cineprize/DataAccessLayer/DataAccessLayerVariable.dart';
import 'package:cineprize/Models/Product.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:http/http.dart' as http;

class ProductReadRest implements ProductReadBase {
  @override
  Future<List<Product>> read() async {
    List<Product> products = [];

    final url = Uri.parse('$baseApiUrl/shop');
    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };

    var response = await http.get(url, headers: headers);

    Map<String, dynamic> responseJson = jsonDecode(response.body);

    for (int i = 0; i < (responseJson['data'] ?? []).length; i++) {
      if (Platform.isIOS && responseJson['data'][i]['canShowIOS'] == 0)
        continue;
      products.add(Product.fromMap(
        responseJson['data'][i],
        responseJson['data'][i]['id'].toString(),
      ));
    }

    return products;
  }
}
