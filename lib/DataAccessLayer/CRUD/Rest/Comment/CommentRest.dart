import 'dart:convert';

import 'package:cineprize/AbstractLayer/CRUD/Comment/CommentBase.dart';
import 'package:cineprize/DataAccessLayer/DataAccessLayerVariable.dart';
import 'package:cineprize/Models/Comment.dart';
import 'package:cineprize/Models/Response.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:http/http.dart' as http;

class CommentRest implements CommentBase {
  @override
  Future<Response> add(
      {required String content, required String videoID}) async {
    final url = Uri.parse('$baseApiUrl/comment');
    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };

    var response = await http.post(url,
        body: {'video_id': videoID, 'text': content}, headers: headers);

    Map<String, dynamic> responseJson = jsonDecode(response.body);

    return Response(
      success: responseJson['success'],
      message: responseJson['message'],
      data: null,
    );
  }

  @override
  Future<Response> delete({required String commentID}) async {
    final url = Uri.parse('$baseApiUrl/comment/delete/$commentID');
    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };

    var response = await http.get(url, headers: headers);

    Map<String, dynamic> responseJson = jsonDecode(response.body);

    return Response(
      success: responseJson['success'],
      message: responseJson['message'].toString(),
      data: null,
    );
  }

  @override
  Future<Response> like({required String commentID}) async {
    final url = Uri.parse('$baseApiUrl/comment/like');
    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };

    var response = await http.post(url,
        body: {
          'user_id': UserManagement.include.user.uid,
          'comment_id': commentID
        },
        headers: headers);

    Map<String, dynamic> responseJson = jsonDecode(response.body);
    return Response(
        success: responseJson['success'], message: responseJson['message']);
  }

  @override
  Future<Response> complain({required String commentID}) async {
    final url = Uri.parse('$baseApiUrl/comment/complain/$commentID');
    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };

    var response = await http.post(url, body: {}, headers: headers);

    Map<String, dynamic> responseJson = jsonDecode(response.body);

    return Response(
      success: responseJson['success'],
      message: responseJson['message'].toString(),
      data: null,
    );
  }

  @override
  Future<Response> ban({required String commentID}) async {
    final url = Uri.parse('$baseApiUrl/commentban');
    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };

    var response = await http.post(url,
        body: {
          'comment_id': commentID,
          'reason': 'Application üzerinden banlandı!',
        },
        headers: headers);

    Map<String, dynamic> responseJson = jsonDecode(response.body);

    return Response(
      success: responseJson['success'],
      message: responseJson['message'].toString(),
      data: null,
    );
  }

  @override
  Future<Response> read({required String videoID, int page = 0}) async {
    final url = Uri.parse('$baseApiUrl/comment?video_id=$videoID&page=$page');
    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };

    var response = await http.get(url, headers: headers);

    Map<String, dynamic> responseJson = jsonDecode(response.body);

    List<Comment> comments = [];

    if (responseJson['success']) {
      for (int i = 0; i < responseJson['data'].length; i++) {
        comments.add(
          Comment.fromMap(responseJson['data'][i]),
        );
      }
    }

    return Response(
      success: responseJson['success'],
      message: responseJson['message'],
      data: comments,
    );
  }

  @override
  Future<Response> block({required String commentID}) async {
    final url = Uri.parse('$baseApiUrl/block');
    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };

    var response =
        await http.post(url, body: {'comment_id': commentID}, headers: headers);

    Map<String, dynamic> responseJson = jsonDecode(response.body);

    return Response(
      success: responseJson['success'],
      message: responseJson['message'].toString(),
      data: null,
    );
  }
}
