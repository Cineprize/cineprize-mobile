import 'dart:convert';

import 'package:cineprize/AbstractLayer/CRUD/AppStatus/AppStatusBase.dart';
import 'package:cineprize/DataAccessLayer/DataAccessLayerVariable.dart';
import 'package:cineprize/Models/Response.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class AppStatusRest implements AppStatusBase {
  @override
  Future<Response> control(
      {required String platform, required String app_build_number}) async {
    final url = Uri.parse('$baseApiUrl/mobile-app-version-control');

    var response = await http.post(url,
        body: {'platform': platform, 'app_build_number': app_build_number},
        headers: {'Accept': 'application/json'});

    Map<String, dynamic> responseJson = jsonDecode(response.body);

    bool success = responseJson['success'];
    String message = responseJson['message'];
    Map<String, dynamic> data = responseJson['data'] ?? {};

    return Response(success: success, message: message, data: data);
  }
}
