import 'dart:convert';

import 'package:cineprize/AbstractLayer/CRUD/Surprise/BuySurpriseBase.dart';
import 'package:cineprize/DataAccessLayer/DataAccessLayerVariable.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class BuySurpriseRest implements BuySurpriseBase {
  @override
  Future<Map<String, dynamic>> buy(
      {required String surprise_id,
      required int bid,
      required authUser}) async {
    final url = Uri.parse('$baseApiUrl/surprise/bid');
    Map<String, String> headers = {'Authorization': 'Bearer ${authUser.token}'};

    var response = await http.post(url,
        headers: headers,
        body: {'surprise_id': surprise_id, 'bid': bid.toString()});

    Map<String, dynamic> responseJson = jsonDecode(response.body);

    String message = responseJson['message'];
    bool success = responseJson['success'];

    return {'message': message, 'success': success};
  }
}
