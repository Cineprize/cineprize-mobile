import 'dart:convert';

import 'package:cineprize/AbstractLayer/CRUD/Surprise/SurpriseReadBase.dart';
import 'package:cineprize/DataAccessLayer/DataAccessLayerVariable.dart';
import 'package:cineprize/Models/Surprise.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:http/http.dart' as http;

class SurpriseReadRest implements SurpriseReadBase {
  @override
  Future<List<Surprise>> read() async {
    List<Surprise> surprises = [];

    final url = Uri.parse('$baseApiUrl/surprise');
    Map<String, String> headers = {
      'Authorization': 'Bearer ${UserManagement.include.user.token}'
    };

    var response = await http.get(url, headers: headers);

    Map<String, dynamic> responseJson = jsonDecode(response.body);

    for (int i = 0; i < responseJson['data'].length; i++) {
      surprises.add(Surprise.fromMap(
        responseJson['data'][i],
        responseJson['data'][i]['id'].toString(),
      ));
    }

    return surprises;
  }
}
