import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/DetailedVideoScreen/Components/CommentElement.dart';
import 'package:cineprize/Models/Response.dart';
import 'package:cineprize/Models/Video.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Comment/CommentService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CommentsModal extends StatefulWidget {
  final BuildContext bottomSheetContext;
  final Video videoModel;
  final int commentCount;

  const CommentsModal(
      {Key? key,
      required this.bottomSheetContext,
      required this.videoModel,
      required this.commentCount})
      : super(key: key);

  @override
  _CommentsModalState createState() => _CommentsModalState();
}

class _CommentsModalState extends State<CommentsModal> {
  bool fetching = true;
  int page = 0;
  int commentCount = 0;
  int commentAdded = 0;

  int lastChildCount = 0;

  List<Widget> widgets = [
    Center(
      child: Container(
        height: 35,
        width: 35,
        margin: EdgeInsets.only(top: 25),
        child: loadingIndicator,
      ),
    )
  ];

  @override
  initState() {
    commentCount = widget.commentCount;
    super.initState();
  }

  _moreCommentsButton() {
    return Container(
      margin: EdgeInsets.only(bottom: 50),
      child: GestureDetector(
        child: Center(
          child: Text(
            'Daha fazla yorum yükle',
            style: kNormalText,
          ),
        ),
        onTap: () async {
          if (!fetching) {
            setState(() {
              widgets.removeLast();
              widgets.add(Container(
                margin: EdgeInsets.only(bottom: 50),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      width: 20,
                      height: 20,
                      child: loadingIndicator,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      'Yükleniyor',
                      style: kNormalText,
                    ),
                  ],
                ),
              ));
            });
            setState(() {
              page += 1;
              fetching = true;
              /* deleted = false; */
            });
          }
        },
      ),
    );
  }

  AppBar appBar() {
    return AppBar(
      title: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Text(
            "Yorumlar",
            style: kNormalText.copyWith(
              fontSize: 17,
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Text(
            commentCount.toString(),
            style: kNormalText.copyWith(
              color: Colors.grey,
              fontSize: 17,
            ),
          ),
        ],
      ),
      actions: [
        TextButton(
          style: ButtonStyle(
            foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
            overlayColor:
                MaterialStateProperty.all<Color>(Colors.grey.withOpacity(.1)),
          ),
          onPressed: () {
            Navigator.pop(widget.bottomSheetContext);
          },
          child: Container(
            margin: EdgeInsets.only(right: 10),
            child: Icon(
              CupertinoIcons.xmark,
              size: 30,
            ),
          ),
        )
      ],
      centerTitle: false,
      automaticallyImplyLeading: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Container(
      height: (height - (height / 2.6)),
      child: Scaffold(
        backgroundColor: Colors.black54,
        appBar: appBar(),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(5.0),
            child: SingleChildScrollView(
              child: FutureBuilder<Response>(
                future: CommentService.include.read(
                  videoID: widget.videoModel.uid,
                  page: page,
                ),
                builder:
                    (BuildContext context, AsyncSnapshot<Response> snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return widgets.length > 0
                        ? Column(children: widgets)
                        : Container(
                            width: 20,
                            height: 20,
                            margin: EdgeInsets.all(15),
                            child: Center(
                              child: loadingIndicator,
                            ),
                          );
                  }

                  if (snapshot.hasError) {
                    return Center(
                      child: Text(
                        'Bir hata oluştu',
                        style: kNormalText,
                      ),
                    );
                  }
                  if (snapshot.connectionState == ConnectionState.done) {
                    if (snapshot.hasError) {
                      return Center(
                        child: Text(
                          'Bir hata oluştu',
                          style: kNormalText,
                        ),
                      );
                    }
                    if (snapshot.hasData && snapshot.data!.success == false) {
                      return Center(
                        child: Text(
                          snapshot.data!.message,
                          maxLines: 4,
                          style: kNormalText,
                        ),
                      );
                    }
                    if (snapshot.hasData) {
                      var data = snapshot.data!.data;

                      if (fetching) {
                        widgets.removeLast();
                        data.forEach((comment) async {
                          commentAdded += 1;
                          widgets.add(
                            CommentElement(
                              videoModel: widget.videoModel,
                              commentModel: comment,
                              height: height,
                            ),
                          );

                          widgets.add(CommentDivider());
                        });
                      }
                      fetching = false;
                      if (widgets.length != 0) {
                        if (lastChildCount != widgets.length) {
                          lastChildCount = widgets.length;
                          if (commentAdded < commentCount) {
                            widgets.add(_moreCommentsButton());
                          }
                        }

                        return Column(children: widgets);
                      } else {
                        return Center(
                          child: Text(
                            'Yorum bulunamadı!',
                            maxLines: 4,
                            style: kNormalText,
                          ),
                        );
                      }
                    }
                  }

                  return Container();
                },
              ),
            ),
          ),
        ),
      ),
    );
  }
}
