import 'package:cineprize/ApplicationLayer/DetailedVideoScreen/Components/CommentElement.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../Constants.dart';
import '../DetailedVideoScreen.dart';
import 'CommentsModal.dart';

class CommentArea extends StatelessWidget {
  const CommentArea({
    Key? key,
    required this.widget,
    required this.commentCount,
    required this.lastCommentWidget,
  }) : super(key: key);

  final DetailedVideoScreen widget;
  final int commentCount;
  final List<Widget> lastCommentWidget;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 3),
      child: TextButton(
        onPressed: () {
          showModalBottomSheet(
            context: context,
            isScrollControlled: true,
            builder: (BuildContext buildContext) {
              return CommentsModal(
                bottomSheetContext: context,
                videoModel: widget.video,
                commentCount: commentCount,
              );
            },
          );
        },
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Yorumlar",
                      style: kNormalText.copyWith(
                        fontSize: 17,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      commentCount.toString(),
                      style: kNormalText.copyWith(
                        color: Colors.grey,
                        fontSize: 17,
                      ),
                    ),
                  ],
                ),
                Icon(
                  CupertinoIcons.arrow_up_arrow_down,
                  color: Colors.grey,
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 5),
              child: ListView.separated(
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  return lastCommentWidget[index];
                },
                itemCount: lastCommentWidget.length,
                separatorBuilder: (context, index) {
                  return CommentDivider();
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
