import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:flutter/cupertino.dart';

class RemovedComment extends StatelessWidget {
  const RemovedComment({
    Key? key,
    required this.message,
  }) : super(key: key);

  final String message;

  @override
  Widget build(BuildContext context) {
    return new Padding(
      padding: const EdgeInsets.only(left: 20.0, top: 5, bottom: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Flexible(
            child: Text(message, style: kNormalText),
          ),
        ],
      ),
    );
  }
}
