import 'package:cineprize/Models/Response.dart';
import 'package:cineprize/Models/Video.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Video/VideoComplainService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ComplainListTile extends StatelessWidget {
  final String title;
  final Video video;
  final Function(Response response) onTap;

  const ComplainListTile({Key? key, required this.title, required this.video, required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return ListTile(
      title: Text(
        title.toString(),
      ),
      onTap: () async {
        Navigator.pop(context);
        var response = await VideoComplainService.include.complain(
          videoID: video.uid,
          reason: title.toString(),
        );
        if (onTap != null) {
          onTap(response);
        }
      },
    );
  }
}
