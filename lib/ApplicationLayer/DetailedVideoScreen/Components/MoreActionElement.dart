import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../Constants.dart';

class MoreActionElement extends StatefulWidget {
  final IconData icon;
  final String title;
  final Function() onTap;

  const MoreActionElement(
      {Key? key, required this.icon, required this.title, required this.onTap})
      : super(key: key);

  @override
  _MoreActionElementState createState() => _MoreActionElementState();
}

class _MoreActionElementState extends State<MoreActionElement> {
  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: ButtonStyle(
          foregroundColor: MaterialStateProperty.all<Color>(Colors.grey),
          overlayColor:
              MaterialStateProperty.all<Color>(Colors.grey.withOpacity(.1))),
      onPressed: widget.onTap,
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 11, horizontal: 20),
        child: Row(
          children: [
            Icon(widget.icon, size: 20, color: Colors.white),
            SizedBox(
              width: 20,
            ),
            Text(
              widget.title,
              style: kNormalText.copyWith(fontSize: 15),
            ),
          ],
        ),
      ),
    );
  }
}
