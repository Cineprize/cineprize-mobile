import 'package:cineprize/ApplicationLayer/Auth/AuthManagement/LoginNavigator.dart';
import 'package:cineprize/ApplicationLayer/Tools/Image/ImageTools.dart';
import 'package:cineprize/Models/Comment.dart';
import 'package:cineprize/Models/Response.dart';
import 'package:cineprize/Models/User.dart';
import 'package:cineprize/Models/Video.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Comment/CommentService.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../Constants.dart';
import 'MoreActionElement.dart';
import 'ReadMoreText.dart';
import 'RemovedComment.dart';

class CommentDivider extends StatelessWidget {
  const CommentDivider({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Divider(color: Colors.grey[700]),
        SizedBox(height: 10),
      ],
    );
  }
}

// ignore: must_be_immutable
class CommentElement extends StatefulWidget {
  final Comment commentModel;
  final int maxLines;
  final Video videoModel;

  int likeCount;
  bool isLiked;
  bool isFeatured;
  String profileImageUrl;

  CommentElement({
    Key? key,
    required this.height,
    required this.commentModel,
    this.isFeatured = false,
    this.maxLines = 2,
    required this.videoModel,
    this.likeCount = 0,
    this.isLiked = false,
    this.profileImageUrl = "",
  }) : super(key: key);

  final double height;

  @override
  _CommentState createState() => _CommentState();
}

class _CommentState extends State<CommentElement> {
  bool isComplained = false;
  bool isDeleted = false;
  String deletedMessage = "Yorum silindi!";

  User userManagement = UserManagement.include.user;

  @override
  void initState() {
    setState(() {
      widget.likeCount = widget.commentModel.likeCount;
      widget.profileImageUrl = widget.commentModel.author.imageURL ?? "";
      widget.isLiked = widget.commentModel.isLiked;
    });
    super.initState();
  }

  complainComment() async {
    Navigator.pop(context);
    if (isComplained) {
      return showSnackBar(
        context,
        "Daha önce şikayet edildi!",
        MediaQuery.of(context).size.width,
        duration: 1000,
        color: Colors.orange,
      );
    }
    Response response = await CommentService.include
        .complain(commentID: widget.commentModel.id);
    setState(() {
      isComplained = true;
    });
    showSnackBar(
      context,
      response.message,
      MediaQuery.of(context).size.width,
      color: response.success ? Colors.green : Colors.orange,
      duration: 1000,
    );
  }

  void likeComment() {
    setState(() {
      CommentService.include.like(commentID: widget.commentModel.id);
      widget.isLiked = !widget.isLiked;
      widget.commentModel.isLiked = widget.isLiked;

      if (widget.isLiked) {
        widget.likeCount = widget.likeCount + 1;
      } else {
        widget.likeCount = widget.likeCount - 1;
      }
      widget.commentModel.likeCount = widget.likeCount;
    });
  }

  deleteComment() async {
    Navigator.pop(context);
    Response response =
        await CommentService.include.delete(commentID: widget.commentModel.id);
    if (response.success) {
      setState(() {
        deletedMessage = "Yorum silindi!";
        isDeleted = true;
      });
    }
    return showSnackBar(
      context,
      response.message,
      MediaQuery.of(context).size.width,
      duration: 1000,
      color: response.success ? Colors.green : Colors.red,
    );
  }

  blockCommentUser() async {
    Navigator.pop(context);
    Response response =
        await CommentService.include.block(commentID: widget.commentModel.id);
    if (response.success) {
      setState(() {
        deletedMessage = "Yorum sahibi engellendi!";
        isDeleted = true;
      });
    }
    return showSnackBar(
      context,
      response.message,
      MediaQuery.of(context).size.width,
      duration: 1000,
      color: response.success ? Colors.green : Colors.red,
    );
  }

  banCommentUser() async {
    Navigator.pop(context);
    Response response =
        await CommentService.include.ban(commentID: widget.commentModel.id);
    if (response.success) {
      setState(() {
        deletedMessage = "Yorum sahibi banlandı!";
        isDeleted = true;
      });
    }
    return showSnackBar(
      context,
      response.message,
      MediaQuery.of(context).size.width,
      duration: 1000,
      color: response.success ? Colors.green : Colors.red,
    );
  }

  @override
  Widget build(BuildContext context) {
    return isDeleted
        ? RemovedComment(message: deletedMessage)
        : new GestureDetector(
            onDoubleTap: likeComment,
            child: Padding(
              padding: const EdgeInsets.only(left: 5, top: 7, bottom: 5),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Flexible(
                    flex: 1,
                    child: Container(
                      decoration: BoxDecoration(
                          shape: BoxShape.circle, color: Colors.black),
                      child: CircleAvatar(
                        radius: 24,
                        child: ClipOval(
                          child: ImageTools.getImage(
                            imageURL: widget.profileImageUrl,
                            gifPlaceholder: gifPlaceholder,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Flexible(
                    flex: 6,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ConstrainedBox(
                            constraints: BoxConstraints(minHeight: 45),
                            //color: Colors.yellow,
                            child: Padding(
                                padding: const EdgeInsets.only(left: 20),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        Text(
                                          widget.commentModel.author.userName,
                                          style: TextStyle(
                                            color: widget.commentModel.author
                                                        .role ==
                                                    'admin'
                                                ? Colors.yellow
                                                : widget.commentModel.author
                                                            .uid ==
                                                        widget
                                                            .videoModel.user.uid
                                                    ? kAppThemeColor
                                                    : Colors.grey,
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        widget.commentModel.author.role ==
                                                    'admin' ||
                                                widget.commentModel.author
                                                        .uid ==
                                                    widget.videoModel.user.uid
                                            ? Icon(
                                                widget.commentModel.author
                                                            .role ==
                                                        'admin'
                                                    ? Icons.stars
                                                    : Icons.play_circle_fill,
                                                color: widget.commentModel
                                                            .author.role ==
                                                        'admin'
                                                    ? Colors.yellow
                                                    : kAppThemeColor,
                                              )
                                            : Container()
                                      ],
                                    ),
                                    SizedBox(height: 5),
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(right: 10.0),
                                      child: ReadMoreText(
                                        widget.commentModel.content,
                                        style: kSmallText.copyWith(
                                          fontSize: 12,
                                          fontWeight: FontWeight.normal,
                                        ),
                                        trimLines: 5,
                                        trimLength: 100,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    )
                                  ],
                                ))),
                        widget.isFeatured
                            ? Container()
                            : Padding(
                                padding: const EdgeInsets.only(left: 20),
                                child: Container(
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      TextButton(
                                        style: ButtonStyle(
                                          foregroundColor:
                                              MaterialStateProperty.all<Color>(
                                                  Colors.grey),
                                          overlayColor:
                                              MaterialStateProperty.all<Color>(
                                                  Colors.grey.withOpacity(.1)),
                                        ),
                                        onPressed: likeComment,
                                        child: Row(
                                          children: [
                                            !widget.isLiked
                                                ? Icon(Icons.favorite_border,
                                                    size: 18)
                                                : Icon(Icons.favorite,
                                                    color: kAppThemeColor,
                                                    size: 18),
                                            SizedBox(width: 5),
                                            Text(
                                              '${widget.likeCount} Beğenme',
                                              style: kSmallText.copyWith(
                                                fontSize: 12,
                                                fontWeight: FontWeight.normal,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      /* More Action Button */
                                      TextButton(
                                        style: ButtonStyle(
                                          foregroundColor:
                                              MaterialStateProperty.all<Color>(
                                                  Colors.grey),
                                          overlayColor:
                                              MaterialStateProperty.all<Color>(
                                                  Colors.grey.withOpacity(.1)),
                                        ),
                                        onPressed: () => _moreActionSheet(),
                                        child: Icon(
                                          Icons.more_vert,
                                          size: 20,
                                        ),
                                      )
                                    ],
                                  ),
                                  //color: Colors.purple,
                                ),
                              ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
  }

  _moreActionSheet() {
    showBottomSheet(
        context: context,
        builder: (BuildContext buildContext) {
          List<Widget> items = userManagement.role == 'admin'
              ? [
                  MoreActionElement(
                    icon: CupertinoIcons.flag,
                    title: 'Bildir',
                    onTap: complainComment,
                  ),
                  MoreActionElement(
                    icon: CupertinoIcons.delete,
                    title: 'Sil',
                    onTap: deleteComment,
                  ),
                  MoreActionElement(
                    icon: CupertinoIcons.shield,
                    title: 'Engelle',
                    onTap: blockCommentUser,
                  ),
                  MoreActionElement(
                    icon: CupertinoIcons.exclamationmark_shield,
                    title: 'Banla',
                    onTap: banCommentUser,
                  ),
                ]
              : (widget.commentModel.author.uid == userManagement.uid
                  ? [
                      MoreActionElement(
                        icon: CupertinoIcons.delete,
                        title: 'Sil',
                        onTap: deleteComment,
                      ),
                    ]
                  : [
                      MoreActionElement(
                        icon: CupertinoIcons.flag,
                        title: 'Bildir',
                        onTap: complainComment,
                      ),
                      MoreActionElement(
                        icon: CupertinoIcons.shield,
                        title: 'Engelle',
                        onTap: blockCommentUser,
                      ),
                    ]);

          items.add(MoreActionElement(
            icon: CupertinoIcons.xmark,
            title: 'İptal',
            onTap: () {
              Navigator.pop(buildContext);
            },
          ));
          return Container(
            decoration: BoxDecoration(
              color: Colors.black38,
              border: Border.all(color: Colors.white, width: 0.2),
              borderRadius: BorderRadius.circular(5),
            ),
            child: ListView.separated(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: items.length,
              separatorBuilder: (context, index) {
                return Divider(color: Colors.grey[700]);
              },
              itemBuilder: (context, index) {
                return items[index];
              },
            ),
          );
        });
  }
}
