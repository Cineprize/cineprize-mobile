import 'package:cineprize/ApplicationLayer/Auth/AuthManagement/LoginNavigator.dart';
import 'package:cineprize/ApplicationLayer/DetailedVideoScreen/Components/ComplainListTile.dart';
import 'package:cineprize/Models/Video.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Video/VideoComplainService.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Video/VideoLikeDislikeService.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';

import '../../Constants.dart';
import 'VideoActionElement.dart';

class VideoActions extends StatefulWidget {
  final Video video;

  const VideoActions({Key? key, required this.video}) : super(key: key);

  @override
  _VideoActionsState createState() => _VideoActionsState();
}

class _VideoActionsState extends State<VideoActions> {
  bool isComplained = false;

  List<String> complainReasonList = [
    'Argo kullanımı',
    '+18 içerik kullanımı',
    'Tütün ürünlerine özendirme',
    'Diğer sebepler',
  ];

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: [
        Expanded(
          flex: 1,
          child: VideoActionElement(
            activeColor: kAppThemeColor,
            icon: CupertinoIcons.exclamationmark_triangle,
            isActive: false,
            onClick: () async {
              if (isComplained) {
                return showSnackBar(
                  context,
                  'Bu videoyu daha önce şikayet ettiniz!',
                  width,
                  color: Colors.orange,
                  duration: 1500,
                );
              }
              showCupertinoModalPopup<void>(
                context: context,
                builder: (BuildContext bContext) => CupertinoActionSheet(
                  title: Text('"${widget.video.title}" videosunu şikayet et'),
                  actions: complainReasonList
                      .map(
                        (e) => CupertinoActionSheetAction(
                          child: Text(e.toString()),
                          isDefaultAction:
                              complainReasonList.last == e.toString(),
                          onPressed: () async {
                            Navigator.pop(context);
                            var response =
                                await VideoComplainService.include.complain(
                              videoID: widget.video.uid,
                              reason: e.toString(),
                            );

                            setState(() {
                              isComplained = true;
                            });
                            return showSnackBar(
                              context,
                              response.message,
                              width,
                              color:
                                  response.success ? Colors.green : Colors.red,
                              duration: 1500,
                            );
                          },
                        ),
                      )
                      .toList(),
                  cancelButton: CupertinoActionSheetAction(
                    child: Text("İptal"),
                    isDestructiveAction: true,
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ),
              );
            },
          ),
        ),
        Expanded(
          child: VideoActionElement(
            activeColor: kAppThemeColor,
            icon: CupertinoIcons.share,
            isActive: false,
            onClick: () {
              final RenderObject box = context.findRenderObject()!;
              Share.share(
                'Cineprize\'da bir video gördüm. Çok güzeldi. Sen de izlemelisin! onelink.to/mef782',
                subject: 'Cineprize | Ödüllü Video Platformu',
                //TODO :: share
                //sharePositionOrigin: box.local(Offset.zero) & box.size,
              );
            },
          ),
        ),
        Expanded(
          child: VideoActionElement(
            activeColor: kAppThemeColor,
            icon: CupertinoIcons.hand_thumbsup,
            isActive: widget.video.isLiked,
            onClick: () {
              VideoLikeDislikeService.include.like(
                videoID: widget.video.uid,
              );
              setState(() {
                widget.video.isDisliked =
                    widget.video.isDisliked == true ? false : false;
                widget.video.isLiked = !widget.video.isLiked;
              });
            },
          ),
        ),
        Expanded(
          child: VideoActionElement(
            activeColor: Colors.red,
            icon: CupertinoIcons.hand_thumbsdown,
            isActive: widget.video.isDisliked,
            onClick: () {
              VideoLikeDislikeService.include
                  .dislike(videoID: widget.video.uid);
              setState(
                () {
                  widget.video.isLiked =
                      widget.video.isLiked == true ? false : false;
                  widget.video.isDisliked = !widget.video.isDisliked;
                },
              );
            },
          ),
        ),
      ],
    );
  }
}
