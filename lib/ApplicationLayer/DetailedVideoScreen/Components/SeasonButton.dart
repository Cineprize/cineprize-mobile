import 'package:cineprize/ApplicationLayer/Auth/AuthManagement/LoginNavigator.dart';
import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Tools/MyAlertDialogs/MyInfoDialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class SeasonButton extends StatefulWidget {
  final bool isWinnable;
  final String message;
  final String buttonText;

  const SeasonButton(
      {Key? key,
      required this.isWinnable,
      required this.message,
      required this.buttonText})
      : super(key: key);

  @override
  _SeasonButtonState createState() => _SeasonButtonState();
}

class _SeasonButtonState extends State<SeasonButton> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 5.0, right: 5.0, bottom: 5.0),
      child: GestureDetector(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              CupertinoIcons.info,
              size: 20,
              color: widget.isWinnable ? kAppThemeColor : Colors.red,
            ),
          ],
        ),
        onTap: () {
          MyInfoDialog(
            loginNavigatorContext!,
            title: 'Bilgi',
            description: widget.message,
            buttonText: 'Tamam',
            alertType: widget.isWinnable ? AlertType.success : AlertType.error,
          );
        },
      ),
    );
  }
}
/* 
class SeasonButtonSkeleton extends StatefulWidget {
  @override
  _SeasonButtonSkeletonState createState() => _SeasonButtonSkeletonState();
}

class _SeasonButtonSkeletonState extends State<SeasonButtonSkeleton> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Padding(
      padding: const EdgeInsets.only(left: 15.0, right: 15.0),
      child: RawMaterialButton(
        fillColor: Colors.transparent,
        splashColor: Colors.transparent,
        padding: EdgeInsets.all(5),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
            side: BorderSide(color: Colors.grey)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(17),
              child: Container(
                padding: const EdgeInsets.only(left: 15.0, right: 15.0),
                width: width / 1.5,
                height: 15,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(17),
                  child: SkeletonAnimation(
                    shimmerColor: shimmerColor,
                    child: Container(
                      padding: EdgeInsets.only(left: 15.0),
                      decoration: BoxDecoration(
                        color: skeletonTextColor,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 5,
            ),
            Icon(
              Icons.info,
              size: 25,
              color: Colors.grey,
            ),
          ],
        ),
        onPressed: () {},
      ),
    );
  }
} */
