import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class VideoActionElement extends StatelessWidget {
  final IconData icon;
  final Function onClick;
  final bool isActive;
  final Color activeColor;

  const VideoActionElement(
      {Key? key,
      required this.icon,
      required this.onClick,
      required this.isActive,
      required this.activeColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: ButtonStyle(
        overlayColor: MaterialStateProperty.all<Color>(Colors.transparent),
      ),
      onPressed: () {
        this.onClick();
      },
      child: Icon(
        icon,
        color: isActive ? activeColor : Colors.white,
        size: 23,
      ),
    );
  }
}
