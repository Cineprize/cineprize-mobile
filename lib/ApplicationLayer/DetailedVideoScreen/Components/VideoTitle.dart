import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class VideoTitle extends StatelessWidget {
  final String text;

  const VideoTitle({Key? key, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 15.0),
      child: Text(text,
          style: TextStyle(
              color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold)),
    );
  }
}
