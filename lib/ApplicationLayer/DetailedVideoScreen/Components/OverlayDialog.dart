//TODO:: Improve OverlayDialog

/* import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../Constants.dart';

class OverlayDialog {
  final BuildContext context;
  final String title;
  final String description;
  final bool success;
  final bool dismissible;
  final bool useRootNavigator;
  final Duration? duration;
  final Function()? onDissmissCallback;
  final Function()? onClick;

  OverlayDialog(
      {required this.context,
      required this.title,
      required this.description,
      this.success = true,
      this.dismissible = false,
      this.useRootNavigator = false,
      this.duration,
      this.onDissmissCallback,
      this.onClick});

  bool _canPop = true;

  Future<bool> canPop() => Future.value(_canPop);

  Future show() {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return showDialog(
        context: this.context,
        barrierDismissible: this.dismissible,
        useRootNavigator: useRootNavigator,
        builder: (BuildContext buildContext) {
          if (this.duration != null) {
            _canPop = false;
            Future.delayed(this.duration!).then((value) => dismiss());
          }
          return WillPopScope(
            onWillPop: canPop,
            child: GestureDetector(
              onTap: () {
                if (this.duration == null) dismiss();
              },
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: width / 8,
                  vertical: height / 2.5,
                ),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Color(0xff1a1c22),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(24.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                              text: '$title\n',
                              style: TextStyle(
                                color:
                                    this.success ? kAppThemeColor : Colors.red,
                                fontSize: 17,
                                fontWeight: FontWeight.bold,
                              ),
                              children: <TextSpan>[
                                TextSpan(
                                    text: this.description,
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 17)),
                              ]),
                        ),
                        this.onClick != null
                            ? RawMaterialButton(
                                fillColor: Colors.transparent,
                                splashColor: Colors.transparent,
                                padding: EdgeInsets.all(5),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8.0),
                                    side: BorderSide(color: Colors.red)),
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Tamam',
                                      textAlign: TextAlign.center,
                                      overflow: TextOverflow.clip,
                                      style: TextStyle(
                                        color: Colors.red,
                                      ),
                                    ),
                                  ],
                                ),
                                onPressed: this.onClick,
                              )
                            : Container()
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        });
  }

  dismiss() {
    _canPop = true;
    Navigator.of(context, rootNavigator: useRootNavigator).pop();
  }
}
 */