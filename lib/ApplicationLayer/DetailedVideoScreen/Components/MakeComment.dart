import 'package:cineprize/ApplicationLayer/Tools/Image/ImageTools.dart';
import 'package:cineprize/Models/Response.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Comment/CommentService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../Constants.dart';

class MakeComment extends StatefulWidget {
  const MakeComment({
    Key? key,
    required this.height,
    required this.imageURL,
    required this.videoID,
    required this.click,
  }) : super(key: key);

  final double height;
  final String imageURL;
  final String videoID;
  final Function(Response) click;

  @override
  _MakeCommentState createState() => _MakeCommentState();
}

class _MakeCommentState extends State<MakeComment> {
  String content = '';
  var _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 20.0, right: 20.0),
      child: Container(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
                flex: 2,
                child: CircleAvatar(
                    radius: 24,
                    child: ClipOval(
                        child: ImageTools.getImage(
                            imageURL: widget.imageURL,
                            gifPlaceholder: gifPlaceholder)))),
            Flexible(
              flex: 9,
              child: Container(
                //color: Colors.greenAccent,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 5.0),
                  child: TextFormField(
                    controller: _controller,
                    minLines: 1,
                    style: TextStyle(color: Colors.white),
                    maxLines: 10,
                    onChanged: (value) {
                      setState(() {
                        content = value;
                      });
                    },
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8.0),
                          borderSide:
                              BorderSide(color: Colors.grey[700]!, width: 1)),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8.0),
                          borderSide:
                              BorderSide(color: Colors.grey[100]!, width: .5)),
                      hintText: 'Video hakkında yorum ekle',
                      hintStyle: TextStyle(
                        color: Colors.grey.withOpacity(0.5),
                        fontSize: 14,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Flexible(
              flex: 2,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    width: 50,
                    height: 50,
                    child: RawMaterialButton(
                      fillColor: Colors.transparent,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100),
                          side: BorderSide(color: kAppThemeColor)),
                      elevation: 0,
                      onPressed: () async {
                        if (content.trim() == '') return;
                        _controller.clear();
                        var response = await CommentService.include.add(
                          videoID: widget.videoID,
                          content: content,
                        );
                        widget.click(response);
                        content = '';
                      },
                      child: Icon(
                        CupertinoIcons.paperplane,
                        color: kAppThemeColor,
                        size: 25,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
