import 'package:flutter/cupertino.dart';

import '../../Constants.dart';

class NoComment extends StatelessWidget {
  const NoComment({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 15),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Henüz yorum yapılmadı',
            style: kHintText,
            textAlign: TextAlign.left,
          ),
        ],
      ),
    );
  }
}
