import 'package:cineprize/ApplicationLayer/Tools/Image/ImageTools.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:skeleton_text/skeleton_text.dart';

import '../../Constants.dart';

class ProfilePicture extends StatelessWidget {
  final String imageURL;

  const ProfilePicture({
    Key? key,
    required this.imageURL,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        AspectRatio(
          aspectRatio: 1,
          child: Container(
            decoration: BoxDecoration(
              color: Colors.black.withOpacity(0.3),
              borderRadius: BorderRadius.circular(5),
            ),
          ),
        ),
        AspectRatio(
            aspectRatio: 1,
            child: imageURL == ""
                ? SkeletonAnimation(
                    shimmerColor: Colors.white24,
                    child: Container(
                      color: Colors.grey[50]!.withOpacity(0.3),
                    ),
                  )
                : ImageTools.getImage(
                    imageURL: imageURL, gifPlaceholder: gifPlaceholder))
      ],
    );
  }
}
