import 'package:cineprize/ApplicationLayer/DetailedVideoScreen/Components/VideoActionElement.dart';
import 'package:cineprize/Models/User.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:skeleton_text/skeleton_text.dart';

import '../../Constants.dart';
import 'CommentElement.dart';
import 'MakeComment.dart';
import 'ProfilePicture.dart';

Widget VideoDetailSkeleton(context) {
  double width = MediaQuery.of(context).size.width;
  double height = MediaQuery.of(context).size.height;
  User user = UserManagement.include.user;
  return Column(
    children: [
      SkeletonAnimation(
        shimmerColor: shimmerColor,
        child: Container(
          width: width,
          height: height / 3.4,
          decoration: BoxDecoration(
            color: skeletonTextColor,
          ),
        ),
      ),

      Card(
        color: Colors.transparent,
        elevation: 0,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            SizedBox(height: 15),
            ClipRRect(
              borderRadius: BorderRadius.circular(17),
              child: Container(
                padding: const EdgeInsets.only(left: 15.0, right: 15.0),
                width: width,
                height: 25,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(17),
                  child: SkeletonAnimation(
                    shimmerColor: shimmerColor,
                    child: Container(
                      padding: EdgeInsets.only(left: 15.0),
                      decoration: BoxDecoration(
                        color: skeletonTextColor,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(height: 5),
            ClipRRect(
              borderRadius: BorderRadius.circular(17),
              child: Container(
                padding: const EdgeInsets.only(left: 15.0, right: 15.0),
                width: width,
                height: 25,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(17),
                  child: SkeletonAnimation(
                    shimmerColor: shimmerColor,
                    child: Container(
                      padding: EdgeInsets.only(left: 15.0),
                      decoration: BoxDecoration(
                        color: skeletonTextColor,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(height: 5),
            ClipRRect(
              borderRadius: BorderRadius.circular(17),
              child: Container(
                padding: const EdgeInsets.only(left: 15.0, right: 15.0),
                width: width,
                height: 25,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(17),
                  child: SkeletonAnimation(
                    shimmerColor: shimmerColor,
                    child: Container(
                      padding: EdgeInsets.only(left: 15.0),
                      decoration: BoxDecoration(
                        color: skeletonTextColor,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            /* user.canEarn ? SeasonButtonSkeleton() : Container(), */
            /* user.canEarn ? SizedBox(height: 20) : Container(), */
            Padding(
              padding: const EdgeInsets.only(
                  left: 15.0, top: 10, bottom: 10, right: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    flex: 3,
                    child: Row(
                      children: [
                        Expanded(
                          flex: 2,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: Container(
                              height: height / 12,
                              child: ProfilePicture(
                                imageURL: "",
                              ),
                            ),
                          ),
                        ),
                        SizedBox(width: 15),
                        Expanded(
                          flex: 3,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(17),
                            child: SkeletonAnimation(
                              shimmerColor: shimmerColor,
                              child: Container(
                                width: width / 3,
                                height: 30,
                                decoration: BoxDecoration(
                                  color: skeletonTextColor,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Expanded(
                          child: VideoActionElement(
                            activeColor: kAppThemeColor,
                            icon: CupertinoIcons.exclamationmark_triangle,
                            isActive: false,
                            onClick: () {},
                          ),
                        ),
                        Expanded(
                          child: VideoActionElement(
                            activeColor: kAppThemeColor,
                            icon: CupertinoIcons.share,
                            isActive: false,
                            onClick: () {},
                          ),
                        ),
                        Expanded(
                          child: VideoActionElement(
                            activeColor: kAppThemeColor,
                            icon: CupertinoIcons.hand_thumbsup,
                            isActive: false,
                            onClick: () {},
                          ),
                        ),
                        Expanded(
                          child: VideoActionElement(
                            activeColor: Colors.red,
                            icon: CupertinoIcons.hand_thumbsdown,
                            isActive: false,
                            onClick: () {},
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      SizedBox(height: 30),
      MakeComment(
        videoID: "0",
        height: height,
        imageURL: UserManagement.include.user.imageURL ?? "",
        click: (_res) {},
      ),
      CommentDivider(),
      // Comments Column
      /* Column(
          children: [
            Container(
              margin: EdgeInsets.only(top: 5, bottom: 25),
              child: Center(
                child: Text(
                  'Yorumlar yükleniyor',
                  style: kBigTextLight,
                ),
              ),
            )
          ],
        ), */
    ],
  );
}
