/* import 'package:cineprize/ApplicationLayer/Auth/AuthManagement/LoginNavigator.dart';
import 'package:cineprize/ApplicationLayer/Components/DetailedVideoScreenWidgets/VideoScreenComponents.dart';
import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Home/AppBar/HomeAppBar.dart';
import 'package:cineprize/Models/Response.dart';
import 'package:cineprize/Models/Video.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Comment/CommentService.dart';
import 'package:flutter/material.dart';

class CommentsScreen extends StatefulWidget {
  final Video videoModel;

  final commentCount;

  CommentsScreen({required this.videoModel, required this.commentCount});

  @override
  _CommentsScreenState createState() => _CommentsScreenState();
}

class _CommentsScreenState extends State<CommentsScreen> {
  final String pdfText = '';
  List<Widget> widgets = [
    Center(
      child: CircularProgressIndicator(),
    )
  ];
  int commentCount = 0;
  int page = 0;
  int maxPage = 0;
  bool fetching = true;
  bool deleted = false;

  List<int> usedIndex = new List();

  @override
  void initState() {
    super.initState();
    setState(() {
      commentCount = widget.commentCount;
      maxPage = (commentCount ~/ 5) + (commentCount % 5 == 0 ? 0 : 1);
    });
  }

  Future<bool> _onWillPop() async {
    /*   try {
      int count = widgets.length >= 10 ? 10 : widgets.length - 1;

      Navigator.pop(
        context,
        {'widgets': widgets.sublist(0, count), 'commentCount': commentCount},
      );
      return Future.value(true);
    } catch (e) {
      print('Has error : ' + e.toString());
      return Future.value(false);
    } */
    return Future.value(!fetching);
  }

  _moreCommentsButton() {
    return Container(
      margin: EdgeInsets.only(bottom: 50),
      child: GestureDetector(
        child: Center(
          child: Text(
            'Daha fazla yorum yükle',
            style: kBigTextLight,
          ),
        ),
        onTap: () async {
          if (page != maxPage && !fetching) {
            setState(() {
              widgets.removeLast();
              widgets.add(Container(
                margin: EdgeInsets.all(30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    CircularProgressIndicator(
                      strokeWidth: 2,
                      backgroundColor: Colors.grey,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      'Daha Fazla Yükleniyor',
                      style: kHintText,
                    ),
                  ],
                ),
              ));
            });
            setState(() {
              page += 1;
              fetching = true;
              deleted = false;
            });
          }
        },
      ),
    );
  }

  _onUpdateScroll(ScrollMetrics metrics) {
    if (metrics.pixels >= metrics.maxScrollExtent ~/ 3 &&
        page != maxPage &&
        !fetching) {
      setState(() {
        widgets.removeLast();
        widgets.add(Container(
          margin: EdgeInsets.all(30),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              CircularProgressIndicator(
                strokeWidth: 2,
                backgroundColor: Colors.grey,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                'Daha Fazla Yükleniyor',
                style: kHintText,
              ),
            ],
          ),
        ));
      });
      setState(() {
        page += 1;
        fetching = true;
        deleted = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return NotificationListener<ScrollNotification>(
      onNotification: (scrollNotification) {
        if (scrollNotification is ScrollUpdateNotification) {
          _onUpdateScroll(scrollNotification.metrics);
        }
      },
      child: WillPopScope(
        onWillPop: _onWillPop,
        child: Container(
          decoration: kMainPagesDecoration,
          child: Scaffold(
            backgroundColor: Colors.transparent,
            appBar: generalAppBar(title: 'Yorumlar'),
            body: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: FutureBuilder<Response>(
                  future: CommentService.include
                      .read(videoID: widget.videoModel.uid, page: page),
                  builder:
                      (BuildContext context, AsyncSnapshot<Response> snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return widgets.length > 0
                          ? Column(children: widgets)
                          : Center(
                              child: CircularProgressIndicator(),
                            );
                    }

                    if (snapshot.hasError) {
                      return Center(
                        child: Text(
                          'Bir hata oluştu',
                          style: kNormalText,
                        ),
                      );
                    }
                    if (snapshot.connectionState == ConnectionState.done) {
                      if (snapshot.hasError) {
                        return Center(
                          child: Text(
                            'Bir hata oluştu',
                            style: kNormalText,
                          ),
                        );
                      }
                      if (snapshot.hasData && snapshot.data.success == false) {
                        return Center(
                          child: Text(
                            snapshot.data.message,
                            maxLines: 4,
                            style: kNormalText,
                          ),
                        );
                      }
                      if (snapshot.hasData) {
                        var data = snapshot.data.data;
                        if (fetching) {
                          usedIndex = new List();
                          widgets.removeLast();

                          data.forEach((comment) async {
                            widgets.add(
                              Comment(
                                videoModel: widget.videoModel,
                                commentModel: comment,
                                height: height,
                                clickBlock: (_index) async {
                                  setState(() {
                                    int usedCount = usedIndex
                                        .where((e) => e < _index)
                                        .length;
                                    _index = _index - (usedCount * 2);
                                    deleted = true;
                                    commentCount -= 1;
                                    maxPage = (commentCount ~/ 5) +
                                        (commentCount % 5 == 0 ? 0 : 1);
                                    widgets.removeRange((_index), (_index) + 2);
                                    usedIndex.add(_index);
                                  });
                                  var response = await CommentService.include
                                      .block(commentID: comment.id);

                                  showSnackBar(
                                    context,
                                    response.message,
                                    width,
                                    color: response.success
                                        ? Colors.green
                                        : Colors.red,
                                    duration: 1500,
                                  );
                                },
                                clickDelete: (_index) async {
                                  setState(() {
                                    int usedCount = usedIndex
                                        .where((e) => e < _index)
                                        .length;
                                    _index = _index - (usedCount * 2);
                                    deleted = true;
                                    commentCount -= 1;
                                    maxPage = (commentCount ~/ 5) +
                                        (commentCount % 5 == 0 ? 0 : 1);
                                    widgets.removeRange((_index), (_index) + 2);
                                    usedIndex.add(_index);
                                  });
                                  var response = await CommentService.include
                                      .delete(commentID: comment.id);

                                  showSnackBar(
                                    context,
                                    response.message,
                                    width,
                                    color: response.success
                                        ? Colors.green
                                        : Colors.red,
                                    duration: 1500,
                                  );
                                },
                                clickComplain: (_index, _isComplained) async {
                                  if (_isComplained) {
                                    return showSnackBar(
                                      context,
                                      'Bu yorumu zaten daha önce şikayet ettiniz!',
                                      width,
                                      color: Colors.red,
                                      duration: 1500,
                                    );
                                  }
                                  setState(() {
                                    int usedCount = usedIndex
                                        .where((e) => e < _index)
                                        .length;
                                    _index = _index - (usedCount * 2);
                                    deleted = true;
                                  });
                                  var response = await CommentService.include
                                      .complain(commentID: comment.id);

                                  showSnackBar(
                                    context,
                                    response.message,
                                    width,
                                    color: response.success
                                        ? Colors.green
                                        : Colors.red,
                                    duration: 1500,
                                  );
                                },
                                clickBan: (_index) async {
                                  showSnackBar(
                                    context,
                                    'İşlemi onaylıyor musunuz ?',
                                    width,
                                    color: Colors.red,
                                    duration: 5000,
                                    buttonText: ['Evet', 'Hayır'],
                                    buttonAction: [
                                      () async {
                                        Scaffold.of(context)
                                            .hideCurrentSnackBar();

                                        setState(() {
                                          int usedCount = usedIndex
                                              .where((e) => e < _index)
                                              .length;
                                          _index = _index - (usedCount * 2);
                                          deleted = true;
                                          commentCount -= 1;
                                          maxPage = (commentCount ~/ 5) +
                                              (commentCount % 5 == 0 ? 0 : 1);
                                          widgets.removeRange(
                                              (_index), (_index) + 2);
                                          usedIndex.add(_index);
                                        });
                                        var response = await CommentService
                                            .include
                                            .ban(commentID: comment.id);

                                        showSnackBar(
                                          context,
                                          response.message,
                                          width,
                                          color: response.success
                                              ? Colors.green
                                              : Colors.red,
                                          duration: 1500,
                                        );
                                      },
                                      () {
                                        Scaffold.of(context)
                                            .hideCurrentSnackBar();
                                      }
                                    ],
                                  );
                                },
                              ),
                            );

                            widgets.add(CommentDivider());
                          });
                        }
                        fetching = false;

                        if (widgets.length != 0) {
                          if (page != maxPage && !deleted) {
                            widgets.add(_moreCommentsButton());
/* 
                              widgets.removeRange(
                                  widgets.length - 1, widgets.length); */
                          }
                          return Column(children: widgets);
                        } else {
                          return Center(
                            child: Text(
                              'Yorum bulunamadı!',
                              maxLines: 4,
                              style: kNormalText,
                            ),
                          );
                        }
                      }
                    }

                    return Container();
                  },
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
 */
