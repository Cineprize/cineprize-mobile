import 'dart:io';
import 'package:cineprize/ApplicationLayer/Auth/AuthManagement/LoginNavigator.dart';
import 'package:cineprize/ApplicationLayer/Components/CustomVideoPlayer/MyVimeoPlayer.dart';
import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/DetailedVideoScreen/Components/CommentElement.dart';
import 'package:cineprize/ApplicationLayer/DetailedVideoScreen/Components/OverlayDialog.dart';
import 'package:cineprize/ApplicationLayer/DetailedVideoScreen/Components/SeasonButton.dart';
import 'package:cineprize/ApplicationLayer/DetailedVideoScreen/Components/VideoActions.dart';
import 'package:cineprize/ApplicationLayer/Home/AppBar/HomeAppBar.dart';
import 'package:cineprize/ApplicationLayer/Home/Bodies/Channels/Channel%20Pages/ChannelPageController.dart';
import 'package:cineprize/Models/Question.dart';
import 'package:cineprize/Models/User.dart';
import 'package:cineprize/Models/Video.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Question/QuestionReplyService.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Question/QuestionViewService.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Video/VideoDetailService.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Video/VideoSetViewService.dart';
import 'package:cineprize/ServiceLayer/Management/ErrorLogManagement.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:cineprize/Tools/Ads/AdmobAds/AdmobInitialize.dart';
//import 'package:cineprize/Tools/Ads/AdmobAds/AdmobInitialize.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
//import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:overlay_dialog/overlay_dialog.dart';
import 'package:video_player/video_player.dart';

import 'Components/CommentArea.dart';
import 'Components/Description.dart';
import 'Components/MakeComment.dart';
import 'Components/NoComment.dart';
import 'Components/ProfilePicture.dart';
import 'Components/VideoDetailSkeleton.dart';
import 'Components/VideoTitle.dart';

class DetailedVideoScreen extends StatefulWidget {
  final Video video;

  DetailedVideoScreen({required this.video});

  @override
  _DetailedVideoScreenState createState() => _DetailedVideoScreenState();
}

class _DetailedVideoScreenState extends State<DetailedVideoScreen> {
  late VideoPlayerController _videoController;

  RewardedAd? _rewardedAd;
  bool _isRewardedAdReady = false;
  RewardedAdLoadCallback? _adListener;

  User user = UserManagement.include.user;

  int commentCount = 0;
  int adPerQuestion = 5;
  List<Question> questions = [];
  int questionQueue = 0;
  late Question question;
  bool answered = false;
  bool onViewAd = false;
  bool initialized = false;
  bool showQuestion = false;
  bool fullscreen = false;
  bool canViewAd = true;

  // For view video
  bool initializedVideo = false;
  int second = 0;
  int playerSecond = 0;

  int coin = 0;
  int maxCoin = 0;

  late String seasonButtonText, seasonButtonMessage;

  var lastQuestionResponse;

  List<Widget> lastCommentWidget = [loadingIndicator];

  @override
  void initState() {
    queryForVideoDetail();
    createRewardedAd();
    super.initState();
  }

  void initializeAd() {
    _adListener = RewardedAdLoadCallback(
      onAdLoaded: (RewardedAd ad) {
        setState(() {
          this._rewardedAd = ad;
          _isRewardedAdReady = true;
        });
      },
      onAdFailedToLoad: (LoadAdError error) {
        setState(() {
          _isRewardedAdReady = false;
        });
        initializeAd();
      },
    );
  }

  viewVideoAd() {
    DialogHelper().hide(context);

    setState(() {
      onViewAd = true;
    });

    _rewardedAd?.show(onUserEarnedReward: (RewardedAd ad, RewardItem reward) {
      setState(() {
        _isRewardedAdReady = false;
        _videoController.play();
        onViewAd = false;
      });
      ad.dispose();

      buildOverlayDialog(
        title: lastQuestionResponse['title'] != null
            ? lastQuestionResponse['title']
            : 'Soru Sistemi',
        description: lastQuestionResponse['message'],
        success: lastQuestionResponse['success'],
        onClick: null,
      );

      Future.delayed(Duration(seconds: 3), () {
        DialogHelper().hide(context);
      });
      createRewardedAd();
    });
  }

  void createRewardedAd() {
    initializeAd();
    AdmobInitialize.instance.rewardedAd(listener: _adListener!);
  }

  Future queryForVideoDetail() async {
    try {
      VideoDetailService.include
          .get(videoId: widget.video.uid)
          .then((response) {
        setState(() {
          questions = response.questions;

          if (response.questions.length != 0) {
            question = response.questions[0];
          }

          adPerQuestion = response.adPerQuestion;

          if (response.lastComments != null) {
            lastCommentWidget = [];
            response.lastComments.forEach((comment) => {
                  lastCommentWidget.add(CommentElement(
                    videoModel: widget.video,
                    isFeatured: true,
                    commentModel: comment,
                    height: MediaQuery.of(context).size.height,
                  ))
                });
          } else {
            lastCommentWidget = [NoComment()];
          }

          canViewAd = response.canViewAd;
          widget.video.isLiked = response.liked;
          widget.video.isDisliked = response.disliked;
          coin = response.view;
          widget.video.isWinnable = response.isWinnable;
          commentCount = response.commentCount;
          seasonButtonMessage = response.seasonButtonMessage;
          seasonButtonText = response.seasonButtonText;
          initialized = true;
        });
      });
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'DetailedVideoScreen > queryForVideoDetail',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());

      Navigator.pop(context);

      return {'success': false, 'message': 'Bir hata oluştu!'};
    }
  }

  @override
  void dispose() {
    _rewardedAd?.dispose();
    this._videoController.pause().then((_) {
      this._videoController.dispose();
    });
    super.dispose();
  }

  queryForCanViewVideoAds() {
    User user = UserManagement.include.user;
    print({user});
    if (!canViewAd) return false;
    print("burası1");
    if (!user.canEarn) return false;
    /* if (isSandbox == false && Platform.isIOS) return false;
    print("burası2"); */
    print("burası3");
    if (adPerQuestion == 0) return false;
    print("burası4");
    if (user.answerCount % adPerQuestion != 0) return false;
    print("burası5");
    return true;
  }

  buildOverlayDialog({
    required String title,
    required String description,
    required bool success,
    Function()? onClick,
  }) async {
    return DialogHelper().show(
      context,
      DialogWidget.custom(
        closable: false,
        child: Material(
          type: MaterialType.transparency,
          child: Theme(
            data: Theme.of(context).copyWith(
                dialogBackgroundColor: Colors.transparent,
                dialogTheme: DialogTheme(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    backgroundColor: Colors.transparent,
                    elevation: 0)),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Color(0xff1a1c22),
              ),
              child: InkWell(
                onTap: () {
                  DialogHelper().hide(context);
                },
                borderRadius: BorderRadius.circular(20),
                child: Padding(
                  padding: EdgeInsets.all(24.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                            text: '$title\n',
                            style: TextStyle(
                              color: success ? kAppThemeColor : Colors.red,
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                            ),
                            children: <TextSpan>[
                              TextSpan(
                                  text: description,
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 17)),
                            ]),
                      ),
                      onClick != null
                          ? RawMaterialButton(
                              fillColor: Colors.transparent,
                              splashColor: Colors.transparent,
                              padding: EdgeInsets.all(5),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8.0),
                                  side: BorderSide(color: Colors.red)),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    'Tamam',
                                    textAlign: TextAlign.center,
                                    overflow: TextOverflow.clip,
                                    style: TextStyle(
                                      color: Colors.red,
                                    ),
                                  ),
                                ],
                              ),
                              onPressed: onClick,
                            )
                          : Container()
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  questionClick(questionID, correctAnswer, mustBe, context) async {
    if (!answered) {
      setState(() {
        answered = true;
      });

      var res = await QuestionReplyService.include
          .reply(questionID: questionID.toString(), answer: mustBe ? "1" : "0");

      Navigator.pop(context);

      if (res['success']) {
        updateUserState(
          res['coin'],
          res['gold'],
        );
      }

      print("rewarded ready: " + _isRewardedAdReady.toString());
      if (await queryForCanViewVideoAds() &&
          res['success'] &&
          _isRewardedAdReady) {
        setState(() {
          lastQuestionResponse = res;
          showQuestion = false;
          answered = false;
          viewVideoAd();
        });
      } else {
        setState(() {
          _videoController.play();
          answered = false;
          showQuestion = false;
        });
        buildOverlayDialog(
          title: res['title'] != null ? res['title'] : 'Soru Sistemi',
          description: res['message'],
          success: res['success'],
          onClick: null,
        );
        Future.delayed(new Duration(seconds: 3))
            .then((value) => DialogHelper().hide(context));
      }
    }
  }

  updateUserState(int coin, int gold) {
    UserManagement.include.user.coin = coin;
    UserManagement.include.user.gold = gold;
  }

  buildQuestion(questionID, questionText, correctAnswer, answerOne, answerTwo) {
    AppBar appBar = homeAppBarWithBackButton();

    setState(() {
      showQuestion = true;
      onViewAd = false;
    });

    questionBottomModal(
        answerOne, answerTwo, questionID, correctAnswer, questionText);
  }

  questionBottomModal(
      answerOne, answerTwo, questionID, correctAnswer, questionText) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    showModalBottomSheet(
      enableDrag: false,
      isDismissible: false,
      isScrollControlled: true,
      builder: (bContext) => Container(
        height: (height - (height / 2.6)),
        child: WillPopScope(
          onWillPop: () {
            return Future.value(false);
          },
          child: Scaffold(
            backgroundColor: Color(0xff1a1c22),
            appBar: AppBar(
              title: Text(
                "Soru",
                style: kNormalText.copyWith(
                  fontSize: 17,
                ),
              ),
              centerTitle: true,
              automaticallyImplyLeading: false,
            ),
            body: Padding(
              padding: const EdgeInsets.all(25.0),
              child: Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        text: 'Soru:  ',
                        style: TextStyle(color: kAppThemeColor, fontSize: 17),
                        children: <TextSpan>[
                          TextSpan(
                            text: questionText.toString(),
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 15),
                    Expanded(
                      flex: 2,
                      child: Container(
                        width: fullscreen ? width / 2 : width,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Expanded(
                              child: Column(
                                children: [
                                  AnswerSelection(
                                    option: 'A',
                                    text: answerOne.toString(),
                                    onTap: () async {
                                      await questionClick(questionID,
                                          correctAnswer, false, context);
                                    },
                                  ),
                                  SizedBox(height: 10),
                                  AnswerSelection(
                                    option: 'B',
                                    text: answerTwo.toString(),
                                    onTap: () async {
                                      await questionClick(questionID,
                                          correctAnswer, true, context);
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
      context: context,
    );
  }

  Future<bool> _onWillPop() async {
    if (!initialized) return Future.value(false);
    if (showQuestion) return Future.value(false);
    return Future.value(true);
  }

  @override
  Widget build(BuildContext context) {
    AppBar appBar = homeAppBarWithBackButton();
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return WillPopScope(
      onWillPop: _onWillPop,
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Container(
          decoration: kMainPagesDecoration,
          child: Scaffold(
            resizeToAvoidBottomInset: true,
            backgroundColor: Colors.transparent,
            appBar: appBar,
            body: SafeArea(
              child: OrientationBuilder(
                builder: (context, orientation) {
                  return SingleChildScrollView(
                    child: !initialized
                        ? VideoDetailSkeleton(context)
                        : Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                height: height / 3.5,
                                child: new MyVimeoPlayer(
                                  //id: widget.video.vimeoID,
                                  videoModel: widget.video,
                                  id: widget.video.vimeoID,
                                  autoPlay: false,
                                  onScreenChanged: (isFullScreen) {
                                    setState(() {
                                      fullscreen = isFullScreen;
                                    });
                                  },
                                  listener: (_controller) {
                                    _videoController = _controller;

                                    if (widget.video.isWinnable) {
                                      _controller.addListener(() {
                                        if (!_controller.value.isPlaying)
                                          return;
                                        if (!UserManagement
                                            .include.user.canEarn) return;
                                        if (questions.length == 0 ||
                                            questions == null) return;
                                        var questionQueue =
                                            questions.indexWhere((element) =>
                                                element.durationSecond ==
                                                _controller
                                                    .value.position.inSeconds);
                                        if (questionQueue != -1 &&
                                            _controller.value.isPlaying) {
                                          setState(() {
                                            _controller.pause();
                                            buildQuestion(
                                              questions[questionQueue].id,
                                              questions[questionQueue].title,
                                              questions[questionQueue]
                                                  .correctAnswer,
                                              questions[questionQueue]
                                                  .answerOne,
                                              questions[questionQueue]
                                                  .answerTwo,
                                            );
                                            QuestionViewService.include.view(
                                                questions[questionQueue].id);
                                            questions.removeAt(questionQueue);
                                          });
                                        }
                                      });
                                      _controller.addListener(() {
                                        if (!_controller.value.isPlaying)
                                          return;
                                        if (!UserManagement
                                            .include.user.canEarn) return;
                                        if (!initializedVideo) {
                                          maxCoin = ((_controller.value.duration
                                                              .inHours
                                                              .toInt() *
                                                          60) +
                                                      _controller.value.duration
                                                          .inMinutes
                                                          .toInt())
                                                  .toInt() *
                                              20;
                                          setState(() {
                                            initializedVideo = true;
                                          });
                                        }

                                        if (coin >= maxCoin) return;

                                        if (_controller.value.isPlaying &&
                                            _controller
                                                    .value.position.inSeconds !=
                                                playerSecond) {
                                          playerSecond = _controller
                                              .value.position.inSeconds;
                                          second += 1;
                                          if (second != 0 &&
                                              second % 60 == 0 &&
                                              _controller.value.isPlaying) {
                                            coin = coin + 20;
                                            if (coin <= maxCoin) {
                                              VideoSetViewService.include
                                                  .set(
                                                videoID:
                                                    widget.video.uid.toString(),
                                                maxCoin: maxCoin,
                                              )
                                                  .then((value) {
                                                updateUserState(
                                                    value[0], value[1]);
                                              });
                                            }
                                          }
                                        }
                                      });
                                    }
                                    return _controller;
                                  },
                                ),
                              ),
                              Column(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(top: 15.0),
                                    child: Row(
                                      children: [
                                        Expanded(
                                            flex: 7,
                                            child: VideoTitle(
                                                text: this.widget.video.title)),
                                        Expanded(
                                          child: user.canEarn
                                              ? SeasonButton(
                                                  buttonText: seasonButtonText,
                                                  message: seasonButtonMessage,
                                                  isWinnable:
                                                      widget.video.isWinnable,
                                                )
                                              : Container(),
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(height: 5),
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Description(
                                            description:
                                                this.widget.video.description),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 15.0,
                                        top: 10,
                                        bottom: 10,
                                        right: 15),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          flex: 3,
                                          child: Row(
                                            children: [
                                              Expanded(
                                                flex: 1,
                                                child: ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                  child: Container(
                                                    height: height / 16,
                                                    child: GestureDetector(
                                                      onTap: () => {
                                                        Navigator.push(
                                                          context,
                                                          MaterialPageRoute(
                                                            builder: (context) =>
                                                                channelPageView(
                                                              channel: this
                                                                  .widget
                                                                  .video
                                                                  .user,
                                                            ),
                                                          ),
                                                        ),
                                                      },
                                                      child: ProfilePicture(
                                                        imageURL: this
                                                                .widget
                                                                .video
                                                                .user
                                                                .imageURL ??
                                                            appLogo,
                                                      ),
                                                    ),
                                                    //color: Colors.yellow,
                                                  ),
                                                ),
                                              ),
                                              SizedBox(width: 15),
                                              Expanded(
                                                flex: 3,
                                                child: GestureDetector(
                                                  onTap: () => {
                                                    Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                        builder: (context) =>
                                                            channelPageView(
                                                          channel: this
                                                              .widget
                                                              .video
                                                              .user,
                                                        ),
                                                      ),
                                                    ),
                                                  },
                                                  child: Text(
                                                    this
                                                            .widget
                                                            .video
                                                            .user
                                                            .name ??
                                                        '',
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: widget
                                                                  .video
                                                                  .user
                                                                  .name!
                                                                  .length >
                                                              15
                                                          ? 11
                                                          : 18,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        //Buraya gelecek
                                        Expanded(
                                          flex: 2,
                                          child: VideoActions(
                                            video: widget.video,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),

                                  SizedBox(height: 20),
                                  MakeComment(
                                    videoID: widget.video.uid,
                                    height: height,
                                    imageURL:
                                        UserManagement.include.user.imageURL ??
                                            "",
                                    click: (_response) {
                                      FocusScope.of(context)
                                          .requestFocus(FocusNode());
                                      showSnackBar(
                                        context,
                                        _response.message,
                                        width,
                                        color: _response.success
                                            ? Colors.green
                                            : Colors.red,
                                        duration: 1500,
                                      );
                                    },
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  CommentDivider(),
                                  // Comments Column
                                  CommentArea(
                                    widget: widget,
                                    commentCount: commentCount,
                                    lastCommentWidget: lastCommentWidget,
                                  )
                                ],
                              ),
                            ],
                          ),
                  );
                },
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class AnswerSelection extends StatelessWidget {
  const AnswerSelection({
    Key? key,
    required this.option,
    required this.text,
    required this.onTap,
  }) : super(key: key);

  final String option;

  final String text;

  final Function onTap;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          border: Border.all(color: kAppThemeColor)),
      child: InkWell(
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        onTap: () async {
          await onTap();
        },
        child: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                  height: 30,
                  width: 30,
                  child: Center(child: Text(option)),
                  decoration: BoxDecoration(
                      color: kAppThemeColor, shape: BoxShape.circle)),
              Padding(
                  padding: const EdgeInsets.only(left: 10.0, right: 5.0),
                  child: Text(text,
                      style: TextStyle(color: Colors.white, fontSize: 15))),
            ],
          ),
        ),
      ),
    );
  }
}
