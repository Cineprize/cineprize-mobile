import 'package:cineprize/ApplicationLayer/Home/Bodies/Store/StoreBody.dart';
import 'package:cineprize/ApplicationLayer/Home/Bodies/Store/SurpriseBody.dart';
import 'package:flutter/material.dart';

final controller = PageController(
  initialPage: 0,
);

final storePageView = PageView(
  controller: controller,
  children: [
    StoreBody(),
    SurpriseBody(),
  ],
);
