import 'package:cached_network_image/cached_network_image.dart';
import 'package:cineprize/ApplicationLayer/Auth/AuthManagement/LoginNavigator.dart';
import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Home/AppBar/HomeAppBar.dart';
import 'package:cineprize/ApplicationLayer/Home/Bodies/Store/StoreBody.dart';
import 'package:cineprize/ApplicationLayer/Tools/Image/ImageTools.dart';
import 'package:cineprize/ApplicationLayer/Tools/MyAlertDialogs/MyInfoDialog.dart';
import 'package:cineprize/ApplicationLayer/Tools/StateUpdateIndicator/StateUpdateIndicator.dart';
import 'package:cineprize/Models/Surprise.dart';
import 'package:cineprize/Models/User.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Surprise/SurpriseReadService.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import 'StorePagesWidgets.dart';

class SurpriseBody extends StatefulWidget {
  @override
  _SurpriseBodyState createState() => _SurpriseBodyState();
}

class _SurpriseBodyState extends State<SurpriseBody> {
  late Future<List<Surprise>> _surpriseFuture;

  @override
  void didUpdateWidget(covariant SurpriseBody oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  void initState() {
    _surpriseFuture = SurpriseReadService.include.read();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final User user = Provider.of<UserManagement>(context).user;
    Size size = MediaQuery.of(context).size;
    double width = size.width;
    return Container(
      decoration: kMainPagesDecoration,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.transparent,
        appBar: homeAppBarWithBackButton(),
        body: Column(
          children: [
            TopCurrentMenuIndicator(
              width: width,
              selectedPage: menus.surprise,
            ),
            Padding(
              padding:
                  const EdgeInsets.only(left: 30.0, right: 30.0, bottom: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Container(
                        height: 30,
                        width: 30,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                  'assets/images/coinIcons/crystal_pink.png'),
                              fit: BoxFit.contain),
                        ),
                      ),
                      SizedBox(width: 5),
                      StateUpdateIndicator.stateControl(
                          context: context,
                          child: Text(
                              'CineGold : ${user.gold} (Teklifte: ${user.usedGold})',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold))),
                    ],
                  ),
                  InkWell(
                    child: Icon(CupertinoIcons.info, size: 25),
                    onTap: () async => {
                      MyInfoDialog(loginNavigatorContext!,
                          title: 'Bilgi',
                          description:
                              'Sürpriz bir zamanda bir ürünün geri sayımı başlar ve 1 hafta içinde en yüksek teklifi veren ödülü alır.',
                          buttonText: 'Tamam',
                          alertType: AlertType.info)
                    },
                  )
                ],
              ),
            ),
            Flexible(
              child: FutureBuilder<List<Surprise>>(
                future: _surpriseFuture,
                initialData: [],
                builder: (BuildContext context,
                    AsyncSnapshot<List<Surprise>> snapshot) {
                  List<Widget> elements = [];

                  if (snapshot.connectionState == ConnectionState.waiting) {
                    elements = buildStoreListSkeletonCard(context, 6, 0);
                  }

                  if (snapshot.hasData) {
                    snapshot.data!.forEach((element) async {
                      elements.add(SurpriseItem(
                        cont: context,
                        width: width,
                        image: ImageTools.getImage(
                            imageURL: element.imageURL,
                            gifPlaceholder: gifPlaceholder),
                        model: element,
                        user: user,
                      ));
                    });

                    return GridView.count(
                      primary: false,
                      padding: const EdgeInsets.all(15),
                      crossAxisSpacing: 10,
                      mainAxisSpacing: 10,
                      crossAxisCount: 2,
                      children: elements,
                    );
                  } else if (snapshot.hasError) {
                    return Center(child: Text('Hata! Sürprizler yüklenemedi.'));
                  } else {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

// ignore: must_be_immutable
class SurpriseItem extends StatefulWidget {
  SurpriseItem({
    Key? key,
    required this.width,
    required this.image,
    required this.model,
    required this.cont,
    this.user,
  }) : super(key: key);

  final double width;
  CachedNetworkImage image;
  final Surprise model;
  final BuildContext cont;
  final user;

  @override
  _SurpriseItemState createState() => _SurpriseItemState();
}

class _SurpriseItemState extends State<SurpriseItem> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  String bid = '';

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        showDialog(
            context: context,
            builder: (_) => new AlertDialog(
                  backgroundColor: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  content: Builder(
                    builder: (_) {
                      // Get available height and width of the build area of this widget. Make a choice depending on the size.
                      var height = MediaQuery.of(context).size.height;
                      var width = MediaQuery.of(context).size.width;

                      return Container(
                        child: SingleChildScrollView(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    CachedNetworkImage(
                                      imageUrl: widget.model.imageURL,
                                      imageBuilder: (context, imageProvider) =>
                                          Container(
                                        width: width * 0.4,
                                        height: width * 0.4,
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          image: DecorationImage(
                                              image: imageProvider,
                                              fit: BoxFit.cover),
                                        ),
                                      ),
                                      placeholder: (context, url) =>
                                          CircularProgressIndicator(),
                                      errorWidget: (context, url, error) =>
                                          Icon(Icons.error),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(height: 10),
                              Text(widget.model.name,
                                  style: TextStyle(
                                      color: kAppThemeColor,
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold)),
                              SizedBox(height: 10),
                              Text(widget.model.description,
                                  style: TextStyle(
                                      color: Colors.grey[700], fontSize: 14),
                                  textAlign: TextAlign.left),
                              /*   RichText(
                                  text: TextSpan(
                                      text: 'Son Teklif: ',
                                      style: TextStyle(
                                          color: kAppThemeColor, fontSize: 14),
                                      children: <TextSpan>[
                                        TextSpan(
                                            text:
                                                '${widget.model.currentPrice} CineGold',
                                            style: TextStyle(
                                                color: Colors.grey[700],
                                                fontSize: 14)),
                                      ]),
                                ),
                                RichText(
                                  text: TextSpan(
                                      text: 'Son Teklif Veren: ',
                                      style: TextStyle(
                                          color: kAppThemeColor, fontSize: 14),
                                      children: <TextSpan>[
                                        TextSpan(
                                            text: '${widget.model.userName}',
                                            style: TextStyle(
                                                color: Colors.grey[700],
                                                fontSize: 14)),
                                      ]),
                                ),
                                SizedBox(height: 15), */
                              /*  Form(
                                  key: this.formKey,
                                  child: Align(
                                    child: Column(
                                      children: [
                                        CustomTextFieldWithoutIcon(
                                          initialValue: this.bid.toString(),
                                          textInputType: TextInputType.number,
                                          onSaved: (_) {
                                            this.bid = _.trim();
                                          },
                                          validator: (__) {
                                            String _ = __.trim();
                                            if (_.length == 0) {
                                              return 'CineGold miktarını boş geçilemez.';
                                            } else if (int.parse(
                                                    widget.model.currentPrice) >
                                                int.parse(_)) {
                                              return 'Teklifiniz daha önceki tekliften yüksek olmalıdır.';
                                            }
                                            return null;
                                          },
                                          hintText: 'Teklif miktarı',
                                        ),
                                        SizedBox(height: 10),
                                        Center(
                                          child: CustomBottomButtonWidget(
                                            text: 'Teklif Ver',
                                            color: Color(0xff33a346),
                                            width: 200,
                                            onTap: () async {
                                              if (this
                                                  .formKey
                                                  .currentState
                                                  .validate()) {
                                                this.formKey.currentState.save();

                                                bid = this.bid;

                                                Map<String, dynamic> result =
                                                    await BuySurpriseService
                                                        .include
                                                        .buy(
                                                            surprise_id:
                                                                widget.model.uid,
                                                            bid: int.parse(bid),
                                                            authUser:
                                                                this.widget.user);

                                                if (result['success']) {
                                                  MyInfoDialog(widget.cont,
                                                      title: 'Başarılı!',
                                                      description:
                                                          result['message'],
                                                      buttonText: 'Tamam',
                                                      alertType:
                                                          AlertType.success);

                                                  widget.model.currentPrice = bid;
                                                  widget.model.user_id =
                                                      this.widget.user.userName;
                                                } else {
                                                  MyInfoDialog(widget.cont,
                                                      title: 'Uyarı!',
                                                      description:
                                                          result['message']
                                                              .toString(),
                                                      buttonText: 'Tamam',
                                                      alertType:
                                                          AlertType.warning);
                                                }

                                                this.bid = '';
                                                setState(() {});
                                              }
                                            },
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ), */
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                ));
      },
      child: Stack(
        children: [
          ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Container(
                  decoration: BoxDecoration(color: Colors.teal[100]),
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: widget.image))),
          widget.model.haveLine
              ? Align(
                  alignment: Alignment(0, 0.8),
                  child: Container(
                    width: double.infinity,
                    color: Color.fromRGBO(0, 0, 0, 0.5),
                    child: Text(widget.model.name,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.bold)),
                  ),
                )
              : Container(),
        ],
      ),
    );
  }
}
