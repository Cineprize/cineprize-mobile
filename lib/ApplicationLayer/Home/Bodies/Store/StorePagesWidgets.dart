import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Home/Bodies/Store/PageController.dart';
import 'package:flutter/material.dart';

enum menus { store, surprise }

class TopCurrentMenuIndicator extends StatefulWidget {
  const TopCurrentMenuIndicator({
    Key? key,
    required this.width,
    required this.selectedPage,
  }) : super(key: key);

  final double width;
  final menus selectedPage;

  @override
  _TopCurrentMenuIndicatorState createState() =>
      _TopCurrentMenuIndicatorState();
}

class _TopCurrentMenuIndicatorState extends State<TopCurrentMenuIndicator> {
  @override
  Widget build(BuildContext context) {
    var selectedMenu = TextStyle(
        color: Colors.white, fontWeight: FontWeight.bold, fontSize: 16);
    var unpickedMenu = TextStyle(fontWeight: FontWeight.bold, fontSize: 16);
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(60), color: Colors.white),
          width: widget.width * 0.45,
          height: 50,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              GestureDetector(
                onTap: () {
                  storePageView.controller.jumpToPage(0);
                },
                child: Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Container(
                    height: 40,
                    width: widget.width * 0.20,
                    child: Center(
                        child: Text(
                      'Mağaza',
                      style: widget.selectedPage == menus.store
                          ? selectedMenu
                          : unpickedMenu,
                    )),
                    decoration: BoxDecoration(
                        color: widget.selectedPage == menus.store
                            ? kAppThemeColor
                            : Colors.white,
                        borderRadius: BorderRadius.circular(60)),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  storePageView.controller.jumpToPage(1);
                },
                child: Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: Container(
                    height: 40,
                    width: widget.width * 0.20,
                    child: Center(
                      child: Text('Sürpriz',
                          style: widget.selectedPage == menus.store
                              ? unpickedMenu
                              : selectedMenu),
                    ),
                    decoration: BoxDecoration(
                        color: widget.selectedPage == menus.store
                            ? Colors.white
                            : kAppThemeColor,
                        borderRadius: BorderRadius.circular(60)),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
