import 'dart:core';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cineprize/ApplicationLayer/Auth/AuthManagement/LoginNavigator.dart';
import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Home/AppBar/HomeAppBar.dart';
import 'package:cineprize/ApplicationLayer/Home/Bodies/Store/StorePagesWidgets.dart';
import 'package:cineprize/ApplicationLayer/Tools/Image/ImageTools.dart';
import 'package:cineprize/ApplicationLayer/Tools/MyAlertDialogs/MyInfoDialog.dart';
import 'package:cineprize/ApplicationLayer/Tools/StateUpdateIndicator/StateUpdateIndicator.dart';
import 'package:cineprize/ApplicationLayer/components/CustomBottomButtonWidget.dart';
import 'package:cineprize/Models/Product.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Shop/BuyProductService.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Shop/ProductReadService.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:skeleton_text/skeleton_text.dart';

class StoreBody extends StatefulWidget {
  @override
  _StoreBodyState createState() => _StoreBodyState();
}

class _StoreBodyState extends State<StoreBody> {
  late Future<List<Product>> _productsFuture;

  @override
  void initState() {
    _productsFuture = ProductReadService.include.read();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final UserManagement userManagement = Provider.of<UserManagement>(context);

    double width = MediaQuery.of(context).size.width;

    return Container(
      decoration: kMainPagesDecoration,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: homeAppBarWithBackButton(),
        body: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            TopCurrentMenuIndicator(width: width, selectedPage: menus.store),
            Padding(
              padding:
                  const EdgeInsets.only(left: 30.0, right: 30.0, bottom: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Container(
                        height: 30,
                        width: 30,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage(
                                    'assets/images/coinIcons/crystal_green.png'),
                                fit: BoxFit.contain)),
                      ),
                      SizedBox(width: 5),
                      StateUpdateIndicator.stateControl(
                        context: context,
                        child: Text(
                          'CineCoin : ${userManagement.user.coin.toString()}',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                  InkWell(
                    child: Icon(CupertinoIcons.info, size: 25),
                    onTap: () async => {
                      MyInfoDialog(loginNavigatorContext!,
                          title: 'Bilgi',
                          description:
                              'Adil olmak için her ayın 1’inde herkesin CineCoin’i 0 olur ve Mağaza stokları tamamen dolar.',
                          buttonText: 'Tamam',
                          alertType: AlertType.info)
                    },
                  )
                ],
              ),
            ),
            Flexible(
              child: FutureBuilder<List<Product>>(
                future: _productsFuture,
                initialData: [],
                builder: (BuildContext context,
                    AsyncSnapshot<List<Product>> snapshot) {
                  List<Widget> elements = [];

                  if (snapshot.connectionState == ConnectionState.waiting) {
                    elements = buildStoreListSkeletonCard(context, 6, 0);
                  }

                  if (snapshot.hasData) {
                    snapshot.data!.forEach((element) {
                      elements.add(ShopItem(
                          cont: context,
                          image: ImageTools.getImage(
                              imageURL: element.imageURL,
                              gifPlaceholder: gifPlaceholder),
                          model: element));
                    });

                    return GridView.count(
                        primary: false,
                        padding: const EdgeInsets.all(15),
                        crossAxisSpacing: 10,
                        mainAxisSpacing: 10,
                        crossAxisCount: 2,
                        children: elements);
                  } else {
                    return Center(child: CircularProgressIndicator());
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

List<Widget> buildStoreListSkeletonCard(
    BuildContext context, int count, double height2) {
  doBuild() {
    return Container(
      color: Colors.transparent,
      child: Card(
        elevation: 0,
        color: Colors.transparent,
        child: ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: SkeletonAnimation(
                shimmerColor: shimmerColor,
                child: Container(
                    decoration: BoxDecoration(color: skeletonColor)))),
      ),
    );
  }

  List<Widget> skeletonElements = [];
  for (int i = 0; i < count; i++) {
    skeletonElements.add(doBuild());
  }
  return skeletonElements;
}

class ShopItem extends StatefulWidget {
  ShopItem(
      {Key? key, required this.image, required this.model, required this.cont})
      : super(key: key);

  CachedNetworkImage image;
  final Product model;
  final BuildContext cont;

  @override
  _ShopItemState createState() => _ShopItemState();
}

class _ShopItemState extends State<ShopItem> {
  late Product modelState;
  late int remainAmountState;

  @override
  void initState() {
    setState(() {
      modelState = widget.model;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return InkWell(
      onTap: () {
        showDialog(
          context: context,
          builder: (_) => AlertDialog(
            backgroundColor: Colors.white,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
            content: Builder(
              builder: (context) {
                // Get available height and width of the build area of this widget. Make a choice depending on the size.

                return Container(
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              CachedNetworkImage(
                                imageUrl: modelState.imageURL,
                                imageBuilder: (context, imageProvider) =>
                                    Container(
                                  width: width * 0.4,
                                  height: width * 0.4,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                        image: imageProvider,
                                        fit: BoxFit.cover),
                                  ),
                                ),
                                placeholder: (context, url) =>
                                    CircularProgressIndicator(),
                                errorWidget: (context, url, error) =>
                                    Icon(Icons.error),
                              ),
                              SizedBox(width: 10),
                              RichText(
                                text: TextSpan(
                                    text: 'Kalan: ',
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 17),
                                    children: <TextSpan>[
                                      TextSpan(
                                          text: '${modelState.count}',
                                          style: TextStyle(
                                              color: kAppThemeColor,
                                              fontWeight: FontWeight.bold))
                                    ]),
                              )
                            ],
                          ),
                        ),
                        SizedBox(height: 20),
                        Text(modelState.name,
                            style: TextStyle(
                                color: kAppThemeColor,
                                fontSize: 20,
                                fontWeight: FontWeight.bold)),
                        SizedBox(height: 10),
                        Text(modelState.description,
                            style: TextStyle(
                                color: Colors.grey[700], fontSize: 14),
                            textAlign: TextAlign.left),
                        SizedBox(height: 20),
                        RichText(
                          text: TextSpan(
                              text: 'Fiyat: ',
                              style: TextStyle(
                                  color: kAppThemeColor, fontSize: 17),
                              children: <TextSpan>[
                                TextSpan(
                                    text: '${modelState.price} CineCoin',
                                    style: TextStyle(
                                        color: Colors.grey[700], fontSize: 17))
                              ]),
                        ),
                        SizedBox(height: 30),
                        Center(
                          child: CustomBottomButtonWidget(
                            text: 'Hediyeni Seç',
                            onTap: () async {
                              Map<String, dynamic> result =
                                  await BuyProductService.include.buy(
                                      product: modelState,
                                      authUser: UserManagement.include.user);

                              if (result['success']) {
                                modelState.count -= 1;
                                MyInfoDialog(widget.cont,
                                    title: 'Başarılı!',
                                    description: result['message'],
                                    buttonText: 'Tamam',
                                    alertType: AlertType.success);
                              } else {
                                MyInfoDialog(widget.cont,
                                    title: 'Uyarı!',
                                    description: result['message'].toString(),
                                    buttonText: 'Tamam',
                                    alertType: AlertType.warning);
                              }
                            },
                            color: Color(0xff33a346),
                            width: 200,
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        );
      },
      child: Stack(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: Container(
                decoration: BoxDecoration(color: Colors.teal[100]),
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: widget.image)),
          ),
          modelState.haveLine
              ? Align(
                  alignment: Alignment(0, 0.8),
                  child: Container(
                    width: double.infinity,
                    color: Color.fromRGBO(0, 0, 0, 0.5),
                    child: Text(modelState.name,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.bold)),
                  ),
                )
              : Container(),
        ],
      ),
    );
  }
}
