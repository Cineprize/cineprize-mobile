import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Home/Bodies/Channels/Channel%20Pages/ChannelPageController.dart';
import 'package:cineprize/ApplicationLayer/Tools/Image/ImageTools.dart';
import 'package:cineprize/Models/Category.dart';
import 'package:cineprize/Models/User.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Channel/CategoryToChannelReadService.dart';
import 'package:flutter/material.dart';
import 'package:skeleton_text/skeleton_text.dart';

class ChannelStack extends StatefulWidget {
  final Category category;

  const ChannelStack({Key? key, required this.category}) : super(key: key);

  @override
  _ChannelStackState createState() => _ChannelStackState();
}

class _ChannelStackState extends State<ChannelStack> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final Category category = this.widget.category;
    return Container(
      decoration: kMainPagesDecoration,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          backgroundColor: kAppBlack,
          centerTitle: true,
          title: Text(category.name, style: kNormalText),
        ),
        body: FutureBuilder<List<User>>(
            future: CategoryToChannelReadService.include
                .read(category_id: category.uid),
            initialData: [],
            builder:
                (BuildContext context, AsyncSnapshot<List<User>> snapshot) {
              List<Widget> _elements = [];
              if (snapshot.connectionState == ConnectionState.waiting) {
                _elements.add(ChannelSkeleton());
                _elements.add(ChannelSkeleton());
                _elements.add(ChannelSkeleton());
                _elements.add(ChannelSkeleton());
                _elements.add(ChannelSkeleton());
                _elements.add(ChannelSkeleton());
                return ListView(children: _elements, primary: false);
              }
              if (snapshot.hasData) {
                List<Widget> elements = [];
                snapshot.data!.forEach((channel) {
                  elements.add(Channel(channel: channel));
                });

                if (snapshot.data!.length != 0) {
                  return ListView(
                    children: elements,
                    primary: false,
                  );
                } else {
                  return Center(
                    child: Text(
                      'Bu kategoriye çok yakında yeni içerikler eklenecektir.',
                      style: TextStyle(color: Colors.grey),
                    ),
                  );
                }
              } else if (snapshot.hasError) {
                return Text(
                    'Kanallar listelenirken bir hata oluştu. Lütfen daha sonra tekrar deneyiniz.',
                    style: TextStyle(color: Colors.grey));
              } else {
                return Center(child: CircularProgressIndicator());
              }
            }),
      ),
    );
  }
}

class Channel extends StatelessWidget {
  final User channel;

  const Channel({Key? key, required this.channel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;

    return GestureDetector(
      onTap: () => {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => channelPageView(channel: this.channel)))
      },
      child: Card(
        elevation: 0,
        margin: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
        color: Colors.transparent,
        child: Row(
          children: [
            ClipOval(
              child: SizedBox(
                height: height / 8,
                width: height / 8,
                child: ImageTools.getImage(
                    imageURL: this.channel.imageURL ?? "",
                    gifPlaceholder: gifPlaceholder),
              ),
            ),
            SizedBox(width: 15),
            Flexible(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    this.channel.name ?? '',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                    textAlign: TextAlign.left,
                  ),
                  SizedBox(height: 10),
                  Text(
                    this.channel.description ?? '',
                    style: TextStyle(color: Colors.white, fontSize: 13),
                    textAlign: TextAlign.left,
                    //softWrap: true,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ChannelSkeleton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    double width2 = MediaQuery.of(context).size.width;

    return GestureDetector(
      child: Card(
        elevation: 0,
        margin: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
        color: Colors.transparent,
        child: Row(
          children: [
            ClipOval(
              child: SizedBox(
                height: height / 8,
                width: height / 8,
                child: Container(
                  height: height,
                  width: width2 / 2.5,
                  child: SkeletonAnimation(
                      shimmerColor: shimmerColor,
                      child: Container(
                          width: width2 / 2.5,
                          height: height,
                          decoration: BoxDecoration(color: skeletonColor))),
                ),
              ),
            ),
            SizedBox(width: 15),
            Flexible(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SkeletonAnimation(
                    shimmerColor: shimmerColor,
                    child: Container(
                        width: width2 / 4,
                        height: 13,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            color: skeletonColor)),
                  ),
                  SizedBox(height: 10),
                  SkeletonAnimation(
                    shimmerColor: shimmerColor,
                    child: Container(
                        width: width2 / 3,
                        height: 13,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            color: skeletonColor)),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
