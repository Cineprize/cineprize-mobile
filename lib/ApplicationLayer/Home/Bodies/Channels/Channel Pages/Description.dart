import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Home/Bodies/Channels/Channel%20Pages/ChannelFavoriteButton.dart';
import 'package:cineprize/ApplicationLayer/Home/Bodies/Channels/Channel%20Pages/ChannelPageWidgets.dart';
import 'package:cineprize/ApplicationLayer/Home/Bodies/Channels/Channel%20Pages/ChannelToVideoList.dart';
import 'package:cineprize/ApplicationLayer/Home/Bodies/Channels/Channel%20Pages/Episodes.dart';
import 'package:cineprize/Models/User.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Channel/ChannelVideosReadService.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/material.dart';
import 'package:html/dom.dart' as htmlParser;

class Description extends StatefulWidget {
  final User channel;

  const Description({Key? key, required this.channel}) : super(key: key);

  @override
  _DescriptionState createState() => _DescriptionState();
}

class _DescriptionState extends State<Description> {
  final User user = UserManagement.include.user;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return Container(
      decoration: kMainPagesDecoration,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: kAppBlack,
          centerTitle: true,
          title: Text(this.widget.channel.name ?? "", style: kNormalText),
        ),
        body: SingleChildScrollView(
          physics: ScrollPhysics(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              ChannelCurrentPageIndicator(
                  channel: this.widget.channel,
                  width: width,
                  selectedPage: channelMenus.description),
              ChannelThumbnail(
                  height: height,
                  channelName: this.widget.channel.name.toString(),
                  channelBackgroundImage:
                      this.widget.channel.backgroundImage ?? ""),
              Padding(
                  padding: const EdgeInsets.only(left: 16.0),
                  child: ChannelFavoriteButton(this.widget.channel, this.user)),
              Padding(
                padding: const EdgeInsets.only(left: 14.0),
                child: Text(this.widget.channel.description.toString(),
                    style: kNormalText.copyWith(fontSize: 16)),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: RichText(
                      text: TextSpan(children: <TextSpan>[
                    TextSpan(
                        text: htmlParser.DocumentFragment.html('&#9646;').text,
                        style:
                            TextStyle(color: Color(0xff3fd490), fontSize: 26)),
                    TextSpan(
                        text: 'Son Yüklenen Videolar',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18)),
                  ])),
                ),
              ),
              ChannelToVideoList(ChannelVideosReadService.include
                  .lastVideos(channel_id: this.widget.channel.uid!)),
            ],
          ),
        ),
      ),
    );
  }
}
