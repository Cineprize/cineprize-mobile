import 'package:cached_network_image/cached_network_image.dart';
import 'package:cineprize/ApplicationLayer/Auth/AuthManagement/LoginNavigator.dart';
import 'package:cineprize/ApplicationLayer/DetailedVideoScreen/DetailedVideoScreen.dart';
import 'package:flutter/material.dart';

Widget buildVerticalVideoCard(
  BuildContext context,
  int index, {
  required video,
  required double height,
  required double width,
  required CachedNetworkImage image,
  required String banner,
}) {
  double width2 = MediaQuery.of(context).size.width;

  return GestureDetector(
    onTap: () => {
      Navigator.push(
          loginNavigatorContext!,
          MaterialPageRoute(
              builder: (BuildContext context) =>
                  DetailedVideoScreen(video: video)))
    },
    child: Container(
      color: Colors.transparent,
      child: Card(
        elevation: 0,
        color: Colors.transparent,
        child: Column(
          children: [
            ClipRRect(
                borderRadius: BorderRadius.circular(17),
                child: Container(
                    height: height,
                    width: width == null ? width2 / 2.5 : width,
                    child: image)),
            Flexible(
                child: Text(banner,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 15,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                    maxLines: 2))
          ],
        ),
      ),
    ),
  );
}
