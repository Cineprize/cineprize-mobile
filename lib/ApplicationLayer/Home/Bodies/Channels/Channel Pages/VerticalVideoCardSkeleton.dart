import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:flutter/material.dart';
import 'package:skeleton_text/skeleton_text.dart';

class VerticalVideoCardSkeleton extends StatefulWidget {
  @override
  _VerticalVideoCardSkeletonState createState() => _VerticalVideoCardSkeletonState();
}

class _VerticalVideoCardSkeletonState extends State<VerticalVideoCardSkeleton> {
  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Container(
      color: Colors.transparent,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Card(
          elevation: 0,
          color: Colors.transparent,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(17),
                child: SkeletonAnimation(
                  shimmerColor: shimmerColor,
                  borderRadius: BorderRadius.circular(17),
                  child: Container(
                    height: height * (0.13),
                    width: width / 2.5,
                    child: SkeletonAnimation(
                        shimmerColor: shimmerColor,
                        borderRadius: BorderRadius.circular(17),
                        child: Container(width: width / 2.5, height: height, decoration: BoxDecoration(color: skeletonColor))),
                    decoration: BoxDecoration(color: skeletonColor.withOpacity(0.4)),
                  ),
                ),
              ),
              SizedBox(width: 20),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SkeletonAnimation(
                      shimmerColor: shimmerColor,
                      borderRadius: BorderRadius.circular(10.0),
                      child: Container(width: width / 2.4, height: 15, decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0), color: skeletonColor))),
                  SizedBox(height: 5),
                  SkeletonAnimation(
                      shimmerColor: shimmerColor,
                      borderRadius: BorderRadius.circular(10.0),
                      child: Container(width: width / 2.4, height: 15, decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0), color: skeletonColor))),
                  SizedBox(height: 5),
                  SkeletonAnimation(
                      shimmerColor: shimmerColor,
                      borderRadius: BorderRadius.circular(10.0),
                      child: Container(width: width / 6, height: 15, decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0), color: skeletonColor))),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
