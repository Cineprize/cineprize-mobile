import 'package:cineprize/ApplicationLayer/Home/Bodies/Channels/Channel%20Pages/Description.dart';
import 'package:cineprize/ApplicationLayer/Home/Bodies/Channels/Channel%20Pages/Episodes.dart';
import 'package:cineprize/Models/User.dart';
import 'package:flutter/material.dart';

final channelPageController = PageController(
  initialPage: 0,
);

PageView channelPageView({required User channel}) {
  return PageView(
    controller: channelPageController,
    children: [
      Description(channel: channel),
      Episodes(channel: channel),
    ],
  );
}
