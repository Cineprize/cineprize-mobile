import 'package:cineprize/ApplicationLayer/Auth/AuthManagement/LoginNavigator.dart';
import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/DetailedVideoScreen/DetailedVideoScreen.dart';
import 'package:cineprize/ApplicationLayer/Home/Bodies/Channels/Channel%20Pages/ChannelPageWidgets.dart';
import 'package:cineprize/ApplicationLayer/Home/Bodies/Channels/Channel%20Pages/ChannelToVideoList.dart';
import 'package:cineprize/ApplicationLayer/Tools/Image/ImageTools.dart';
import 'package:cineprize/Models/User.dart';
import 'package:cineprize/Models/Video.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Channel/ChannelVideosReadService.dart';
import 'package:flutter/material.dart';
import 'package:html/dom.dart' as htmlParser;

class Episodes extends StatefulWidget {
  final User channel;

  const Episodes({Key? key, required this.channel}) : super(key: key);

  @override
  _EpisodesState createState() => _EpisodesState();
}

class _EpisodesState extends State<Episodes> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Container(
      decoration: kMainPagesDecoration,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: kAppBlack,
          centerTitle: true,
          title: Text(this.widget.channel.name ?? "", style: kNormalText),
        ),
        body: SingleChildScrollView(
          physics: ScrollPhysics(),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              ChannelCurrentPageIndicator(
                  channel: this.widget.channel,
                  width: width,
                  selectedPage: channelMenus.episodes),
              ChannelThumbnail(
                  height: height,
                  channelName: this.widget.channel.name ?? "",
                  channelBackgroundImage:
                      this.widget.channel.backgroundImage ?? ""),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: RichText(
                      text: TextSpan(children: <TextSpan>[
                    TextSpan(
                      text: htmlParser.DocumentFragment.html('&#9646;').text,
                      style: TextStyle(color: Color(0xff3fd490), fontSize: 26),
                    ),
                    TextSpan(
                        text: 'Videolar',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18)),
                  ])),
                ),
              ),
              ChannelToVideoList(ChannelVideosReadService.include
                  .videos(channel_id: this.widget.channel.uid!)),
            ],
          ),
        ),
      ),
    );
  }
}

class ChannelThumbnail extends StatelessWidget {
  final String channelName;
  final String channelBackgroundImage;

  const ChannelThumbnail({
    Key? key,
    required this.channelName,
    required this.channelBackgroundImage,
    required this.height,
  }) : super(key: key);

  final double height;

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomLeft,
      children: [
        //API Hazır olduğunda kullanılacak yapı
        SizedBox(
          height: height / 4.5,
          child: InteractiveViewer(
            child: ImageTools.getImage(
                imageURL: this.channelBackgroundImage,
                gifPlaceholder: gifPlaceholder),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                channelName,
                style: kNormalText,
                textAlign: TextAlign.left,
              ),
              /*           Row(
                children: [
                  Icon(Icons.star, color: Colors.orange),
                  Icon(Icons.star, color: Colors.orange),
                  Icon(Icons.star, color: Colors.orange),
                  Icon(Icons.star, color: Colors.orange),
                  Icon(Icons.star, color: Colors.black),
                ],
              ),*/
            ],
          ),
        ),
      ],
    );
  }
}

class VerticalVideoCard extends StatefulWidget {
  final Video video;

  VerticalVideoCard({Key? key, required this.video});

  @override
  _VerticalVideoCardState createState() => _VerticalVideoCardState();
}

class _VerticalVideoCardState extends State<VerticalVideoCard> {
  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Container(
      color: Colors.transparent,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: GestureDetector(
          onTap: () => {
            Navigator.push(
                loginNavigatorContext!,
                MaterialPageRoute(
                    builder: (BuildContext context) =>
                        DetailedVideoScreen(video: this.widget.video)))
          },
          child: Card(
            elevation: 0,
            color: Colors.transparent,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(17),
                  child: Container(
                    height: height * (0.13),
                    width: width * (0.429),
                    child: InteractiveViewer(
                        child: ImageTools.getImage(
                            imageURL: this.widget.video.imageURL,
                            gifPlaceholder: gifPlaceholder)),
                    decoration: BoxDecoration(color: skeletonColor),
                  ),
                ),
                SizedBox(width: 20),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                        width: width / 2.4,
                        height: height / 12,
                        child: Text(this.widget.video.title,
                            style: kNormalText,
                            maxLines: 4,
                            overflow: TextOverflow.ellipsis))
                    // Text('00:16', style: TextStyle(color: Colors.grey, fontSize: 13))
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
