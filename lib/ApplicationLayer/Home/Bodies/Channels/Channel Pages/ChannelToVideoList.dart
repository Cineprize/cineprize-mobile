import 'package:cineprize/ApplicationLayer/Home/Bodies/Channels/Channel%20Pages/Episodes.dart';
import 'package:cineprize/ApplicationLayer/Home/Bodies/Channels/Channel%20Pages/VerticalVideoCardSkeleton.dart';
import 'package:cineprize/Models/Video.dart';
import 'package:flutter/material.dart';

class ChannelToVideoList extends StatefulWidget {
  Future<List<Video>> future;

  ChannelToVideoList(this.future);

  @override
  _ChannelToVideoListState createState() => _ChannelToVideoListState();
}

class _ChannelToVideoListState extends State<ChannelToVideoList> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Video>>(
        future: this.widget.future,
        initialData: [],
        builder: (BuildContext context, AsyncSnapshot<List<Video>> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            List<Widget> _elements = [];
            for (int i = 0; i < 5; i++) {
              _elements.add(VerticalVideoCardSkeleton());
            }

            return Column(children: _elements);
          }

          if (snapshot.hasData) {
            List<Widget> elements = [];
            snapshot.data!.forEach((video) {
              elements.add(VerticalVideoCard(video: video));
            });
            return Column(children: elements);
          } else if (snapshot.hasError) {
            return Text('HATA!');
          } else {
            return Text(
                'Videolar yüklenemedi.\nLütfen daha sonra tekrar deneyiniz.');
          }
        });
  }
}
