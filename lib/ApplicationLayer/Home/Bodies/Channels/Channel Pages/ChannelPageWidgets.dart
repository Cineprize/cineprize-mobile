import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Home/Bodies/Channels/Channel%20Pages/ChannelPageController.dart';
import 'package:cineprize/Models/User.dart';
import 'package:flutter/material.dart';

enum channelMenus { description, episodes }

class ChannelCurrentPageIndicator extends StatefulWidget {
  const ChannelCurrentPageIndicator({
    Key? key,
    required this.channel,
    required this.width,
    required this.selectedPage,
  }) : super(key: key);

  final User channel;
  final double width;
  final channelMenus selectedPage;

  @override
  _ChannelCurrentPageIndicator createState() => _ChannelCurrentPageIndicator();
}

class _ChannelCurrentPageIndicator extends State<ChannelCurrentPageIndicator> {
  @override
  Widget build(BuildContext context) {
    var selectedMenu = TextStyle(
        color: Colors.white, fontWeight: FontWeight.bold, fontSize: 16);
    var unpickedMenu = TextStyle(fontWeight: FontWeight.bold, fontSize: 16);
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(60), color: Colors.white),
          width: widget.width * 0.45,
          height: 50,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              GestureDetector(
                onTap: () {
                  channelPageView(channel: this.widget.channel)
                      .controller
                      .jumpToPage(0);
                },
                child: Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Container(
                    height: 40,
                    width: widget.width * 0.20,
                    child: Center(
                        child: Text(
                      'Açıklama',
                      style: widget.selectedPage == channelMenus.description
                          ? selectedMenu
                          : unpickedMenu,
                    )),
                    decoration: BoxDecoration(
                        color: widget.selectedPage == channelMenus.description
                            ? kAppThemeColor
                            : Colors.white,
                        borderRadius: BorderRadius.circular(60)),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  channelPageView(channel: this.widget.channel)
                      .controller
                      .jumpToPage(1);
                },
                child: Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: Container(
                    height: 40,
                    width: widget.width * 0.20,
                    child: Center(
                      child: Text('Bölümler',
                          style: widget.selectedPage == channelMenus.episodes
                              ? selectedMenu
                              : unpickedMenu),
                    ),
                    decoration: BoxDecoration(
                        color: widget.selectedPage == channelMenus.episodes
                            ? kAppThemeColor
                            : Colors.white,
                        borderRadius: BorderRadius.circular(60)),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
