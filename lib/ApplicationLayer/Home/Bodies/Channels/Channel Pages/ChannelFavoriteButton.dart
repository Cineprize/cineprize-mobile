import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/Models/User.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Channel/ChannelFavoriteService.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/material.dart';
import 'package:skeleton_text/skeleton_text.dart';

class ChannelFavoriteButton extends StatefulWidget {
  User channel;
  User authUser;

  ChannelFavoriteButton(this.channel, this.authUser);

  @override
  _ChannelFavoriteButtonState createState() => _ChannelFavoriteButtonState();
}

class _ChannelFavoriteButtonState extends State<ChannelFavoriteButton> {
  String? favoriteButtonText;
  Map favoriteButtonStatusText = {
    'yes': 'Favoriniz',
    'no': 'Favorilerime Ekle +'
  };
  late Future<bool> isFavoriteFuture;
  bool isReady = false;

  @override
  void initState() {
    this.isFavoriteFuture = ChannelFavoriteService.include
        .isFavorite(channel_id: this.widget.channel.uid!);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<bool>(
      future: this.isFavoriteFuture,
      initialData: null,
      builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
        this.isReady = snapshot.data != null;
        return RawMaterialButton(
          fillColor: Colors.transparent,
          padding: EdgeInsets.all(5),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0),
              side: BorderSide(color: kAppThemeColor)),
          child: this.isReady
              ? Text(
                  (this.favoriteButtonText != null
                      ? this.favoriteButtonText
                      : (snapshot.data!
                          ? this.favoriteButtonStatusText['yes']
                          : this.favoriteButtonStatusText['no'])),
                  style: TextStyle(color: kAppThemeColor))
              : SkeletonAnimation(
                  shimmerColor: shimmerColor,
                  borderRadius: BorderRadius.circular(10.0),
                  child: Container(
                      width: 75,
                      height: 14,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          color: skeletonColor))),
          onPressed: () async {
            final Map<String, dynamic> response = await ChannelFavoriteService
                .include
                .addOrRemove(channel_id: this.widget.channel.uid!);
            setState(() => {
                  this.favoriteButtonText = (response['isFavorite'] as bool)
                      ? this.favoriteButtonStatusText['yes']
                      : this.favoriteButtonStatusText['no']
                });
          },
        );
      },
    );
  }
}
