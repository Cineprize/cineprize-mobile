import 'package:flutter/material.dart';

class VerticalVideoList extends StatefulWidget {
  final ScrollController scrollController;
  final double height;
  final String barHeading;
  final List<Widget> children;

  const VerticalVideoList(
      {Key? key,
      required this.barHeading,
      required this.children,
      required this.height,
      required this.scrollController})
      : super(key: key);

  @override
  _VerticalVideoList createState() => _VerticalVideoList();
}

class _VerticalVideoList extends State<VerticalVideoList> {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return LimitedBox(
      maxHeight: this.widget.height == null ? height / 3.5 : this.widget.height,
      child: ListView(
        controller: this.widget.scrollController == null
            ? null
            : this.widget.scrollController,
        physics: BouncingScrollPhysics(),
        cacheExtent: 100,
        addAutomaticKeepAlives: true,
        scrollDirection: Axis.vertical,
        children: this.widget.children,
      ),
    );
  }
}

// Skeleton Area

// class CustomVerticalVideoBarSkeleton extends StatefulWidget {
//   final ScrollController scrollController;
//   final double height;

//   const CustomVerticalVideoBarSkeleton(
//       {Key key, this.height, this.scrollController})
//       : super(key: key);

//   @override
//   _CustomHorizontalVideoBarSkeletonState createState() =>
//       _CustomHorizontalVideoBarSkeletonState();
// }

// class _CustomHorizontalVideoBarSkeletonState
//     extends State<CustomVerticalVideoBarSkeleton> {
//   @override
//   Widget build(BuildContext context) {
//     //double height = MediaQuery.of(context).size.height;
//     double height = widget.height;
//     return Padding(
//       padding: const EdgeInsets.only(left: 15.0),
//       child: Column(
//         mainAxisAlignment: MainAxisAlignment.start,
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           SizedBox(
//             height: 5,
//           ),
//           LimitedBox(
//             maxHeight: this.widget.height + 50,
//             child: ListView(
//               controller: this.widget.scrollController == null
//                   ? null
//                   : this.widget.scrollController,
//               physics: BouncingScrollPhysics(),
//               cacheExtent: 100,
//               addAutomaticKeepAlives: true,
//               scrollDirection: Axis.vertical,
//               children: buildVideoListSkeletonCard(context, 3, height),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
