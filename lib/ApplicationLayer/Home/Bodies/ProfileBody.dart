import 'dart:io';

import 'package:cineprize/ApplicationLayer/Auth/AuthManagement/LoginNavigator.dart';
import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Home/AppBar/HomeAppBar.dart';
import 'package:cineprize/ApplicationLayer/Home/Profile/SubPages/Chat.dart';
import 'package:cineprize/ApplicationLayer/Home/Profile/SubPages/CouponScreen.dart';
import 'package:cineprize/ApplicationLayer/Home/Profile/SubPages/DetailedProfileScreen.dart';
import 'package:cineprize/ApplicationLayer/Home/Profile/SubPages/Gains.dart';
import 'package:cineprize/ApplicationLayer/Home/Profile/SubPages/InviteEarn.dart';
import 'package:cineprize/ApplicationLayer/Home/Profile/SubPages/SalesScreen.dart';
import 'package:cineprize/ApplicationLayer/Home/Profile/SubPages/SettingsScreen.dart';
import 'package:cineprize/ApplicationLayer/Tools/Image/ImageTools.dart';
import 'package:cineprize/ApplicationLayer/Tools/MyAlertDialogs/MyInfoDialog.dart';
import 'package:cineprize/ApplicationLayer/Tools/StateUpdateIndicator/StateUpdateIndicator.dart';
import 'package:cineprize/Models/User.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class ProfileBody extends StatefulWidget {
  @override
  _ProfileBodyState createState() => _ProfileBodyState();
}

class _ProfileBodyState extends State<ProfileBody> {
  @override
  Widget build(BuildContext context) {
    final User user = Provider.of<UserManagement>(context).user;

    double height = MediaQuery.of(context).size.height;

    return CupertinoPageScaffold(
      child: Container(
        decoration: kMainPagesDecoration,
        child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: generalAppBar(title: 'Profil'),
          body: Container(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: Column(
                      children: [
                        SizedBox(height: 10),
                        ClipOval(
                            child: SizedBox(
                                height: height / 6,
                                width: height / 6,
                                child: ImageTools.getImage(
                                    imageURL: user.imageURL ?? "",
                                    gifPlaceholder: profileGifPlaceholder))),
                        SizedBox(height: 10),
                        Text(user.userName.toLowerCase(),
                            style: TextStyle(
                                fontSize: 25,
                                color: Colors.white,
                                fontWeight: FontWeight.bold)),
                        SizedBox(height: 15),
                        user.canEarn
                            ? Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Row(
                                    children: [
                                      Container(
                                        height: 25,
                                        width: 25,
                                        decoration: BoxDecoration(
                                            image: DecorationImage(
                                                image: AssetImage(
                                                    'assets/images/coinIcons/crystal_green.png'),
                                                fit: BoxFit.contain)),
                                      ),
                                      SizedBox(width: 5),
                                      StateUpdateIndicator.stateControl(
                                        context: context,
                                        child: Text(
                                            'CineCoin : ${user.coin.toString()}',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 15,
                                                fontWeight: FontWeight.bold)),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Container(
                                        height: 25,
                                        width: 25,
                                        decoration: BoxDecoration(
                                            image: DecorationImage(
                                                image: AssetImage(
                                                    'assets/images/coinIcons/crystal_pink.png'),
                                                fit: BoxFit.contain)),
                                      ),
                                      SizedBox(width: 5),
                                      StateUpdateIndicator.stateControl(
                                        context: context,
                                        child: Text('CineGold: ${user.gold}',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 15,
                                                fontWeight: FontWeight.bold)),
                                      ),
                                    ],
                                  ),
                                ],
                              )
                            : Container(),
                        SizedBox(height: 20),
                        /* Platform.isAndroid
                            ? ProfileElement(text: 'CinePremium', routeTo: SubscriptionScreen(), isLocked: false, imagePath: 'assets/images/premium/crown.png')
                            : Container(), */
                        ProfileElement(
                            text: 'Profil',
                            isLocked: false,
                            routeTo: DetailedProfileScreen()),
                        //ProfileElement(text: 'Başvuru', isLocked: false, routeTo: AppealScreen()),
                        user.canEarn
                            ? ProfileElement(
                                text: 'Satın Alımlarım',
                                isLocked: false,
                                routeTo: SalesScreen())
                            : Container(),

                        ProfileElement(
                            text: user.canEarn
                                ? 'Davet Et, Kazan'
                                : 'Arkadaşlarını Davet Et',
                            isLocked: false,
                            routeTo: InviteEarn()),
                        user.canEarn && Platform.isAndroid
                            ? ProfileElement(
                                text: 'Kupon Gir, Kazan',
                                isLocked: false,
                                routeTo: CouponScreen())
                            : Container(),
                        ProfileElement(
                            text:
                                user.canEarn ? 'Kazançlar' : 'Bizi Takip Edin',
                            isLocked: false,
                            routeTo: Gains()),
                        user.canEarn
                            ? StateUpdateIndicator.stateControl(
                                context: context,
                                child: ProfileElement(
                                    text: 'Sohbet',
                                    isLocked: user.isLocked,
                                    routeTo: Chat()))
                            : Container(),

                        ProfileElement(
                          text: 'Ayarlar',
                          isLocked: false,
                          routeTo: SettingsScreen(),
                        ),

                        // ProfileElement(text: 'Anket', isLocked: false, routeTo: SurveyScreen()),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class ProfileElement extends StatefulWidget {
  final String text;
  final Widget routeTo;
  final bool isLocked;
  final String? imagePath;

  const ProfileElement(
      {Key? key,
      required this.text,
      required this.routeTo,
      required this.isLocked,
      this.imagePath})
      : super(key: key);

  @override
  _ProfileElementState createState() => _ProfileElementState();
}

class _ProfileElementState extends State<ProfileElement> {
  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      splashColor: Colors.transparent,
      hoverColor: Colors.transparent,
      highlightColor: Colors.transparent,
      onPressed: () {
        if (widget.isLocked) return;
        Navigator.push(
            loginNavigatorContext!,
            MaterialPageRoute(
                builder: (loginNavigatorContext) => widget.routeTo));
      },
      child: Column(
        children: [
          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              if (widget.imagePath != null)
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Center(
                      child: Text(widget.text, style: kBigText),
                    ),
                    SizedBox(width: 15),
                    Image(
                        color: Colors.white,
                        fit: BoxFit.contain,
                        height: MediaQuery.of(context).size.width * 0.05,
                        width: MediaQuery.of(context).size.width * 0.05,
                        image: AssetImage(widget.imagePath!))
                  ],
                )
              else
                Text(widget.text, style: kBigText),
              Row(
                children: [
                  widget.isLocked
                      ? InkWell(
                          child: Icon(Icons.info, size: 25),
                          onTap: () async => {
                                MyInfoDialog(loginNavigatorContext!,
                                    title: 'Bilgi',
                                    description:
                                        'Mağazadan ürün aldığınızda sohbet kilidi kalkacaktır.',
                                    buttonText: 'Tamam',
                                    alertType: AlertType.info)
                              })
                      : Container(),
                  SizedBox(width: 10),
                  Icon(
                      widget.isLocked ? Icons.lock : Icons.keyboard_arrow_right,
                      size: 25),
                ],
              ),
            ],
          ),
          SizedBox(height: 10),
          Divider(color: Colors.white24),
        ],
      ),
    );
  }
}
