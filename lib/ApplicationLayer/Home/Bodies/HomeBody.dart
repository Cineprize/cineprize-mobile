import 'dart:async';
import 'dart:io';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:cineprize/ApplicationLayer/Auth/AuthManagement/LoginNavigator.dart';
import 'package:cineprize/ApplicationLayer/Components/CustomHorizontalVideoBar.dart';
import 'package:cineprize/ApplicationLayer/Components/VideoCardWidget.dart';
import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Home/AppBar/HomeAppBar.dart';
import 'package:cineprize/ApplicationLayer/Home/Mods/GiveawayScreen.dart';
import 'package:cineprize/ApplicationLayer/Home/Mods/SurveyScreen.dart';
import 'package:cineprize/ApplicationLayer/Home/Profile/SubPages/Gains.dart';
import 'package:cineprize/ApplicationLayer/Home/Profile/SubPages/InviteEarn.dart';
import 'package:cineprize/ApplicationLayer/Home/Profile/SubPages/sssScreen.dart';
import 'package:cineprize/ApplicationLayer/Tools/Image/ImageTools.dart';
import 'package:cineprize/ApplicationLayer/components/Custom5pcsSlider.dart';
import 'package:cineprize/ApplicationLayer/components/SquareCardWidget.dart';
import 'package:cineprize/CommonLayer/Video/VideoQueryType.dart';
import 'package:cineprize/Models/CategoryWithVideo.dart';
import 'package:cineprize/Models/Response.dart';
import 'package:cineprize/Models/Slider.dart' as SliderModel;
import 'package:cineprize/Models/Video.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Category/CategoryService.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Slider/SliderReadService.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Video/VideoReadService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:skeleton_text/skeleton_text.dart';
import 'package:url_launcher/url_launcher.dart';

class HomeBody extends StatefulWidget {
  @override
  _HomeBodyState createState() => _HomeBodyState();
}

class _HomeBodyState extends State<HomeBody> {
  int _current = 0;
  late Future<List<CategoryWithVideo>> categoryWithVideoFuture;
  late Future<List<CategoryWithVideo>> categoryWithVideoOthersFuture;
  late Future<List<SliderModel.Slider>> slidersFuture;
  late Future<List<Video>> newsFuture;
  late Future<Response> categoriesFuture;
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    this.categoryWithVideoFuture =
        VideoReadService.include.categoryWithVideoHome();
    this.categoryWithVideoOthersFuture =
        VideoReadService.include.categoryWithVideoHomeOther();
    this.slidersFuture = SliderReadService.include.read();
    this.newsFuture =
        VideoReadService.include.read(videoQueryType: VideoQueryType.News);
    this.categoriesFuture = CategoryService.include.read();
    super.initState();
  }

  launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'İstek başarısız! $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return RefreshIndicator(
      key: _refreshIndicatorKey,
      onRefresh: () async {
        setState(() {
          this.categoryWithVideoFuture =
              VideoReadService.include.categoryWithVideoHome();
          this.categoryWithVideoOthersFuture =
              VideoReadService.include.categoryWithVideoHomeOther();
          this.slidersFuture = SliderReadService.include.read();
          this.newsFuture = VideoReadService.include
              .read(videoQueryType: VideoQueryType.News);
        });
      },
      child: CupertinoPageScaffold(
        child: Container(
          decoration: kMainPagesDecoration,
          child: Scaffold(
            backgroundColor: Colors.transparent,
            appBar: homeAppBar(),
            body: SingleChildScrollView(
              clipBehavior: Clip.hardEdge,
              physics: BouncingScrollPhysics(),
              child: Column(
                children: [
                  SizedBox(height: 10),
                  //Haberler Carousel Slider
                  FutureBuilder<List<SliderModel.Slider>>(
                      future: this.slidersFuture,
                      initialData: [],
                      builder: (BuildContext context,
                          AsyncSnapshot<List<SliderModel.Slider>> snapshot) {
                        if (snapshot.connectionState ==
                            ConnectionState.waiting) {
                          return sliderItemNewSkeleton(width, height);
                        }

                        if (snapshot.hasData) {
                          return Stack(
                            children: [
                              CarouselSlider(
                                items: snapshot.data!.map((i) {
                                  return i.pageName == ""
                                      ? sliderItem(i)
                                      : GestureDetector(
                                          child: sliderItem(i),
                                          onTap: () async {
                                            switch (i.pageName) {
                                              case 'Survey':
                                                Navigator.push(
                                                    loginNavigatorContext!,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            SurveyScreen()));
                                                break;
                                              case 'References':
                                                Navigator.push(
                                                    loginNavigatorContext!,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            InviteEarn()));
                                                break;
                                              case 'Gains':
                                                Navigator.push(
                                                    loginNavigatorContext!,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            Gains()));
                                                break;
                                              case 'SSS':
                                                Navigator.push(
                                                    loginNavigatorContext!,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            sssScreen()));
                                                break;
                                              case 'Giveaway':
                                                Navigator.push(
                                                    loginNavigatorContext!,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            GiveawayScreen()));
                                                break;
                                              default:
                                                await launchURL(i.pageName!);
                                                break;
                                            }
                                          },
                                        );
                                }).toList(),
                                options: CarouselOptions(
                                  height: height / 4,
                                  enableInfiniteScroll: true,
                                  disableCenter: true,
                                  pauseAutoPlayOnTouch: true,
                                  viewportFraction: 0.85,
                                  autoPlay: true,
                                  enlargeCenterPage: true,
                                  autoPlayCurve: Curves.fastOutSlowIn,
                                  autoPlayAnimationDuration:
                                      Duration(milliseconds: 800),
                                  aspectRatio: 16 / 9,
                                  onPageChanged: (index, reason) => {
                                    setState(() => {_current = index})
                                  },
                                ),
                              ),
                            ],
                          );
                        } else {
                          return sliderItemNewSkeleton(width, height);
                        }
                      }),
                  //5 li yatay kaydırlabilir listview (Anket,Gündem,Çekiliş,Yardım,Tahmin)
                  SizedBox(height: 20),
                  //1 - Yeni Gelenler
                  CustomHorizontalVideoBar(
                    barHeading: 'Yeni Gelenler',
                    children: [
                      FutureBuilder<List<Video>>(
                        future: this.newsFuture,
                        initialData: [],
                        builder: (BuildContext contextVideo,
                            AsyncSnapshot<List<Video>> snapshotVideo) {
                          List<Widget> videoElements = [];

                          if (snapshotVideo.connectionState ==
                              ConnectionState.waiting) {
                            return Row(
                                children: buildVideoListSkeletonCard(
                                    context, 6, height * (0.13),
                                    width: width * (0.429)));
                          }
                          if (snapshotVideo.hasData) {
                            snapshotVideo.data?.forEach((video) async {
                              videoElements.add(buildVideoCard(context,
                                  height: height * (0.13),
                                  width: width * (0.45),
                                  image: ImageTools.getImage(
                                      imageURL: video.imageURL,
                                      gifPlaceholder: gifPlaceholder),
                                  banner: video.title,
                                  video: video));
                            });
                            return Row(children: videoElements);
                          } else {
                            return Container();
                          }
                        },
                      ),
                    ],
                  ),
                  //Kategorilere göre listeleme
                  FutureBuilder<List<CategoryWithVideo>>(
                    future: this.categoryWithVideoFuture,
                    initialData: [],
                    builder: (BuildContext context,
                        AsyncSnapshot<List<CategoryWithVideo>> snapshot) {
                      List<Widget> elements = [];

                      if (snapshot.connectionState == ConnectionState.waiting) {
                        elements.add(CustomHorizontalVideoBar(
                            barHeading: '',
                            children: buildVideoListSkeletonCard(
                                context, 6, height * (0.13),
                                width: width * (0.429))));
                        return Column(children: elements);
                      }
                      if (snapshot.hasData) {
                        snapshot.data!.forEach((categoryWithVideo) async {
                          elements.add(CustomHorizontalVideoBar(
                              barHeading: categoryWithVideo.name,
                              children: videoListCreate(categoryWithVideo)));
                        });

                        return Column(children: elements);
                      } else {
                        return Center(
                            child: Column(children: [
                          CircularProgressIndicator(),
                          Text('Videolar yükleniyor...')
                        ]));
                      }
                    },
                  ),
                  SizedBox(height: 5),
                  //4 - Ekstra Kazançlar
                  /*  Platform.isAndroid || isSandbox == true
                      ? GainsList(
                          barHeading: 'Ekstra Kazançlar',
                          itemBuilder: (BuildContext context, int index) =>
                              buildGainsCard(loginNavigatorContext, index, height: height * (0.095), width: width * (0.33), image: extraEarningsList[index].videoThumbnail),
                          itemCount: extraEarningsList.length,
                          height: height * (0.18))
                      : Container(), */
                  Padding(
                    padding: EdgeInsets.only(left: 15.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        HorizontalVideoBarHeading(barHeading: 'CineMod'),
                        SizedBox(height: 5),
                        Custom5pcsSlider(height: height)
                      ],
                    ),
                  ),
                  SizedBox(height: 10),
                  //Kategorilere göre listeleme
                  FutureBuilder<List<CategoryWithVideo>>(
                    future: this.categoryWithVideoOthersFuture,
                    initialData: [],
                    builder: (BuildContext context,
                        AsyncSnapshot<List<CategoryWithVideo>> snapshot) {
                      List<Widget> elements = [];

                      if (snapshot.connectionState == ConnectionState.waiting) {
                        elements.add(CustomHorizontalVideoBar(
                            barHeading: '',
                            children: buildVideoListSkeletonCard(
                                context, 6, 100,
                                width: width * (0.429))));
                        return Column(children: elements);
                      }
                      if (snapshot.hasData) {
                        snapshot.data!.forEach((categoryWithVideo) async {
                          elements.add(CustomHorizontalVideoBar(
                              barHeading: categoryWithVideo.name,
                              children: videoListCreate(categoryWithVideo)));
                        });

                        return Column(children: elements);
                      } else {
                        return Center(
                            child: Column(children: [
                          CircularProgressIndicator(),
                          Text('Videolar yükleniyor...')
                        ]));
                      }
                    },
                  ),
                  SizedBox(height: 5),
                  //5 - Kategoriler
                  CustomHorizontalVideoBar(
                    barHeading: 'Kategoriler',
                    children: [
                      FutureBuilder<Response>(
                        future: this.categoriesFuture,
                        builder: (BuildContext contextVideo,
                            AsyncSnapshot<Response> snapshotCategory) {
                          List<Widget> categoryElements = [];
                          if (snapshotCategory.connectionState ==
                              ConnectionState.waiting) {
                            for (int i = 0; i < 5; i++) {
                              categoryElements
                                  .add(buildSquareCardSkeleton(context));
                            }

                            return Row(children: categoryElements);
                          } else {
                            if (!snapshotCategory.data!.success)
                              return Text(snapshotCategory.data!.message);
                            if (snapshotCategory.hasData) {
                              snapshotCategory.data!.data.forEach(
                                (category) {
                                  categoryElements.add(buildSquareCard(context,
                                      category: category));
                                },
                              );
                              return Row(children: categoryElements);
                            } else if (snapshotCategory.hasError) {
                              return Text('Bir sorun oluştu!');
                            } else {
                              return Center(child: CircularProgressIndicator());
                            }
                          }
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget sliderItem(SliderModel.Slider i) => Builder(
        builder: (BuildContext context) => ClipRRect(
          borderRadius: BorderRadius.circular(17),
          child: Container(
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(17)),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: ImageTools.getImage(
                    imageURL: i.imageURL!, gifPlaceholder: gifPlaceholder)),
          ),
        ),
      );

  Widget sliderItemNewSkeleton(width, height) => Stack(
        children: [
          CarouselSlider(
            items: [1, 1].map((i) {
              return ClipRRect(
                borderRadius: BorderRadius.circular(17),
                child: Container(
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.circular(17)),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: SkeletonAnimation(
                        shimmerColor: shimmerColor,
                        child: Container(
                            width: width,
                            height: height,
                            decoration: BoxDecoration(color: skeletonColor))),
                  ),
                ),
              );
            }).toList(),
            options: CarouselOptions(
                height: height / 4,
                enableInfiniteScroll: true,
                disableCenter: true,
                pauseAutoPlayOnTouch: true,
                viewportFraction: 0.85,
                autoPlay: true,
                enlargeCenterPage: true,
                autoPlayCurve: Curves.fastOutSlowIn,
                autoPlayAnimationDuration: Duration(milliseconds: 800),
                aspectRatio: 16 / 9,
                onPageChanged: (index, reason) {
                  setState(() => {_current = index});
                }),
          ),
        ],
      );

// anasayfadaki kategorileri listelerken
  List<Widget> videoListCreate(CategoryWithVideo categoryWithVideo) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    List<Widget> videoElements = [];
    categoryWithVideo.videos.forEach((video) {
      videoElements.add(buildVideoCard(context,
          height: height * (0.13),
          width: width * (0.45),
          image: ImageTools.getImage(
              imageURL: video.imageURL, gifPlaceholder: gifPlaceholder),
          banner: video.title,
          video: video));
    });
    return videoElements;
  }
}
