import 'dart:core';

import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Home/AppBar/HomeAppBar.dart';
import 'package:flutter/material.dart';

class NoAccessScreen extends StatefulWidget {
  final String message;

  const NoAccessScreen(
      {Key? key, this.message = 'Bu sayfayı görüntüleme yetkiniz bulunamadı!'})
      : super(key: key);

  @override
  _NoAccessScreenState createState() => _NoAccessScreenState();
}

class _NoAccessScreenState extends State<NoAccessScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: kMainPagesDecoration,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: homeAppBarWithBackButton(),
        body: Center(
          child: Text(
            widget.message.toString(),
            textAlign: TextAlign.center,
            style: kBigText,
            softWrap: true,
            overflow: TextOverflow.clip,
          ),
        ),
      ),
    );
  }
}
