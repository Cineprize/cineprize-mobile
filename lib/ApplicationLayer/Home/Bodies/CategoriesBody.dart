import 'package:cineprize/ApplicationLayer/Components/DarkBox.dart';
import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Home/AppBar/HomeAppBar.dart';
import 'package:cineprize/Models/Response.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Category/CategoryService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CategoriesBody extends StatefulWidget {
  @override
  _CategoriesBodyState createState() => _CategoriesBodyState();
}

class _CategoriesBodyState extends State<CategoriesBody> {
  late Future<Response> _categoriesFuture;

  @override
  void initState() {
    _categoriesFuture = CategoryService.include.read();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      child: Container(
        decoration: kMainPagesDecoration,
        child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: generalAppBar(title: 'Kategoriler'),
          body: FutureBuilder<Response>(
            future: _categoriesFuture,
            builder: (BuildContext context, AsyncSnapshot<Response> snapshot) {
              List<Widget> elements = [];
              if (snapshot.connectionState == ConnectionState.waiting) {
                for (int i = 0; i < 10; i++) elements.add(DarkBoxSkeleton());

                return GridView.count(
                    primary: false,
                    padding: const EdgeInsets.all(20),
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10,
                    crossAxisCount: 2,
                    children: elements);
              } else {
                if (!snapshot.data!.success)
                  return Text(snapshot.data!.message);
                if (snapshot.hasData) {
                  snapshot.data!.data.forEach((category) {
                    elements.add(DarkBox(category: category));
                  });

                  return GridView.count(
                      primary: false,
                      padding: const EdgeInsets.all(20),
                      crossAxisSpacing: 10,
                      mainAxisSpacing: 10,
                      crossAxisCount: 2,
                      children: elements);
                } else if (snapshot.hasError) {
                  return Text('Bir sorun oluştu.');
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              }
            },
          ),
        ),
      ),
    );
  }
}
