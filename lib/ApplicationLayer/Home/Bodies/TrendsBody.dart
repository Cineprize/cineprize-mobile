import 'package:cineprize/ApplicationLayer/Auth/AuthManagement/LoginNavigator.dart';
import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/DetailedVideoScreen/DetailedVideoScreen.dart';
import 'package:cineprize/ApplicationLayer/Home/AppBar/HomeAppBar.dart';
import 'package:cineprize/ApplicationLayer/Tools/Image/ImageTools.dart';
import 'package:cineprize/Models/Video.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Video/TrendReadService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:skeleton_text/skeleton_text.dart';

final String path = 'assets/images/CarouselSliderImage/';

class TrendsBody extends StatefulWidget {
  @override
  _TrendsBodyState createState() => _TrendsBodyState();
}

class _TrendsBodyState extends State<TrendsBody> {
  late Future<List<Video>> _trendsFuture;

  @override
  void initState() {
    _trendsFuture = TrendReadService.include.read();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      child: Container(
        decoration: kMainPagesDecoration,
        child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: generalAppBar(title: 'Trendler'),
          body: FutureBuilder<List<Video>>(
            future: _trendsFuture,
            initialData: [],
            builder: (BuildContext context,
                AsyncSnapshot<List<Video>> snapshotVideo) {
              List<Widget> videoElements = [];
              if (snapshotVideo.connectionState == ConnectionState.waiting) {
                videoElements.add(buildTripSkeletonCard(context));
                videoElements.add(buildTripSkeletonCard(context));
                videoElements.add(buildTripSkeletonCard(context));
                videoElements.add(buildTripSkeletonCard(context));
                return Container(
                    color: kAppBlack, child: ListView(children: videoElements));
              }
              if (snapshotVideo.hasData) {
                snapshotVideo.data?.forEach((video) {
                  videoElements.add(buildTripCard(context, video));
                });
                return Container(
                    color: kAppBlack, child: ListView(children: videoElements));
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            },
          ),
        ),
      ),
    );
  }

  Widget buildTripCard(BuildContext context, Video video) {
    return GestureDetector(
      onTap: () => {
        Navigator.push(
            loginNavigatorContext!,
            MaterialPageRoute(
                builder: (BuildContext context) =>
                    DetailedVideoScreen(video: video)))
      },
      child: Container(
        child: Card(
          color: kAppBlack,
          child: Column(
            children: [
              ListTile(
                contentPadding:
                    EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                leading: ClipOval(
                  child: AspectRatio(
                    aspectRatio: 1,
                    child: Container(
                      decoration: BoxDecoration(shape: BoxShape.circle),
                      child: ImageTools.getImage(
                          imageURL: video.user.imageURL ?? "",
                          gifPlaceholder: gifPlaceholder),
                    ),
                  ),
                ),
                title: Text(video.title, style: kNormalText),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                child: Container(
                  height: MediaQuery.of(context).size.width * 0.6,
                  child: ImageTools.getImage(
                      imageURL: video.imageURL, gifPlaceholder: gifPlaceholder),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildTripSkeletonCard(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return GestureDetector(
      child: Container(
        child: Card(
          color: kAppBlack,
          child: Column(
            children: [
              ListTile(
                contentPadding:
                    EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                leading: ClipOval(
                  child: AspectRatio(
                    aspectRatio: 1,
                    child: Container(
                      decoration: BoxDecoration(shape: BoxShape.circle),
                      child: SkeletonAnimation(
                        shimmerColor: shimmerColor,
                        child: Container(
                          width: width / 2.5,
                          height: height,
                          decoration: BoxDecoration(
                            color: skeletonColor,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                title: SkeletonAnimation(
                  shimmerColor: shimmerColor,
                  child: Container(
                    width: width,
                    height: 13,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        color: skeletonColor),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                child: Container(
                  height: MediaQuery.of(context).size.width * 0.6,
                  child: SkeletonAnimation(
                    shimmerColor: shimmerColor,
                    child: Container(
                      width: width,
                      height: height,
                      decoration: BoxDecoration(
                        color: skeletonColor,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
