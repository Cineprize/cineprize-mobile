import 'package:cineprize/ApplicationLayer/Auth/AuthManagement/LoginNavigator.dart';
import 'package:cineprize/ApplicationLayer/Home/Bodies/CategoriesBody.dart';
import 'package:cineprize/ApplicationLayer/Home/Bodies/Helpers/NoAccessScreen.dart';
import 'package:cineprize/ApplicationLayer/Home/Bodies/ProfileBody.dart';
import 'package:cineprize/ApplicationLayer/Home/Bodies/TrendsBody.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'Bodies/HomeBody.dart';
import 'Bodies/Store/PageController.dart';

enum TabItemEnum { Trends, Categories, Home, Profile, Store }

final double bottomIconSize = 20;

class TabItem {
  static List<BottomNavigationBarItem> getBottomNavigationBarItems() {
    final List<BottomNavigationBarItem> items = [];

    items.addAll([
      //Trends
      BottomNavigationBarItem(
        activeIcon: Image(
            image: AssetImage(
                'assets/images/NavigstionBarIcons/active/active_trend.png'),
            height: bottomIconSize),
        icon: Image(
            image: AssetImage(
                'assets/images/NavigstionBarIcons/passive/Trend@6x.png'),
            height: bottomIconSize),
      ),
      //Categories
      BottomNavigationBarItem(
        activeIcon: Image(
            image: AssetImage(
                'assets/images/NavigstionBarIcons/active/active_category.png'),
            height: bottomIconSize),
        icon: Image(
            image: AssetImage(
                'assets/images/NavigstionBarIcons/passive/category.png'),
            height: bottomIconSize),
      ),
      //Home
      BottomNavigationBarItem(
        activeIcon: Image(
            width: 100,
            image: AssetImage(
                'assets/images/NavigstionBarIcons/active/active_home.png'),
            height: bottomIconSize),
        icon: Image(
            image:
                AssetImage('assets/images/NavigstionBarIcons/passive/home.png'),
            height: bottomIconSize),
      ),
      //Profile
      BottomNavigationBarItem(
        activeIcon: Image(
            image: AssetImage(
                'assets/images/NavigstionBarIcons/active/active_profile.png'),
            height: bottomIconSize),
        icon: Image(
            image: AssetImage(
                'assets/images/NavigstionBarIcons/passive/profile.png'),
            height: bottomIconSize),
      ),
    ]);

    //if (Platform.isAndroid)
    //Shop
    items.add(BottomNavigationBarItem(
      activeIcon: Image(
          image: AssetImage(
              'assets/images/NavigstionBarIcons/active/active_shop.png'),
          height: bottomIconSize),
      icon: Image(
          image:
              AssetImage('assets/images/NavigstionBarIcons/passive/shop.png'),
          height: bottomIconSize),
    ));
    return items;
  }

  static Map<TabItemEnum, Widget> getSelectedPage() {
    return {
      TabItemEnum.Trends: TrendsBody(),
      TabItemEnum.Categories: CategoriesBody(),
      TabItemEnum.Home: HomeBody(),
      TabItemEnum.Profile: ProfileBody(),
      TabItemEnum.Store:
          Provider.of<UserManagement>(loginNavigatorContext!).user.canEarn
              ? storePageView
              : NoAccessScreen(
                  message:
                      'Hesabınız hediye kazanımına kapalı!\nBu sayfayı görüntüleyemezsiniz.',
                ),
    };
  }

  static Map<TabItemEnum, GlobalKey<NavigatorState>> navigatorKeys = {
    TabItemEnum.Trends: GlobalKey<NavigatorState>(),
    TabItemEnum.Categories: GlobalKey<NavigatorState>(),
    TabItemEnum.Home: GlobalKey<NavigatorState>(),
    TabItemEnum.Profile: GlobalKey<NavigatorState>(),
    TabItemEnum.Store: GlobalKey<NavigatorState>(),
  };
}
