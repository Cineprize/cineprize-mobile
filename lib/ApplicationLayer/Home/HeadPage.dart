// import 'package:appodeal_flutter/appodeal_flutter.dart';
// import 'package:cineprize/ApplicationLayer/Constants.dart';
// import 'package:cineprize/ApplicationLayer/Home/AppBar/HomeAppBar.dart';
// import 'package:cineprize/ApplicationLayer/Home/Bodies/CategoriesBody.dart';
// import 'package:cineprize/ApplicationLayer/Home/Bodies/HomeBody.dart';
// import 'package:cineprize/ApplicationLayer/Home/Bodies/ProfileBody.dart';
// import 'package:cineprize/ApplicationLayer/Home/Bodies/Store/StoreBody.dart';
// import 'package:cineprize/ApplicationLayer/Home/Bodies/TrendsBody.dart';
// import 'package:flutter/material.dart';
// import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

// class HeadPage extends StatefulWidget {
//   @override
//   _HeadPageState createState() => _HeadPageState();
// }

// class _HeadPageState extends State<HeadPage> {

//   final tabs = [
//     TrendsBody(),
//     CategoriesBody(),
//     HomeBody(),
//     ProfileBody(),
//     StoreBody(),
//   ];

//   final appBars = [
//     generalAppBar(title: 'Trendler'),
//     generalAppBar(title: 'Kategoriler'),
//     homeAppBar(),
//     generalAppBar(title: 'Profil'),
//     generalAppBar(title: 'Mağaza'),
//   ];

//   //BottomNavigationBarPageIndex
//   //Starting Index
//   //2 is HomeBody
//   int _currentIndex = 2;

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       decoration: kMainPagesDecoration,
//       child: Scaffold(
//         backgroundColor: Colors.transparent,
//         appBar: appBars[_currentIndex],
//         body: tabs[_currentIndex],
//         bottomNavigationBar: BottomNavigationBar(
//           showSelectedLabels: false,
//           showUnselectedLabels: false,
//           onTap: (index) {
//             setState(() {
//               _currentIndex = index;
//             });
//           },
//           type: BottomNavigationBarType.fixed,
//           backgroundColor: kBottomNavigationBarBackgroundColor,
//           currentIndex: _currentIndex,
//           iconSize: 25,
//           items: [
//             //Trends Page - 1
//             BottomNavigationBarItem(
//               activeIcon: Icon(
//                 MdiIcons.fire,
//                 color: kBottomNavigationBarIconActive,
//               ),
//               icon: Icon(
//                 MdiIcons.fire,
//                 color: kBottomNavigationBarIconInActive,
//               ),
//               title: BottomNavBarText(
//                 text: '',
//               ),
//             ),
//             //Categories Page - 2
//             BottomNavigationBarItem(
//               activeIcon: Icon(
//                 MdiIcons.playCircle,
//                 color: kBottomNavigationBarIconActive,
//               ),
//               icon: Icon(
//                 MdiIcons.playCircle,
//                 color: kBottomNavigationBarIconInActive,
//               ),
//               title: BottomNavBarText(
//                 text: '',
//               ),
//             ),
//             //Home Page - 3
//             BottomNavigationBarItem(
//               activeIcon: Icon(
//                 MdiIcons.home,
//                 color: kBottomNavigationBarIconActive,
//               ),
//               icon: Icon(
//                 MdiIcons.home,
//                 color: kBottomNavigationBarIconInActive,
//               ),
//               title: BottomNavBarText(
//                 text: '',
//               ),
//             ),
//             //Profile Page - 4
//             BottomNavigationBarItem(
//               activeIcon: Icon(
//                 Icons.person,
//                 color: kBottomNavigationBarIconActive,
//               ),
//               icon: Icon(
//                 Icons.person,
//                 color: kBottomNavigationBarIconInActive,
//               ),
//               title: BottomNavBarText(
//                 text: '',
//               ),
//             ),
//             //Shopping Page
//             BottomNavigationBarItem(
//               activeIcon: Icon(
//                 MdiIcons.shoppingOutline,
//                 color: kBottomNavigationBarIconActive,
//               ),
//               icon: Icon(
//                 MdiIcons.shoppingOutline,
//                 color: kBottomNavigationBarIconInActive,
//               ),
//               title: BottomNavBarText(
//                 text: '',
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }

// class BottomNavBarText extends StatelessWidget {
//   final String text;

//   const BottomNavBarText({Key key, this.text}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Text(
//       text,
//       style: TextStyle(color: Colors.black),
//     );
//   }
// }
