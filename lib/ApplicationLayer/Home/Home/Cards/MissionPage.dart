import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Home/AppBar/HomeAppBar.dart';
import 'package:cineprize/ApplicationLayer/Tools/Image/ImageTools.dart';
import 'package:cineprize/Models/User.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MissionPage extends StatefulWidget {
  @override
  _MissionPageState createState() => _MissionPageState();
}

class _MissionPageState extends State<MissionPage> {
  @override
  Widget build(BuildContext context) {
    final User user = Provider.of<UserManagement>(context).user;
    final double width = MediaQuery.of(context).size.width;
    final double height = MediaQuery.of(context).size.height;

    return Container(
      decoration: kMainPagesDecoration,
      child: Scaffold(
        appBar: generalAppBar(title: 'Görevler'),
        backgroundColor: Colors.transparent,
        body: SingleChildScrollView(
          child: Column(
            children: [
              new Container(
                height: height / 4,
                width: width,
                margin: EdgeInsets.only(left: 22, right: 22, top: 22),
                decoration: new BoxDecoration(
                  color: new Color(0xff3cd08c),
                  shape: BoxShape.rectangle,
                  borderRadius: new BorderRadius.circular(8.0),
                ),
                child: Column(
                  children: [
                    // RESİM VE YAZI
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(16),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(8),
                            child: Container(
                              height: width / 4,
                              width: width / 4,
                              child: ImageTools.getImage(imageURL: 'https://www.imago-images.de/imagoextern/bilder/stockfotos/imago-images-aurora-borealis-fotos.jpg'),
                            ),
                          ),
                        ),
                        Flexible(
                          child: Padding(
                            padding: const EdgeInsets.only(top: 16, right: 16),
                            child: Text(
                              'text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text ',
                              style: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.w500),
                              textAlign: TextAlign.center,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 7,
                            ),
                          ),
                        )
                      ],
                    ),
                    // ÖDÜL MİKTARI VE YÖNLENDİRMELİ BUTON
                    Row(
                      children: [],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
