import 'dart:convert';

import 'package:cineprize/ApplicationLayer/Auth/AuthManagement/LoginNavigator.dart';
import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Tools/MyAlertDialogs/MyInfoDialog.dart';
import 'package:cineprize/DataAccessLayer/DataAccessLayerVariable.dart';
import 'package:cineprize/Models/User.dart';
import 'package:cineprize/ServiceLayer/Management/StateManagement.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

Widget buildGainsCard(BuildContext context, int index,
    {double? height, double? width, dynamic image, required String banner}) {
  double width2 = MediaQuery.of(context).size.width;
  return Container(
    color: Colors.transparent,
    child: GestureDetector(
      onTap: () async {
        User user = Provider.of<UserManagement>(context, listen: false).user;

        final url = Uri.parse('$baseApiUrl/ad-watch-permission');
        var response = await http
            .post(url, headers: {'Authorization': 'Bearer ${user.token}'});

        Map<String, dynamic> responseJson = jsonDecode(response.body);

        if (!responseJson['success']) {
          MyInfoDialog(context,
              title: 'Uyarı!',
              description: responseJson['message'],
              buttonText: 'Tamam',
              alertType: AlertType.warning);
        }

        // reklam gösteriliyor
        /*  if (await Appodeal.isReadyForShow(AdType.REWARD)) {
          Appodeal.setRewardCallback((event) async {
            if (event == 'onRewardedVideoClosed') {
              final String url = '$baseApiUrl/ad-watched';

              var response = await http.post(url,
                  headers: {'Authorization': 'Bearer ${user.token}'});

              Map<String, dynamic> responseJson = jsonDecode(response.body);

              if (responseJson['success']) {
                User newUser = User.fromMap(responseJson['data']);

                user.coin = newUser.coin;
                user.gold = newUser.gold;
                user.usedGold = newUser.usedGold;

                StateManagement.include.state = ViewStateEnum.Idle;
                return MyInfoDialog(loginNavigatorContext,
                    title: 'Tebrikler!',
                    description: responseJson['message'],
                    buttonText: 'Tamam',
                    alertType: AlertType.success);
              } else {
                StateManagement.include.state = ViewStateEnum.Idle;
              }
            }
          });
          await Appodeal.show(AdType.REWARD);
        } */
      },
      child: Card(
        elevation: 0,
        color: Colors.transparent,
        child: Column(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(17),
              child: Container(
                height: height,
                width: width == null ? width2 / 2.5 : width,
                decoration: BoxDecoration(
                  image: DecorationImage(image: image, fit: BoxFit.cover),
                  color: Colors.transparent,
                ),
              ),
            ),
            SizedBox(height: 5),
            RichText(
              text: TextSpan(
                text: 'İzle, ',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                    fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(
                      text: 'KAZAN',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: kAppThemeColor)),
                ],
              ),
            )
          ],
        ),
      ),
    ),
  );
}
