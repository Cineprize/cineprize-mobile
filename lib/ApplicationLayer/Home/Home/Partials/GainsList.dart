import 'package:flutter/material.dart';
import 'package:html/dom.dart' as htmlParser;

class GainsList extends StatefulWidget {
  final ScrollController? scrollController;
  final double? height;
  final String barHeading;
  final Function(BuildContext, int) itemBuilder;
  final int itemCount;

  const GainsList(
      {Key? key,
      required this.barHeading,
      required this.itemBuilder,
      required this.itemCount,
      this.height,
      this.scrollController})
      : super(key: key);

  @override
  _GainsListState createState() => _GainsListState();
}

class _GainsListState extends State<GainsList> {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Padding(
      padding: const EdgeInsets.only(left: 15.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          HorizontalGainsListHeading(barHeading: widget.barHeading),
          SizedBox(height: 5),
          LimitedBox(
            maxHeight: widget.height == null ? height / 5.2 : widget.height!,
            child: ListView.builder(
              controller: widget.scrollController == null
                  ? null
                  : widget.scrollController,
              physics: BouncingScrollPhysics(),
              addAutomaticKeepAlives: true,
              scrollDirection: Axis.horizontal,
              itemCount: widget.itemCount,
              itemBuilder: (bContext, index) =>
                  widget.itemBuilder(bContext, index),
            ),
          ),
        ],
      ),
    );
  }
}

class HorizontalGainsListHeading extends StatefulWidget {
  const HorizontalGainsListHeading({
    Key? key,
    required this.barHeading,
  }) : super(key: key);

  final String barHeading;

  @override
  _HorizontalGainsListHeadingState createState() =>
      _HorizontalGainsListHeadingState();
}

class _HorizontalGainsListHeadingState
    extends State<HorizontalGainsListHeading> {
  @override
  Widget build(BuildContext context) {
    return RichText(
        text: TextSpan(children: <TextSpan>[
      TextSpan(
          text: htmlParser.DocumentFragment.html('').text,
          style: TextStyle(color: Color(0xff3fd490), fontSize: 26)),
      TextSpan(
          text: widget.barHeading,
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
    ]));
  }
}
