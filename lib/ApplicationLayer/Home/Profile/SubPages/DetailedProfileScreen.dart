import 'dart:io';

import 'package:cineprize/ApplicationLayer/Auth/Verification/PhoneVerification.dart';
import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Home/AppBar/HomeAppBar.dart';
import 'package:cineprize/ApplicationLayer/Tools/Image/ImageTools.dart';
import 'package:cineprize/ApplicationLayer/Tools/MyAlertDialogs/MyInfoDialog.dart';
import 'package:cineprize/ApplicationLayer/Tools/StateUpdateIndicator/StateUpdateIndicator.dart';
import 'package:cineprize/ApplicationLayer/components/CustomBottomButtonWidget.dart';
import 'package:cineprize/ApplicationLayer/components/DropDownWidget.dart';
import 'package:cineprize/ApplicationLayer/components/RowText.dart';
import 'package:cineprize/Models/User.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Profile/ProfileService.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
/* import 'package:easy_mask/easy_mask.dart'; */
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:image_picker/image_picker.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

extension StringExtension on String {
  String capitalize() {
    return '${this[0].toUpperCase()}${this.substring(1)}';
  }
}

class DetailedProfileScreen extends StatefulWidget {
  @override
  _DetailedProfileScreenState createState() => _DetailedProfileScreenState();
}

class _DetailedProfileScreenState extends State<DetailedProfileScreen> {
  final User user = UserManagement.include.user;
  DateTime birthDate = UserManagement.include.user.birthDate!;
  String? name = UserManagement.include.user.name ?? "",
      lastName = UserManagement.include.user.lastName ?? "",
      gender = UserManagement.include.user.gender ?? "",
      email = UserManagement.include.user.email,
      province = UserManagement.include.user.province ?? "",
      district = UserManagement.include.user.district ?? "",
      address = UserManagement.include.user.address ?? "",
      phoneNumber = UserManagement.include.user.phoneNumber;

  File? _selectedImage;
  final picker = ImagePicker();

  Future getImage() async {
    final PickedFile? pickedFile =
        await picker.getImage(source: ImageSource.gallery);

    if (pickedFile != null) {
      setState(() {
        _selectedImage = File(pickedFile.path);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Container(
      decoration: kMainPagesDecoration,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: generalAppBar(title: 'Profil Bilgileri'),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: StateUpdateIndicator.stateControl(
                context: context,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(height: 15),
                    GestureDetector(
                      //Profile Picture Change Button
                      onTap: () async {
                        await getImage();
                      },
                      child: Stack(
                        alignment: AlignmentDirectional.bottomEnd,
                        children: [
                          _selectedImage != null
                              ? Container(
                                  width: height / 6,
                                  height: height / 6,
                                  decoration: new BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: new DecorationImage(
                                      fit: BoxFit.fill,
                                      image: Image.file(_selectedImage!,
                                              height: height / 6,
                                              fit: BoxFit.cover)
                                          .image,
                                    ),
                                  ),
                                )
                              : ClipOval(
                                  child: SizedBox(
                                    height: height / 6,
                                    width: height / 6,
                                    child: ImageTools.getImage(
                                        imageURL: user.imageURL ?? "",
                                        gifPlaceholder: gifPlaceholder),
                                  ),
                                ),
                          CircleAvatar(
                              child: Icon(CupertinoIcons.camera, size: 20),
                              radius: 20),
                        ],
                      ),
                    ),
                    SizedBox(height: 15),
                    Text(UserManagement.include.user.userName.toString(),
                        style: TextStyle(
                            fontSize: 25,
                            color: Colors.white,
                            fontWeight: FontWeight.bold)),
                    SizedBox(height: 15),
                    RowText(
                      text: 'İsim:',
                      editableText: _customFormField(name ?? "", (_) {
                        this.name = _.trim();
                      }),
                    ),
                    RowText(
                      text: 'Soyisim:',
                      editableText: _customFormField(lastName ?? "", (_) {
                        this.lastName = _.trim();
                      }),
                    ),
                    RowText(
                      text: 'Doğum tarihi:',
                      editableText: ListTile(
                        contentPadding: EdgeInsets.all(0),
                        title: Text(
                          '${birthDate.day < 10 ? '0' + birthDate.day.toString() : birthDate.day.toString()}.${birthDate.month < 10 ? '0' + birthDate.month.toString() : birthDate.month.toString()}.${birthDate.year}',
                          style: kNormalText,
                        ),
                        trailing: Icon(
                          Icons.arrow_drop_down,
                          color: Colors.white,
                          size: 24,
                        ),
                        onTap: _datePicker,
                      ),
                    ),
                    RowText(
                      text: 'Cinsiyet:',
                      editableText: DropdownWidget(
                        hintText: '',
                        title: 'Cinsiyet',
                        items: ['Belirtmek istemiyorum', 'Erkek', 'Kadın'],
                        currentItem: gender ?? 'Belirtmek istemiyorum',
                        itemCallBack: (String status) {
                          this.gender = status.trim();
                        },
                      ),
                    ),
                    RowText(
                        text: 'E-mail:',
                        editableText: _customFormField(email ?? "", (_) {
                          this.email = _.trim();
                        }, isReadOnly: true)),
                    RowText(
                        text: 'İl:',
                        editableText: _customFormField(province ?? "", (_) {
                          this.province = _.trim();
                        })),
                    RowText(
                        text: 'İlçe:',
                        editableText: _customFormField(district ?? "", (_) {
                          this.district = _.trim();
                        })),
                    RowText(
                        text: 'Adres:',
                        editableText: _customFormField(address ?? "", (_) {
                          this.address = _.trim();
                        })),
                    GestureDetector(
                      onTap: () async {
                        var result = await Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => PhoneVerification(),
                          ),
                        );
                        if (result['success']) {
                          setState(() {
                            phoneNumber = result['phone'];
                          });
                          MyInfoDialog(context,
                              title: 'Başarılı!',
                              description:
                                  "Telefon numaranız başarıyla doğrulandı!",
                              buttonText: 'Tamam',
                              alertType: AlertType.success);
                        }
                      },
                      child: RowText(
                        text: 'Telefon:',
                        editableText: Container(
                          child: phoneNumber != null
                              ? Text(
                                  phoneNumber!.replaceAllMapped(
                                      RegExp(r'(\d{3})(\d{3})(\d{2})(\d{2})'),
                                      (Match m) =>
                                          "(${m[1]}) ${m[2]} ${m[3]} ${m[4]}"),
                                  style: kNormalText,
                                )
                              : Text(
                                  "0 (5XX) XXX XX XX",
                                  style: kHintText,
                                ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 15.0),
                      child: CustomBottomButtonWidget(
                        text: 'Güncelle',
                        onTap: () async {
                          var response = await ProfileService.include.update(
                            data: {
                              'name':
                                  this.name != "" ? this.name.toString() : '',
                              'lastName': this.lastName != ""
                                  ? this.lastName.toString()
                                  : '',
                              'birthDate': this.birthDate != ""
                                  ? this.birthDate.toString()
                                  : '',
                              'gender': this.gender != ""
                                  ? this.gender.toString()
                                  : '',
                              //'email': this.email,
                              'province': this.province != ""
                                  ? this.province.toString()
                                  : '',
                              'district': this.district != ""
                                  ? this.district.toString()
                                  : '',
                              'address': this.address != ""
                                  ? this.address.toString()
                                  : '',

                              'imagePath': this._selectedImage != null
                                  ? this._selectedImage?.path.toString()
                                  : null,
                            },
                          );
                          print(response.toString());
                          if (response.errorMessage != null) {
                            _onAlertButtonsPressed(
                                context,
                                response.errorMessage!,
                                'Başarısız',
                                AlertType.error);
                            return false;
                          } else {
                            _onAlertButtonsPressed(
                                context,
                                'Güncelleme işlemi başarılı!',
                                'Başarılı',
                                AlertType.success);
                            return true;
                          }
                        },
                        color: Color(0xff3fd490),
                        width: double.infinity,
                      ),
                    ),
                  ],
                )),
          ),
        ),
      ),
    );
  }

  _datePicker() async {
    DateTime now = DateTime.now();
    await DatePicker.showDatePicker(
      context,
      showTitleActions: true,
      minTime: DateTime(1900, 1, 1),
      maxTime: DateTime(now.year - 17, now.month, now.day),
      onConfirm: (date) {
        if (date != null)
          setState(() {
            birthDate = date;
          });
      },
      currentTime: birthDate == null ? DateTime.now() : birthDate,
      locale: LocaleType.tr,
    );
  }

  _onAlertButtonsPressed(
      context, String response, String title, AlertType alertType) {
    return Alert(
      context: context,
      style: AlertStyle(
        backgroundColor: kPopUpBackgroundColor,
        animationType: AnimationType.grow,
        isCloseButton: false,
        isOverlayTapDismiss: false,
        descStyle: TextStyle(
          fontWeight: FontWeight.bold,
          color: Colors.white,
        ),
        descTextAlign: TextAlign.center,
        animationDuration: Duration(milliseconds: 400),
        titleStyle: TextStyle(
            fontWeight: FontWeight.bold, color: Colors.white, fontSize: 25),
        alertBorder: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
      ),
      buttons: [
        DialogButton(
          child: Text('Tamam',
              style: TextStyle(color: Colors.white, fontSize: 18)),
          onPressed: () async {
            Navigator.of(context, rootNavigator: true).pop(true);
          },
          color: Color.fromRGBO(0, 179, 134, 1.0),
        ),
      ],
      type: alertType,
      title: title,
      desc: response,
    ).show();
  }
}

Widget _customFormField(String value, Function(String) onChanged,
    {bool? isReadOnly, bool? isPhoneNumber}) {
  return TextFormField(
    //TODO:: Mask
    inputFormatters: isPhoneNumber == null
        ? null
        : /* [TextInputMask(mask: '(999) 999 99 99')] */ null,
    readOnly: isReadOnly == null ? false : isReadOnly,
    initialValue: value == null ? '' : value,
    style: kNormalText,
    decoration: InputDecoration(
        counterStyle: TextStyle(color: Colors.white),
        hintStyle: kHintText,
        border: InputBorder.none,
        errorMaxLines: 1),
    onChanged: (_) => onChanged(_),
  );
}
