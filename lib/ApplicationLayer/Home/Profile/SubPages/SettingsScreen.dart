import 'package:cineprize/ApplicationLayer/Auth/AuthManagement/LoginNavigator.dart';
import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Home/AppBar/HomeAppBar.dart';
import 'package:cineprize/ApplicationLayer/Home/Bodies/ProfileBody.dart';
import 'package:cineprize/ApplicationLayer/Home/Profile/SubPages/PrivacyScreen.dart';
import 'package:cineprize/ApplicationLayer/Home/Profile/SubPages/UserAgreementScreen.dart';
import 'package:cineprize/ApplicationLayer/Home/Profile/SubPages/sssScreen.dart';
import 'package:cineprize/ServiceLayer/Auth/Rest/AuthService.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Profile/ProfileService.dart';
import 'package:cineprize/ServiceLayer/Management/StateManagement.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class SettingsScreen extends StatefulWidget {
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  bool onProccess = false;
  late bool canEarn;

  @override
  void initState() {
    setState(() {
      canEarn = UserManagement.include.user.canEarn;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: kMainPagesDecoration,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: generalAppBar(title: 'Ayarlar'),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15.0),
          child: Column(
            children: [
              // ProfileElement(
              //   text: 'Bildirim Ayarları',
              //   routeTo: yapimAsamasiWidget(),
              //   isLocked: true,
              // ),
              // ProfileElement(
              //   text: 'Şifremi Değiştir',
              //   routeTo: yapimAsamasiWidget(),
              //   isLocked: true,
              // ),
              ProfileElement(
                  text: 'Gizlilik', routeTo: PrivacyScreen(), isLocked: false),

              ProfileElement(
                  text: 'S.S.S.', routeTo: sssScreen(), isLocked: false),
              ProfileElement(
                  text: 'Kullanıcı Sözleşmesi',
                  routeTo: UserAgreementScreen(),
                  isLocked: false),
              RawMaterialButton(
                hoverColor: Colors.transparent,
                highlightColor: Colors.transparent,
                splashColor: Colors.transparent,
                onPressed: () {},
                child: Column(
                  children: [
                    SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Hediye kazanmak ister misiniz ?',
                            style: kBigText),
                        onProccess
                            ? loadingIndicator
                            : CupertinoSwitch(
                                value: UserManagement.include.user.canEarn,
                                onChanged: (value) async {
                                  if (onProccess) return;
                                  setState(() {
                                    onProccess = true;
                                  });

                                  var response = await ProfileService.include
                                      .update(data: {
                                    'canEarn': value ? '1' : '0',
                                  }, isSetState: false);
                                  UserManagement.include.user.canEarn = value;
                                  setState(() {
                                    onProccess = false;
                                  });

                                  Navigator.of(context).pop(true);
                                  StateManagement.include.state =
                                      ViewStateEnum.Busy;

                                  Future.delayed(new Duration(seconds: 1), () {
                                    StateManagement.include.state =
                                        ViewStateEnum.Idle;
                                    if (response.errorMessage == '') {
                                      _onAlertButtonsPressed(
                                          loginNavigatorContext,
                                          'İşlem Başarılı!',
                                          'Başarılı',
                                          AlertType.success);
                                    } else {
                                      _onAlertButtonsPressed(
                                          loginNavigatorContext,
                                          response.errorMessage!,
                                          'Başarısız',
                                          AlertType.error);
                                    }
                                  });
                                },
                              )
                      ],
                    ),
                    SizedBox(height: 10),
                    Divider(color: Colors.white24),
                  ],
                ),
              ),
              RawMaterialButton(
                hoverColor: Colors.transparent,
                highlightColor: Colors.transparent,
                splashColor: Colors.transparent,
                onPressed: () async {
                  bool result = await _onAlertButtonsPressedForLogout(
                      loginNavigatorContext);

                  if (result) {
                    Navigator.pop(context);
                    await AuthService.include.logout();
                  }
                },
                child: Column(
                  children: [
                    SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Çıkış Yap', style: kBigText),
                        Icon(Icons.keyboard_arrow_right, size: 25)
                      ],
                    ),
                    SizedBox(height: 10),
                    Divider(color: Colors.white24),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _onAlertButtonsPressed(
      context, String response, String title, AlertType alertType) {
    return Alert(
      context: context,
      style: AlertStyle(
        backgroundColor: kPopUpBackgroundColor,
        animationType: AnimationType.grow,
        isCloseButton: false,
        isOverlayTapDismiss: false,
        descStyle: TextStyle(
          fontWeight: FontWeight.bold,
          color: Colors.white,
        ),
        descTextAlign: TextAlign.center,
        animationDuration: Duration(milliseconds: 400),
        titleStyle: TextStyle(
            fontWeight: FontWeight.bold, color: Colors.white, fontSize: 25),
        alertBorder: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
      ),
      buttons: [
        DialogButton(
          child: Text(
            'Tamam',
            style: TextStyle(color: Colors.white, fontSize: 18),
          ),
          onPressed: () async {
            Navigator.of(context).pop(true);
          },
          color: Color.fromRGBO(0, 179, 134, 1.0),
        ),
      ],
      type: alertType,
      title: title,
      desc: response,
    ).show();
  }
}

// TODO : alert değişecek
_onAlertButtonsPressedForLogout(context) {
  return Alert(
    context: context,
    style: AlertStyle(
      backgroundColor: kPopUpBackgroundColor,
      animationType: AnimationType.grow,
      isCloseButton: false,
      isOverlayTapDismiss: false,
      descStyle: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
      descTextAlign: TextAlign.center,
      animationDuration: Duration(milliseconds: 400),
      titleStyle: TextStyle(
          fontWeight: FontWeight.bold, color: Colors.white, fontSize: 25),
      alertBorder:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
    ),
    type: AlertType.warning,
    title: 'Onay',
    desc: 'Çıkış yapmak istediğinizden emin misiniz?',
    buttons: [
      DialogButton(
        child:
            Text('Evet', style: TextStyle(color: Colors.white, fontSize: 18)),
        color: Color.fromRGBO(0, 179, 134, 1.0),
        onPressed: () async => {Navigator.of(context).pop(true)},
      ),
      DialogButton(
          child: Text('Hayır',
              style: TextStyle(color: Colors.white, fontSize: 18)),
          onPressed: () => Navigator.of(context).pop(false),
          color: Colors.redAccent),
    ],
  ).show();
}
