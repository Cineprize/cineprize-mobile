import 'dart:io';

import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Home/AppBar/HomeAppBar.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';

class sssScreen extends StatefulWidget {
  @override
  _sssScreenState createState() => _sssScreenState();
}

class _sssScreenState extends State<sssScreen> {
  List<Map<String, String>> content = [
    {
      'question': 'CinePrize nedir?',
      'response':
          'CinePrize sizi interaktif içeriklerimize dahil ederek, izlerken hem eğlendiren hem de yaptığınız tüm eylemler karşılığında sizlere CineCoin ve CineGold vererek bu kazandıklarınız ile mağazadan istediğinizi almanıza fırsat tanıyan bir video platformudur.'
    },
    {
      'question': 'Nasıl CineCoin ve CineGold kazanabilirim?',
      'response':
          'İnteraktif videolarımızı izlerken gelen sorulara doğru yanıt vererek kazanabilirsiniz( yanlış yanıt asla eksiltmez). İzlediğiniz her bir videonun her dakikası için kazanırsınız. Anasayfa da yer alan ‘’ Ekstra Kazançlar ‘’ kategorisinden izlediğiniz her reklamdan kazanabilirsiniz. Arkadaşlarınızı uygulamaya referans kodunuz ile davet ederek kazanabilirsiniz. Bizim sosyal medya hesaplarımızı takip ederek kazanabilirsiniz.'
    },
    {
      'question': 'Günde en fazla kaç reklam izleyebilirim?',
      'response':
          'Günde en fazla 15 reklam izleyebilirsiniz ancak arka arkaya en fazla 5 reklam izlenebilir. Her 5 reklamda 1saat beklemeniz gerekmektedir diğer 5 reklamı. Yani toplamda en hızlı 3 saat içerisinde 15 reklam izleyebilirsiniz 1 gün içerisinde.'
    },
    {
      'question': 'Reklamlardan ne kadar CineCoin ve CineGold kazanabilirim?',
      'response':
          'İzlediğiniz her reklamdan 30 CineCoin ve 30 CineGold kazanırsınız. Yani günde sadece reklamlardan bile 450 CineCoin ve 450 CineGold kazanabilirsiniz.'
    },
    {
      'question': 'Videolardan ne kadar CineCoin ve CineGold kazanabilirim?',
      'response':
          'İzlediğiniz her içeriğimizin her 1 dakikası için 20 CineCoin ve 20 CineGold kazanırsınız.'
    },
    {
      'question':
          'Videolardaki sorulara doğru yanıt vererek ne kadar kazanabilirim?',
      'response':
          'Videolarda verilmiş her doğru yanıt için 50 CineCoin ve 50 CineGold kazanırsınız.'
    },
    {
      'question':
          'Referans kodum ile kaç kişi çağırabilirim ve ne kadar kazanırım?',
      'response':
          'Referans kodu ile en fazla 5 arkadaşınızı davet edebilirsiniz. Referans kodunuz ile gelen her arkadaşınız için 200 CineCoin ve 200 CineCoin kazanırsınız' +
              (Platform.isAndroid ? '(hem siz hem arkadaşınız)' : '') +
              '. Toplamda 1.000 CineCoin ve 1.000 CineGold kazanabilirsiniz.'
    },
    {
      'question': 'Arkadaşımı nasıl davet edebilirim?',
      'response':
          'Uygulama içerisinde ‘’profil’’ alanında ‘’Davet Et, Kazan’’ a tıklayarak Referans kodunuzu arkadaşınızla paylaşınız. Arkadaşınız sizin referans kodunuzu üye olurken girdiği taktirde CineCoin ve CineGoldları' +
              (Platform.isAndroid ? 'hem siz hem arkadaşınız' : '') +
              ' kazanırsınız.'
    },
    {
      'question': 'CoinCoin ve CineGold ne işe yarar?',
      'response':
          'CineCoin: Kazandığınız Coinleri mağazada ‘’aylık’’ sekmesindeki ödüllerle dilediğiniz şekilde harcayabilirsiniz.\n\nCineGold: Kazandığınız CineGoldları mağazada ‘’Sürpriz’’ sekmesinde ki ödüllere teklif vererek açık artırma usulü ile sahip olabilirsiniz. Her en büyük teklif verenin ismi ürün penceresinde belirir ve ansızın geri sayım başlar ve beliren geri sayım süresi sonunda ürün en çok teklifi verene gider.'
    },
    {
      'question': 'CineCoin ve CineGold ne kadar süreyle yenilenir?',
      'response':
          'CineCoinleriniz her ay sıfırlanır ve yeni bir ödül sezonu başlar. CineGoldlarınız her yıl sıfırlanır ve yeni bir ödül yılı sezonu başlar.'
    },
    {
      'question': 'Çok fazla ödül var ama yine de ödüller artacak mı?',
      'response':
          'Ödüllerimizin sürekli daha da fazla artış gösterecektir. Yeni gelecek olan güncellemeler ile hedefimiz bütün kullanıcılarımıza ödüller vermek herkesin kazanması.'
    },
    {
      'question': 'Mağazadan ödül nasıl alabilirim?',
      'response':
          'Çok basit! Eğer CineCoinleriniz yetiyorsa mağazadan dilediğiniz ödülü hemen alabilirsiniz.'
    },
    {
      'question': 'Mağazadan aldığım ürün bana nasıl ulaşacak?',
      'response':
          'Mağazadan ürün hediye seçtikten sonra, Profilinizden ‘’sohbet’’e tıklayarak bizimle iletişime geçebilirsiniz. (ödülün size doğruca ulaşması için, Profilinizdeki ‘’isim-soy isim-adres-eposta-tel no’’ bilgilerinin doğru olması gerekmektedir)'
    },
    {
      'question': 'Mağazadan neler alabilirim?',
      'response':
          'Mağazadan 20’den fazla kategoride yüzlerce farklı ürün alabilirsiniz. (Örnek; teknolojik aletler, pc ekipmanları, konsollar, gamer ürünleri, seyahatler, uçak biletleri, gezi turları, giyim kuşam, kozmetik ürünler, kitaplar, eğitim setleri ve daha bir çok kategori..)'
    },
  ];

  List<Map<String, String>> contentForNoEarn = [
    {
      'question': 'CinePrize nedir?',
      'response':
          'CinePrize sizi interaktif içeriklerimize dahil ederek, izlerken hem eğlendiren bir video platformudur.'
    },
    {
      'question': 'Arkadaşımı nasıl davet edebilirim?',
      'response':
          'Uygulama içerisinde ‘’profil’’ alanında ‘’Arkadaşlarını Davet Et’’ e tıklayarak Referans kodunuzu arkadaşınızla paylaşınız.'
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: kMainPagesDecoration,
      child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: generalAppBar(title: 'S.S.S'),
          body: SingleChildScrollView(child: getSSS())),
    );
  }

  Column getSSS() {
    List<Widget> elements = [];
    for (Map<String, String> data in (UserManagement.include.user.canEarn
        ? this.content
        : this.contentForNoEarn)) {
      elements.add(SSSCards(
          heading: data['question'] as String,
          content: data['response'] as String));
    }

    return Column(children: elements);
  }
}

class SSSCards extends StatefulWidget {
  final String heading;
  final String content;

  SSSCards({required this.heading, required this.content});

  @override
  _SSSCardsState createState() => _SSSCardsState();
}

class _SSSCardsState extends State<SSSCards> {
  @override
  Widget build(BuildContext context) {
    buildList() {
      return Container(
        width: MediaQuery.of(context).size.width,
        color: kAppBlack,
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: [
              Text(this.widget.content,
                  style: TextStyle(fontSize: 15, color: Colors.grey[500]),
                  textAlign: TextAlign.center)
            ],
          ),
        ),
      );
    }

    return ExpandableNotifier(
        child: Padding(
      padding: const EdgeInsets.only(top: 4, bottom: 4, right: 8, left: 8),
      child: ScrollOnExpand(
        child: Card(
          color: Color(0xff4cd596),
          clipBehavior: Clip.antiAlias,
          child: Column(
            children: <Widget>[
              ExpandablePanel(
                //TODO :: check collaps widget
                collapsed: Container(),
                theme: const ExpandableThemeData(
                    headerAlignment: ExpandablePanelHeaderAlignment.center,
                    tapBodyToExpand: true,
                    tapBodyToCollapse: true,
                    hasIcon: true,
                    iconColor: Colors.white),
                header: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Row(children: [
                      Expanded(
                          flex: 1, child: Text(widget.heading, style: kBigText))
                    ])),
                expanded: buildList(),
              ),
            ],
          ),
        ),
      ),
    ));
  }
}
