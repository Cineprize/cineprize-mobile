import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Home/AppBar/HomeAppBar.dart';
import 'package:flutter/material.dart';

class PrivacyScreen extends StatefulWidget {
  @override
  _PrivacyScreenState createState() => _PrivacyScreenState();
}

class _PrivacyScreenState extends State<PrivacyScreen> {
  final String pdfText = '''
GİZLİLİK SÖZLEŞMESİ
 
CinePrize uygulamasına üye olarak CinePrize hizmetlerini kullanmanın koşullarından biri olarak bazı bilgileriniz CinePrize tarafından alınmakta, işlenmekte ve saklanmaktadır. CinePrize gizliliğinizi aşağıda yer alan kurallar çerçevesinde korumaktadır.
 
CinePrize verdiğiniz bilgilerin bazıları zorunlu olarak verilen (e-posta, IP bilgileri gibi) bilgiler, bazıları da tercihinize bağlı olarak bize verdiğiniz veya erişmemize onay verdiğiniz bilgilerdir. CinePrize, verdiğiniz bu bilgiler ile varsa sağladığınız içerikleri dilediği sürece, bilgiler ve içerikler tarafınızdan silinse dahi saklayabilir.
 
CinePrize, e-posta adresi ve diğer iletişim bilgilerinizi hesabınızla ilgili işlem yapmak, gerektiğinde sizinle iletişime geçilmesini sağlamak, bilgilendirme yapmak için kullanmaktadır. CinePrize, verdiğiniz bilgileri ve varsa sağladığınız içerikleri kendine veya başkalarına ait reklam, tanıtım, bildiri, duyurular için kullanabilir. CinePrize, düzenlediği ödüllü yarışma veya çekilişlere katılmanız
durumunda tarafınıza ait bilgileri gerekli mercilerle paylaşabilir, yarışma veya çekiliş sonuçlarını açıklarken kullanabilir. Bilgilerin anonimleştirilmek şartıyla tarafımızca kullanılması veya üçüncü kişilerle paylaşılması da mümkündür. CinePrize üyeliği gizlenemediğinden, üye olduğunuz bilgisi kamuya açık olacaktır.
 
CinePrize, bilgilerinizi saklama konusunda azami özeni göstermekle beraber, CinePrize tarafından saklanan verilerinizin yer aldığı sisteme izinsiz girilmesi, sistemin işleyişinin bozulması veya değiştirilmesi suretiyle bilgilerinizin elde edilmesi, değiştirilmesi veya silinmesi halinde CinePrize'ın sorumluluğu bulunmamaktadır. Siteyi her ziyaret ettiğinizde, IP adresiniz, işletim sisteminiz, kullandığınız cihaz (android, iOS vs.), bağlantı zamanı ve süre bilgileriniz ve benzeri bilgiler otomatik olarak kaydedilmekte olup, izniniz gerekmeksizin elde edilen bu bilgilerinizin 3. şahıslarla paylaşılmamak kaydıyla CinePrize tarafından kişisel bilgilerinizle ilişkilendirerek veya anonim olarak kullanılması mümkündür.
Resmi mercilerden usulüne uygun talep gelmesi halinde bilgileriniz ilgili mercilere iletilecektir. Uygulamada bulunduğunuz süre boyunca “cookie” olarak da adlandırılan çerezlerin veya site kullanım verilerini analiz etme amaçlı javascript kodlarının veya benzer iz sürme verilerinin mobil cihazınıza ya da tablet cihazınıza yerleştirilmesi söz konusu olabilir. Çerezler basit metin dosyalarından ibaret olup, kimlik ve sair özel bilgiler içermez, bu nevi kişisel bilgi içermemekle beraber, oturum bilgileri ve benzeri veriler saklanır ve sizi tekrar tanımak için kullanılabilir.
 
CinePrize zaman zaman uygulama içine, tanıtım içeriklerine veya reklamlarına üçüncü şahıslara ait internet sitelerine ilişkin bilgi ve linkleri dahil edebilir. Bu linklere tıklayarak diğer internet sitelerine girdiğinizde, söz konusu sitelerinin ya da bu sitelerin uygulamaları kontrolümüz altında olmadığı gibi, bu gizlilik politikası erişebildiğiniz bu diğer siteler bakımından geçerli değildir.
 
Bu sözleşme, uygulamaya yeni özellikler eklendikçe veya kullanıcılarımızdan yeni öneriler geldikçe yeniden, önceden tarafınıza herhangi bir bildirimde bulunulmaksızın düzenlenebilir ve güncellenebilir. Bu nedenle, CinePrize uygulamasını her ziyaret edişinizde gizlilik politikasını yeniden gözden geçirmenizi öneririz. Bu belge en son 15/12/2020 tarihinde güncellenmiştir.
 
CinePrize uygulamasını kullanarak, Gizlilik Politikası’nı kabul ettiğinizi ifade etmiş olursunuz. Siteyi ziyaret ederken ve/veya üye (kayıtlı kullanıcı) olurken dikkat etmeniz gereken hususlar hakkında Kullanıcı Sözleşmesi’ni incelemenizi öneririz.
 
R&D Apps A.Ş
İstanbul / Türkiye
E-Posta: hello@cineprize.com

''';

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: kMainPagesDecoration,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: generalAppBar(title: 'Gizlilik'),
        body: SingleChildScrollView(child: Padding(padding: const EdgeInsets.all(8.0), child: Text(pdfText, style: TextStyle(color: Color(0xff6b6c6b))))),
      ),
    );
  }
}
