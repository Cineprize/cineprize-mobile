import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Home/AppBar/HomeAppBar.dart';
import 'package:cineprize/ApplicationLayer/components/CustomBottomButtonWidget.dart';
import 'package:cineprize/ApplicationLayer/components/RowText.dart';
import 'package:flutter/material.dart';

class AppealScreen extends StatefulWidget {
  @override
  _AppealScreenState createState() => _AppealScreenState();
}

class _AppealScreenState extends State<AppealScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: kMainPagesDecoration,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: generalAppBar(title: 'Başvuru'),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SpacedNormalText(text: 'Kanal Adı'),
                SpacedNormalText(text: 'Abone Sayısı'),
                SpacedNormalText(text: 'Ortalama İzleyici Sayısı'),
                SpacedNormalText(text: 'Kanal Kategorisi'),
                SizedBox(height: 50),
                CustomBottomButtonWidget(
                  text: 'Başvuruyu Gönder',
                  onTap: () {},
                  width: double.infinity,
                  color: Color(0xff3fd490),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class SpacedNormalText extends StatelessWidget {
  final String text;

  const SpacedNormalText({Key? key, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
            padding: const EdgeInsets.symmetric(vertical: 15.0),
            child: NormalText(text: text)),
        Divider(color: Colors.white24)
      ],
    );
  }
}

class IconlessTextWidget extends StatelessWidget {
  final String hintText;

  const IconlessTextWidget({
    Key? key,
    required this.hintText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      child: Container(
        padding: EdgeInsets.only(left: 15),
        decoration: BoxDecoration(
            color: Colors.grey[800], borderRadius: BorderRadius.circular(30)),
        child: Row(
          children: [
            Flexible(
              child: Padding(
                padding: const EdgeInsets.only(left: 10.0),
                child: TextFormField(
                  style: kNormalText,
                  textAlign: TextAlign.start,
                  decoration: InputDecoration(
                      counterStyle: TextStyle(color: Colors.white),
                      hintText: hintText,
                      hintStyle: kHintText,
                      border: InputBorder.none),
                  onChanged: (input) {},
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
