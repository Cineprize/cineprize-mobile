import 'package:cineprize/ApplicationLayer/Auth/AuthManagement/LoginNavigator.dart';
import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Tools/StateUpdateIndicator/StateUpdateIndicator.dart';
import 'package:cineprize/Models/User.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/FollowWin/FollowWinService.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class ExpandableCards extends StatefulWidget {
  final String heading;
  final String content;
  final String url;
  final String type;
  final AssetImage buttonImage;
  final User authUser;

  ExpandableCards(
      {required this.heading,
      required this.content,
      required this.buttonImage,
      required this.url,
      required this.type,
      required this.authUser});

  @override
  _ExpandableCardsState createState() => _ExpandableCardsState();
}

class _ExpandableCardsState extends State<ExpandableCards> {
  @override
  Widget build(BuildContext context) {
    Map<String, dynamic> status = {
      'instagram': widget.authUser.instagramFollow,
      'twitter': widget.authUser.twitterFollow,
      'telegram': widget.authUser.telegramFollow,
    };

    buildList() {
      return Container(
        color: kAppBlack,
        width: MediaQuery.of(context).size.width,
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: [
              Text(this.widget.content,
                  style: TextStyle(fontSize: 15, color: Colors.grey[500]),
                  textAlign: TextAlign.center),
              SizedBox(height: 20),
              RawMaterialButton(
                onPressed: () async {
                  final String type = this.widget.type;
                  await FollowWinService.include
                      .follow(type: type, authUser: this.widget.authUser);

                  await _launchURL(this.widget.url);
                  setState(() {});
                },
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Image(
                        image: widget.buttonImage,
                        fit: BoxFit.fitHeight,
                        height: 50)),
              ),
            ],
          ),
        ),
      );
    }

    return ExpandableNotifier(
        child: Padding(
      padding: const EdgeInsets.all(10),
      child: ScrollOnExpand(
        child: Card(
          color: status[widget.type] ? Colors.grey : Color(0xff4cd596),
          clipBehavior: Clip.antiAlias,
          child: Column(
            children: <Widget>[
              ExpandablePanel(
                //TODO :: Check Collapsed Widget
                collapsed: Container(),
                theme: const ExpandableThemeData(
                  headerAlignment: ExpandablePanelHeaderAlignment.center,
                  tapBodyToExpand: true,
                  tapBodyToCollapse: true,
                  hasIcon: true,
                  iconColor: Colors.white,
                ),
                header: Container(
                    child: Padding(
                  padding: const EdgeInsets.all(25.0),
                  child: Row(children: [
                    Expanded(
                        flex: 50, child: Text(widget.heading, style: kBigText)),
                    Expanded(
                        child: StateUpdateIndicator.stateControl(
                            context: loginNavigatorContext!,
                            child: status[widget.type]
                                ? Icon(CupertinoIcons.check_mark)
                                : Container())),
                  ]),
                )),
                expanded: buildList(),
              ),
            ],
          ),
        ),
      ),
    ));
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'İstek başarısız! $url';
    }
  }
}
