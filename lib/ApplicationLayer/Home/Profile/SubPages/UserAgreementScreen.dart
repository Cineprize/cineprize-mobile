import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Home/AppBar/HomeAppBar.dart';
import 'package:flutter/material.dart';

class UserAgreementScreen extends StatefulWidget {
  @override
  _UserAgreementScreenState createState() => _UserAgreementScreenState();
}

class _UserAgreementScreenState extends State<UserAgreementScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: kMainPagesDecoration,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: generalAppBar(title: 'Kullanıcı Sözleşmesi'),
        body: SingleChildScrollView(child: Padding(padding: const EdgeInsets.all(8.0), child: Text(userAgreement, style: TextStyle(color: Color(0xff6b6c6b))))),
      ),
    );
  }
}
