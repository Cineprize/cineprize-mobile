import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Home/AppBar/HomeAppBar.dart';
import 'package:cineprize/ApplicationLayer/Tools/Image/ImageTools.dart';
import 'package:cineprize/Models/Sale.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Shop/SaleReadService.dart';
import 'package:flutter/material.dart';

class SalesScreen extends StatefulWidget {
  @override
  _SalesScreenState createState() => _SalesScreenState();
}

class _SalesScreenState extends State<SalesScreen> {
  late Future<Map<String, dynamic>> _salesFuture;

  @override
  void initState() {
    _salesFuture = SaleReadService.include.read();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: kMainPagesDecoration,
      child: Scaffold(
        appBar: generalAppBar(title: 'Satın Alımlarım'),
        backgroundColor: Colors.transparent,
        body: FutureBuilder<Map<String, dynamic>>(
          initialData: null,
          future: this._salesFuture,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            List<Widget> elements = [];
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(child: CircularProgressIndicator());
            } else {
              bool success = snapshot.data['success'];

              String message = snapshot.data['message'] as String;

              if (!success)
                return Center(
                    child:
                        Text(message, style: TextStyle(color: Colors.white)));

              int i = 0;
              (snapshot.data['data'] as List<Sale>).forEach((sale) {
                DateTime date = sale.created_at;
                if (i != 0) elements.add(Divider(color: Colors.white24));
                elements.add(ListTile(
                  leading: Container(
                      height: MediaQuery.of(context).size.width * 0.2,
                      width: MediaQuery.of(context).size.width * 0.2,
                      child:
                          ImageTools.getImage(imageURL: sale.product.imageURL)),
                  title: Text('${sale.product.name}',
                      style: TextStyle(color: Colors.white, fontSize: 14)),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Fiyat: ${sale.price} CineCoin',
                          style: TextStyle(color: Colors.white, fontSize: 11)),
                      Text('Sipariş Kodu: ${sale.orderCode}',
                          style: TextStyle(color: Colors.white, fontSize: 11)),
                    ],
                  ),
                  trailing: Text('${date.day}/${date.month}/${date.year}',
                      style: TextStyle(color: Colors.white, fontSize: 11)),
                ));
                i++;
              });

              return ListView(
                  scrollDirection: Axis.vertical, children: elements);
            }
          },
        ),
      ),
    );
  }
}
