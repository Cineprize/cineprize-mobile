import 'package:cineprize/ApplicationLayer/Components/CustomBottomButtonWidget.dart';
import 'package:cineprize/ApplicationLayer/Components/CustomTextFieldWithoutIcon.dart';
import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Home/AppBar/HomeAppBar.dart';
import 'package:cineprize/ApplicationLayer/Tools/MyAlertDialogs/MyInfoDialog.dart';
import 'package:cineprize/Models/User.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Coupon/EnterCouponService.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class CouponScreen extends StatefulWidget {
  @override
  _CouponScreenState createState() => _CouponScreenState();
}

class _CouponScreenState extends State<CouponScreen> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  String code = '';

  @override
  Widget build(BuildContext context) {
    final User user = Provider.of<UserManagement>(context).user;

    return Container(
      decoration: kMainPagesDecoration,
      child: Scaffold(
        appBar: generalAppBar(title: 'Kupon'),
        backgroundColor: Colors.transparent,
        body: SingleChildScrollView(
          reverse: true,
          child: Column(
            children: [
              Form(
                key: this.formKey,
                child: Align(
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: CustomTextFieldWithoutIcon(
                          initialValue: this.code.toString(),
                          textInputType: TextInputType.text,
                          hintText: 'Kupon kodu giriniz.',
                          onSaved: (_) {
                            this.code = _.trim();
                          },
                          validator: (__) {
                            String _ = __.trim();
                            if (_.length == 0) {
                              return 'Kupon kodu boş geçilemez.';
                            } else if (_.length != 6) {
                              return 'Kupon kodu 6 haneli olmalıdır.';
                            }
                            return null;
                          },
                        ),
                      ),
                      SizedBox(height: 10),
                      Center(
                        child: CustomBottomButtonWidget(
                          text: 'Gönder',
                          color: Color(0xff33a346),
                          width: 200,
                          onTap: () async {
                            if (this.formKey.currentState!.validate()) {
                              this.formKey.currentState!.save();

                              Map<String, dynamic> result =
                                  await EnterCouponService.include
                                      .enter(code: this.code, authUser: user);

                              if (result['success']) {
                                MyInfoDialog(context,
                                    title: 'Başarılı!',
                                    description: result['message'],
                                    buttonText: 'Tamam',
                                    alertType: AlertType.success);
                              } else {
                                MyInfoDialog(context,
                                    title: 'Uyarı!',
                                    description: result['message'].toString(),
                                    buttonText: 'Tamam',
                                    alertType: AlertType.warning);
                              }

                              this.code = '';
                              setState(() {});
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
