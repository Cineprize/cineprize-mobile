import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Home/AppBar/HomeAppBar.dart';
import 'package:cineprize/ApplicationLayer/Home/Profile/SubPages/ExpandableCards.dart';
import 'package:cineprize/Models/User.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

final List<String> contents = ['Bizim instagram hesabımızı takip edin', 'Bizim Twitter hesabımızı takip edin', 'Bizim Telegram grubumuza katılın'];

final List<AssetImage> buttonImages = [
  AssetImage('assets/images/SocialMediaLayers/Instagram.png'),
  AssetImage('assets/images/SocialMediaLayers/Twitter.png'),
  AssetImage('assets/images/SocialMediaLayers/Telegram.png'),
];

class Gains extends StatefulWidget {
  @override
  _GainsState createState() => _GainsState();
}

class _GainsState extends State<Gains> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final User user = Provider.of<UserManagement>(context).user;

    return Container(
      decoration: kMainPagesDecoration,
      child: Scaffold(
        appBar: generalAppBar(title: 'Kazançlar'),
        backgroundColor: Colors.transparent,
        body: SingleChildScrollView(
          child: Column(
            children: [
              ExpandableCards(
                  heading: 'Instagram\'da Takip Et' + (user.canEarn ? ' Kazan' : ''),
                  content: contents[0] + (user.canEarn ? ' ve anında 100 CineCoin ve 100 CineGold kazanın.' : ''),
                  buttonImage: buttonImages[0],
                  url: 'https://www.instagram.com/cineprize/',
                  type: 'instagram',
                  authUser: user),
              ExpandableCards(
                  heading: 'Twitter\'da Takip Et' + (user.canEarn ? ' Kazan' : ''),
                  content: contents[1] + (user.canEarn ? ' ve anında 100 CineCoin ve 100 CineGold kazanın.' : ''),
                  buttonImage: buttonImages[1],
                  url: 'https://twitter.com/cineprize',
                  type: 'twitter',
                  authUser: user),
              ExpandableCards(
                  heading: 'Telegram Grubuna Katıl' + (user.canEarn ? ' Kazan' : ''),
                  content: contents[2] + (user.canEarn ? ' ve anında 100 CineCoin ve 100 CineGold kazanın.' : ''),
                  buttonImage: buttonImages[2],
                  url: 'https://t.me/cineprize',
                  type: 'telegram',
                  authUser: user),
            ],
          ),
        ),
      ),
    );
  }
}
