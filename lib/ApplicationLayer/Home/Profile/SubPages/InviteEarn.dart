import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Home/AppBar/HomeAppBar.dart';
import 'package:cineprize/Models/User.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';

class InviteEarn extends StatefulWidget {
  @override
  _InviteEarnState createState() => _InviteEarnState();
}

class _InviteEarnState extends State<InviteEarn> {
  @override
  Widget build(BuildContext context) {
    final UserManagement userManagement = Provider.of<UserManagement>(context);

    User user = userManagement.user;
    double width = MediaQuery.of(context).size.width;
    return Container(
      decoration: kMainPagesDecoration,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: generalAppBar(
            title: user.canEarn ? 'Davet Et, Kazan' : 'Arkadaşlarını Davet Et'),
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.all(40),
            child: Column(
              children: [
                Text(
                    'Arkadaşlarınız CinePrize\'a üye olurken sizin \'Referans Kodunuz\' ' +
                        (user.canEarn
                            ? 'u girerse, her 1 kişi için 200 CineCoin ve 200 CineGold kazanırsınız.'
                            : 'ile kayıt olabilir.'),
                    style: TextStyle(color: Colors.grey[700], fontSize: 15),
                    textAlign: TextAlign.center),
                SizedBox(height: 20),
                Container(
                  width: width / 1.5,
                  height: 50,
                  color: Color(0xff3cd08c),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('Referans Kodun',
                          style: TextStyle(
                              color: Colors.white70,
                              fontStyle: FontStyle.italic)),
                      Text('${user.userName}', style: kBigText)
                    ],
                  ),
                ),
                SizedBox(height: 10),
                GestureDetector(
                  onTap: () {
                    Share.share(
                      ' \'${user.userName}\'  Kanka bu referans kodum ile CinePrize\'a üye ol. İkimizde 200 cinecoin ve 200 cinegold kazanalım. 💰 Markette ne varsa ücretsiz alalım 🤩 kisa.link/OrR7',
                      subject: 'Cineprize | Ödüllü Video Platformu',
                    );
                  },
                  child: Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      color: Color(0xff3cd08c),
                      borderRadius: BorderRadius.circular(20),
                    ),
                    width: width / 1.5,
                    height: 50,
                    child: Center(
                      child: Text('Tıkla, Paylaş', style: kBoldText),
                    ),
                  ),
                ),
                SizedBox(height: 20),
                ColumnWithHeading(
                    heading: 'Referans Olunan Kullanıcılar',
                    children: getReferences()),
                SizedBox(height: 40),
                ColumnWithHeading(
                  heading: 'Referans Olan Kullanıcı',
                  children: [
                    referenceElement(
                        text: user.refer == null ? 'Bulunamadı' : user.refer!)
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  getReferences() {
    List<Reference>? references =
        UserManagement.include.user.references ?? null;

    List<Widget> widgets = [];
    Widget widget;
    if (references != null) {
      for (Reference reference in references) {
        widget = referenceElement(text: reference.userName);
        widgets.add(widget);
      }
      return widgets;
    }
    widget = referenceElement(text: 'Hiç referansınız yok.');
    widgets.add(widget);

    return widgets;
  }

  Widget referenceElement({required String text}) {
    return Text(text, style: TextStyle(fontSize: 16, color: Colors.white));
  }
}

class ColumnWithHeading extends StatelessWidget {
  final String heading;
  final List<Widget> children;

  const ColumnWithHeading(
      {Key? key, required this.heading, required this.children})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(heading,
            style: TextStyle(
                fontSize: 18,
                color: Colors.white70,
                fontWeight: FontWeight.bold)),
        //Referans Olunan Kullanıcıların Verisini Aşağıdaki Column'un içine çekebilirsiniz.
        Column(children: children)
      ],
    );
  }
}
