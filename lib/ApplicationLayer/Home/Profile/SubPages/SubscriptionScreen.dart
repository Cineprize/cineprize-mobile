// import 'dart:async';
// import 'dart:io';
//
// import 'package:cineprize/ApplicationLayer/Constants.dart';
// import 'package:cineprize/ApplicationLayer/Home/AppBar/HomeAppBar.dart';
// import 'package:cineprize/Tools/Subs/consumable_store.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:in_app_purchase/in_app_purchase.dart';
//
// const bool _kAutoConsume = true;
//
// const String _kConsumableId = 'CinePremium';
// String _kSubscriptionId = Platform.isAndroid ? 'com.cineprize.android' : 'com.cineprize';
//
// List<String> _kAndroidProductIds = <String>['cinepremium'];
//
// List<String> _kiOSProductIds = <String>['CINEPRIZE.PREMIUM'];
//
// //////////////////////////////////////////////////////////////////////////////////////////////////// sabitler
//
// class SubscriptionScreen extends StatefulWidget {
//   @override
//   _SubscriptionScreenState createState() => _SubscriptionScreenState();
// }
//
// class _SubscriptionScreenState extends State<SubscriptionScreen> {
//   final InAppPurchaseConnection _connection = InAppPurchaseConnection.instance;
//   StreamSubscription<List<PurchaseDetails>> _subscription;
//   List<String> _notFoundIds = [];
//   List<ProductDetails> _products = [];
//   List<PurchaseDetails> _purchases = [];
//   List<String> _consumables = [];
//   bool _isAvailable = false;
//   bool _purchasePending = false;
//   bool _loading = true;
//   String _queryProductError;
//
//   @override
//   void initState() {
//     Stream purchaseUpdated = InAppPurchaseConnection.instance.purchaseUpdatedStream;
//     _subscription = purchaseUpdated.listen((purchaseDetailsList) {
//       _listenToPurchaseUpdated(purchaseDetailsList);
//     }, onDone: () {
//       _subscription.cancel();
//     }, onError: (error) {
//       // handle error here.
//     });
//     initStoreInfo();
//     super.initState();
//   }
//
//   Future<void> initStoreInfo() async {
//     final bool isAvailable = await _connection.isAvailable();
//
//     if (!isAvailable) {
//       setState(() {
//         _isAvailable = isAvailable;
//         _products = [];
//         _purchases = [];
//         _notFoundIds = [];
//         _consumables = [];
//         _purchasePending = false;
//         _loading = false;
//       });
//       return;
//     }
//
//     ProductDetailsResponse productDetailResponse =
//         await _connection.queryProductDetails(Platform.isIOS ? _kiOSProductIds.toSet() : _kAndroidProductIds.toSet()); //_kProductIds.toSet());
//     if (productDetailResponse.error != null) {
//       setState(() {
//         _queryProductError = productDetailResponse.error.message;
//         _isAvailable = isAvailable;
//         _products = productDetailResponse.productDetails;
//         _purchases = [];
//         _notFoundIds = productDetailResponse.notFoundIDs;
//         _consumables = [];
//         _purchasePending = false;
//         _loading = false;
//       });
//       return;
//     }
//
//     if (productDetailResponse.productDetails.isEmpty) {
//       setState(() {
//         _queryProductError = null;
//         _isAvailable = isAvailable;
//         _products = productDetailResponse.productDetails;
//         _purchases = [];
//         _notFoundIds = productDetailResponse.notFoundIDs;
//         _consumables = [];
//         _purchasePending = false;
//         _loading = false;
//       });
//       return;
//     }
//
//     final QueryPurchaseDetailsResponse purchaseResponse = await _connection.queryPastPurchases();
//     if (purchaseResponse.error != null) {
//       // handle query past purchase error..
//     }
//     final List<PurchaseDetails> verifiedPurchases = [];
//     for (PurchaseDetails purchase in purchaseResponse.pastPurchases) {
//       if (await _verifyPurchase(purchase)) {
//         verifiedPurchases.add(purchase);
//       }
//     }
//     List<String> consumables = await ConsumableStore.load();
//     setState(() {
//       _isAvailable = isAvailable;
//       _products = productDetailResponse.productDetails;
//       _purchases = verifiedPurchases;
//       _notFoundIds = productDetailResponse.notFoundIDs;
//       _consumables = consumables;
//       _purchasePending = false;
//       _loading = false;
//     });
//   }
//
//   @override
//   void dispose() {
//     _subscription.cancel();
//     super.dispose();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     /*  List<Widget> stack = [];
//     if (_queryProductError == null) {
//       stack.add(
//         Column(
//           children: [
//             //_buildConnectionCheckTile(),
//             _buildProductList(),
//
//             // _buildConsumableBox(),
//           ],
//         ),
//       );
//     } else {
//       stack.add(Center(
//         child: Text(_queryProductError),
//       ));
//     }
//     if (_purchasePending) {
//       stack.add(
//         Stack(
//           children: [
//             Opacity(
//               opacity: 0.3,
//               child: const ModalBarrier(dismissible: false, color: Colors.grey),
//             ),
//             Center(child: CircularProgressIndicator()),
//           ],
//         ),
//       );
//     } */
//
//     return CupertinoPageScaffold(
//         child: Container(
//       decoration: kMainPagesDecoration,
//       child: Scaffold(
//         backgroundColor: Colors.transparent,
//         appBar: generalAppBar(title: 'CinePremium'),
//         body: Column(
//           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//           crossAxisAlignment: CrossAxisAlignment.center,
//           children: [
//             //_buildConnectionCheckTile(),
//
//             _purchasePending
//                 ? Stack(
//                     children: [
//                       Opacity(
//                         opacity: 0.3,
//                         child: const ModalBarrier(dismissible: false, color: Colors.grey),
//                       ),
//                       Center(child: CircularProgressIndicator()),
//                     ],
//                   )
//                 : _queryProductError == null
//                     ? _buildProductList()
//                     : Center(
//                         child: Text(_queryProductError),
//                       ),
//             // _buildConsumableBox(),
//           ],
//         ),
//       ),
//     ));
//   }
//
//   Card _buildConnectionCheckTile() {
//     if (_loading) {
//       return Card(child: ListTile(title: const Text('Trying to connect...')));
//     }
//     final Widget storeHeader = ListTile(
//       leading: Icon(_isAvailable ? Icons.check : Icons.block, color: _isAvailable ? Colors.green : ThemeData.light().errorColor),
//       title: Text('The store is ' + (_isAvailable ? 'available' : 'unavailable') + '.'),
//     );
//     final List<Widget> children = <Widget>[storeHeader];
//
//     if (!_isAvailable) {
//       children.addAll([
//         Divider(),
//         ListTile(
//           title: Text('Not connected', style: TextStyle(color: ThemeData.light().errorColor)),
//           subtitle: const Text('Unable to connect to the payments processor. Has this app been configured correctly? See the example README for instructions.'),
//         ),
//       ]);
//     }
//     return Card(child: Column(children: children));
//   }
//
//   Widget _buildProductList() {
//     if (_loading) {
//       return Card(child: (ListTile(leading: CircularProgressIndicator(), title: Text('Üyelikler yükleniyor...'))));
//     }
//     if (!_isAvailable) {
//       return Card();
//     }
//     //final ListTile productHeader = ListTile(title: Text('Products for Sale'));
//     List<Widget> productList = <Widget>[];
//     if (_notFoundIds.isNotEmpty) {
//       productList.add(ListTile(
//           title: Text('[${_notFoundIds.join(', ')}] not found', style: TextStyle(color: ThemeData.light().errorColor)),
//           subtitle: Text('This app needs special configuration to run. Please see example/README.md for instructions.')));
//     }
//
//     // This loading previous purchases code is just a demo. Please do not use this as it is.
//     // In your app you should always verify the purchase data using the `verificationData` inside the [PurchaseDetails] object before trusting it.
//     // We recommend that you use your own server to verity the purchase data.
//     Map<String, PurchaseDetails> purchases = Map.fromEntries(_purchases.map((PurchaseDetails purchase) {
//       if (purchase.pendingCompletePurchase) {
//         InAppPurchaseConnection.instance.completePurchase(purchase);
//       }
//       return MapEntry<String, PurchaseDetails>(purchase.productID, purchase);
//     }));
//     productList.addAll(_products.map(
//       (ProductDetails productDetails) {
//         PurchaseDetails previousPurchase = purchases[productDetails.id];
//         return Column(
//           children: [
//             SizedBox(
//               height: 25,
//             ),
//             Container(
//               width: MediaQuery.of(context).size.width * 0.9,
//               child: ClipRRect(
//                 borderRadius: BorderRadius.circular(10.0),
//                 child: Image(
//                   image: AssetImage('assets/images/premium/image.gif'),
//                 ),
//               ),
//             ),
//             SizedBox(
//               height: 20,
//             ),
//             Row(
//               mainAxisAlignment: MainAxisAlignment.center,
//               children: [
//                 Center(
//                   child: Text(
//                     productDetails.title,
//                     style: TextStyle(color: Colors.white, fontSize: 30),
//                   ),
//                 ),
//                 SizedBox(
//                   width: 15,
//                 ),
//                 Image(
//                   color: Colors.white,
//                   fit: BoxFit.contain,
//                   height: MediaQuery.of(context).size.width * 0.1,
//                   width: MediaQuery.of(context).size.width * 0.1,
//                   image: AssetImage('assets/images/premium/crown.png'),
//                 ),
//               ],
//             ),
//             SizedBox(
//               height: 15,
//             ),
//             Container(
//               height: MediaQuery.of(context).size.height * 0.2,
//               margin: EdgeInsets.only(left: 30, right: 30),
//               child: Column(
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: [
//                   Text(
//                     '* Marketi sadece Premium üyeler kullanabilir.',
//                     style: TextStyle(color: Colors.white, fontSize: 14),
//                     maxLines: 1,
//                   ),
//                   Text(
//                     '* Günlük otomatik 450 CineCoin hesabınıza yatar.',
//                     style: TextStyle(color: Colors.white, fontSize: 14),
//                     maxLines: 1,
//                   ),
//                   Text(
//                     '* Günlük otomatik 450 CineGold hesabınıza yatar.',
//                     style: TextStyle(color: Colors.white, fontSize: 14),
//                     maxLines: 1,
//                   ),
//                   Text(
//                     '* Şimdi Premium olanlara çok yakında extra özellikler.',
//                     style: TextStyle(color: Colors.white, fontSize: 14),
//                     maxLines: 1,
//                   ),
//                 ],
//               ),
//             ),
//             SizedBox(
//               height: 15,
//             ),
//             RaisedButton(
//               shape: RoundedRectangleBorder(
//                 borderRadius: BorderRadius.circular(40.0),
//                 side: BorderSide(color: Colors.white),
//               ),
//               child: Container(
//                 width: MediaQuery.of(context).size.width * 0.75,
//                 height: MediaQuery.of(context).size.height * 0.07,
//                 child: Center(
//                   child: Row(
//                     mainAxisAlignment: MainAxisAlignment.center,
//                     children: [
//                       Text(
//                         previousPurchase != null ? 'Premium üyesiniz' : 'Aylık ${productDetails.price} premium ol!',
//                         style: TextStyle(fontSize: 20, color: Colors.black),
//                       ),
//                     ],
//                   ),
//                 ),
//               ),
//               color: Color(0xff3fd490),
//               onPressed: () {
//                 if (previousPurchase == null) {
//                   PurchaseParam purchaseParam = PurchaseParam(
//                       productDetails: productDetails,
//                       //TODO :: Değiştirilebilir
//                       // applicationUserName: null,
//                       applicationUserName: _kSubscriptionId,
//                       sandboxTesting: false);
//                   if (productDetails.id == _kConsumableId) {
//                     _connection.buyConsumable(purchaseParam: purchaseParam, autoConsume: _kAutoConsume || Platform.isIOS);
//                   } else {
//                     _connection.buyNonConsumable(purchaseParam: purchaseParam);
//                   }
//                 }
//               },
//             ),
//             //_bottomAppBar(context),
//           ],
//         );
//       },
//     ));
//
//     return Column(children: productList);
//   }
//
//   /* Card _buildConsumableBox() {
//     if (_loading) {
//       return Card(
//           child: (ListTile(
//               leading: CircularProgressIndicator(),
//               title: Text('Fetching consumables...'))));
//     }
//     if (!_isAvailable || _notFoundIds.contains(_kConsumableId)) {
//       return Card();
//     }
//     final ListTile consumableHeader =
//         ListTile(title: Text('Purchased consumables'));
//     final List<Widget> tokens = _consumables.map((String id) {
//       return GridTile(
//         child: IconButton(
//           icon: Icon(
//             Icons.stars,
//             size: 42.0,
//             color: Colors.orange,
//           ),
//           splashColor: Colors.yellowAccent,
//           onPressed: () => consume(id),
//         ),
//       );
//     }).toList();
//     return Card(
//         child: Column(children: <Widget>[
//       consumableHeader,
//       Divider(),
//       GridView.count(
//         crossAxisCount: 5,
//         children: tokens,
//         shrinkWrap: true,
//         padding: EdgeInsets.all(16.0),
//       )
//     ]));
//   } */
//
//   Future<void> consume(String id) async {
//     await ConsumableStore.consume(id);
//     final List<String> consumables = await ConsumableStore.load();
//     setState(() {
//       _consumables = consumables;
//     });
//   }
//
//   void showPendingUI() {
//     setState(() {
//       _purchasePending = true;
//     });
//   }
//
//   void deliverProduct(PurchaseDetails purchaseDetails) async {
//     // IMPORTANT!! Always verify a purchase purchase details before delivering the product.
//     if (purchaseDetails.productID == _kConsumableId) {
//       await ConsumableStore.save(purchaseDetails.purchaseID);
//       List<String> consumables = await ConsumableStore.load();
//       setState(() {
//         _purchasePending = false;
//         _consumables = consumables;
//       });
//     } else {
//       setState(() {
//         _purchases.add(purchaseDetails);
//         _purchasePending = false;
//       });
//     }
//   }
//
//   void handleError(IAPError error) {
//     setState(() {
//       _purchasePending = false;
//     });
//   }
//
//   Future<bool> _verifyPurchase(PurchaseDetails purchaseDetails) {
//     // IMPORTANT!! Always verify a purchase before delivering the product.
//     // For the purpose of an example, we directly return true.
//     return Future<bool>.value(true);
//   }
//
//   void _handleInvalidPurchase(PurchaseDetails purchaseDetails) {
//     // handle invalid purchase here if  _verifyPurchase` failed.
//   }
//
//   void _listenToPurchaseUpdated(List<PurchaseDetails> purchaseDetailsList) {
//     purchaseDetailsList.forEach((PurchaseDetails purchaseDetails) async {
//       if (purchaseDetails.status == PurchaseStatus.pending) {
//         showPendingUI();
//       } else {
//         if (purchaseDetails.status == PurchaseStatus.error) {
//           handleError(purchaseDetails.error);
//         } else if (purchaseDetails.status == PurchaseStatus.purchased) {
//           bool valid = await _verifyPurchase(purchaseDetails);
//           if (valid) {
//             deliverProduct(purchaseDetails);
//           } else {
//             _handleInvalidPurchase(purchaseDetails);
//             return;
//           }
//         }
//         if (Platform.isAndroid) {
//           if (!_kAutoConsume && purchaseDetails.productID == _kConsumableId) {
//             await InAppPurchaseConnection.instance.consumePurchase(purchaseDetails);
//           }
//         }
//         if (purchaseDetails.pendingCompletePurchase) {
//           await InAppPurchaseConnection.instance.completePurchase(purchaseDetails);
//         }
//       }
//     });
//   }
// }
// /*
// import 'package:cineprize/ApplicationLayer/Home/AppBar/HomeAppBar.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
//
// import '../../../Constants.dart';
//
// //===============================================================
//
// class SubscriptionScreen extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return CupertinoPageScaffold(
//       child: Container(
//         decoration: kMainPagesDecoration,
//         child: Scaffold(
//           backgroundColor: Colors.transparent,
//           appBar: generalAppBar(title: 'Premium'),
//           body: Container(
//             child: SingleChildScrollView(
//               child: Column(
//                 children: [
//                   SizedBox(
//                     height: 15,
//                   ),
//                   Positioned(
//                     width: MediaQuery.of(context).size.width,
//                     height: MediaQuery.of(context).size.height * 0.3,
//                     top: MediaQuery.of(context).size.height * 0.01,
//                     //left: MediaQuery.of(context).size.width * 0.05,
//                     child: Column(
//                       mainAxisAlignment: MainAxisAlignment.center,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: [
//                         Container(
//                           width: MediaQuery.of(context).size.width * 0.9,
//                           child: ClipRRect(
//                             borderRadius: BorderRadius.circular(10.0),
//                             child: Image(
//                               image:
//                                   AssetImage('assets/images/premium/image.gif'),
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),
//                   ),
//                   Positioned(
//                     top: MediaQuery.of(context).size.height * 0.33,
//                     bottom: MediaQuery.of(context).size.height * 0.1,
//                     width: MediaQuery.of(context).size.width,
//                     child: Column(
//                       children: [
//                         Row(
//                           mainAxisAlignment: MainAxisAlignment.center,
//                           children: [
//                             Center(
//                               child: Text(
//                                 '1 Aylık Premium',
//                                 style: TextStyle(
//                                     color: Colors.white, fontSize: 30),
//                               ),
//                             ),
//                             SizedBox(
//                               width: 15,
//                             ),
//                             Image(
//                               color: Colors.white,
//                               fit: BoxFit.contain,
//                               height: MediaQuery.of(context).size.width * 0.1,
//                               width: MediaQuery.of(context).size.width * 0.1,
//                               image:
//                                   AssetImage('assets/images/premium/crown.png'),
//                             ),
//                           ],
//                         ),
//                         SizedBox(
//                           height: 15,
//                         ),
//                         Container(
//                           height: MediaQuery.of(context).size.height * 0.3,
//                           margin: EdgeInsets.only(left: 30, right: 30),
//                           child: Column(
//                             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                             children: [
//                               Center(
//                                 child: Text(
//                                   '* Premium olmasanız bile zorunlu reklam asla yok.',
//                                   style: TextStyle(
//                                       color: Colors.white, fontSize: 15),
//                                 ),
//                               ),
//                               Center(
//                                 child: Text(
//                                   '* İsteğe bağlı izlenen reklamlar tamamen kaldırılır.',
//                                   style: TextStyle(
//                                       color: Colors.white, fontSize: 15),
//                                 ),
//                               ),
//                               Center(
//                                 child: Text(
//                                   '* Günlük otomatik 450 CineCoin hesabınıza yatar.',
//                                   style: TextStyle(
//                                       color: Colors.white, fontSize: 15),
//                                 ),
//                               ),
//                               Center(
//                                 child: Text(
//                                   '* Günlük otomatik 450 CineGold hesabınıza yatar.',
//                                   style: TextStyle(
//                                       color: Colors.white, fontSize: 15),
//                                 ),
//                               ),
//                               Center(
//                                 child: Text(
//                                   '* Şimdi Premium olanlara çok yakında extra özellikler.',
//                                   style: TextStyle(
//                                       color: Colors.white, fontSize: 14),
//                                 ),
//                               ),
//                             ],
//                           ),
//                         ),
//                         SizedBox(
//                           height: 25,
//                         ),
//                         RaisedButton(
//                           shape: RoundedRectangleBorder(
//                             borderRadius: BorderRadius.circular(40.0),
//                             side: BorderSide(color: Colors.white),
//                           ),
//                           child: Container(
//                             width: MediaQuery.of(context).size.width * 0.75,
//                             height: MediaQuery.of(context).size.height * 0.07,
//                             child: Center(
//                               child: Row(
//                                 mainAxisAlignment: MainAxisAlignment.center,
//                                 children: [
//                                   Text(
//                                     'Aylık 4.99TL'ye premium ol!',
//                                     style: TextStyle(
//                                         fontSize: 20, color: Colors.black),
//                                   ),
//                                 ],
//                               ),
//                             ),
//                           ),
//                           color: Color(0xff3fd490),
//                           onPressed: () {
//                             print('Test');
//                             Navigator.push(
//                               context,
//                               MaterialPageRoute(
//                                   builder: (context) => SubscriptionScreen()),
//                             );
//                           },
//                         )
//                       ],
//                     ),
//                   ),
//                   //_bottomAppBar(context),
//                 ],
//               ),
//             ),
//           ),
//         ),
//       ),
//     );
//   }
//
//   /* Widget _topBar(BuildContext context) {
//     return Positioned(
//       width: MediaQuery.of(context).size.width,
//       height: MediaQuery.of(context).size.height * 0.3,
//       top: MediaQuery.of(context).size.height * 0.01,
//       //left: MediaQuery.of(context).size.width * 0.05,
//       child: Column(
//         mainAxisAlignment: MainAxisAlignment.center,
//         crossAxisAlignment: CrossAxisAlignment.center,
//         children: [
//           Container(
//             width: MediaQuery.of(context).size.width * 0.9,
//             child: ClipRRect(
//               borderRadius: BorderRadius.circular(10.0),
//               child: Image(
//                 image: AssetImage('assets/images/premium/image.gif'),
//               ),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// */
//   /* Widget _centralContent(BuildContext context) {
//     return Positioned(
//       top: MediaQuery.of(context).size.height * 0.33,
//       bottom: MediaQuery.of(context).size.height * 0.1,
//       width: MediaQuery.of(context).size.width,
//       child: Column(
//         children: [
//           Row(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: [
//               Center(
//                 child: Text(
//                   '1 Aylık Premium',
//                   style: TextStyle(color: Colors.white, fontSize: 30),
//                 ),
//               ),
//               SizedBox(
//                 width: 15,
//               ),
//               Image(
//                 color: Colors.white,
//                 fit: BoxFit.contain,
//                 height: MediaQuery.of(context).size.width * 0.1,
//                 width: MediaQuery.of(context).size.width * 0.1,
//                 image: AssetImage('assets/images/premium/crown.png'),
//               ),
//             ],
//           ),
//           SizedBox(
//             height: 15,
//           ),
//           Container(
//             height: MediaQuery.of(context).size.height * 0.3,
//             margin: EdgeInsets.only(left: 30, right: 30),
//             child: Column(
//               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//               children: [
//                 Center(
//                   child: Text(
//                     '* Premium olmasanız bile zorunlu reklam asla yok.',
//                     style: TextStyle(color: Colors.white, fontSize: 15),
//                   ),
//                 ),
//                 Center(
//                   child: Text(
//                     '* İsteğe bağlı izlenen reklamlar tamamen kaldırılır.',
//                     style: TextStyle(color: Colors.white, fontSize: 15),
//                   ),
//                 ),
//                 Center(
//                   child: Text(
//                     '* Günlük otomatik 450 CineCoin hesabınıza yatar.',
//                     style: TextStyle(color: Colors.white, fontSize: 15),
//                   ),
//                 ),
//                 Center(
//                   child: Text(
//                     '* Günlük otomatik 450 CineGold hesabınıza yatar.',
//                     style: TextStyle(color: Colors.white, fontSize: 15),
//                   ),
//                 ),
//                 Center(
//                   child: Text(
//                     '* Şimdi Premium olanlara çok yakında extra özellikler.',
//                     style: TextStyle(color: Colors.white, fontSize: 14),
//                   ),
//                 ),
//               ],
//             ),
//           ),
//           /*  SizedBox(
//           height: 15,
//         ),
//         Center(
//           child: Text(
//             '4.99 TL Aylık',
//             style: TextStyle(color: Colors.white, fontSize: 17),
//           ),
//         ), */
//           SizedBox(
//             height: 25,
//           ),
//           RaisedButton(
//             shape: RoundedRectangleBorder(
//               borderRadius: BorderRadius.circular(40.0),
//               side: BorderSide(color: Colors.white),
//             ),
//             child: Container(
//                 width: MediaQuery.of(context).size.width * 0.75,
//                 height: MediaQuery.of(context).size.height * 0.07,
//                 child: Center(
//                     child: Row(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: [
//                     Text(
//                       'Aylık 4.99TL'ye premium ol!',
//                       style: TextStyle(fontSize: 20, color: Colors.black),
//                     ),
//                     /* Icon(
//                     Icons.subscriptions,
//                     color: Colors.black,
//                     size: 40,
//                   ), */
//                   ],
//                 ))),
//             color: Color(0xff3fd490),
//             onPressed: () {
//               print('Test');
//               Navigator.push(
//                 context,
//                 MaterialPageRoute(builder: (context) => SubscriptionScreen()),
//               );
//             },
//           )
//         ],
//       ),
//     );
//   } */
//
//   Widget _bottomAppBar(BuildContext context) {
//     return Positioned(
//       bottom: MediaQuery.of(context).size.height * 0,
//       left: 0,
//       right: 0,
//       child: Column(
//         children: [Text('MENU PLACEHOLDER')],
//       ),
//     );
//   }
// }
//
//  */
