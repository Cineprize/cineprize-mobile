import 'package:flutter/material.dart';

//Thumb Route
final String routeForThumbs = 'assets/images/ExtraGains/';

class ExtraEarnings {
  final String? videoBanner;
  final AssetImage videoThumbnail;

  ExtraEarnings({this.videoBanner, required this.videoThumbnail});
}

// Sample Data for ExtraEarnings
final List<ExtraEarnings> extraEarningsList = [
  ExtraEarnings(videoThumbnail: AssetImage('${routeForThumbs}1.png')),
  ExtraEarnings(videoThumbnail: AssetImage('${routeForThumbs}2.png')),
  ExtraEarnings(videoThumbnail: AssetImage('${routeForThumbs}3.png')),
  ExtraEarnings(videoThumbnail: AssetImage('${routeForThumbs}1.png')),
  ExtraEarnings(videoThumbnail: AssetImage('${routeForThumbs}2.png')),
  ExtraEarnings(videoThumbnail: AssetImage('${routeForThumbs}3.png')),
  ExtraEarnings(videoThumbnail: AssetImage('${routeForThumbs}1.png')),
  ExtraEarnings(videoThumbnail: AssetImage('${routeForThumbs}2.png')),
  ExtraEarnings(videoThumbnail: AssetImage('${routeForThumbs}3.png')),
];
