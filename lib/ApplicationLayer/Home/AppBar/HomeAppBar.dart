import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:flutter/material.dart';

AppBar homeAppBar() {
  return AppBar(
    elevation: 0,
    automaticallyImplyLeading: false,
    // actions: [
    //   Padding(
    //     padding: kAppBarActionsPadding,
    //     child: Icon(
    //       Icons.search,
    //       size: kHomePageAppBarIconSize,
    //     ),
    //   )
    // ],
    title: SizedBox(
        child: Image(
            image: AssetImage('assets/images/badge.png'),
            fit: BoxFit.fitHeight),
        height: 40,
        width: double.infinity),
    centerTitle: true,
  );
}

AppBar generalAppBar({required String title}) {
  return AppBar(centerTitle: true, title: Text(title, style: kNormalText));
}

AppBar giveawayAppBar({required String title}) {
  return AppBar(
    centerTitle: true,
    iconTheme: IconThemeData(color: Colors.white),
    automaticallyImplyLeading: true,
    elevation: 0,
    title: Text(
      title,
      style: kNormalText,
    ),
  );
}

AppBar homeAppBarWithBackButton({context}) {
  /*  return AppBar(
    leading: GestureDetector(
      onTap: () {
        Navigator.pop(context);
      },
      child: Icon(
        Icons.menu, // add custom icons also
      ),
    ),
    iconTheme: IconThemeData(color: Colors.white),
    automaticallyImplyLeading: true,
    elevation: 0,
    title: SizedBox(
        child: Image(
            image: AssetImage('assets/images/badge.png'),
            fit: BoxFit.fitHeight),
        height: 40),
    centerTitle: true,
  ); */
  return context != null
      ? AppBar(
          leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.arrow_back_ios_sharp, // add custom icons also
              size: 25,
            ),
          ),
          iconTheme: IconThemeData(color: Colors.white),
          automaticallyImplyLeading: true,
          elevation: 0,
          title: SizedBox(
              child: Image(
                  image: AssetImage('assets/images/badge.png'),
                  fit: BoxFit.fitHeight),
              height: 40),
          centerTitle: true,
        )
      : AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          automaticallyImplyLeading: true,
          elevation: 0,
          title: SizedBox(
              child: Image(
                  image: AssetImage('assets/images/badge.png'),
                  fit: BoxFit.fitHeight),
              height: 40),
          centerTitle: true,
        );
}
