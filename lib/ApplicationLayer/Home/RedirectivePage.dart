import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Home/CupertinoManagement.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RedirectivePage extends StatelessWidget {
  final TabItemEnum currentTab;

  final Map<TabItemEnum, Widget> selectedTabItem;
  final Map<TabItemEnum, GlobalKey<NavigatorState>> navigatorKeys;

  const RedirectivePage({
    Key? key,
    required this.currentTab,
    required this.selectedTabItem,
    required this.navigatorKeys,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoTabScaffold(
      backgroundColor: Colors.transparent,
      tabBar: CupertinoTabBar(
        currentIndex: currentTab.index,
        backgroundColor: kAppBlack,
        items: TabItem.getBottomNavigationBarItems(),
        onTap: (index) => TabItemEnum.values[index],
      ),
      tabBuilder: (context, index) {
        final TabItemEnum value = TabItemEnum.values[index];

        return CupertinoTabView(
          builder: (_) => selectedTabItem[value]!,
          navigatorKey: navigatorKeys[value],
        );
      },
    );
  }
}
