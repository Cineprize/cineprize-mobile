import 'package:cached_network_image/cached_network_image.dart';
import 'package:cineprize/ApplicationLayer/Auth/AuthManagement/LoginNavigator.dart';
import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Home/AppBar/HomeAppBar.dart';
import 'package:cineprize/Models/Giveaway.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Giveaway/GiveawayJoinService.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Giveaway/GiveawayReadService.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:skeleton_text/skeleton_text.dart';

class GiveawayScreen extends StatefulWidget {
  @override
  _GiveawayScreenState createState() => _GiveawayScreenState();
}

class _GiveawayScreenState extends State<GiveawayScreen> {
  late Future<Giveaway> _giveawayFuture;

  @override
  void initState() {
    _giveawayFuture = GiveawayReadService.include.read();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Container(
      decoration: kMainPagesDecoration,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: generalAppBar(title: 'Profil Bilgileri'),
        body: SingleChildScrollView(
          child: FutureBuilder(
              future: _giveawayFuture,
              builder: (context, AsyncSnapshot snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Center(child: CircularProgressIndicator());
                }
                if (snapshot.hasError) {
                  return Center(child: CircularProgressIndicator());
                }
                if (!snapshot.hasData) {
                  return Center(
                      child: Text('Mevcut bir çekiliş bulunamadı!',
                          style: kBigText));
                }
                Giveaway giveaway = snapshot.data;
                List<String> dateList = DateFormat('dd/MM/yyyy')
                    .format(giveaway.endDate)
                    .split('/');
                return Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                  child: Column(
                    children: [
                      SizedBox(height: 25),
                      Container(
                        width: width * 0.9,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(10.0),
                          child: CachedNetworkImage(
                            imageUrl: giveaway.imageURL,
                            placeholder: (context, url) => SkeletonAnimation(
                              borderRadius: BorderRadius.circular(17),
                              child: Container(
                                width: width * 0.9,
                                height: height / 3.6,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(17),
                                    color: Colors.grey[300]),
                              ),
                            ),
                            errorWidget: (context, url, error) =>
                                new Icon(Icons.error),
                          ),
                        ),
                      ),
                      SizedBox(height: 20),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Center(
                            child: Text(giveaway.title,
                                style: TextStyle(
                                    color: Colors.white, fontSize: 30)),
                          ),
                        ],
                      ),
                      SizedBox(height: 15),
                      Container(
                        margin: EdgeInsets.only(left: 30, right: 30),
                        child: Text(giveaway.description,
                            style: kNormalText.copyWith(color: Colors.white70)),
                      ),
                      SizedBox(height: 15),
                      Container(
                        alignment: Alignment.centerLeft,
                        margin: EdgeInsets.only(left: 30, right: 30),
                        child: Text(
                          'Son Katılım Tarihi: ' +
                              dateList[0] +
                              ' ' +
                              getMonthText(int.parse(dateList[1])) +
                              ' ' +
                              dateList[2],
                          style: kNormalText.copyWith(color: Colors.white70),
                        ),
                      ),
                      SizedBox(height: 15),
                      RaisedButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(40.0),
                            side: BorderSide(color: Colors.white)),
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.75,
                          height: MediaQuery.of(context).size.height * 0.07,
                          child: Center(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  giveaway.attendable
                                      ? '${giveaway.price.toString()} CineCoin\'e Bilet Al'
                                      : 'Katılamazsınız',
                                  style: TextStyle(
                                      fontSize: 20, color: Colors.black),
                                ),
                              ],
                            ),
                          ),
                        ),
                        color: Color(0xff3fd490),
                        onPressed: () async {
                          if (!giveaway.attendable)
                            return showSnackBar(context,
                                'Çekilişe katılım süresi doldu.', width,
                                color: Colors.red, duration: 1000);

                          if (UserManagement.include.user.coin < giveaway.price)
                            return showSnackBar(
                                context, 'Yeterli bakiyeniz bulunmuyor.', width,
                                color: Colors.red, duration: 1000);

                          Map<String, dynamic> response =
                              await GiveawayJoinService.include
                                  .join(giveaway.id);
                          setState(() {
                            if (response['success']) {
                              UserManagement.include.user.coin =
                                  response['data'];
                              return showSnackBar(
                                  context, response['message'], width,
                                  duration: 1000);
                            } else {
                              return showSnackBar(
                                  context, response['message'], width,
                                  color: Colors.red, duration: 1000);
                            }
                          });
                        },
                      )
                    ],
                  ),
                );
              }),
        ),
      ),
    );
  }

  getMonthText(int month) {
    switch (month) {
      case 1:
        return 'Ocak';
      case 2:
        return 'Şubat';
      case 3:
        return 'Mart';
      case 4:
        return 'Nisan';
      case 5:
        return 'Mayıs';
      case 6:
        return 'Haziran';
      case 7:
        return 'Temmuz';
      case 8:
        return 'Ağustos';
      case 9:
        return 'Eylül';
      case 10:
        return 'Ekim';
      case 11:
        return 'Kasım';
      case 12:
        return 'Aralık';
      default:
        return 'Ocak';
    }
  }
}
