import 'package:cineprize/ApplicationLayer/Components/ModLoadingElement.dart';
import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Home/AppBar/HomeAppBar.dart';
import 'package:cineprize/ApplicationLayer/Tools/Image/ImageTools.dart';
import 'package:cineprize/ApplicationLayer/Tools/MyAlertDialogs/MyInfoDialog.dart';
import 'package:cineprize/Models/Mission.dart';
import 'package:cineprize/Models/Response.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Mission/MissionService.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:url_launcher/url_launcher.dart';

class MissionScreen extends StatefulWidget {
  @override
  _MissionScreenState createState() => _MissionScreenState();
}

class _MissionScreenState extends State<MissionScreen> {
  late Future<Response> missionsFuture;

  @override
  void initState() {
    this.missionsFuture = MissionService.include.read();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Container(
      decoration: kMainPagesDecoration,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: generalAppBar(title: 'Görev'),
        body: SingleChildScrollView(
          child: Column(
            children: [
              FutureBuilder<Response>(
                future: this.missionsFuture,
                initialData: Response(),
                builder:
                    (BuildContext context, AsyncSnapshot<Response> snapshot) {
                  List<Widget> elements = [];

                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return ModLoadingElement();
                  }
                  if (snapshot.hasData) {
                    if (!snapshot.data!.success) {
                      return Container(
                        margin: EdgeInsets.all(16),
                        child: Center(
                          child: Text(
                            snapshot.data!.message,
                            style: kNormalText,
                          ),
                        ),
                      );
                    }
                    snapshot.data?.data?.forEach((mission) async {
                      elements.add(MissionElement(
                        mission: mission,
                        height: height,
                        width: width,
                      ));
                    });
                    return Column(children: elements);
                  } else {
                    return Container(
                      margin: EdgeInsets.all(16),
                      child: Center(
                        child: Text(
                          "Bir hata oluştu!",
                          style: kNormalText,
                        ),
                      ),
                    );
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class MissionElement extends StatefulWidget {
  final Mission mission;
  final double width;
  final double height;

  const MissionElement(
      {Key? key,
      required this.mission,
      required this.width,
      required this.height})
      : super(key: key);

  @override
  _MissionElementState createState() => _MissionElementState();
}

class _MissionElementState extends State<MissionElement> {
  double padding = 20;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(widget.height * 0.01),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
        color: const Color(0xff3cb37d),
        boxShadow: [
          BoxShadow(
              color: const Color(0x3f000000),
              offset: Offset(0, 4),
              blurRadius: 14)
        ],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Padding(
            padding:
                EdgeInsets.only(top: padding, right: padding, left: padding),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                          color: const Color(0x3f000000),
                          offset: Offset(0, 4),
                          blurRadius: 15)
                    ],
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(17),
                    child: Container(
                      height: widget.height / 7,
                      width: widget.width / 3.5,
                      child: ImageTools.getImage(
                        imageURL: widget.mission.image,
                        gifPlaceholder: gifPlaceholder,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 20,
                ),
                Expanded(
                  child: Text(
                    widget.mission.text,
                    style: kNormalText,
                    maxLines: 6,
                    softWrap: true,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 25,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: EdgeInsets.only(bottom: padding, left: padding),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: UserManagement.include.user.canEarn
                      ? [
                          Text(
                            'KAZANÇ',
                            style: kBoldText.copyWith(fontSize: 14),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          _buildEarn(widget.mission.coin, widget.mission.gold),
                        ]
                      : [
                          Padding(
                            padding: EdgeInsets.only(bottom: padding * 3),
                            child: Container(
                              child: Container(),
                            ),
                          ),
                        ],
                ),
              ),
              GestureDetector(
                onTap: () async {
                  if (widget.mission.completed) {
                    MyInfoDialog(
                      context,
                      title: 'Uyarı!',
                      description: "Bu görevi daha önce tamamladınız.",
                      buttonText: 'Tamam',
                      alertType: AlertType.warning,
                    );
                    return;
                  }
                  if (widget.mission.clicked) {
                    if (widget.mission.url.toString().trim() != "") {
                      await launchURL(widget.mission.url);
                    }
                    return null;
                  }

                  var response = await MissionService.include
                      .click(missionId: widget.mission.id.toString());

                  MyInfoDialog(
                    context,
                    title: response.success ? 'Başarılı!' : 'Hata!',
                    description: response.message,
                    buttonText: 'Tamam',
                    alertType:
                        response.success ? AlertType.success : AlertType.error,
                    func: () async {
                      setState(() {
                        widget.mission.clicked = true;
                      });
                      if (widget.mission.url.toString().trim() != "" &&
                          response.success) {
                        await launchURL(widget.mission.url);
                      }
                    },
                  );
                },
                child: Container(
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                          color: const Color(0x3f000000),
                          offset: Offset(0, 4),
                          blurRadius: 15)
                    ],
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(40),
                      bottomLeft: Radius.circular(40),
                    ),
                    gradient: LinearGradient(
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight,
                      colors: widget.mission.clicked
                          ? [Colors.grey, Colors.green[100]!]
                          : [Colors.yellow, Colors.orange],
                    ),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(15),
                    child: Text(
                      widget.mission.clicked
                          ? (widget.mission.completed
                              ? 'Tamamlandı'
                              : 'Görev Alındı')
                          : 'Görevi Al',
                      style: kBoldText.copyWith(fontSize: 16),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

_buildEarn(coin, gold) {
  return Row(
    children: [
      coin != 0
          ? Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset(
                  'assets/images/coinIcons/crystal_green.png',
                  width: 40,
                  height: 40,
                ),
                SizedBox(
                  width: 5,
                ),
                Text(
                  coin.toString(),
                  style: kBigText.copyWith(fontSize: 25),
                )
              ],
            )
          : Container(),
      gold != 0
          ? Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset(
                  'assets/images/coinIcons/crystal_pink.png',
                  width: 40,
                  height: 40,
                ),
                SizedBox(
                  width: 5,
                ),
                Text(
                  gold.toString(),
                  style: kBigText.copyWith(fontSize: 25),
                )
              ],
            )
          : Container(),
    ],
  );
}

launchURL(String url) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    print('istek başarısız');
    throw 'İstek başarısız! $url';
  }
}
