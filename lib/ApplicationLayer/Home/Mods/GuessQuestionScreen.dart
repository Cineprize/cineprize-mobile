import 'package:cineprize/ApplicationLayer/Auth/AuthManagement/LoginNavigator.dart';
import 'package:cineprize/ApplicationLayer/Components/CustomBottomButtonWidget.dart';
import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Home/AppBar/HomeAppBar.dart';
import 'package:cineprize/ApplicationLayer/Tools/MyAlertDialogs/MyInfoDialog.dart';
import 'package:cineprize/Models/Response.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Guess/GuessService.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

late Response guessQuestionResult;

class GuessQuestionScreen extends StatefulWidget {
  @override
  _GuessQuestionScreenState createState() => _GuessQuestionScreenState();
}

class _GuessQuestionScreenState extends State<GuessQuestionScreen> {
  double heightValue = 0.3;
  String title = 'Tahmin';
  late bool option_busy;

  late bool success;
  late String message;
  late List<dynamic> data;
  Map<String, bool> answers = new Map<String, bool>();
  int question_order = 0;
  late Map<String, dynamic> question;

  @override
  void initState() {
    this.success = guessQuestionResult.success;
    this.message = guessQuestionResult.message;
    this.data = guessQuestionResult.data;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    this.option_busy = true;
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    Widget element;

    if (this.success) {
      if (this.answers.keys.length == this.data.length) {
        this.title = 'Tahmin Tamamlandı';

        element = Padding(
          padding: const EdgeInsets.only(top: 50),
          child: Center(
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              Text('Sorular başarıyla tamamlandı.',
                  style: TextStyle(color: Colors.white, fontSize: 22)),
              SizedBox(height: 15),
              CustomBottomButtonWidget(
                  text: 'Devam etmek için tıklayınız.',
                  color: Color(0xff33a346),
                  width: width * 0.6,
                  onTap: () {
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                            builder: (context) => LoginNavigator()),
                        (Route<dynamic> route) => false);
                  })
            ]),
          ),
        );
      } else {
        this.question = this.data[this.question_order];
        this.title = 'Tahmin ' +
            (this.question_order + 1).toString() +
            '/' +
            this.data.length.toString();

        element = Column(
          children: [
            //Soru container
            Expanded(
              flex: 1,
              child: Padding(
                padding: EdgeInsets.only(top: 8, left: 8, right: 8),
                child: Container(
                  width: width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8.0),
                    color: const Color(0xff545454),
                    boxShadow: [
                      BoxShadow(
                          color: const Color(0x3f000000),
                          offset: Offset(0, 4),
                          blurRadius: 14)
                    ],
                  ),
                  child: Center(
                      child: Container(
                          margin: EdgeInsets.all(16),
                          child:
                              Text(this.question['text'], style: kSurveyText))),
                ),
              ),
            ),
            SizedBox(height: 15),
            //Cevaplar
            Expanded(
                flex: 2,
                child: Column(children: [
                  this._optionWidget(this.question['answer_one'], value: false),
                  this._optionWidget(this.question['answer_two'], value: true),
                ])),
          ],
        );

        element = Container(
          height: height * heightValue,
          margin: EdgeInsets.all(height * 0.01),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8.0),
              color: const Color(0xff3cb37d),
              boxShadow: [
                BoxShadow(
                    color: const Color(0x3f000000),
                    offset: Offset(0, 4),
                    blurRadius: 14)
              ]),
          child: element,
        );
      }
    } else {
      element = Center(child: Text(this.message, style: kNormalText));
    }

    return Container(
      decoration: kMainPagesDecoration,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: generalAppBar(title: this.title),
        body: SingleChildScrollView(child: element),
      ),
    );
  }

  Widget _optionWidget(String text, {required bool value}) {
    double answerBorderRadius = 20.0;
    double answerMargin = 11.0;

    double width = MediaQuery.of(context).size.width;
    return GestureDetector(
      child: Container(
        margin: EdgeInsets.all(answerMargin),
        padding: EdgeInsets.all(12),
        width: width,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(answerBorderRadius),
            color: const Color(0xff545454),
            boxShadow: [
              BoxShadow(
                  color: const Color(0x3f000000),
                  offset: Offset(0, 4),
                  blurRadius: 14)
            ]),
        child: Center(
            child: Text(text,
                overflow: TextOverflow.clip,
                style: kSurveyText.copyWith(fontSize: 13))),
      ),
      onTap: () async {
        if (this.option_busy) {
          this.answers[this.question['id'].toString()] = value;

          if (this.answers.keys.length == this.data.length) {
            final Response response = await GuessService.include.completed(
                answers: this.answers, authUser: UserManagement.include.user);
            MyInfoDialog(context,
                title: 'Başarılı',
                description: response.message,
                buttonText: 'Devam et',
                alertType: AlertType.success);
          }
          setState(() {
            this.question_order++;
          });
        }
      },
    );
  }
}
