import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Home/AppBar/HomeAppBar.dart';
import 'package:cineprize/Tools/Ads/AdmobAds/AdHelper.dart';
import 'package:cineprize/Tools/Ads/AdmobAds/AdmobInitialize.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';

class DemoScreen extends StatefulWidget {
  @override
  _DemoScreenState createState() => _DemoScreenState();
}

class _DemoScreenState extends State<DemoScreen> {
  RewardedAd? _rewardedAd;
  bool _isRewardedAdReady = false;
  RewardedAdLoadCallback? _adListener;

  @override
  void initState() {
    super.initState();
    initializeAd();
  }

  // TODO:: android için yorum satırından çıkar
  void initializeAd() {
    _adListener = RewardedAdLoadCallback(
      onAdLoaded: (RewardedAd ad) {
        setState(() {
          this._rewardedAd = ad;
          _isRewardedAdReady = true;
        });
      },
      onAdFailedToLoad: (LoadAdError error) {
        setState(() {
          _isRewardedAdReady = false;
        });
        createRewardedAd();
      },
    );
    createRewardedAd();
  }

  viewVideoAd() {
    _rewardedAd!.show(onUserEarnedReward: (RewardedAd ad, RewardItem reward) {
      setState(() {
        _isRewardedAdReady = false;
      });
      ad.dispose();

      createRewardedAd();
    });
  }

  void createRewardedAd() {
    AdmobInitialize.instance.rewardedAd(listener: _adListener!);
  }

  @override
  void dispose() {
    _rewardedAd!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: kMainPagesDecoration,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: generalAppBar(title: 'Görev'),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 72),
              ),
              ElevatedButton(
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 48.0,
                    vertical: 12.0,
                  ),
                  child: Text('Reklamı Aç'),
                ),
                style: ElevatedButton.styleFrom(
                  primary: Theme.of(context).accentColor,
                ),
                onPressed: () {
                  viewVideoAd();
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
