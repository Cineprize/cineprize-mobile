import 'package:cineprize/ApplicationLayer/Components/ModLoadingElement.dart';
import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Home/AppBar/HomeAppBar.dart';
import 'package:cineprize/ApplicationLayer/Tools/MyAlertDialogs/MyInfoDialog.dart';
import 'package:cineprize/Models/Survey.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Survey/SurveyService.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class SurveyScreen extends StatefulWidget {
  @override
  _SurveyScreenState createState() => _SurveyScreenState();
}

class _SurveyScreenState extends State<SurveyScreen> {
  @override
  Widget build(BuildContext context) {
    double answerBorderRadius = 20.0;
    double answerMargin = 11.0;
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Container(
      decoration: kMainPagesDecoration,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: generalAppBar(title: 'Anket'),
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              FutureBuilder<Map<String, dynamic>>(
                future: SurveyService.include
                    .get(authUser: UserManagement.include.user),
                initialData: null,
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return ModLoadingElement();
                  } else {
                    Map<String, dynamic> data = snapshot.data;

                    if (!data['success']) {
                      return Center(
                          child: Container(
                              margin: EdgeInsets.all(16),
                              child:
                                  Text(data['message'], style: kSurveyText)));
                    }

                    if (snapshot.hasData) {
                      Survey survey = (data['data'] as Survey);

                      List<Widget> optionWidgets = [];
                      survey.options!.forEach((option) {
                        optionWidgets.add(this._optionWidget(option['content'],
                            answerMargin, answerBorderRadius, width,
                            survey_id: survey.uid!,
                            survey_option_id: option['id'].toString()));
                      });

                      return Container(
                        margin: EdgeInsets.all(height * 0.01),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                          color: const Color(0xff3cb37d),
                          boxShadow: [
                            BoxShadow(
                                color: const Color(0x3f000000),
                                offset: Offset(0, 4),
                                blurRadius: 14)
                          ],
                        ),
                        child: ListView(
                          shrinkWrap: true,
                          children: [
                            //Soru container
                            Padding(
                              padding:
                                  EdgeInsets.only(top: 8, left: 8, right: 8),
                              child: Container(
                                width: width,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8.0),
                                  color: const Color(0xff545454),
                                  boxShadow: [
                                    BoxShadow(
                                        color: const Color(0x3f000000),
                                        offset: Offset(0, 4),
                                        blurRadius: 14)
                                  ],
                                ),
                                child: Center(
                                    child: Container(
                                        margin: EdgeInsets.all(16),
                                        child: Text(survey.content!,
                                            style: kSurveyText))),
                              ),
                            ),
                            SizedBox(height: 15),
                            //Cevaplar
                            SingleChildScrollView(
                                child: Column(children: optionWidgets)),
                          ],
                        ),
                      );
                    } else if (snapshot.hasError) {
                      return Text('Bir sorun oluştu!');
                    } else {
                      return Container(
                          margin: EdgeInsets.all(16),
                          child: Center(child: CircularProgressIndicator()));
                    }
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _optionWidget(
      String text, double margin, double borderRadius, double width,
      {required String survey_id, required String survey_option_id}) {
    return GestureDetector(
      child: Container(
        margin: EdgeInsets.all(margin),
        padding: EdgeInsets.all(12),
        width: width,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(borderRadius),
            color: const Color(0xff545454),
            boxShadow: [
              BoxShadow(
                  color: const Color(0x3f000000),
                  offset: Offset(0, 4),
                  blurRadius: 14)
            ]),
        child: Center(
            child: Text(text,
                overflow: TextOverflow.clip,
                style: kSurveyText.copyWith(fontSize: 13))),
      ),
      onTap: () async {
        final Map<String, dynamic> result = await SurveyService.include.choose(
            survey_id: survey_id,
            survey_option_id: survey_option_id,
            authUser: UserManagement.include.user);

        if (result['success']) {
          MyInfoDialog(context,
              title: 'Anket Tamamlandı',
              description: result['message'],
              buttonText: 'Devam et',
              alertType: AlertType.success);
        }
        setState(() {});
      },
    );
  }
}
