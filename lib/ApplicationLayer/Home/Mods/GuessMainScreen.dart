import 'package:cineprize/ApplicationLayer/Components/CustomBottomButtonWidget.dart';
import 'package:cineprize/ApplicationLayer/Components/CustomTextFieldWithoutIcon.dart';
import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Home/AppBar/HomeAppBar.dart';
import 'package:cineprize/ApplicationLayer/Home/Mods/GuessQuestionScreen.dart';
import 'package:cineprize/ApplicationLayer/Tools/MyAlertDialogs/MyInfoDialog.dart';
import 'package:cineprize/Models/Response.dart';
import 'package:cineprize/Models/User.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Guess/GuessService.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/Survey/SurveyService.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class GuessMainScreen extends StatefulWidget {
  @override
  _GuessMainScreenState createState() => _GuessMainScreenState();
}

class _GuessMainScreenState extends State<GuessMainScreen> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  String bet = '';
  late Future<Response> _scoreFuture;
  late Future<Map<String, dynamic>> _statusFuture;

  @override
  void initState() {
    _scoreFuture =
        GuessService.include.score(authUser: UserManagement.include.user);
    _statusFuture =
        GuessService.include.status(authUser: UserManagement.include.user);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final User user = Provider.of<UserManagement>(context).user;

    double width = MediaQuery.of(context).size.width;
    return Container(
      decoration: kMainPagesDecoration,
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.transparent,
        appBar: generalAppBar(title: 'Tahmin Ana Sayfa'),
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ///////////////////////////// STATUS /////////////////////////////
              Padding(
                padding: const EdgeInsets.only(top: 30, bottom: 30),
                child: FutureBuilder<Map<String, dynamic>>(
                  future: _statusFuture,
                  initialData: null,
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    Widget element = Container();
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      element = Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            width: 20,
                            height: 20,
                            child: loadingIndicator,
                          ),
                          SizedBox(width: 10),
                          Center(
                              child: Text("Yükleniyor...", style: kNormalText)),
                        ],
                      );
                    } else {
                      if (snapshot.hasData) {
                        final Map<String, dynamic> data = snapshot.data;
                        if (data['status'] == 'bet') {
                          element = Form(
                            key: this.formKey,
                            child: Align(
                              child: Column(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(20.0),
                                    child: CustomTextFieldWithoutIcon(
                                      initialValue: this.bet.toString(),
                                      textInputType: TextInputType.number,
                                      hintText: 'Tahmin tutarını giriniz.',
                                      onSaved: (_) {
                                        this.bet = _.trim();
                                      },
                                      validator: (__) {
                                        String _ = __.trim();
                                        if (_.length == 0 || int.parse(_) < 1) {
                                          return 'Tahmin tutarı en az 1 CineCoin olmalıdır.';
                                        }
                                        return null;
                                      },
                                    ),
                                  ),
                                  SizedBox(height: 10),
                                  Center(
                                    child: CustomBottomButtonWidget(
                                      text: 'Tahmin yap',
                                      color: Color(0xff33a346),
                                      width: width * 0.6,
                                      onTap: () async {
                                        if (this
                                            .formKey
                                            .currentState!
                                            .validate()) {
                                          this.formKey.currentState!.save();

                                          Response result =
                                              await GuessService.include.bet(
                                                  bet: this.bet,
                                                  authUser: user);

                                          if (result.success) {
                                            MyInfoDialog(context,
                                                title: 'Başarılı!',
                                                description: result.message,
                                                buttonText: 'Devam et',
                                                alertType: AlertType.success);
                                          } else {
                                            MyInfoDialog(context,
                                                title: 'Uyarı!',
                                                description:
                                                    result.message.toString(),
                                                buttonText: 'Tamam',
                                                alertType: AlertType.warning);
                                          }

                                          this.bet = '';
                                          setState(() {
                                            this._statusFuture =
                                                GuessService.include.status(
                                                    authUser: UserManagement
                                                        .include.user);
                                          });
                                        }
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        } else if (data['status'] == 'start') {
                          element = Center(
                              child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(snapshot.data['message'],
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 17)),
                              SizedBox(height: 20),
                              CustomBottomButtonWidget(
                                  text: 'Tahminleri yanıtlamaya başla',
                                  color: Color(0xff33a346),
                                  width: width * 0.6,
                                  onTap: () async {
                                    guessQuestionResult =
                                        await GuessService.include.questions(
                                            authUser:
                                                UserManagement.include.user);
                                    print(guessQuestionResult);
                                    Navigator.pushAndRemoveUntil(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                GuessQuestionScreen()),
                                        (Route<dynamic> route) => false);
                                  }),
                            ],
                          ));
                        } else if (data['status'] == 'no' ||
                            data['status'] == 'completed') {
                          element = Center(
                              child: Text(snapshot.data['message'],
                                  style: kSurveyText));
                        }
                      } else {
                        element = Center(
                            child:
                                Text("Bir hata oluştu!", style: kNormalText));
                      }
                    }

                    return element;
                  },
                ),
              ),
              Divider(color: Colors.white24),
              ///////////////////////////// SCORE /////////////////////////////
              Padding(
                padding: const EdgeInsets.only(top: 30),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('Son Tahmin Verileri',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w400)),
                    SizedBox(height: 15),
                    FutureBuilder<Response>(
                      future: _scoreFuture,
                      initialData: null,
                      builder: (BuildContext context,
                          AsyncSnapshot<Response> snapshot) {
                        Widget element;
                        if (snapshot.connectionState ==
                            ConnectionState.waiting) {
                          element = Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                width: 20,
                                height: 20,
                                child: CircularProgressIndicator(
                                    strokeWidth: 1,
                                    valueColor:
                                        new AlwaysStoppedAnimation<Color>(
                                            Colors.white)),
                              ),
                              SizedBox(width: 10),
                              Center(
                                  child: Text("Yükleniyor...",
                                      style: kNormalText)),
                            ],
                          );
                        } else {
                          if (snapshot.hasData) {
                            element = Center(
                                child: Text(snapshot.data!.message,
                                    style: kSurveyText));
                          } else {
                            element = Center(
                                child: Text("Bir hata oluştu!",
                                    style: kNormalText));
                          }
                        }

                        return element;
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _optionWidget(
      String text, double margin, double borderRadius, double width,
      {required String survey_id, required String survey_option_id}) {
    return GestureDetector(
      child: Container(
        margin: EdgeInsets.all(margin),
        padding: EdgeInsets.all(12),
        width: width,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(borderRadius),
            color: const Color(0xff545454),
            boxShadow: [
              BoxShadow(
                  color: const Color(0x3f000000),
                  offset: Offset(0, 4),
                  blurRadius: 14)
            ]),
        child: Center(
            child: Text(text,
                overflow: TextOverflow.clip,
                style: kSurveyText.copyWith(fontSize: 13))),
      ),
      onTap: () async {
        final Map<String, dynamic> result = await SurveyService.include.choose(
            survey_id: survey_id,
            survey_option_id: survey_option_id,
            authUser: UserManagement.include.user);

        if (result['success']) {
          MyInfoDialog(context,
              title: 'Başarılı',
              description: result['message'],
              buttonText: 'Devam et',
              alertType: AlertType.success);
        } else {
          MyInfoDialog(context,
              title: 'Hata',
              description: result['message'],
              buttonText: 'Tamam',
              alertType: AlertType.error);
        }
        setState(() {});
      },
    );
  }
}
