import 'package:flutter/material.dart';

//Trend Video Model
class Trend {
  final AssetImage channelProfilePicture;
  final String videoBanner;
  final AssetImage videoThumbnail;

  Trend(
      {required this.channelProfilePicture,
      required this.videoBanner,
      required this.videoThumbnail});
}
