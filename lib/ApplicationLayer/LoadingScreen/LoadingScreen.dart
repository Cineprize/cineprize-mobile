import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:flutter/material.dart';

class LoadingScreen extends StatefulWidget {
  @override
  _LoadingScreenState createState() => _LoadingScreenState();
}

class _LoadingScreenState extends State<LoadingScreen> {
  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final double logo_width = width * 0.4;
    return Scaffold(
      body: Container(
        color: kSplashScreenBackgroundColor,
        child: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image(image: AssetImage(logo), width: logo_width),
            /*
            SizedBox(height: 50),
            Container(
              width: width * 0.75,
              child: LinearProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(Color.fromRGBO(113, 193, 164, 1)), minHeight: 8, backgroundColor: Colors.black),
            ), 
            */
          ],
        )),
      ),
    );
  }
}
