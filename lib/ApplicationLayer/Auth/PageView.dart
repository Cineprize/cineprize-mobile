import 'package:cineprize/ApplicationLayer/Auth/Login/LoginScreen.dart';
import 'package:cineprize/ApplicationLayer/Auth/Register/RegisterScreen.dart';
import 'package:flutter/material.dart';

final controller = PageController(
  initialPage: 0,
);

final pageView = PageView(
  controller: controller,
  children: [
    LoginScreen(),
    RegisterScreen(),
  ],
);

final pageViewRegister = PageView(
  controller: controller,
  children: [
    RegisterScreen(),
    LoginScreen(),
  ],
);
