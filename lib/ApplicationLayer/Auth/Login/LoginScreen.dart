import 'package:cineprize/ApplicationLayer/Auth/AuthManagement/LoginNavigator.dart';
import 'package:cineprize/ApplicationLayer/Auth/ForgetPassword/ForgetPasswordScreen.dart';
import 'package:cineprize/ApplicationLayer/Auth/PageView.dart';
import 'package:cineprize/ApplicationLayer/Components/CustomObsecureTextFieldWidget.dart';
import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Tools/MyAlertDialogs/MyInfoDialog.dart';
import 'package:cineprize/ApplicationLayer/components/CustomBottomButtonWidget.dart';
import 'package:cineprize/ApplicationLayer/components/CustomTextFieldWidget.dart';
import 'package:cineprize/ApplicationLayer/components/LogoWidget.dart';
import 'package:cineprize/ApplicationLayer/components/PageButtonsWidget.dart';
import 'package:cineprize/ServiceLayer/Auth/Rest/AuthService.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  String id = loginEmail, password = loginPassword;

  //String id = '37beykozlu37@gmail.com', password = '123123123';
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    Provider.of<UserManagement>(context);
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        key: _scaffoldKey,
        body: Padding(
          padding: const EdgeInsets.all(15.0),
          child: SingleChildScrollView(
            reverse: true,
            child: Form(
              key: formKey,
              child: Align(
                child: Container(
                  child: Column(
                    children: [
                      SizedBox(height: height / 5),
                      LogoWidget(),
                      SizedBox(height: 40),
                      ButtonBar(
                        alignment: MainAxisAlignment.spaceAround,
                        children: [
                          PageButtonsWidget(
                            text: 'Giriş Yap',
                            textColor: kAppThemeColor,
                            onPressed: () {},
                          ),
                          PageButtonsWidget(
                            text: 'Kayıt Ol',
                            textColor: Colors.white,
                            onPressed: () {
                              setState(() {
                                controller.jumpToPage(
                                    pageViewInitialValue == 0 ? 1 : 0);
                              });
                            },
                          )
                        ],
                      ),
                      CustomTextFieldWidget(
                        initialValue: this.id,
                        hintText: 'E-posta',
                        icon: Icon(CupertinoIcons.mail, color: Colors.white),
                        textInputType: TextInputType.emailAddress,
                        onSaved: (_) {
                          this.id = _.trim();
                        },
                        validator: (__) {
                          String _ = __.trim();
                          if (_.length == 0) {
                            return 'E-posta alanı boş geçilemez.';
                          } else if (!_.contains(RegExp(
                              r'(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)'))) {
                            return 'Lütfen geçerli bir e-posta adresi giriniz.';
                          }
                          return null;
                        },
                      ),
                      SizedBox(height: 15),
                      CustomObsecureTextFieldWidget(
                        initialValue: this.password,
                        hintText: 'Parola',
                        icon: Icon(CupertinoIcons.lock, color: Colors.white),
                        onSaved: (_) {
                          this.password = _.trim();
                        },
                        isObsecure: true,
                        validator: (__) {
                          String _ = __.trim();
                          if (_.length == 0) {
                            return 'Parola alanı boş geçilemez.';
                          }
                          return null;
                        },
                      ),
                      SizedBox(height: 10),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => ForgetPasswordScreen(),
                              ),
                            );
                          },
                          child: Padding(
                            padding: EdgeInsets.only(left: width / 15),
                            child: Text(
                              'Parolamı Unuttum ?',
                              textAlign: TextAlign.right,
                              style: kSmallText.copyWith(color: kAppThemeColor),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      CustomBottomButtonWidget(
                        text: 'Giriş Yap',
                        onTap: () async {
                          if (this.formKey.currentState!.validate()) {
                            this.formKey.currentState!.save();

                            loginEmail = this.id;
                            loginPassword = this.password;
                            pageViewInitialValue = 0;

                            await AuthService.include
                                .login(id: this.id, password: this.password)
                                .then((user) async {
                              if (user.errorMessage != null) {
                                MyInfoDialog(loginNavigatorContext!,
                                    title: 'Hata!',
                                    description: user.errorMessage!,
                                    buttonText: 'Yeniden Dene',
                                    alertType: AlertType.error);
                              } else {
                                loginEmail = loginPassword = '';
                              }
                            });
                          }
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
