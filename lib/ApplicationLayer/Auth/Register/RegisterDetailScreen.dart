import 'package:cineprize/ApplicationLayer/Auth/AuthManagement/LoginNavigator.dart';
import 'package:cineprize/ApplicationLayer/Auth/Register/Partials/RegisterValidation.dart';
import 'package:cineprize/ApplicationLayer/Components/CustomBottomButtonWidget.dart';
import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Tools/MyAlertDialogs/MyInfoDialog.dart';

import 'package:cineprize/ApplicationLayer/components/CustomTextFieldWidget.dart';
import 'package:cineprize/ApplicationLayer/components/LogoWidget.dart';
import 'package:cineprize/Models/User.dart';
import 'package:cineprize/ServiceLayer/Auth/Rest/AuthService.dart';

import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:provider/provider.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class RegisterDetailScreen extends StatefulWidget {
  const RegisterDetailScreen({Key? key}) : super(key: key);

  @override
  _RegisterDetailScreenState createState() => _RegisterDetailScreenState();
}

class _RegisterDetailScreenState extends State<RegisterDetailScreen> {
  late DateTime birthDate;
  String refer = '', gender = '';
  late int canEarn;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  bool isRegistering = false;

  @override
  void initState() {
    super.initState();
    DateTime now = DateTime.now();
    birthDate = DateTime(now.year - 17, now.month, now.day);
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    final UserManagement userManagement = Provider.of<UserManagement>(context);

    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: SingleChildScrollView(
          reverse: true,
          child: Form(
            key: formKey,
            child: Column(
              children: [
                SizedBox(height: height / 6),
                LogoWidget(),
                SizedBox(height: 40),
                //Date Time Picker
                Container(
                  padding: EdgeInsets.only(left: 15),
                  decoration: BoxDecoration(
                    color: Colors.grey[800],
                    borderRadius: BorderRadius.circular(30),
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(CupertinoIcons.calendar, color: Colors.white),
                      SizedBox(width: 10),
                      Flexible(
                        child: ListTile(
                          contentPadding: EdgeInsets.all(0),
                          title: Text(
                              'Doğum Tarihi: ${birthDate.day}.${birthDate.month}.${birthDate.year}',
                              style: kHintText),
                          trailing: Icon(Icons.arrow_drop_down,
                              color: Colors.grey[700], size: 24),
                          onTap: _datePicker,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.only(left: 15, top: 13, bottom: 13),
                  decoration: BoxDecoration(
                    color: Colors.grey[800],
                    borderRadius: BorderRadius.circular(30),
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(CupertinoIcons.person, color: Colors.white),
                      // Padding(
                      //   padding: const EdgeInsets.only(left: 10.0),
                      //   child: Text(
                      //     'Cinsiyet',
                      //     style: kHintText,
                      //   ),
                      // ),
                      SizedBox(width: 10),
                      Flexible(
                        child: DropdownButtonFormField<String>(
                          //hint: Text('Cinsiyet',style: kHintText,),
                          decoration: InputDecoration.collapsed(
                              hintText: 'Cinsiyet',
                              hintStyle: TextStyle(color: Colors.grey[500])),
                          style: kHintText,
                          items: <String>[
                            'Belirtmek istemiyorum',
                            'Kadın',
                            'Erkek'
                          ].map((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                          onChanged: (_) {
                            this.gender = _!;
                          },
                        ),
                      ),
                      SizedBox(width: 15)
                    ],
                  ),
                ),
                SizedBox(height: 15),
                CustomTextFieldWidget(
                  initialValue: this.refer,
                  textInputType: TextInputType.text,
                  hintText: 'Referans Kullanıcı Adı (İsteğe Bağlı)',
                  icon: Icon(CupertinoIcons.person_2, color: Colors.white),
                  onSaved: (_) {
                    this.refer = _.trim();
                  },
                  validator: (__) {
                    String _ = __.trim();
                    if (_.length == 0) {
                      return null;
                    } else if (_.length < referNameMinLength) {
                      return 'Referans kodu en az $referNameMinLength karakter olmalıdır.';
                    } else if (_.length > referNameMaxLength) {
                      return 'Referans kodu en fazla $referNameMaxLength karakter olmalıdır.';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.only(left: 15, top: 13, bottom: 13),
                  decoration: BoxDecoration(
                    color: Colors.grey[800],
                    borderRadius: BorderRadius.circular(30),
                  ),
                  child: Row(
                    children: [
                      Icon(CupertinoIcons.money_dollar_circle,
                          color: Colors.white),
                      SizedBox(width: 10),
                      Flexible(
                        child: DropdownButtonFormField<String>(
                          decoration: InputDecoration.collapsed(
                              hintText: 'Hediye kazanmak ister misiniz?',
                              hintStyle: TextStyle(color: Colors.grey[500])),
                          style: kHintText,
                          items: <String>[
                            'Evet',
                            'Hayır',
                          ].map((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                          onChanged: (_) {
                            this.canEarn = _.toString() == 'Evet' ? 1 : 0;
                          },
                        ),
                      ),
                      SizedBox(width: 15)
                    ],
                  ),
                ),
                SizedBox(height: 15),
                CustomBottomButtonWidget(
                  text: 'Kaydet',
                  onSuccess: () async {
                    print("selam");
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => LoginNavigator(),
                      ),
                    );
                  },
                  onTap: () async {
                    if (this.formKey.currentState!.validate()) {
                      this.formKey.currentState!.save();
                      if (this.gender == null) {
                        MyInfoDialog(
                          context,
                          title: 'Hata!',
                          description: 'Cinsiyet alanı gereklidir.',
                          buttonText: 'Yeniden Dene',
                          alertType: AlertType.error,
                        );
                        return false;
                      }
                      if (this.birthDate == DateTime.now()) {
                        return false;
                      }

                      User user = await AuthService.include.registerDetail(
                        birthDate: this.birthDate,
                        gender: this.gender,
                        refer: this.refer,
                        canEarn: this.canEarn.toString(),
                        userName: registerUserName == ''
                            ? userManagement.user.userName
                            : registerUserName,
                        password: registerPassword,
                      );

                      if (user.errorMessage != null) {
                        MyInfoDialog(context,
                            title: 'Hata!',
                            description: user.errorMessage!,
                            buttonText: 'Yeniden Dene',
                            alertType: AlertType.error);

                        return false;
                      } else {
                        registerUserName =
                            registerEmail = registerPassword = '';
                        pageViewInitialValue = 0;
                        return true;
                      }
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _datePicker() async {
    DateTime now = DateTime.now();
    await DatePicker.showDatePicker(
      context,
      showTitleActions: true,
      minTime: DateTime(1900, 1, 1),
      maxTime: DateTime(now.year - 17, now.month, now.day),
      onConfirm: (date) {
        if (date != null)
          setState(() {
            birthDate = date;
          });
      },
      currentTime: birthDate == null ? DateTime.now() : birthDate,
      locale: LocaleType.tr,
    );
  }
}
