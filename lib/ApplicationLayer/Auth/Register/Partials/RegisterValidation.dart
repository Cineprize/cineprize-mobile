final userNameMinLength = 3;
final userNameMaxLength = 15;

final referNameMinLength = 3;
final referNameMaxLength = 15;

final emailMinLength = 5;
final emailMaxLength = 50;

final passwordMinLength = 6;
final passwordMaxLength = 255;
