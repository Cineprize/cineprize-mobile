import 'dart:io';

import 'package:cineprize/ApplicationLayer/Auth/AuthManagement/LoginNavigator.dart';
import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/components/IntroductionPage/DotsDecorator.dart';
import 'package:cineprize/ApplicationLayer/components/IntroductionPage/IntroductionScreen.dart';
import 'package:cineprize/ApplicationLayer/components/IntroductionPage/PageDecoration.dart';
import 'package:cineprize/ApplicationLayer/components/IntroductionPage/PageViewModel.dart';
import 'package:cineprize/Tools/SharedPreferences/MySharedPreferences.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class IntroductionPage extends StatefulWidget {
  @override
  _IntroductionPageState createState() => _IntroductionPageState();
}

class _IntroductionPageState extends State<IntroductionPage> {
  final introKey = GlobalKey<IntroductionScreenState>();

  Widget _buildImage(String assetName) => Align(
        child: SvgPicture.asset(
            'assets/images/full_quality_intro_pictures/newintros/$assetName.svg',
            width: 350.0),
        alignment: Alignment.bottomCenter,
      );

  List<Widget> svgs = [
    Align(
      child: SvgPicture.asset(
          'assets/images/full_quality_intro_pictures/newintros/1.svg',
          width: 350.0),
      alignment: Alignment.bottomCenter,
    ),
    Align(
      child: SvgPicture.asset(
          'assets/images/full_quality_intro_pictures/newintros/2.svg',
          width: 350.0),
      alignment: Alignment.bottomCenter,
    ),
    Align(
      child: SvgPicture.asset(
          'assets/images/full_quality_intro_pictures/newintros/3.svg',
          width: 350.0),
      alignment: Alignment.bottomCenter,
    ),
    Align(
      child: SvgPicture.asset(
          'assets/images/full_quality_intro_pictures/newintros/4.svg',
          width: 350.0),
      alignment: Alignment.bottomCenter,
    )
  ];

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    //precacheImage(image1, context);
    precachePicture(
      ExactAssetPicture(SvgPicture.svgStringDecoder,
          'assets/images/full_quality_intro_pictures/newintros/1.svg'),
      context,
    );
    precachePicture(
      ExactAssetPicture(SvgPicture.svgStringDecoder,
          'assets/images/full_quality_intro_pictures/newintros/2.svg'),
      context,
    );
    precachePicture(
      ExactAssetPicture(SvgPicture.svgStringDecoder,
          'assets/images/full_quality_intro_pictures/newintros/3.svg'),
      context,
    );
    precachePicture(
      ExactAssetPicture(SvgPicture.svgStringDecoder,
          'assets/images/full_quality_intro_pictures/newintros/4.svg'),
      context,
    );

    /*precacheImage(image2.image, context);
    precacheImage(image3.image, context);
    precacheImage(image4.image, context);*/
  }

  @override
  Widget build(BuildContext context) {
    TextStyle introductionTextGreen = TextStyle(
        color: Color(0xff3fd490),
        fontSize: MediaQuery.of(context).size.width <= 320 ? 20 : 25,
        fontFamily: 'Balooo');
    TextStyle introductionTextWhite = TextStyle(
        color: Colors.white,
        fontSize: MediaQuery.of(context).size.width <= 320 ? 20 : 25,
        fontFamily: 'Balooo');
    TextStyle bodyStyle = introductionTextWhite;

    var pageDecoration = PageDecoration(
      titleTextStyle: TextStyle(
          fontSize: MediaQuery.of(context).size.width <= 320 ? 22 : 28.0,
          fontWeight: FontWeight.w700),
      bodyTextStyle: bodyStyle,
      descriptionPadding: MediaQuery.of(context).size.width <= 320
          ? EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 8.0)
          : EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
      contentPadding: MediaQuery.of(context).size.width <= 320
          ? EdgeInsets.only(right: 16, left: 16, top: 8)
          : EdgeInsets.all(16),
      footerPadding: MediaQuery.of(context).size.width <= 320
          ? EdgeInsets.zero
          : EdgeInsets.symmetric(vertical: 24.0),
      pageColor: Colors.black,
      imagePadding: EdgeInsets.zero,
    );

    return Platform.isAndroid
        ? IntroductionScreen(
            key: introKey,
            pages: [
              PageViewModel(
                body: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(children: <TextSpan>[
                    TextSpan(
                      text:
                          'Sevdiğin youtuberların Cineprize\'a özel içeriklerini izle, izlediğin her dakika başı ',
                      style: introductionTextWhite,
                    ),
                    TextSpan(text: 'CineCoin ', style: introductionTextGreen),
                    TextSpan(text: 've ', style: introductionTextWhite),
                    TextSpan(text: 'CineGold ', style: introductionTextGreen),
                    TextSpan(text: 'kazan', style: introductionTextWhite)
                  ]),
                ),
                image: svgs[0],
                decoration: pageDecoration,
              ),
              PageViewModel(
                body: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(children: <TextSpan>[
                    TextSpan(
                        text:
                            'Video içerisinde gelen video ile alakalı interaktif tahmin sorularını doğru cevapla, yine ',
                        style: introductionTextWhite),
                    TextSpan(text: 'CineCoin ', style: introductionTextGreen),
                    TextSpan(text: 've ', style: introductionTextWhite),
                    TextSpan(text: 'CineGold ', style: introductionTextGreen),
                    TextSpan(text: 'kazan', style: introductionTextWhite)
                  ]),
                ),
                image: svgs[1],
                decoration: pageDecoration,
              ),
              PageViewModel(
                body: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(children: <TextSpan>[
                    TextSpan(
                        text: 'Sadece canın isterse reklam izleyerek ',
                        style: introductionTextWhite),
                    TextSpan(text: 'CineCoin ', style: introductionTextGreen),
                    TextSpan(text: 've ', style: introductionTextWhite),
                    TextSpan(text: 'CineGold ', style: introductionTextGreen),
                    TextSpan(text: 'kazan', style: introductionTextWhite)
                  ]),
                ),
                image: svgs[2],
                decoration: pageDecoration,
              ),
              PageViewModel(
                body: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(children: <TextSpan>[
                    TextSpan(
                        text: 'Kazandığın tüm ', style: introductionTextWhite),
                    TextSpan(text: 'CineCoin ', style: introductionTextGreen),
                    TextSpan(text: 've ', style: introductionTextWhite),
                    TextSpan(text: 'CineGold ', style: introductionTextGreen),
                    TextSpan(
                        text: '\'ları mağazada dilediğince harca',
                        style: introductionTextWhite)
                  ]),
                ),
                image: svgs[3],
                footer: RaisedButton(
                  onPressed: () async {
                    await MySharedPreferences.setInt(
                        key: 'introductionPage', value: 1);
                    introductionPageViewed = 1;
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => LoginNavigator()));
                  },
                  child: Padding(
                    padding: EdgeInsets.all(5.0),
                    child: Text('Devam',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: MediaQuery.of(context).size.width <= 320
                              ? 20
                              : 30,
                        )),
                  ),
                  color: kAppThemeColor,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0)),
                ),
                decoration: pageDecoration,
              ),
            ],
            // onSkip: () => _onIntroEnd(context), // You can override onSkip callback
            // OnSkip geri aramayı geçersiz kılabilirsiniz
            // showSkipButton: true,
            showNextButton: true,
            showSkipButton: true,
            skipFlex: 0,
            nextFlex: 0,
            /*     skip: const Text(
        'Skip',
        style: TextStyle(color: Colors.white),
      ),*/
            next: const Icon(CupertinoIcons.arrow_right, color: Colors.white),
            done: Row(
              children: [
                Text('Atla',
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        color: Colors.white,
                        fontSize: 19)),
                SizedBox(width: 3),
                Icon(CupertinoIcons.arrow_right, color: Colors.white)
              ],
            ),
            onDone: () async => {
              await MySharedPreferences.setInt(
                  key: 'introductionPage', value: 1),
              introductionPageViewed = 1,
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => LoginNavigator())),
            },
            dotsDecorator: const DotsDecorator(
              size: Size(10.0, 10.0),
              color: Color(0xFFBDBDBD),
              activeSize: Size(22.0, 10.0),
              activeShape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(25.0)),
              ),
            ),
          )
        : IntroductionScreen(
            key: introKey,
            pages: [
              PageViewModel(
                body: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(children: <TextSpan>[
                    TextSpan(
                      text:
                          'Sevdiğin youtuberların Cineprize\'a özel içeriklerini izle, izlediğin her dakika başı ',
                      style: introductionTextWhite,
                    ),
                    TextSpan(text: 'CineCoin ', style: introductionTextGreen),
                    TextSpan(text: 've ', style: introductionTextWhite),
                    TextSpan(text: 'CineGold ', style: introductionTextGreen),
                    TextSpan(text: 'kazan', style: introductionTextWhite)
                  ]),
                ),
                image: svgs[0],
                decoration: pageDecoration,
              ),
              PageViewModel(
                body: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(children: <TextSpan>[
                    TextSpan(
                        text:
                            'Video içerisinde gelen video ile alakalı interaktif tahmin sorularını doğru cevapla, yine ',
                        style: introductionTextWhite),
                    TextSpan(text: 'CineCoin ', style: introductionTextGreen),
                    TextSpan(text: 've ', style: introductionTextWhite),
                    TextSpan(text: 'CineGold ', style: introductionTextGreen),
                    TextSpan(text: 'kazan', style: introductionTextWhite)
                  ]),
                ),
                image: svgs[1],
                decoration: pageDecoration,
              ),
              PageViewModel(
                body: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(children: <TextSpan>[
                    TextSpan(
                        text: 'Kazandığın tüm ', style: introductionTextWhite),
                    TextSpan(text: 'CineCoin ', style: introductionTextGreen),
                    TextSpan(text: 've ', style: introductionTextWhite),
                    TextSpan(text: 'CineGold ', style: introductionTextGreen),
                    TextSpan(
                        text: '\'ları mağazada dilediğince harca',
                        style: introductionTextWhite)
                  ]),
                ),
                image: svgs[3],
                footer: RaisedButton(
                  onPressed: () async {
                    await MySharedPreferences.setInt(
                        key: 'introductionPage', value: 1);
                    introductionPageViewed = 1;
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => LoginNavigator()));
                  },
                  child: Padding(
                    padding: EdgeInsets.all(5.0),
                    child: Text('Devam',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: MediaQuery.of(context).size.width <= 320
                              ? 20
                              : 30,
                        )),
                  ),
                  color: kAppThemeColor,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0)),
                ),
                decoration: pageDecoration,
              ),
            ],
            // onSkip: () => _onIntroEnd(context), // You can override onSkip callback
            // OnSkip geri aramayı geçersiz kılabilirsiniz
            // showSkipButton: true,
            showNextButton: true,
            showSkipButton: true,
            skipFlex: 0,
            nextFlex: 0,
            /*     skip: const Text(
        'Skip',
        style: TextStyle(color: Colors.white),
      ),*/
            next: const Icon(CupertinoIcons.arrow_right, color: Colors.white),
            done: Row(
              children: [
                Text('Atla',
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        color: Colors.white,
                        fontSize: 19)),
                SizedBox(width: 3),
                Icon(CupertinoIcons.arrow_right, color: Colors.white)
              ],
            ),
            onDone: () async => {
              await MySharedPreferences.setInt(
                  key: 'introductionPage', value: 1),
              introductionPageViewed = 1,
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => LoginNavigator())),
            },
            dotsDecorator: const DotsDecorator(
              size: Size(10.0, 10.0),
              color: Color(0xFFBDBDBD),
              activeSize: Size(22.0, 10.0),
              activeShape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(25.0)),
              ),
            ),
          );
  }
}
