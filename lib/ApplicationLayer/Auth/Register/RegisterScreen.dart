import 'package:cineprize/ApplicationLayer/Auth/AuthManagement/LoginNavigator.dart';
import 'package:cineprize/ApplicationLayer/Auth/PageView.dart';
import 'package:cineprize/ApplicationLayer/Auth/Register/Partials/RegisterValidation.dart';
import 'package:cineprize/ApplicationLayer/Components/CustomBottomButtonWidget.dart';
import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Tools/MyAlertDialogs/MyInfoDialog.dart';
import 'package:cineprize/ApplicationLayer/components/Agreement Dialog/AgreementDialog.dart'
    as fullDialog;
import 'package:cineprize/ApplicationLayer/components/CustomObsecureTextFieldWidget.dart';
import 'package:cineprize/ApplicationLayer/components/CustomTextFieldWidget.dart';
import 'package:cineprize/ApplicationLayer/components/LogoWidget.dart';
import 'package:cineprize/ApplicationLayer/components/PageButtonsWidget.dart';
import 'package:cineprize/Models/User.dart';
import 'package:cineprize/ServiceLayer/Auth/Rest/AuthService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import 'RegisterDetailScreen.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  bool checkBoxState = false;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  String email = registerEmail,
      password = registerPassword,
      userName = registerUserName;

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
        resizeToAvoidBottomInset: true,
        key: _scaffoldKey,
        body: Padding(
          padding: const EdgeInsets.all(15.0),
          child: SingleChildScrollView(
            reverse: true,
            child: Form(
              key: formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(height: (height / 4.4) + 2),
                  LogoWidget(),
                  SizedBox(height: 40),
                  ButtonBar(
                    alignment: MainAxisAlignment.spaceAround,
                    children: [
                      PageButtonsWidget(
                        text: 'Giriş Yap',
                        textColor: Colors.white,
                        onPressed: () {
                          setState(() {
                            controller
                                .jumpToPage(pageViewInitialValue == 1 ? 1 : 0);
                          });
                        },
                      ),
                      PageButtonsWidget(
                          text: 'Kayıt Ol',
                          textColor: kAppThemeColor,
                          onPressed: () {})
                    ],
                  ),
                  CustomTextFieldWidget(
                    textInputType: TextInputType.text,
                    initialValue: this.userName,
                    hintText: 'Kullanıcı Adı',
                    icon: Icon(CupertinoIcons.checkmark_shield,
                        color: Colors.white),
                    //formatter: UpperCaseTextFormatter(),
                    onSaved: (_) {
                      this.userName = _.trim();
                    },
                    validator: (__) {
                      String _ = __.trim();
                      if (_.length == 0) {
                        return 'Kullanıcı adı alanı boş geçilemez.';
                      } else if (_.length < userNameMinLength) {
                        return 'Kullanıcı adı en az $userNameMinLength karakter olmalıdır.';
                      } else if (_.length > userNameMaxLength) {
                        return 'Kullanıcı adı en fazla $userNameMaxLength karakter olmalıdır.';
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: 15),
                  CustomTextFieldWidget(
                    initialValue: this.email,
                    hintText: 'E-posta',
                    icon: Icon(CupertinoIcons.mail, color: Colors.white),
                    textInputType: TextInputType.emailAddress,
                    onSaved: (_) {
                      this.email = _.trim();
                    },
                    validator: (__) {
                      String _ = __.trim();
                      if (_.length == 0) {
                        return 'E-posta alanı boş geçilemez.';
                      } else if (_.length < emailMinLength) {
                        return 'E-posta en az $emailMinLength karakter olmalıdır.';
                      } else if (_.length > emailMaxLength) {
                        return 'E-posta en fazla $emailMaxLength karakter olmalıdır.';
                      } else if (!_.contains(RegExp(
                          r'(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)'))) {
                        return 'Lütfen geçerli bir e-posta adresi giriniz.';
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: 15),
                  CustomObsecureTextFieldWidget(
                    initialValue: this.password,
                    hintText: 'Parola',
                    icon: Icon(CupertinoIcons.lock, color: Colors.white),
                    onSaved: (_) {
                      this.password = _.trim();
                    },
                    validator: (__) {
                      String _ = __.trim();
                      if (_.length == 0) {
                        return 'Parola alanı boş geçilemez.';
                      } else if (_.length < passwordMinLength) {
                        return 'Parola en az $passwordMinLength karakter olmalıdır.';
                      } else if (_.length > passwordMaxLength) {
                        return 'Parola en fazla $passwordMaxLength karakter olmalıdır.';
                      }
                      return null;
                    },
                    isObsecure: true,
                  ),
                  // SizedBox(height: 15),
                  Row(
                    children: [
                      Checkbox(
                          activeColor: Color(0xff3fd490),
                          checkColor: Colors.white,
                          focusColor: Colors.white,
                          value: checkBoxState,
                          onChanged: (bool? value) {
                            setState(() {
                              checkBoxState = value!;
                            });
                          }),
                      GestureDetector(
                        onTap: () {
                          _openAgreeDialog(context);
                        },
                        child: RichText(
                          text: TextSpan(
                            children: <TextSpan>[
                              TextSpan(
                                  text: 'Üyelik Sözleşmesi',
                                  style: TextStyle(color: kAppThemeColor)),
                              TextSpan(text: '\'ni okudum, onaylıyorum.')
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  // SizedBox(height: 15),
                  CustomBottomButtonWidget(
                    text: 'Kayıt Ol',
                    onSuccess: () async {
                      Navigator.pushReplacement(
                        loginNavigatorContext!,
                        MaterialPageRoute(
                          builder: (loginNavigatorContext) =>
                              RegisterDetailScreen(),
                        ),
                      );
                    },
                    onTap: () async {
                      if (checkBoxState) {
                        if (this.formKey.currentState!.validate()) {
                          this.formKey.currentState!.save();

                          //Global Variables
                          registerUserName = this.userName;
                          registerEmail = this.email;
                          registerPassword = this.password;
                          pageViewInitialValue = 1;

                          User user = await AuthService.include.register(
                            userName: this.userName,
                            email: this.email,
                            password: this.password,
                          );
                          if (user.errorMessage != null) {
                            MyInfoDialog(loginNavigatorContext!,
                                title: 'Hata!',
                                description: user.errorMessage!,
                                buttonText: 'Yeniden Dene',
                                alertType: AlertType.error);
                            return false;
                          } else {
                            return true;
                          }
                        }
                      } else {
                        MyInfoDialog(loginNavigatorContext!,
                            title: 'Onay',
                            description: 'Üyelik Sözleşmesi kabul edilmelidir',
                            buttonText: 'Tamam',
                            alertType: AlertType.warning);
                        return false;
                      }
                    },
                  ),
                ],
              ),
            ),
          ),
        ));
  }

  Future _openAgreeDialog(context) async {
    String result = await Navigator.of(context).push(
      MaterialPageRoute(
          fullscreenDialog: true,
          builder: (BuildContext context) {
            //true to display with a dismiss button rather than a return navigation arrow
            return fullDialog.CreateAgreement(checkBoxState: checkBoxState);
          }),
    );
    if (result == 'accepted')
      setState(() {
        checkBoxState = true;
      });
  }
}

class UpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return TextEditingValue(
      text: newValue.text.toLowerCase(),
      selection: newValue.selection,
    );
  }
}
