import 'dart:async';

import 'package:cineprize/ApplicationLayer/Components/CustomBottomButtonWidget.dart';
import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Tools/MyAlertDialogs/MyInfoDialog.dart';
import 'package:cineprize/ApplicationLayer/components/CustomTextFieldWidget.dart';
import 'package:cineprize/ApplicationLayer/components/LogoWidget.dart';

import 'package:cineprize/ServiceLayer/Auth/Verification/PhoneVerificationService.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
/* import 'package:easy_mask/easy_mask.dart'; */
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class PhoneVerification extends StatefulWidget {
  const PhoneVerification({Key? key}) : super(key: key);

  @override
  _PhoneVerificationState createState() => _PhoneVerificationState();
}

class _PhoneVerificationState extends State<PhoneVerification> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  String phone = "", code = "";
  bool onProccess = false;
  bool codeSent = false;
  bool initialized = false;
  late Timer _timer;
  int time = 59;

  void startTimer({time = 59}) {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) {
        if (time == 0) {
          setState(() {
            timer.cancel();
          });
        } else {
          setState(() {
            time--;
            this.time = time;
          });
        }
      },
    );
  }

  @override
  void initState() {
    PhoneVerificationService.include.checkCode().then((response) {
      if (response.success) {
        setState(() {
          this.phone = response.data['phone'].replaceAllMapped(
              RegExp(r'(\d{3})(\d{3})(\d{2})(\d{2})'),
              (Match m) => "(${m[1]}) ${m[2]} ${m[3]} ${m[4]}");
          startTimer(time: response.data['remainingTime']);
          codeSent = true;
          initialized = true;
        });
      } else {
        setState(() {
          initialized = true;
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    final UserManagement userManagement = Provider.of<UserManagement>(context);

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        automaticallyImplyLeading: false,
        backgroundColor: Colors.black,
        leading: TextButton(
          style: ButtonStyle(
            overlayColor: MaterialStateProperty.all<Color>(Colors.transparent),
          ),
          onPressed: () {
            Navigator.pop(context, {'success': false});
          },
          child: Icon(
            CupertinoIcons.back,
            color: Colors.white,
            size: 30,
          ),
        ),
      ),
      resizeToAvoidBottomInset: true,
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: SingleChildScrollView(
          reverse: true,
          child: Form(
            key: formKey,
            child: initialized
                ? Column(
                    children: [
                      SizedBox(height: height / 6),
                      LogoWidget(),
                      SizedBox(height: 40),
                      CustomTextFieldWidget(
                        textInputType: TextInputType.number,
                        hintText: 'Telefon numaranız',
                        initialValue: this.phone,
                        readOnly: onProccess
                            ? true
                            : (codeSent ? (time != 0) : false),
                        icon: Icon(Icons.phone, color: Colors.white),
                        formatters: [phoneFormatter],
                        onSaved: (_) {
                          if (_.trim() != '') {
                            this.phone = _;
                          }
                        },
                        validator: (__) {
                          String _ = __;
                          if (_.length == 0) {
                            return null;
                          } else if (_.length < 15) {
                            setState(() {
                              onProccess = false;
                            });
                            return 'Telefon numarası en az 15 karakter olmalıdır.';
                          }
                          return null;
                        },
                      ),
                      SizedBox(height: 15),
                      CustomBottomButtonWidget(
                        text: codeSent
                            ? time != 0
                                ? 'Kalan süre ' +
                                    (((time / 60).truncate().toString()) +
                                        ':' +
                                        (time % 60).toString().padLeft(2, '0'))
                                : 'Tekrar Gönder'
                            : 'Kod Gönder',
                        onTap: initialized &&
                                (!codeSent || (codeSent && time == 0))
                            ? () async {
                                setState(() {
                                  onProccess = true;
                                });
                                if (this.formKey.currentState!.validate()) {
                                  this.formKey.currentState!.save();
                                  if (this.phone == null) {
                                    MyInfoDialog(
                                      context,
                                      title: 'Hata!',
                                      description:
                                          'Telefon numarası alanı gereklidir.',
                                      buttonText: 'Yeniden Dene',
                                      alertType: AlertType.error,
                                    );
                                    setState(() {
                                      onProccess = false;
                                    });
                                    return false;
                                  }

                                  var response = await PhoneVerificationService
                                      .include
                                      .sendCode(phone: this.phone);

                                  MyInfoDialog(
                                    context,
                                    title: response.success
                                        ? 'Başarılı!'
                                        : 'Hata!',
                                    description: response.message,
                                    buttonText: response.success
                                        ? 'Tamam'
                                        : 'Yeniden Dene',
                                    alertType: response.success
                                        ? AlertType.success
                                        : AlertType.error,
                                    func: () {
                                      if (response.success) {
                                        setState(() {
                                          startTimer(
                                              time: response
                                                  .data['remainingTime']);
                                          codeSent = true;
                                          onProccess = false;
                                        });
                                      } else {
                                        if (response.data['remainingTime'] !=
                                            null) {
                                          setState(() {
                                            startTimer(
                                                time: response
                                                    .data['remainingTime']);
                                            codeSent = true;
                                            onProccess = false;
                                          });
                                          return true;
                                        } else {
                                          setState(() {
                                            codeSent = false;
                                            onProccess = false;
                                          });
                                          Navigator.pop(
                                              context, {'success': false});
                                          return false;
                                        }
                                      }
                                    },
                                  );
                                  return true;
                                }
                              }
                            : () {},
                      ),
                      SizedBox(height: 15),
                      codeSent
                          ? Column(
                              children: [
                                CustomTextFieldWidget(
                                  initialValue: this.code,
                                  textInputType: TextInputType.number,
                                  hintText: 'Doğrulama kodu',
                                  readOnly: onProccess,
                                  icon: Icon(Icons.shield, color: Colors.white),
                                  /* formatters: [phoneFormatter], */
                                  onSaved: (_) {
                                    this.code = _;
                                  },
                                  validator: (__) {
                                    String _ = __;
                                    if (_.length == 0) {
                                      return null;
                                    } else if (_.length < 6) {
                                      return 'Doğrulama kodu en az 6 karakter olmalıdır.';
                                    }
                                    return null;
                                  },
                                ),
                                SizedBox(height: 15),
                              ],
                            )
                          : Container(),
                      codeSent
                          ? CustomBottomButtonWidget(
                              text: 'Kodu Doğrula',
                              onTap: () async {
                                setState(() {
                                  onProccess = true;
                                });
                                if (this.formKey.currentState!.validate()) {
                                  this.formKey.currentState!.save();
                                  if (this.phone == null) {
                                    MyInfoDialog(
                                      context,
                                      title: 'Hata!',
                                      description:
                                          'Telefon numarası alanı gereklidir.',
                                      buttonText: 'Yeniden Dene',
                                      alertType: AlertType.error,
                                    );
                                    setState(() {
                                      onProccess = false;
                                    });
                                    return false;
                                  }

                                  if (this.code == null && this.codeSent) {
                                    MyInfoDialog(
                                      context,
                                      title: 'Hata!',
                                      description:
                                          'Onay kodu alanı gereklidir.',
                                      buttonText: 'Yeniden Dene',
                                      alertType: AlertType.error,
                                    );
                                    setState(() {
                                      onProccess = false;
                                    });
                                    return false;
                                  }

                                  var response = await PhoneVerificationService
                                      .include
                                      .verifyCode(
                                          phone: this.phone, code: this.code);

                                  setState(() {
                                    onProccess = false;
                                  });
                                  if (response.success) {
                                    try {
                                      UserManagement.include.user.phoneNumber =
                                          this.phone;
                                      Navigator.pop(context, {
                                        'success': true,
                                        'phone': this.phone
                                      });
                                      return true;
                                    } catch (e) {
                                      MyInfoDialog(context,
                                          title: 'Hata!',
                                          description:
                                              "Bir hata oluştu! Lütfen daha sonra tekrar deneyin.",
                                          buttonText: 'Tamam',
                                          alertType: AlertType.error);
                                      Navigator.pop(context, {'success': true});
                                      return false;
                                    }
                                  } else {
                                    MyInfoDialog(context,
                                        title: response.success
                                            ? 'Başarılı!'
                                            : 'Hata!',
                                        description: response.message,
                                        buttonText: response.success
                                            ? 'Tamam'
                                            : 'Yeniden Dene',
                                        alertType: response.success
                                            ? AlertType.success
                                            : AlertType.error);
                                    return response.success;
                                  }
                                }
                              },
                            )
                          : Container(),
                    ],
                  )
                : Container(),
          ),
        ),
      ),
    );
  }
}
