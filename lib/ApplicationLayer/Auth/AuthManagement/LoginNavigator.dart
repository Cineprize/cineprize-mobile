import 'dart:io';

import 'package:cineprize/ApplicationLayer/Auth/PageView.dart';
import 'package:cineprize/ApplicationLayer/Auth/Register/RegisterDetailScreen.dart';
import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Home/CupertinoManagement.dart';
import 'package:cineprize/ApplicationLayer/Home/RedirectivePage.dart';
import 'package:cineprize/ApplicationLayer/LoadingScreen/LoadingScreen.dart';
import 'package:cineprize/CommonLayer/AppStatus/AppStatuses.dart';
import 'package:cineprize/Models/Response.dart';
import 'package:cineprize/ServiceLayer/Auth/Rest/CurrentUser/CurrentUserService.dart';
import 'package:cineprize/ServiceLayer/CRUD/Rest/AppStatus/AppStatusService.dart';
import 'package:cineprize/ServiceLayer/Management/ErrorLogManagement.dart';
import 'package:cineprize/ServiceLayer/Management/StateManagement.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:url_launcher/url_launcher.dart';

BuildContext? loginNavigatorContext = null;

var registerUserName = '',
    registerEmail = '',
    registerPassword = '',
    pageViewInitialValue = 0;
var loginEmail = '', loginPassword = '';
var introductionPageViewed = 0;
ImageProvider? profilePhoto;

/* List<String> sandboxDevices = [
  'E25DB375-B403-4252-A74A-C05DCBA56818', // Apple iPad Wifi
  '054075E0-2359-420D-9931-89AEFDF4EA5A', // Apple iPhone Xr
]; */

/* bool isSandbox = false; */

class LoginNavigator extends StatefulWidget {
  @override
  _LoginNavigatorState createState() => _LoginNavigatorState();
}

class _LoginNavigatorState extends State<LoginNavigator> {
  Future<Response>? appStatusFuture;
  TabItemEnum currentTab = TabItemEnum.Home;
  @override
  void initState() {
    this.appStatusFuture = AppStatusService.include.control();
  }

  @override
  Widget build(BuildContext context) {
    Provider.of<CurrentUserService>(context);
    loginNavigatorContext = context;

    StateManagement stateManagement = Provider.of<StateManagement>(context);
    UserManagement userManagement = Provider.of<UserManagement>(context);

    // uygulamanın arka plan resmi cacheleniyor
    precacheImage(AssetImage('assets/images/Home/background.png'), context);

    if (introductionPageViewed == 0) {
      //Intro page e girecekse cachelenecek
      precachePicture(
          ExactAssetPicture(SvgPicture.svgStringDecoder,
              'assets/images/full_quality_intro_pictures/newintros/1.svg'),
          context);
    }

    try {
      switch (stateManagement.state) {
        case ViewStateEnum.Idle:
          return FutureBuilder<Response>(
              future: this.appStatusFuture,
              initialData: Response(
                  success: false, message: 'CinePrize sürümün güncel.'),
              // ignore: missing_return
              builder:
                  // ignore: missing_return
                  (BuildContext context, AsyncSnapshot<Response> snapshot) {
                Response data = snapshot.data!;

                if (snapshot.connectionState == ConnectionState.waiting) {
                  return LoadingScreen();
                }

                if (snapshot.hasData) {
                  if (data.success) {
                    return Container(
                      decoration: kMainPagesDecoration,
                      child: Scaffold(
                        backgroundColor: Colors.transparent,
                        body: Center(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16),
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(data.message,
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 20)),
                                  if (data.data['type'] == MANDATORY_UPDATE)
                                    Column(
                                      children: [
                                        SizedBox(height: 15),
                                        DialogButton(
                                          color:
                                              Color.fromRGBO(0, 179, 134, 1.0),
                                          child: Text(
                                              'Güncellemek için hemen tıkla!',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 18)),
                                          onPressed: () async => {
                                            await launchURL(
                                                data.data['link'] as String)
                                          },
                                        )
                                      ],
                                    ),
                                ]),
                          ),
                        ),
                      ),
                    );
                  } else {
                    if (userManagement.user.uid == null) {
                      if (pageViewInitialValue == 0) {
                        return pageView;
                      }
                      return pageViewRegister;
                    }
                    if (userManagement.user.birthDate == null) {
                      return RegisterDetailScreen();
                    }

                    /* getId().then((deviceID) {
                      isSandbox = sandboxDevices.contains(deviceID);
                    }); */

                    return returnRedirectivePage();
                  }
                } else {
                  return LoadingScreen();
                }
              });
        case ViewStateEnum.Busy:
          print("şuan bizi");
          return LoadingScreen();
        default:
          return LoadingScreen();
      }
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'ApplicationLayer > LoginNavigator',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());
      return LoadingScreen();
    }
  }

  returnRedirectivePage() {
    return WillPopScope(
      onWillPop: () async =>
          !await TabItem.navigatorKeys[currentTab]!.currentState!.maybePop(),
      child: RedirectivePage(
          navigatorKeys: TabItem.navigatorKeys,
          currentTab: currentTab,
          selectedTabItem: TabItem.getSelectedPage()),
    );
  }
}

launchURL(String url) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'İstek başarısız! $url';
  }
}

Future<String> getId() async {
  final DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  if (Platform.isIOS) {
    return (await deviceInfo.iosInfo).identifierForVendor; // unique ID on iOS
  } else if (Platform.isAndroid) {
    return (await deviceInfo.androidInfo).androidId; // unique ID on Android
  } else {
    return "";
  }
}

void showSnackBar(BuildContext context, String text, double width,
    {Color color = Colors.green,
    List<String>? buttonText,
    List<Function()>? buttonAction,
    int duration = 500}) {
  Scaffold.of(context).showSnackBar(
    SnackBar(
      elevation: 6.0,
      behavior: SnackBarBehavior.floating,
      duration: new Duration(milliseconds: duration),
      backgroundColor: color,
      content: buttonText != null && buttonAction != null
          ? Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: width / 2,
                  child: Text(
                    text,
                    maxLines: 2,
                    style: kNormalText,
                    textAlign: TextAlign.left,
                  ),
                ),
                Container(
                  width: width / 3,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      GestureDetector(
                          child: Text(buttonText[0].toString(),
                              style: kNormalText),
                          onTap: buttonAction[0]),
                      GestureDetector(
                          child: Text(buttonText[1].toString(),
                              style: kNormalText),
                          onTap: buttonAction[1])
                    ],
                  ),
                ),
              ],
            )
          : Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                    child: Text(text,
                        maxLines: 4,
                        style: kNormalText,
                        textAlign: TextAlign.center))
              ],
            ),
    ),
  );
}
