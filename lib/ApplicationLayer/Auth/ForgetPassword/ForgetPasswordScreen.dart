import 'package:cineprize/ApplicationLayer/Auth/AuthManagement/LoginNavigator.dart';
import 'package:cineprize/ApplicationLayer/Auth/Register/Partials/RegisterValidation.dart';
import 'package:cineprize/ApplicationLayer/Tools/MyAlertDialogs/MyInfoDialog.dart';
import 'package:cineprize/ApplicationLayer/components/CustomBottomButtonWidget.dart';
import 'package:cineprize/ApplicationLayer/components/CustomTextFieldWidget.dart';
import 'package:cineprize/ApplicationLayer/components/LogoWidget.dart';
import 'package:cineprize/ServiceLayer/Auth/Rest/AuthService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class ForgetPasswordScreen extends StatefulWidget {
  const ForgetPasswordScreen({Key? key}) : super(key: key);

  @override
  _ForgetPasswordScreenState createState() => _ForgetPasswordScreenState();
}

class _ForgetPasswordScreenState extends State<ForgetPasswordScreen> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  String email = "";
  bool isFetching = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: SingleChildScrollView(
          reverse: true,
          child: Form(
            key: formKey,
            child: Column(
              children: [
                SizedBox(height: height / 4),
                LogoWidget(),
                SizedBox(height: 40),
                SizedBox(height: 15),
                CustomTextFieldWidget(
                  initialValue: this.email,
                  hintText: 'E-posta',
                  icon: Icon(CupertinoIcons.mail, color: Colors.white),
                  textInputType: TextInputType.emailAddress,
                  onSaved: (_) {
                    this.email = _.trim();
                  },
                  validator: (__) {
                    String _ = __.trim();
                    if (_.length == 0) {
                      return 'E-posta alanı boş geçilemez.';
                    } else if (_.length < emailMinLength) {
                      return 'E-posta en az $emailMinLength karakter olmalıdır.';
                    } else if (_.length > emailMaxLength) {
                      return 'E-posta en fazla $emailMaxLength karakter olmalıdır.';
                    } else if (!_.contains(RegExp(
                        r'(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)'))) {
                      return 'Lütfen geçerli bir e-posta adresi giriniz.';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 15),
                CustomBottomButtonWidget(
                  text: 'Şifremi sıfırla',
                  onTap: () async {
                    if (this.formKey.currentState!.validate()) {
                      this.formKey.currentState!.save();
                      var response = await AuthService.include
                          .forgetPassword(email: this.email);
                      MyInfoDialog(
                        context,
                        title: response.success ? 'Başarılı!' : 'Hata!',
                        description: response.message,
                        buttonText: 'Tamam',
                        alertType: response.success
                            ? AlertType.success
                            : AlertType.error,
                        func: () {
                          if (response.success) Navigator.of(context).pop();
                        },
                      );
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
