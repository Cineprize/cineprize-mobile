import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

//Formatter
TextInputFormatter phoneFormatter = MaskTextInputFormatter(
    mask: '(###) ### ## ##', filter: {"#": RegExp(r'[0-9]')});

// Logo
const String logo = 'assets/images/cineprize_logo.png';

//Gif Place Holder
const String gifPlaceholder = 'assets/images/cineprize_logo.png';

//Profile Gif Place Holder
const String profileGifPlaceholder = 'assets/images/profileGifPlaceholder.png';

//App Logo Link
const String appLogo =
    'https://www.linkpicture.com/q/cineprize_logo_small_1.png';

//Colors
const Color kAppThemeColor = Color(0xff3fd490);
const Color kAppBlack = Color(0xff181d21);

Color skeletonColor = Colors.grey[50]!.withOpacity(0.5);
Color skeletonTextColor = Colors.grey[50]!.withOpacity(0.3);
Color shimmerColor = Colors.white24;
//*******************************

//Pop-Up
const Color kPopUpBackgroundColor = Color(0xff1a1d21);
const Color kPopUpButtonColor = Color(0xff1a1d21);
const Color kPopUpButtonBorderColor = Colors.grey;

const TextStyle kNormalText = TextStyle(color: Colors.white);

const TextStyle kBoldText =
    TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18);

const TextStyle kSmallText = TextStyle(color: Colors.white, fontSize: 13);

const TextStyle kBigText =
    TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold);

const TextStyle kBigTextLight =
    TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.normal);

const TextStyle kHeader =
    TextStyle(color: Colors.white, fontSize: 22, fontWeight: FontWeight.bold);

const TextStyle kHintText = TextStyle(color: Colors.grey, fontSize: 15);

const TextStyle kIntroductionTextGreen =
    TextStyle(color: Color(0xff3fd490), fontSize: 25, fontFamily: 'Balooo');

const TextStyle kIntroductionTextWhite =
    TextStyle(color: Colors.white, fontSize: 25, fontFamily: 'Balooo');

const TextStyle kSurveyText = TextStyle(color: Colors.white, fontSize: 15);

const EdgeInsets kAppBarActionsPadding = EdgeInsets.only(right: 20.0);

//Main Page's Decoration (TrendsPage,CategoriesPage,HomePage,ProfilePage and StorePage)
const BoxDecoration kMainPagesDecoration = BoxDecoration(
  image: DecorationImage(
    image: AssetImage('assets/images/Home/background.png'),
    fit: BoxFit.cover,
  ),
);

//Bottom Navigation Bar's Constants
const Color kBottomNavigationBarIconInActive = Colors.white;
const Color kBottomNavigationBarIconActive = Color(0xff3fd490);
const Color kBottomNavigationBarBackgroundColor = Color(0xff191d21);

//HomePage AppBar Constants
const double kHomePageAppBarIconSize = 27;

//Splash Screen constants
const Color kSplashScreenBackgroundColor = Colors.black;

const kInputDecoration = InputDecoration(
  hintText: 'Enter a value.',
  contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
  border: OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(32.0)),
  ),
  enabledBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Colors.black, width: 1.0),
    borderRadius: BorderRadius.all(Radius.circular(32.0)),
  ),
  focusedBorder: OutlineInputBorder(
    borderSide:
        //email and password border
        BorderSide(color: Colors.black, width: 2.0),
    borderRadius: BorderRadius.all(Radius.circular(32.0)),
  ),
);

const loadingIndicator = CircularProgressIndicator(
  strokeWidth: 1,
  valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
);

const kHintStyle = TextStyle(color: Colors.white);
const userAgreement = '''
KULLANICI SÖZLEŞMESİ

CinePrize Kullanıcı ve Gizlilik sözleşmesi. (Bundan sonra kısaca sözleşme olarak anılacaktır.) Akıllı Ambalaj Yazılım A.Ş (Bundan sonra kısaca “CinePrize” olarak anılacaktır.) CinePrize uygulamasına üye olarak giriş yapan ziyaretçiler (Bundan sonra kısaca “Kullanıcı” olarak anılacaktır.) arasında akdedilmiştir.
 
ÖNEMLİ NOT: APPLE BİR SPONSOR DEĞİLDİR VE BU YARIŞMA İLE BİR İLGİSİ BULUNMAMAKTADIR

1. Kullanıcı, CinePrize uygulamasına (App Store / Google Play üzerinden mobil cihazına indirerek)
üye olarak bu sözleşmeyi kabul etmiş sayılmaktadır.
 
2. Kullanıcı, CinePrize uygulamasını kullanarak bu sözleşme’ye, CinePrize tarafından yayınlanan ve yayınlanacak olan diğer her türlü kullanım koşuluna ve Türkiye Cumhuriyeti mevzuatına uygun davranmayı taahhüt etmiş sayılmaktadır. Aksi halde; tüm hukuki ve cezai sorumluluk Kullanıcı’ya ait olacaktır.
 
3. Üyelik hesabında hukuka aykırılık unsuru taşıyan hiçbir veri yer almamalıdır. Başkası gibi davranarak, izin almaksızın herhangi bir bireye ait bir isim kullanarak ya da insanları kışkırtacak bir kullanıcı adı oluşturulamaz.
 
4. Kullanıcı, üyelik hesabı için doğru ve eksiksiz bir biçimde bilgi sağlama ve bu bilgileri sürekli güncel tutmakla yükümlüdür.
 
5. Kullanıcı, CinePrize uygulamasında yer alan “Üyeliğimi Sil” butonuna tıklayarak üyeliğini kapatabilir. Üyelik hesabı kapatıldıktan 30 gün sonra tüm veriler istisnasız silinir.
 
6. Kullanıcı, CinePrize’ın dilediği zaman üyelerini herhangi bir sebeble ödüllendirebileceği gibi tek taraflı olarak üyeliğine son verebileceğini, üyelik hesabını geçici ya da kalıcı olarak kapatabileceğini, üyelik hesabındaki bilgileri silebileceğini, üyelerine sağladığı puan / puanları silebileceğini, böyle bir durumda CinePrize’den hiçbir talepte bulunamayacağını gayrikabili
rücu biçimde kabul etmiştir.
 
7. Üyeler tarafından 1 (bir) yıl boyunca işletilmeyen, 1 (bir) yıl boyunca CinePrize’ye giriş yapılmayan veya güncellenmeyen hesaplar CinePrize tarafından silinebilir. Kullanıcı silme işleminden sonra hak ettiği tüm puan ve ödülleri kaybetmiş olur.
 
8. CinePrize alan adı ile üyelerin hesapları da dahil olmak üzere alt alan adlarına ilişkin tüm haklar CinePrize’ye aittir. Üyelerin, üyelik hesaplarının bulunduğu alt alan adları üzerinde hiçbir hakkı bulunmamaktadır. Silinen üyelik hesaplarının alt alan adları farklı üyelerce kullanılabilir.
 
9. CinePrize, herhangi bir içeriği, herhangi bir zamanda içeriği yükleyen Kullanıcı’ya haber vermeksizin ve herhangi bir sebep göstermeksizin kendi takdiri doğrultusunda kaldırma düzenleme, değiştirme, engelleme hakkına sahiptir. Kullanıcı tarafından sağlanan içeriklerden
CinePrize hiçbir şekilde sorumlu değildir. CinePrize, ilkelerine ve hukuka aykırı olan içerikleri - sayfaları yayından kaldırma hakkına sahip olup; böyle bir durumda Kullanıcı, CinePrize’den hiçbir talepte bulunmayacağını gayrikabili rücu biçimde kabul etmiştir.
 
10. CinePrize’de sunulan hizmetleri kullanmak dışında, CinePrize’de yer alan içerikleri başka amaçlar için kullanmak, taklit etmek, değiştirmek, dağıtımını yapmak veya depolamak yasaktır. Kullanıcı, CinePrize’de yer alan içerikleri satamaz, reklam gibi amaçlar doğrultusunda istismar edemez ve üçüncü kişilerin hakkını zedeleyecek şekilde kullanamaz.
 
11. CinePrize’de yayınlanan hiçbir içeriğin üçüncü kişiler tarafından yayın hakkı içerik sahibinden izin alınmadan hiçbir mecrada hiçbir şekilde kullanılamaz, yayınlanamaz.
 
12. CinePrize uygulama belli alanlarını veya kategorilerini, CinePrize’de kullanılan bazı hizmetleri ücretli hale getirebilir ve bu ücretleri tek taraflı olarak belirleyebilir.
 
13. CinePrize, üyelik hesaplarının güvenliği için azami dikkat gösterecektir. Ancak üyelik hesabında kayıtlı bilgiler ve hesap şifresinin güvenliği Kullanıcı’nın sorumluluğundadır. Kullanıcı, üyelik hesabına ilişkin bilgileri hiçbir zaman 3. kişilerle paylaşmamalıdır. Üyelik hesabında Kullanıcı’nın kontrolü dışında bir durum oluşması durumda derhal CinePrize ile irtibata geçilmelidir. Kullanıcı’nın üyelik hesabı şifresinin 3. kişiler tarafından ele geçirilmesi ve 3. kişiler tarafından hukuka aykırı içerik sağlanması dahil olmak üzere her türlü kötü niyetli kullanım, Kullanıcı’nın sorumluluğunda olup; kötü niyetli kullanımdan kaynaklanan her türlü hukuki ve cezai yükümlülük Kullanıcı’ya aittir.
 
14. Kullanıcı, diğer kullanıcı ve üyelerin CinePrize’ı kullanmasını engelleyecek ya da zorlaştıracak veya veritabanlarını ya da sunucuları bozacak veya işlemez hale getirecek eylemlerde bulunamaz, herhangi bir yazılım, donanım veya iletişim unsuruna zarar veremez, virüs içeren yazılım veya başka bir bilgisayar kodu, dosya oluşturamaz, yetkisi olmayan herhangi bir sisteme, dataya, şifreye ulaşmaya çalışamaz, direkt veya dolaylı olarak, verilen hizmetlerdeki algoritmaları ve kodları deşifre edecek, işlevlerini bozacak davranışlarda bulunamaz, içerikleri değiştiremez, dönüştüremez, çeviremez, alıntı göstermeksizin başka sitelerde yayınlayamaz, kanunen yasak bilgiler içeren mesajlar, zincir posta, yazılım virüsü gibi 3. kişilere zarar verebilecek içerikleri dağıtamaz, diğer kullanıcıların bilgisayarındaki bilgilere ya da yazılıma zarar verecek program ve/veya bilgiler gönderemez, bunun gibi hukuka aykırı ve CinePrize ile Akıllı Ambalaj Yazılım A.Ş’ye zarar verecek davranışlarda bulunamaz. Aksi halde; CinePrize tarafından tüm hukuki ve cezai işlemleri başlatma ve taleplerde bulunma hakkı saklıdır.
 
15. CinePrize, Kullanıcı’nın bu Sözleşme’ye aykırı davranması, CinePrize uygulamasını kullanırken ya da CinePrize uygulaması’na üye olurken verdiği bilgilerin hatalı veya gerçeğe aykırı olması, CinePrize’da verilen hizmetleri kötü niyetli şekilde kullanması veya CinePrize’ın gerekli görmesi halinde CinePrize uygulamasını kullanmasını engelleyebilir veya üyeliğini gerekçe göstermeksizin sona erdirebilir. Bu durumlarda CinePrize’ın ayrıca hukuki yollara başvurma hakkı saklıdır.
 
16. Fotoğraf, tarif, metot, kod, program, işleyiş tasarım, logo, resim, yazı dahil olmak üzere CinePrize uygulamasında yer alan tüm içerikler ile CinePrize markası ve cineprize.com alan adı, alt alan adları ve tüm sayfalara ilişkin tüm fikri ve sınai mülkiyet hakları CinePrize’a aittir veya CinePrize tarafından bunların kullanımına ilişkin olarak hak sahiplerinden kanunlara uygun şekilde lisans alınmıştır. Bu sebeple; söz konusu içerikler CinePrize’dan izin alınmaksızın hiçbir surette kullanılamaz, yayınlanamaz, işlenemez, çoğaltılamaz, yayılamaz, temsil suretiyle faydalanılamaz, işaret, ses ve/veya görüntü nakline yarayan araçlarla umuma iletilemez. Aksi halde; CinePrize tarafından tüm hukuki ve cezai işlemleri başlatma ve taleplerde bulunma hakkı saklıdır.
 
17. Tüm mali hakları CinePrize’a ait olduğu belirtilen içerikler ile tüm fotoğraf, tarif, metot, kod, program, işleyiş ve görsellere ilişkin şahsi kullanımın dışında, kopyalama, işleme ve internet başta olmak üzere radyo-televizyon, uydu ve kablo gibi telli veya telsiz yayın yapan kuruluşlar vasıtasıyla veya dijital iletim de dahil, işaret, ses ve/veya görüntü nakline yarayan araçlar ve bunlarla sınırlı kalmamak kaydıyla her türlü araçla içeriği yayınlanamaz.
 
18. CinePrize’da  yer alan içeriğin alıntılanması, makul sınırlarda kalmak ve kaynak gösterme ve ilgili içeriğe ilişkin sitemize bağlantı vermek kaydıyla mümkündür. Makul sınır, eserin veya sair içeriğin basit bir özetinden ibaret olup, içeriğe vakıf olunması için cineprize.com’un ziyaret edilmesi ihtiyacını ortadan kaldırmayacak düzeyde olmalıdır. Her durumda, CinePrize’da yer alan içeriklerden herhangi birinin tamamının alıntılanması CinePrize’ın yazılı izni olmadıkça, yukarıdaki şartlara uyulsa dahi mümkün değildir. Konuya ilişkin Fikir ve Sanat Eserleri Kanunu hükümleri saklıdır.
 
19. Kullanıcı’nın yayın hakkına sahip olmadığı içerik bakımından, Fikir ve Sanat Eserleri Kanunu’ndaki usule uygun ve atıfta bulunmak şartıyla içerik sağlaması gerekmekte olup; aksi halde Kullanıcı, CinePrize ve cineprize.com 'un uğradığı ve uğrayabileceği doğrudan ve dolaylı, maddi ve manevi tüm zararını tazmin etmekle yükümlüdür.
 
20. CinePrize uygulamanın virüs ve sair zararlı içerik barındırmaması için azami gayret sarf etmekle birlikte Kullanıcı’nın teknik sorunlar yaşaması veya diğer nedenlerle mobil cihazlarına, tablet cihazlarına, bilgisayarına virüs geçmesi veya herhangi bir zarara uğraması halinde uğranacak zararlardan (bunlarla sınırlı kalmamak kaydıyla Kullanıcı’nın bilgisayarı veya programlarının hasar görmesinden) dolayı CinePrize’ın sorumluluğu bulunmamakta olup; bu sebeple cineprize.com'dan hiçbir talepte bulunulamaz. Kullanıcı, anılan zararlara uğramamak için gerekli önlemleri almakla yükümlüdür.
 
21. CinePrize uygulama içindeki hizmetlerinin yapısını Kullanıcı’ya önceden bildirim yapmaksızın dilediği zaman değiştirebilir ve/veya hizmetleri veya bazı özelliklerini geçici veya kalıcı olarak sonlandırabilir.
 
22. CinePrize uygulama içinde yer alan içerik, hizmet, imkan ve sair unsurların yayınının devamlılık arz etmesine önem göstermekle birlikte yayının ve/veya CinePrize uygulamasının üzerinden sunulan hizmetlerin kesintisiz olacağına dair garanti vermemektedir. Böyle bir durumda cineprize.com'un sorumluluğu bulunmamaktadır.
 
23. CinePrize hizmet aldığı 3. kişi veya kuruluşların kusurundan dolayı yaşanabilecek herhangi bir veri kaybı durumunda, CinePrize’ın sorumluluğu bulunmamakta olup; kaybolan bilgi ve verilerin yeniden girilmesi sorumluluğu Kullanıcı’ya aittir. Bu sebeple; Kullanıcı’nın verilerini korumak üzere kendi önlemlerini alması önerilmektedir.
 
24. Kullanıcı’nın CinePrize’de verdiği bilgilerin bazıları zorunlu olarak verilen (e-posta adresleri, IP bilgileri gibi) bilgiler, bazıları da Kullanıcı’nın tercihine bağlı olarak CinePrize’a verdiği veya CinePrize uygulamasının erişimine onay verdiği bilgilerdir. CinePrize, kullanıcılarına daha iyi hizmet sunabilmek için bu bilgileri dilediği sürece, bilgiler tarafınızdan silinse dahi saklayabilir. Ancak CinePrize, IP bilgilerini yasal süreden sonra saklamayacaktır.
 
25. CinePrize, Kullanıcı’nın bilgilerini saklama konusunda azami özeni göstermekle beraber Cineprize.com uygulaması tarafından saklanan Kullanıcı verilerinin yer aldığı sisteme izinsiz girilmesi, sistemin işleyişinin bozulması veya değiştirilmesi suretiyle bilgilerinizin elde edilmesi, değiştirilmesi veya silinmesi halinde CinePrize’un sorumluluğu bulunmamaktadır. Kullanıcı, uygulamayı her ziyaret ettiğinde, IP adresi, işletim sistemi, kullandığı sistem (Apple, Android vs.), bağlantı zamanı, süre bilgileri ve benzeri bilgiler otomatik olarak kaydedilmekte olup; Kullanıcı’nın izni gerekmeksizin elde edilen bu bilgilerin 3. kişilerle paylaşılmamak kaydıyla CinePrize tarafından kişisel bilgilerinizle ilişkilendirilerek veya anonim olarak kullanılması mümkündür.
 
26. CinePrize, resmi mercilerden usulüne uygun talep gelmesi halinde Kullanıcı bilgilerini ilgili mercilere iletecektir.
 
27. Kullanıcı, CinePrize uygulamasını ziyaret ettiği süre boyunca CinePrize, “cookie” olarak da adlandırılan çerezlerin veya uygulama kullanım verilerini analiz etme amaçlı javascript kodları veya benzer iz sürme verileri mobil / tablet cihazlarına yerleştirilebilir. Çerezler basit metin dosyalarından ibaret olup, kimlik ve sair özel bilgiler içermez, bu nevi kişisel bilgi içermemekle beraber oturum bilgileri ve benzeri veriler saklanır ve Kullanıcı’yı tekrar tanımak için kullanılabilir.
 
28. CinePrize, zaman zaman uygulama içinde, tanıtım yazılarına veya reklamlarına 3. kişilere ait internet sitelerine ilişkin bilgi ve linkleri dahil edebilir. Kullanıcı’nın bu linklere tıklayarak diğer internet sitelerine girmesi halinde söz konusu siteler ya da bu sitelerin uygulamaları cineprize.com'un kontrolü altında olmadığı gibi bu Sözleşme, erişebilen bu diğer siteler bakımından geçerli değildir. Bu siteler ile sitelerin uygulamalarında yer alan bilgilerin doğruluğuna ilişkin olarak CinePrize’ın hiçbir sorumluluğu bulunmamaktadır. Bu sitelerde yer alan bilgilerin doğruluğuna, bilgi kullanımına, gizlilik ilkelerine ve içeriğine ilişkin olarak CinePrize’ın hiçbir sorumluluğu bulunmamaktadır.
 
29. CinePrize dilediği zaman ya da uygun gördüğü taktirde Kullanıcı hesabında biriken puanları silebilir, puan ekleyebilir, puanları ile hediye seçtiği ürünleri iptal edebilir, iptal edilen siparişler için puan iadesi yapmayabilir. Kullanıcı bu ve benzeri durumlar oluştuğunda CinePrize üzerinde herhangi bir hak talebinde bulunamaz.
 
30. CinePrize dilediği zaman ya da uygun gördüğü taktirde Kullanıcı hesabını silebilir, Kullanıcı’nın uygulamaya girmesini engelleyebilir. Kullanıcı bu ve benzeri durumlar oluştuğunda CinePrize üzerinde herhangi bir hak talebinde bulunamaz.
 
31. Bu Sözleşme’nin uygulanmasından doğabilecek her türlü uyuşmazlığın çözümünde Türk Hukuku uygulanacak olup; İstanbul Merkez (Çağlayan) Mahkemeleri ve İcra Müdürlükleri yetkili olacaktır.
 
32. Taraflar, bu Sözleşme’den doğabilecek ihtilaflarda, CinePrize’ın defter ve kayıtları ile bilgisayar kayıtlarının (e-posta yazışmaları, internet trafik bilgisi ve erişim kayıtları[log] ve benzeri) 6100 sayılı Hukuk Muhakemeleri Kanunu’nun 193. maddesi anlamında muteber, bağlayıcı, kesin ve münhasır delil teşkil edeceğini ve bu maddenin Delil Sözleşmesi niteliğinde olduğunu kabul,
beyan ve taahhüt ederler.
 
33. Tarafların birbirlerine gönderecekleri bildirimler, Kullanıcı’nın CinePrize uygulamasında bildirdiği adres ve/veya e-posta adresi ile CinePrize’ın bu Sözleşme’de belirtmiş olduğu adres ve/veya e-posta adresine gönderilecektir. Kullanıcı’nın uygulama içinde bildirdiği adres ve/veya e-posta adresinde bir değişiklik olması halinde, yeni adres ve/veya e-posta adresini Cineprize.com'a yazılı olarak bildirecektir. CinePrize’nin adres ve/veya e-posta adresinde bir değişiklik olması halinde ilgili değişiklik bu Sözleşme’ye aktarılacaktır. Aksi takdirde; Kullanıcı’nın Cineprize.com'a bildirdiği adres ile e-posta adreslerine ve CinePrize'ın bu Sözleşme’de yer alan adres ile e-posta adreslerine yapılacak tebligatlar, geçerli tebligatın tüm hukuki sonuçlarını doğuracaktır.
 
34. Bu Sözleşme’nin herhangi bir maddesinin geçersiz ya da uygulanamaz hale gelmesi, Sözleşme’nin diğer maddelerini geçersiz kılmayacaktır.
 
35. Taraflar’ın bu Sözleşme’den kaynaklanan haklarını kısmen veya tamamen kullanmamaları veya bunlara aykırı hareket etmeleri, hiçbir suretle bu haklardan feragat ettikleri anlamına gelmeyecektir.
 
36. Bu Sözleşme, uygulamaya yeni özellikler eklendikçe veya uygulama Kullanıcılarından yeni öneriler geldikçe yeniden, önceden Kullanıcılara herhangi bir bildirimde bulunulmaksızın düzenlenebilir ve güncellenebilir. Bu nedenle, Kullanıcı’nın CinePrize uygulamasını her ziyaret edişinde bu Sözleşme’yi yeniden gözden geçirmesi önerilmektedir. Bu belge, en son 15.12.2020 tarihinde güncellenmiştir. Üyelik sözleşmesiyle veya CinePrize uygulamalarıyla ilgili herhangi bir sorunuz varsa iletişime geçin: 

Tebligat Adresleri Kullanıcı’nın uygulama içinde bildirdiği adres, bu sözleşme ile ilgili olarak yapılacak her türlü bildirim için yasal adres kabul edilir. Taraflar, mevcut adreslerindeki değişiklikleri yazılı olarak diğer tarafa 3 (üç) gün içinde bildirmedikçe, eski adreslere yapılacak bildirimlerin geçerli olacağını ve kendilerine yapılmış sayılacağını kabul ederler. İşbu sözleşme nedeniyle CinePrize’nin Kullanıcı’nın kayıtlı e.posta adresini kullanarak yapacağı
her türlü bildirim aynen geçerli olup, e-postanın CinePrize tarafından yollanmasından 1 (bir) gün sonra Kullanıcı’ya ulaştığı kabul edilecektir. Kullanıcı, bu katılım sözleşmesinde yer alan maddelerin tümünü okuduğunu, anladığını, kabul ettiğini ve kendisiyle ilgili olarak verdiği bilgilerin doğruluğunu onayladığını beyan, kabul ve taahhüt eder.

37. CinePrize’da kazanılan tüm puan değerleri tarafımızca belirlenmiş olup, yeni gelecek olan güncellemeler ile değişim gösterebilir.

38. CinePrize kullanıcıların kural ihlallerini engellemek adına; hatalardan faydalanan, haksız kazanç elde eden, yasaklı programlar kullanan kullanıcıları tespit edip adaleti sağlamak için IP adreslerinize erişim sağlacaktır.

39. Tüm kullanıcılarımız, içeriklerimize gelen yorumlarda kötü maksatlı yorumlar farkettiğinde, uygulama içinde belirledikleri yorumu tutup kaydırarak rahatlıkla şikayette bulunabilirler. Tarafımız bu şikayetleri 24 saat içinde değerlendirip aksiyon alacaktır ve raporlarını tutacaktır.

40. Tüm kullanıcılarımız, CinePrize uygulaması içerisinde kontrollü bir şekilde yüklenmiş olan videolardan rahatsız olmaları halinde; videoların altında bulunan alandan video hakkında şikayet iletebilirler. Tarafımız bu şikayetleri 24 saat içinde değerlendirip aksiyon alacaktır ve raporlarını tutacaktır.

41. Uygulamamız yarışma, çekiliş benzeri içerikler barındırmasından dolayı +17 yaş sınırı zorunluluğu bulunmaktadır.

42. CinePrize uygulaması içine kullanıcılar tarafından eklenilen tüm içerikler; içerik filtrelerimizden geçtikten sonra eklenmekte olup, herhangi bir sorun olması durumunda 24 saat içinde tespit edildikten sonra müdahalesi (içeriğin kaldırılıp, içerik sahibinin hesabının sistemden engellenmesi) gerçekleştirilip tarafımızca raporlanmaktadır.

KİŞİSEL VERİLERİN KORUNMASINA İLİŞKİN BİLGİLENDİRME
6698 sayılı Kişisel Verilerin Korunması Kanunu uyarınca kişisel verilerinizle ilgili olarak sizleri bilgilendirmek istiyoruz.
 
Akıllı Ambalaj Yazılım A.Ş (CinePrize) veri sorumlusu sıfatıyla kişisel verilerinizi, uygulamaya kayıt esnasında toplayarak üyelik sözleşmenizin kurulması ve ifasıyla doğrudan ilgili olmaları nedeniyle işlemektedir.
 
CineCoin ile yapabilecekleriniz; 

Mağaza bölümündeki ürünleri hemen alabilirsiniz. 

Çekilişe katılabilirsiniz. Çekilişte aldığınız her bilet bir hak demektir. Ne kadar çok çekiliş bileti alırsanız şansınız o kadar artar. Çekiliş instagram canlı yayınında güveni daha da çok artırmak için bizden bağımsız bir internet sitesinde yapılacaktır.

CineCoinler ayda bir sıfırlanır. (Her ayın ilk günü)

CineGold ile yapılabilecekleriniz; 

Biriktirdiğiniz CineGoldlar ile sürpriz sekmesinde istediğiniz bir ürüne sürpriz vakitte açılan geri sayım ile birlikte teklif verebilirsiniz. En son büyük teklifi veren ürünün sahibi olur.

CineGoldlar yılda bir sıfırlanır. ( Her yıl ocak ayının ilk günü)


Çekiliş ; 

Ayda 2 defa gerçekleşir. İlki ayın 15’inde gerçekleşir. İkincisi ayın son günü gerçekleşir. Çekiliş bileti almanız için CineGold kullanmanız gerekir. Aldığınız her bilet size ekstra bir hak demektir. Çekilişler bizimle bağlantısı olmayan bir web sitesinde yapılacaktır güven amaçlı. Kazanan kişi instagramda canlı yayınla gösterilecektir.

Yarışma; 

Uygulama içindeki içeriklerimizi izleyerek bir yarışa giriyorsunuz. İzlediğiniz her videodan durduğunuz dakika başı 20 CineCoin ve 20 CineGold kazanırsınız. Her videodan sadece 1 kere kazanabilirsiniz. İçeriklerimizin içinde gelen sorulara doğru yanıt vererek 50 CineCoin e CineGold kazanabilirsiniz. Kazandığınız CineCoinler ile mağazadan istediğiniz ürünü alabilirsiniz. Kazandığınız CineGoldlar ile istediğiniz ürüne süpriz de teklif verebilirsiniz.

CinePrize’ın, kişisel verilerinizi işleme amacı:
 
(i) Kimliğinizi doğrulamak,
 
(ii) Müşteri desteği sağlamak ve sorun gidermek,
 
(iii) Servis güncellemeleri ve hatalar hakkında sizi bilgilendirmek,
 
(iv) Uygulama ile ilgili kampanyalar hakkında sizi bilgilendirmek,
 
(v) Kargonun, teslimatla ilgili olarak size ulaşması gerektiğinde sizi arayabilmesini sağlamaktır.
 
Kişisel verileriniz yurtiçi ve/veya yurtdışında bulunan güvenli sunucularımızda saklanmaktadır.
 
CinePrize, sözü edilen istisna dışında, hukuken gerekli veya yükümlü olmadıkça kişisel verilerinizi ve/veya trafik verilerinizi ya da iletişim içeriğini sizin açık izniniz olmadan üçüncü kişilere aktarmamaktadır.
 
KVKK’da belirlenen ve rıza gerektirmeyen hallerin yanı sıra, CinePrize uygulama vasıtasıyla edinilen onayınıza istinaden, size, hizmetlerine ilişkin kampanya ve duyurularını iletebilecek ve müşteri memnuniyeti sağlamak amacıyla tarafınızla temasa geçebilecektir.
Kişisel verilerinizin korunmasına ilişkin haklarınız aşağıda belirtilmiştir:
 
(i) Kişisel verilerinizin işlenip işlenmediğini öğrenmek,
 
(ii) Kişisel verileriniz işlenmişse buna ilişkin bilgi talep etmek,
 
(iii) Kişisel verilerinizin hangi amaçla işlendiğini ve bu amaçlara uygun kullanılıp kullanılmadığını öğrenmek,
 
(iv) Kişisel verilerinizin yurt içinde veya yurt dışında aktarıldığı üçüncü kişileri bilmek,
 
(v) Kişisel verilerinizin eksik veya yanlış işlenmiş olması halinde bunların düzeltilmesini istemek,
 
(vi) Kişisel verilerinizin silinmesini veya yok edilmesini talep etmek,
 
(vii) Düzeltme, silme ve yok etme taleplerinizin kişisel verilerinizin aktarıldığı üçüncü kişilere bildirilmesini istemek, işlenen verilerinizin münhasıran otomatik sistemler vasıtasıyla analiz edilmesi suretiyle şahsınız aleyhine bir sonucun ortaya çıkmasına itiraz etmek,
 
(viii) Kişisel verilerinizin kanuna aykırı olarak işlenmesi sebebiyle zarara uğramanız halinde zararın giderilmesini talep etmek.
 
Kişisel verilerinizin korunmasına ilişkin her türlü talebinizi hello@cineprize.com adresine iletebilirsiniz. Talepleriniz titizlikle değerlendirilecek ve tarafınıza en kısa zamanda bir dönüş yapılacaktır.
 
Kişisel verilerinizin korunmasına ilişkin olarak yukarıda yer verilen bilgiler, kullanım koşullarına da eklenmiş olup bu bilgilere, Uygulama aracılığıyla her zaman erişebilirsiniz.
 
R&D Apps A.Ş
E-Posta: hello@cineprize.com

''';
