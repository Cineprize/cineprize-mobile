import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:flutter/material.dart';

class CustomAppBody extends StatefulWidget {
  final double padding;
  final MainAxisAlignment mainAxisAlignment;
  final CrossAxisAlignment crossAxisAlignment;
  final List<Widget> children;

  const CustomAppBody({
    Key? key,
    required this.children,
    required this.mainAxisAlignment,
    required this.crossAxisAlignment,
    required this.padding,
  }) : super(key: key);

  @override
  _CustomAppBodyState createState() => _CustomAppBodyState();
}

class _CustomAppBodyState extends State<CustomAppBody> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        decoration: kMainPagesDecoration,
        child: Padding(
          padding: EdgeInsets.all(widget.padding == null ? 0 : widget.padding),
          child: Column(
            crossAxisAlignment: widget.crossAxisAlignment == null ? CrossAxisAlignment.center : widget.crossAxisAlignment,
            mainAxisAlignment: widget.mainAxisAlignment == null ? MainAxisAlignment.center : widget.mainAxisAlignment,
            children: widget.children,
          ),
        ),
      ),
    );
  }
}
