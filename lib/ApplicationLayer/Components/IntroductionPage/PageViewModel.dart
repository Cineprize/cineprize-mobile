import 'package:cineprize/ApplicationLayer/components/IntroductionPage/PageDecoration.dart';
import 'package:flutter/material.dart';

class PageViewModel {
  /// Title of page

  /// Title of page
  final Widget? titleWidget;

  /// Text of page (description)
  final RichText body;

  /// Widget content of page (description)
  final Widget? bodyWidget;

  /// Image of page
  /// Tips: Wrap your image with an alignment widget like Align or Center
  final Widget image;

  /// Footer widget, you can add a button for example
  final Widget? footer;

  /// Page decoration
  /// Contain all page customizations, like page color, text styles
  final PageDecoration decoration;

  PageViewModel({
    this.titleWidget,
    required this.body,
    this.bodyWidget,
    required this.image,
    this.footer,
    this.decoration = const PageDecoration(),
  })  : assert(
          body != null || bodyWidget != null,
          'You must provide either body (String) or bodyWidget (Widget).',
        ),
        assert(
          (body == null) != (bodyWidget == null),
          'You can not provide both body and bodyWidget.',
        );
}
