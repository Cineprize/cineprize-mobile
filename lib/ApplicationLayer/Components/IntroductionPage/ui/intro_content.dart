import 'package:cineprize/ApplicationLayer/components/IntroductionPage/PageViewModel.dart';
import 'package:flutter/material.dart';

class IntroContent extends StatelessWidget {
  final PageViewModel page;

  const IntroContent({Key? key, required this.page}) : super(key: key);

  Widget _buildWidget(Widget? widget, RichText? text, TextStyle style) {
    return widget ?? text!;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: page.decoration.contentPadding,
      child: Column(
        children: [
          SizedBox(height: 30),
          Padding(
            padding: page.decoration.descriptionPadding,
            child: _buildWidget(
              page.bodyWidget!,
              page.body,
              page.decoration.bodyTextStyle,
            ),
          ),
          if (page.footer != null)
            Padding(
              padding: page.decoration.footerPadding,
              child: page.footer,
            ),
        ],
      ),
    );
  }
}
