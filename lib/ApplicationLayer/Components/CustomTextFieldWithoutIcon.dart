//Custom Text Field

import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CustomTextFieldWithoutIcon extends StatelessWidget {
  final String hintText;
  final String initialValue;
  final TextInputType textInputType;
  final Function(String) onSaved;
  final Function(String) validator;

  const CustomTextFieldWithoutIcon({
    Key? key,
    required this.hintText,
    required this.initialValue,
    required this.textInputType,
    required this.validator,
    required this.onSaved,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      maxLines: 1,
      initialValue: this.initialValue,
      //Yukarıda TextField için tanımladığım fonksiyonu kullandım
      style: kNormalText,
      keyboardType: textInputType,
      textAlign: TextAlign.start,
      decoration: InputDecoration(
          filled: true,
          fillColor: Colors.grey[800],
          enabledBorder: baseBorder,
          errorBorder: baseBorder,
          focusedBorder: baseBorder,
          focusedErrorBorder: baseBorder,
          counterStyle: TextStyle(color: Colors.white),
          hintText: this.hintText,
          hintStyle: kHintText,
          border: InputBorder.none,
          errorMaxLines: 1),
      validator: (_) => this.validator(_!),
      onSaved: (_) => this.onSaved(_!),
    );
  }

  OutlineInputBorder get baseBorder {
    return OutlineInputBorder(borderRadius: BorderRadius.circular(30));
  }
}
