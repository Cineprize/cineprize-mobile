import 'package:cached_network_image/cached_network_image.dart';
import 'package:cineprize/ApplicationLayer/Auth/AuthManagement/LoginNavigator.dart';
import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/DetailedVideoScreen/DetailedVideoScreen.dart';
import 'package:flutter/material.dart';
import 'package:skeleton_text/skeleton_text.dart';

Widget buildVideoCard(
  BuildContext context, {
  required video,
  required double height,
  required double width,
  required CachedNetworkImage image,
  required String banner,
}) {
  double width2 = MediaQuery.of(context).size.width;
  return GestureDetector(
    onTap: () {
      Navigator.push(
          loginNavigatorContext!,
          MaterialPageRoute(
              builder: (BuildContext context) =>
                  DetailedVideoScreen(video: video)));
    },
    child: Container(
      color: Colors.transparent,
      width: width == null ? width2 / 2.5 : width,
      child: Card(
        elevation: 0,
        color: Colors.transparent,
        child: Column(
          children: [
            ClipRRect(
                borderRadius: BorderRadius.circular(17),
                child: Container(
                    height: height,
                    width: width == null ? width2 / 2.5 : width,
                    child: image)),
            Flexible(
              child: Padding(
                  padding: const EdgeInsets.only(top: 8),
                  child: Text(banner,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 15,
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2)),
            )
          ],
        ),
      ),
    ),
  );
}

List<Widget> buildVideoListSkeletonCard(
    BuildContext context, int count, double height,
    {double? width}) {
  double width2 = MediaQuery.of(context).size.width;

  doBuild() {
    return Container(
      color: Colors.transparent,
      child: Card(
        elevation: 0,
        color: Colors.transparent,
        child: Column(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(17),
              child: Container(
                height: height,
                width: width == null ? width2 / 2.5 : width,
                child: SkeletonAnimation(
                    shimmerColor: shimmerColor,
                    child: Container(
                        width: width == null ? width2 / 2.5 : width,
                        height: height,
                        decoration: BoxDecoration(color: skeletonColor))),
              ),
            ),
            SizedBox(height: 10),
            Flexible(
              child: SkeletonAnimation(
                shimmerColor: shimmerColor,
                child: Container(
                  width: width2 / 4,
                  height: 10,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: skeletonColor,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  List<Widget> skeletonElements = [];
  for (int i = 0; i < count; i++) {
    skeletonElements.add(doBuild());
  }
  return skeletonElements;
}
