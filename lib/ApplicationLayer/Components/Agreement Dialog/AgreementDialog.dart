import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class CreateAgreement extends StatefulWidget {
  bool checkBoxState;

  CreateAgreement({Key? key, required this.checkBoxState}) : super(key: key);

  @override
  _CreateAgreementState createState() => _CreateAgreementState();
}

class _CreateAgreementState extends State<CreateAgreement> {
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Üyelik Sözleşmesi'),
          actions: [
            FlatButton(
              onPressed: () {
                Navigator.of(context).pop('accepted');
                setState(() {
                  widget.checkBoxState = true;
                });
              },
              child: Text(
                'Kabul et',
                style: Theme.of(context).textTheme.subtitle1!.copyWith(
                      color: Colors.white,
                    ),
              ),
            ),
          ],
        ),
        backgroundColor: Color(0xff1a1d21),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              userAgreement,
              style: TextStyle(color: Color(0xff6b6c6b)),
            ),
          ),
        ),
      ),
    );
  }
}
