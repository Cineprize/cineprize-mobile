import 'dart:async';

import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/Models/Video.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:video_player/video_player.dart';
import 'package:wakelock/wakelock.dart';

import 'src/fullscreen_player.dart';
import 'src/quality_links.dart';

//Класс видео плеера
class MyVimeoPlayer extends StatefulWidget {
  final String id;
  final bool autoPlay;
  final bool looping;
  final int position;
  final Video videoModel;
  final Function(bool) onScreenChanged;
  final Function(VideoPlayerController) listener;

  MyVimeoPlayer({
    required this.id,
    required this.autoPlay,
    this.looping = false,
    required this.videoModel,
    this.position = 0,
    required this.listener,
    Key? key,
    required this.onScreenChanged,
  }) : super(key: key);

  @override
  _MyVimeoPlayerState createState() => _MyVimeoPlayerState(
      id, autoPlay, looping, videoModel, position, listener);
}

class _MyVimeoPlayerState extends State<MyVimeoPlayer> {
  String _id;
  bool autoPlay = false;
  bool looping = false;
  bool _overlay = true;
  bool fullScreen = false;
  bool initialized = false;
  int position;
  Video videoModel;
  late Function(VideoPlayerController) listener;

  _MyVimeoPlayerState(this._id, this.autoPlay, this.looping, this.videoModel,
      this.position, listener);

  //Custom controller
  VideoPlayerController? _controller;
  Future<void>? initFuture;

  //Quality Class
  late QualityLinks _quality;
  late Map _qualityValues;
  var _qualityValue;

  //Переменная перемотки
  bool _seek = false;

  //Переменные видео
  late double videoHeight;
  late double videoWidth;
  late double videoMargin;

  //Переменные под зоны дабл-тапа
  double doubleTapRMargin = 36;
  double doubleTapRWidth = 400;
  double doubleTapRHeight = 160;
  double doubleTapLMargin = 10;
  double doubleTapLWidth = 400;
  double doubleTapLHeight = 160;

  bool animatedRight = false;
  bool animatedLeft = false;
  final int animationTime = 300;

  //Last

  @override
  void initState() {
    //Create class

    Wakelock.disable();
    _quality = QualityLinks(_id);

    //Инициализация контроллеров видео при получении данных из Vimeo
    _quality.getQualitiesSync().then((value) {
      _qualityValues = value;
      _qualityValue = value[value.lastKey()];
      _controller = VideoPlayerController.network(_qualityValue);
      _controller!.setLooping(looping);
      //if (autoPlay) _controller!.play();

      initFuture = _controller!.initialize().then((value) {
        _controller = widget.listener(_controller!);
      });

      //Обновление состояние приложения и перерисовка
      setState(() {
        SystemChrome.setPreferredOrientations(
            [DeviceOrientation.portraitDown, DeviceOrientation.portraitUp]);
        SystemChrome.setEnabledSystemUIOverlays(
            [SystemUiOverlay.top, SystemUiOverlay.bottom]);
      });
    });

    super.initState();
  }

  updateUserState(int coin, int gold) {
    UserManagement.include.user.coin = coin;
    UserManagement.include.user.gold = gold;
  }

  //Отрисовываем элементы плеера
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Stack(
      alignment: AlignmentDirectional.center,
      children: <Widget>[
        GestureDetector(
          child: FutureBuilder(
              future: initFuture,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  double delta = MediaQuery.of(context).size.width -
                      MediaQuery.of(context).size.height *
                          _controller!.value.aspectRatio;

                  if (MediaQuery.of(context).orientation ==
                          Orientation.portrait ||
                      delta < 0) {
                    videoHeight = MediaQuery.of(context).size.width /
                        _controller!.value.aspectRatio;
                    videoWidth = MediaQuery.of(context).size.width;
                    videoMargin = 0;
                  } else {
                    videoHeight = MediaQuery.of(context).size.height;
                    videoWidth = videoHeight * _controller!.value.aspectRatio;
                    videoMargin =
                        (MediaQuery.of(context).size.width - videoWidth) / 2;
                  }

                  if (_seek && _controller!.value.duration.inSeconds > 2) {
                    _controller!.seekTo(Duration(microseconds: position));
                    _seek = false;
                  }

                  //Отрисовка элементов плеера
                  return Stack(
                    children: <Widget>[
                      Container(
                        height: videoHeight,
                        width: videoWidth,
                        margin: EdgeInsets.only(left: videoMargin),
                        child: VideoPlayer(_controller!),
                      ),
                      _videoOverlay(),
                    ],
                  );
                } else {
                  return Center(
                      heightFactor: 6,
                      child: CircularProgressIndicator(
                        strokeWidth: 4,
                        valueColor:
                            AlwaysStoppedAnimation<Color>(Color(0xFF22A3D2)),
                      ));
                }
              }),
          onTap: () {
            //Редактируем размер области дабл тапа при показе оверлея.
            // Сделано для открытия кнопок 'Во весь экран' и 'Качество'
            setState(() {
              _overlay = !_overlay;
              if (_overlay) {
                doubleTapRHeight = videoHeight - 36;
                doubleTapLHeight = videoHeight - 10;
                doubleTapRMargin = 36;
                doubleTapLMargin = 10;
              } else if (!_overlay) {
                doubleTapRHeight = videoHeight + 36;
                doubleTapLHeight = videoHeight + 16;
                doubleTapRMargin = 0;
                doubleTapLMargin = 0;
              }
            });
          },
        ),
        GestureDetector(
            //======= Перемотка назад =======//
            child: Container(
              width: doubleTapLWidth / 2 - 30,
              height: doubleTapLHeight - 46,
              margin: EdgeInsets.fromLTRB(
                  0, 10, doubleTapLWidth / 2 + 30, doubleTapLMargin + 20),
              decoration: BoxDecoration(
                  //color: Colors.green,
                  ),
              child: _overlay && _controller != null
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        AnimatedDefaultTextStyle(
                          style: animatedLeft
                              ? TextStyle(
                                  fontSize: 30,
                                  color: kAppThemeColor,
                                  fontWeight: FontWeight.bold)
                              : TextStyle(
                                  fontSize: 0,
                                  color: kAppThemeColor.withOpacity(0.5),
                                  fontWeight: FontWeight.bold),
                          duration: Duration(milliseconds: animationTime),
                          onEnd: () {
                            setState(() {
                              animatedLeft = false;
                            });
                          },
                          child: Text(
                            '-5',
                          ),
                        ),
                        IconButton(
                          icon: Icon(Icons.replay_5, size: 35),
                          onPressed: () {
                            setState(() {
                              animatedLeft = true;

                              _controller!.seekTo(Duration(
                                  seconds:
                                      _controller!.value.position.inSeconds -
                                          5));
                            });
                          },
                        )
                      ],
                    )
                  : Center(
                      child: AnimatedDefaultTextStyle(
                        style: animatedLeft
                            ? TextStyle(
                                fontSize: 30,
                                color: kAppThemeColor,
                                fontWeight: FontWeight.bold)
                            : TextStyle(
                                fontSize: 0,
                                color: kAppThemeColor.withOpacity(0.5),
                                fontWeight: FontWeight.bold),
                        duration: Duration(milliseconds: animationTime),
                        onEnd: () {
                          setState(() {
                            animatedLeft = false;
                          });
                        },
                        child: Text(
                          '-5',
                        ),
                      ),
                    ),
            ),

            // Изменение размера блоков дабл тапа. Нужно для открытия кнопок
            // 'Во весь экран' и 'Качество' при включенном overlay
            onTap: () {
              setState(() {
                _overlay = !_overlay;

                if (_overlay) {
                  doubleTapRHeight = videoHeight - 36;
                  doubleTapLHeight = videoHeight - 10;
                  doubleTapRMargin = 36;
                  doubleTapLMargin = 10;
                } else if (!_overlay) {
                  doubleTapRHeight = videoHeight + 36;
                  doubleTapLHeight = videoHeight + 16;
                  doubleTapRMargin = 0;
                  doubleTapLMargin = 0;
                }
              });
            },
            onDoubleTap: () {
              setState(() {
                animatedLeft = true;
                _controller!.seekTo(Duration(
                    seconds: _controller!.value.position.inSeconds - 5));
              });
            }),
        GestureDetector(
            child: Container(
              //======= Перемотка вперед =======//
              width: doubleTapRWidth / 2 - 45,
              height: doubleTapRHeight - 60,
              margin: EdgeInsets.fromLTRB(doubleTapRWidth / 2 + 45,
                  doubleTapRMargin, 0, doubleTapRMargin + 30),
              decoration: BoxDecoration(
                  // color: Colors.yellow,
                  ),
              child: _overlay && _controller != null
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        IconButton(
                          icon: Icon(Icons.forward_5, size: 35),
                          onPressed: () {
                            setState(() {
                              animatedRight = true;

                              _controller!.seekTo(Duration(
                                  seconds:
                                      _controller!.value.position.inSeconds +
                                          5));
                            });
                          },
                        ),
                        AnimatedDefaultTextStyle(
                          style: animatedRight
                              ? TextStyle(
                                  fontSize: 30,
                                  color: kAppThemeColor,
                                  fontWeight: FontWeight.bold)
                              : TextStyle(
                                  fontSize: 0,
                                  color: kAppThemeColor.withOpacity(0.5),
                                  fontWeight: FontWeight.bold),
                          duration: Duration(milliseconds: animationTime),
                          onEnd: () {
                            setState(() {
                              animatedRight = false;
                            });
                          },
                          child: Text(
                            '+5',
                          ),
                        )
                      ],
                    )
                  : Center(
                      child: AnimatedDefaultTextStyle(
                        style: animatedRight
                            ? TextStyle(
                                fontSize: 30,
                                color: kAppThemeColor,
                                fontWeight: FontWeight.bold)
                            : TextStyle(
                                fontSize: 0,
                                color: kAppThemeColor.withOpacity(0.5),
                                fontWeight: FontWeight.bold),
                        duration: Duration(milliseconds: animationTime),
                        onEnd: () {
                          setState(() {
                            animatedRight = false;
                          });
                        },
                        child: Text(
                          '+5',
                        ),
                      ),
                    ),
            ),
            // Изменение размера блоков дабл тапа. Нужно для открытия кнопок
            // 'Во весь экран' и 'Качество' при включенном overlay
            onTap: () {
              setState(() {
                _overlay = !_overlay;
                if (_overlay) {
                  doubleTapRHeight = videoHeight - 36;
                  doubleTapLHeight = videoHeight - 10;
                  doubleTapRMargin = 36;
                  doubleTapLMargin = 10;
                } else if (!_overlay) {
                  doubleTapRHeight = videoHeight + 36;
                  doubleTapLHeight = videoHeight + 16;
                  doubleTapRMargin = 0;
                  doubleTapLMargin = 0;
                }
              });
            },
            onDoubleTap: () {
              setState(() {
                animatedRight = true;

                _controller!.seekTo(Duration(
                    seconds: _controller!.value.position.inSeconds + 5));
              });
            }),
      ],
    ));
  }

  //================================ Quality ================================//
  void _settingModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.white,
        builder: (BuildContext bc) {
          //Формирования списка качества
          final children = <Widget>[];
          _qualityValues.forEach((elem, value) => (children.add(new ListTile(
              title: new Text(' ${elem.toString()} fps'),
              onTap: () => {
                    //Обновление состояние приложения и перерисовка
                    setState(() {
                      _controller!.pause();
                      _qualityValue = value;
                      position = _controller!.value.position.inMicroseconds;
                      _controller =
                          VideoPlayerController.network(_qualityValue);
                      _controller!.setLooping(true);
                      _seek = true;
                      initFuture = _controller!.initialize().then((value) {
                        _controller = widget.listener(_controller!);
                      });
                      _controller!.play();
                    }),
                  }))));
          //Вывод элементов качество списком
          return Container(
            child: Wrap(
              children: children,
            ),
          );
        });
  }

  //================================ OVERLAY ================================//
  Widget _videoOverlay() {
    return _overlay
        ? Stack(
            children: <Widget>[
              Center(
                child: IconButton(
                    padding: EdgeInsets.only(
                      top: videoHeight / 2 - 60,
                      bottom: videoHeight / 2 - 5,
                    ),
                    icon: _controller!.value.isPlaying
                        ? Icon(Icons.pause, size: 60.0)
                        : Icon(Icons.play_arrow, size: 60.0),
                    onPressed: () {
                      setState(() {
                        if (_controller!.value.isPlaying) {
                          _controller!.pause();
                          Wakelock.disable();
                        } else {
                          _controller!.play();
                          Wakelock.enable();
                        }
                      });
                    }),
              ),
              Container(
                margin: EdgeInsets.only(
                    top: videoHeight - 70, left: videoWidth + videoMargin - 50),
                child: IconButton(
                    alignment: AlignmentDirectional.center,
                    icon: Icon(Icons.fullscreen, size: 30.0),
                    onPressed: () async {
                      bool isPlaying = _controller!.value.isPlaying;
                      print("Screen: FullScreen");
                      setState(() {
                        //_controller!.pause();
                        widget.onScreenChanged(true);
                        SystemChrome.setPreferredOrientations(
                          [
                            DeviceOrientation.landscapeLeft,
                            DeviceOrientation.landscapeRight
                          ],
                        );
                        SystemChrome.setEnabledSystemUIOverlays([]);
                      });

                      var response = await Navigator.push(
                        context,
                        PageRouteBuilder(
                          opaque: false,
                          pageBuilder: (BuildContext context, _, __) =>
                              FullscreenPlayer(
                            id: _id,
                            autoPlay: isPlaying,
                            videoModel: widget.videoModel,
                            controller: _controller!,
                            position:
                                _controller!.value.position.inMicroseconds,
                            initFuture: initFuture!,
                            qualityValue: _qualityValue,
                            quality: _quality,
                          ),
                          transitionsBuilder: (___, Animation<double> animation,
                              ____, Widget child) {
                            return FadeTransition(
                              opacity: animation,
                              child: ScaleTransition(
                                  scale: animation, child: child),
                            );
                          },
                        ),
                      );
                      /* position = response['pos']; */
                      setState(() {
                        //if (response['isPlaying']) _controller!.play();
                        print("Screen: Normal");
                        widget.onScreenChanged(false);
                        SystemChrome.setPreferredOrientations([
                          DeviceOrientation.portraitDown,
                          DeviceOrientation.portraitUp
                        ]);
                        SystemChrome.setEnabledSystemUIOverlays(
                            [SystemUiOverlay.top, SystemUiOverlay.bottom]);
                      });
                    }),
              ),
              Container(
                margin: EdgeInsets.only(left: videoWidth + videoMargin - 48),
                child: IconButton(
                  icon: Icon(Icons.settings, size: 26.0),
                  onPressed: () {
                    _settingModalBottomSheet(context);
                    setState(() {});
                  },
                ),
              ),
              Container(
                //===== Ползунок =====//
                margin: EdgeInsets.only(
                    top: videoHeight - 50, left: videoMargin), //CHECK IT
                child: _videoOverlaySlider(),
              )
            ],
          )
        : Center(
            child: Container(
              height: 5,
              width: videoWidth,
              margin: EdgeInsets.only(top: videoHeight - 35),
              child: VideoProgressIndicator(
                _controller!,
                allowScrubbing: false,
                colors: VideoProgressColors(
                  playedColor: kAppThemeColor,
                  backgroundColor: Color(0x5515162B),
                  bufferedColor: Color(0x5583D8F7),
                ),
                padding: EdgeInsets.only(top: 2),
              ),
            ),
          );
  }

  //=================== ПОЛЗУНОК ===================//
  Widget _videoOverlaySlider() {
    return ValueListenableBuilder(
      valueListenable: _controller!,
      builder: (context, VideoPlayerValue value, child) {
        if (!value.hasError && value.isInitialized) {
          return Row(
            children: <Widget>[
              Container(
                width: 46,
                alignment: Alignment(0, 0),
                child: Text(
                  value.position.inMinutes.toString() +
                      ':' +
                      (value.position.inSeconds - value.position.inMinutes * 60)
                          .toString(),
                  style: TextStyle(color: Colors.white60),
                ),
              ),
              Container(
                height: 20,
                width: videoWidth - 92,
                child: VideoProgressIndicator(
                  _controller!,
                  allowScrubbing: false,
                  colors: VideoProgressColors(
                    playedColor: kAppThemeColor,
                    backgroundColor: Color(0x5515162B),
                    bufferedColor: Color(0x5583D8F7),
                  ),
                  padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
                ),
              ),
              Container(
                width: 46,
                alignment: Alignment(0, 0),
                child: Text(
                  value.duration.inMinutes.toString() +
                      ':' +
                      (value.duration.inSeconds - value.duration.inMinutes * 60)
                          .toString(),
                  style: TextStyle(color: Colors.white60),
                ),
              ),
            ],
          );
        } else {
          return Container();
        }
      },
    );
  }

  @override
  void dispose() {
    _controller!.dispose();
    Wakelock.disable();
    super.dispose();
  }
}
