library vimeoplayer;

import 'dart:async';

import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/Models/Video.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:video_player/video_player.dart';
import 'package:wakelock/wakelock.dart';

import 'quality_links.dart';

//Класс видео плеера во весь экран
class FullscreenPlayer extends StatefulWidget {
  final String id;
  final bool autoPlay;
  final bool? looping;
  final VideoPlayerController controller;
  final position;
  final Future<void> initFuture;
  final String qualityValue;
  final Video videoModel;
  final QualityLinks quality;

  FullscreenPlayer({
    required this.id,
    required this.autoPlay,
    this.looping,
    required this.controller,
    this.position,
    required this.initFuture,
    required this.qualityValue,
    required this.videoModel,
    required this.quality,
    Key? key,
  }) : super(key: key);

  @override
  _FullscreenPlayerState createState() => _FullscreenPlayerState(
      id,
      autoPlay,
      looping!,
      controller,
      position,
      initFuture,
      qualityValue,
      videoModel,
      quality);
}

class _FullscreenPlayerState extends State<FullscreenPlayer> {
  String _id;
  bool autoPlay = false;
  bool looping = false;
  bool _overlay = true;
  bool fullScreen = true;
  bool initialized = false;
  Video videoModel;

  late VideoPlayerController controller;
  late VideoPlayerController _controller;

  int position;

  Future<void> initFuture;
  var qualityValue;

  _FullscreenPlayerState(
      this._id,
      this.autoPlay,
      this.looping,
      this.controller,
      this.position,
      this.initFuture,
      this.qualityValue,
      this.videoModel,
      this._quality);

  // Quality Class
  QualityLinks _quality;
  late Map _qualityValues;

  //Переменная перемотки
  bool _seek = false;

  //Переменные видео
  late double videoHeight;
  late double videoWidth;
  late double videoMargin;

  //Переменные под зоны дабл-тапа
  double doubleTapRMarginFS = 36;
  double doubleTapRWidthFS = 700;
  double doubleTapRHeightFS = 300;
  double doubleTapLMarginFS = 10;
  double doubleTapLWidthFS = 700;
  double doubleTapLHeightFS = 400;

  //Custom Variables

  bool animatedRight = false;
  bool animatedLeft = false;
  final int animationTime = 300;

  @override
  void initState() {
    //Инициализация контроллеров видео при получении данных из Vimeo
    _controller = controller;

    // Подгрузка списка качеств видео
    _quality = QualityLinks(_id); //Create class
    _quality.getQualitiesSync().then((value) {
      _qualityValues = value;
      if (autoPlay) Wakelock.enable();
    });

    setState(() {
      SystemChrome.setPreferredOrientations(
        [DeviceOrientation.landscapeLeft, DeviceOrientation.landscapeRight],
      );
      SystemChrome.setEnabledSystemUIOverlays([]);
    });

    super.initState();
  }

  Future<bool> _onWillPop() {
    bool isPlaying = _controller.value.isPlaying;

    Navigator.pop(context, {
      'pos': _controller.value.position.inMicroseconds,
      'isPlaying': isPlaying
    });
    return Future.value(true);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        body: Center(
          child: Stack(
            alignment: AlignmentDirectional.center,
            children: <Widget>[
              GestureDetector(
                child: FutureBuilder(
                    future: initFuture,
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.done) {
                        //Управление шириной и высотой видео
                        double delta = MediaQuery.of(context).size.width -
                            MediaQuery.of(context).size.height *
                                _controller.value.aspectRatio;
                        if (MediaQuery.of(context).orientation ==
                                Orientation.portrait ||
                            delta < 0) {
                          videoHeight = MediaQuery.of(context).size.width /
                              _controller.value.aspectRatio;
                          videoWidth = MediaQuery.of(context).size.width;
                          videoMargin = 0;
                        } else {
                          videoHeight = MediaQuery.of(context).size.height;
                          videoWidth =
                              videoHeight * _controller.value.aspectRatio;
                          videoMargin =
                              (MediaQuery.of(context).size.width - videoWidth) /
                                  2;
                        }
                        //Переменные дабл тапа, зависимые от размеров видео
                        doubleTapRWidthFS = videoWidth;
                        doubleTapRHeightFS = videoHeight - 36;
                        doubleTapLWidthFS = videoWidth;
                        doubleTapLHeightFS = videoHeight;

                        /* if (_seek && _controller.value.duration.inSeconds > 2) {
                          _controller.seekTo(Duration(microseconds: position));
                          _seek = false;
                        } */

                        //Отрисовка элементов плеера
                        return Stack(
                          children: <Widget>[
                            Container(
                              height: videoHeight,
                              width: videoWidth,
                              margin: EdgeInsets.only(left: videoMargin),
                              child: VideoPlayer(_controller),
                            ),
                            _videoOverlay(),
                          ],
                        );
                      } else {
                        return Center(
                          heightFactor: 6,
                          child: CircularProgressIndicator(
                            strokeWidth: 4,
                            valueColor: AlwaysStoppedAnimation<Color>(
                              Color(0xFF22A3D2),
                            ),
                          ),
                        );
                      }
                    }),
                //Редактируем размер области дабл тапа при показе оверлея.
                // Сделано для открытия кнопок 'Во весь экран' и 'Качество'
                onTap: () {
                  setState(() {
                    _overlay = !_overlay;
                    if (_overlay) {
                      doubleTapRHeightFS = videoHeight - 36;
                      doubleTapLHeightFS = videoHeight - 10;
                      doubleTapRMarginFS = 36;
                      doubleTapLMarginFS = 10;
                    } else if (!_overlay) {
                      doubleTapRHeightFS = videoHeight + 36;
                      doubleTapLHeightFS = videoHeight;
                      doubleTapRMarginFS = 0;
                      doubleTapLMarginFS = 0;
                    }
                  });
                },
              ),
              GestureDetector(
                  child: Container(
                    width: doubleTapLWidthFS / 2 - 35,
                    height: doubleTapLHeightFS - 50,
                    margin: EdgeInsets.fromLTRB(
                        10, 0, doubleTapLWidthFS / 2 + 30, 40),
                    decoration: BoxDecoration(
                        //color: Colors.blue,
                        ),
                    child: _overlay && _controller != null
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              AnimatedDefaultTextStyle(
                                style: animatedLeft
                                    ? TextStyle(
                                        fontSize: 35,
                                        color: kAppThemeColor,
                                        fontWeight: FontWeight.bold)
                                    : TextStyle(
                                        fontSize: 0,
                                        color: kAppThemeColor.withOpacity(0.5),
                                        fontWeight: FontWeight.bold),
                                duration: Duration(milliseconds: animationTime),
                                onEnd: () {
                                  setState(() {
                                    animatedLeft = false;
                                  });
                                },
                                child: Text(
                                  '-5',
                                ),
                              ),
                              IconButton(
                                icon: Icon(Icons.replay_5, size: 50),
                                onPressed: () {
                                  setState(() {
                                    animatedLeft = true;

                                    _controller.seekTo(Duration(
                                        seconds: _controller
                                                .value.position.inSeconds -
                                            5));
                                  });
                                },
                              )
                            ],
                          )
                        : Center(
                            child: AnimatedDefaultTextStyle(
                              style: animatedLeft
                                  ? TextStyle(
                                      fontSize: 35,
                                      color: kAppThemeColor,
                                      fontWeight: FontWeight.bold)
                                  : TextStyle(
                                      fontSize: 0,
                                      color: kAppThemeColor.withOpacity(0.5),
                                      fontWeight: FontWeight.bold),
                              duration: Duration(milliseconds: animationTime),
                              onEnd: () {
                                setState(() {
                                  animatedLeft = false;
                                });
                              },
                              child: Text(
                                '-5',
                              ),
                            ),
                          ),
                  ),
                  //Редактируем размер области дабл тапа при показе оверлея.
                  // Сделано для открытия кнопок 'Во весь экран' и 'Качество'
                  onTap: () {
                    setState(() {
                      _overlay = !_overlay;
                      if (_overlay) {
                        doubleTapRHeightFS = videoHeight - 36;
                        doubleTapLHeightFS = videoHeight - 10;
                        doubleTapRMarginFS = 36;
                        doubleTapLMarginFS = 10;
                      } else if (!_overlay) {
                        doubleTapRHeightFS = videoHeight + 36;
                        doubleTapLHeightFS = videoHeight;
                        doubleTapRMarginFS = 0;
                        doubleTapLMarginFS = 0;
                      }
                    });
                  },
                  onDoubleTap: () {
                    setState(() {
                      animatedLeft = true;
                      _controller.seekTo(Duration(
                          seconds: _controller.value.position.inSeconds - 5));
                    });
                  }),
              GestureDetector(
                  child: Container(
                    width: doubleTapRWidthFS / 2 - 35,
                    height: doubleTapRHeightFS - 50,
                    margin: EdgeInsets.fromLTRB(doubleTapRWidthFS / 2 + 45, 10,
                        20, doubleTapLMarginFS + 40),
                    decoration: BoxDecoration(
                        // color: Colors.red,
                        ),
                    child: _overlay && _controller != null
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              IconButton(
                                icon: Icon(Icons.forward_5, size: 50),
                                onPressed: () {
                                  setState(() {
                                    animatedRight = true;

                                    _controller.seekTo(Duration(
                                        seconds: _controller
                                                .value.position.inSeconds +
                                            5));
                                  });
                                },
                              ),
                              AnimatedDefaultTextStyle(
                                style: animatedRight
                                    ? TextStyle(
                                        fontSize: 35,
                                        color: kAppThemeColor,
                                        fontWeight: FontWeight.bold)
                                    : TextStyle(
                                        fontSize: 0,
                                        color: kAppThemeColor.withOpacity(0.5),
                                        fontWeight: FontWeight.bold),
                                duration: Duration(milliseconds: animationTime),
                                onEnd: () {
                                  setState(() {
                                    animatedRight = false;
                                  });
                                },
                                child: Text(
                                  '+5',
                                ),
                              )
                            ],
                          )
                        : Center(
                            child: AnimatedDefaultTextStyle(
                              style: animatedRight
                                  ? TextStyle(
                                      fontSize: 35,
                                      color: kAppThemeColor,
                                      fontWeight: FontWeight.bold)
                                  : TextStyle(
                                      fontSize: 0,
                                      color: kAppThemeColor.withOpacity(0.5),
                                      fontWeight: FontWeight.bold),
                              duration: Duration(milliseconds: animationTime),
                              onEnd: () {
                                setState(() {
                                  animatedRight = false;
                                });
                              },
                              child: Text(
                                '+5',
                              ),
                            ),
                          ),
                  ),
                  //Редактируем размер области дабл тапа при показе оверлея.
                  // Сделано для открытия кнопок 'Во весь экран' и 'Качество'
                  onTap: () {
                    setState(() {
                      _overlay = !_overlay;
                      if (_overlay) {
                        doubleTapRHeightFS = videoHeight - 36;
                        doubleTapLHeightFS = videoHeight - 10;
                        doubleTapRMarginFS = 36;
                        doubleTapLMarginFS = 10;
                      } else if (!_overlay) {
                        doubleTapRHeightFS = videoHeight + 36;
                        doubleTapLHeightFS = videoHeight;
                        doubleTapRMarginFS = 0;
                        doubleTapLMarginFS = 0;
                      }
                    });
                  },
                  onDoubleTap: () {
                    setState(() {
                      animatedRight = true;
                      _controller.seekTo(Duration(
                          seconds: _controller.value.position.inSeconds + 5));
                    });
                  }),
            ],
          ),
        ),
      ),
    );
  }

  //================================ Quality ================================//
  /* void _settingModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.white,
        builder: (BuildContext bc) {
          final children = <Widget>[];
          _qualityValues.forEach((elem, value) => (children.add(new ListTile(
              title: new Text(' ${elem.toString()} fps'),
              onTap: () => {
                    //Обновление состояние приложения и перерисовка
                    setState(() {
                      _controller.pause();
                      position = _controller.value.position.inMicroseconds;
                      _controller = VideoPlayerController.network(value);
                      _controller.setLooping(true);
                      _seek = true;
                      initFuture = _controller.initialize().then((value) {
                        _controller = widget.listener(_controller);
                        _controller.addListener(() {
                          if (!_controller.value.isPlaying) return;
                          print('2 Coin: $coin MaxCoin: $maxCoin');
                          if (coin >= maxCoin) return;
                          if (_controller.value.isPlaying &&
                              _controller.value.position.inSeconds !=
                                  playerSecond) {
                            playerSecond = _controller.value.position.inSeconds;
                            second += 1;
                            if (second != 0 &&
                                second % 60 == 0 &&
                                _controller.value.isPlaying) {
                              coin = coin + 20;
                              if (coin <= maxCoin) {
                                VideoSetViewService.include
                                    .set(
                                  videoID: widget.videoModel.uid.toString(),
                                )
                                    .then((value) {
                                  updateUserState(value[0], value[1]);
                                });
                              }
                            }
                          }
                        });
                      });
                      _controller.play();
                    }),
                  }))));

          return Container(
            height: videoHeight,
            child: ListView(
              children: children,
            ),
          );
        });
  } */

  //================================ OVERLAY ================================//
  Widget _videoOverlay() {
    return _overlay
        ? Stack(
            children: <Widget>[
              GestureDetector(
                child: Center(
                  child: Container(
                    width: videoWidth,
                    height: videoHeight,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.centerRight,
                        end: Alignment.centerLeft,
                        colors: [
                          const Color(0x662F2C47),
                          const Color(0x662F2C47)
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Center(
                child: IconButton(
                    padding: EdgeInsets.only(
                      top: videoHeight / 2 - 60,
                      bottom: videoHeight / 2 - 5,
                    ),
                    icon: _controller.value.isPlaying
                        ? Icon(Icons.pause, size: 60.0)
                        : Icon(Icons.play_arrow, size: 60.0),
                    onPressed: () {
                      setState(() {
                        if (_controller.value.isPlaying) {
                          _controller.pause();
                          Wakelock.disable();
                        } else {
                          _controller.play();
                          Wakelock.enable();
                        }
                      });
                    }),
              ),
              Container(
                margin: EdgeInsets.only(
                    top: videoHeight - 80, left: videoWidth + videoMargin - 50),
                child: IconButton(
                    alignment: AlignmentDirectional.center,
                    icon: Icon(Icons.fullscreen, size: 35.0),
                    onPressed: () {
                      bool isPlaying = _controller.value.isPlaying;

                      Navigator.pop(context, {
                        'pos': _controller.value.position.inMicroseconds,
                        'isPlaying': isPlaying
                      });
                    }),
              ),
              /*  Container(
                margin: EdgeInsets.only(left: videoWidth + videoMargin - 48),
                child: IconButton(
                    icon: Icon(Icons.settings, size: 26.0),
                    onPressed: () {
                      //Burası değişebilir
                      _settingModalBottomSheet(context);
                      setState(() {});
                    }),
              ), */
              Container(
                //===== Ползунок =====//
                margin: EdgeInsets.only(
                    top: videoHeight - 40, left: videoMargin), //CHECK IT
                child: _videoOverlaySlider(),
              )
            ],
          )
        : Center();
  }

  //=================== ПОЛЗУНОК ===================//
  Widget _videoOverlaySlider() {
    return ValueListenableBuilder(
      valueListenable: _controller,
      builder: (context, VideoPlayerValue value, child) {
        if (!value.hasError && value.isInitialized) {
          return Row(
            children: <Widget>[
              Container(
                width: 46,
                alignment: Alignment(0, 0),
                child: Text(
                  value.position.inMinutes.toString() +
                      ':' +
                      (value.position.inSeconds - value.position.inMinutes * 60)
                          .toString(),
                  style: TextStyle(color: Colors.white60),
                ),
              ),
              Container(
                height: 20,
                width: videoWidth - 92,
                child: VideoProgressIndicator(
                  _controller,
                  allowScrubbing: false,
                  colors: VideoProgressColors(
                    playedColor: kAppThemeColor,
                    backgroundColor: Color(0x5515162B),
                    bufferedColor: Color(0x5583D8F7),
                  ),
                  padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
                ),
              ),
              Container(
                width: 46,
                alignment: Alignment(0, 0),
                child: Text(
                  value.duration.inMinutes.toString() +
                      ':' +
                      (value.duration.inSeconds - value.duration.inMinutes * 60)
                          .toString(),
                  style: TextStyle(color: Colors.white60),
                ),
              ),
            ],
          );
        } else {
          return Container();
        }
      },
    );
  }
}
