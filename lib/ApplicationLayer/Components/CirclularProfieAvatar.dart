import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

// ignore: must_be_immutable
class CircularProfileAvatar extends StatefulWidget {
  CircularProfileAvatar(
    this.imageUrl, {
    Key? key,
    this.initialsText = const Text(''),
    this.cacheImage = true,
    this.radius = 50.0,
    this.borderWidth = 0.0,
    this.borderColor = Colors.white,
    this.backgroundColor = Colors.white,
    this.elevation = 0.0,
    this.showInitialTextAbovePicture = false,
    required this.onTap,
    this.foregroundColor = Colors.transparent,
    required this.placeHolder,
    required this.errorWidget,
    required this.imageBuilder,
    required this.animateFromOldImageOnUrlChange,
    required this.child,
    required this.selectedImage,
  }) : super(key: key);

  File selectedImage;

  final double radius;

  final double elevation;

  final double borderWidth;

  final Color borderColor;

  final Color backgroundColor;

  final Color foregroundColor;

  final String imageUrl;

  final Text initialsText;

  final bool showInitialTextAbovePicture;

  final bool cacheImage;

  final GestureTapCallback onTap;

  final PlaceholderWidgetBuilder placeHolder;

  final LoadingErrorWidgetBuilder errorWidget;

  final ImageWidgetBuilder imageBuilder;

  final bool animateFromOldImageOnUrlChange;

  final Widget child;

  @override
  _CircularProfileAvatarState createState() => _CircularProfileAvatarState();
}

class _CircularProfileAvatarState extends State<CircularProfileAvatar> {
  late Widget _initialsText;

  @override
  Widget build(BuildContext context) {
    _initialsText = Center(child: widget.initialsText);
    return GestureDetector(
      onTap: widget.onTap,
      child: Material(
        type: MaterialType.circle,
        elevation: widget.elevation,
        color: widget.borderColor,
        child: Container(
            height: widget.radius * 2,
            width: widget.radius * 2,
            padding: EdgeInsets.all(widget.borderWidth),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(widget.radius),
                color: widget.borderColor),
            child: Center(
              child: Container(
                decoration: BoxDecoration(
                    color: widget.backgroundColor,
                    borderRadius: BorderRadius.circular(widget.radius)),
                child: widget.child == null
                    ? Stack(
                        fit: StackFit.expand,
                        children: widget.imageUrl.isEmpty
                            ? <Widget>[_initialsText]
                            : widget.showInitialTextAbovePicture
                                ? <Widget>[
                                    profileImage(
                                        selectedImage: widget.selectedImage),
                                    Container(
                                      decoration: BoxDecoration(
                                        color: widget.foregroundColor,
                                        borderRadius: BorderRadius.circular(
                                            widget.radius),
                                      ),
                                    ),
                                    _initialsText,
                                  ]
                                : <Widget>[
                                    _initialsText,
                                    profileImage(
                                        selectedImage: widget.selectedImage),
                                  ],
                      )
                    : child(),
              ),
            )),
      ),
    );
  }

  Widget child() {
    return ClipRRect(
      borderRadius: BorderRadius.circular(widget.radius),
      child: Container(
        height: widget.radius * 2 - widget.borderWidth,
        width: widget.radius * 2 - widget.borderWidth,
        child: widget.child,
      ),
    );
  }

  Widget profileImage({required File selectedImage}) {
    return selectedImage == null
        ? ClipRRect(
            borderRadius: BorderRadius.circular(widget.radius),
            child: CachedNetworkImage(
              fit: BoxFit.cover,
              imageUrl: widget.imageUrl,
              errorWidget: widget.errorWidget,
              placeholder: widget.placeHolder,
              imageBuilder: widget.imageBuilder,
              useOldImageOnUrlChange:
                  widget.animateFromOldImageOnUrlChange == null
                      ? false
                      : widget.animateFromOldImageOnUrlChange,
            ),
          )
        : ClipRRect(
            borderRadius: BorderRadius.circular(widget.radius),
            child: Image.file(
              selectedImage,
              fit: BoxFit.cover,
            ),
          );
  }
}
