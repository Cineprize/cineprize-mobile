import 'package:cineprize/ApplicationLayer/Auth/Login/LoginScreen.dart';
import 'package:cineprize/ApplicationLayer/Auth/Register/RegisterScreen.dart';
import 'package:cineprize/ApplicationLayer/Components/PageButtonsWidget.dart';
import 'package:flutter/material.dart';

class CustomButtonBarWidget extends StatefulWidget {
  final Color textColor1;
  final Color textColor2;

  const CustomButtonBarWidget(
      {Key? key, required this.textColor1, required this.textColor2})
      : super(key: key);

  @override
  _CustomButtonBarWidgetState createState() => _CustomButtonBarWidgetState();
}

class _CustomButtonBarWidgetState extends State<CustomButtonBarWidget> {
  @override
  Widget build(BuildContext context) {
    return ButtonBar(
      alignment: MainAxisAlignment.spaceAround,
      children: [
        PageButtonsWidget(
          text: 'Giriş Yap',
          textColor: widget.textColor1,
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => LoginScreen()),
            );
          },
        ),
        PageButtonsWidget(
          text: 'Kayıt Ol',
          textColor: widget.textColor2,
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => RegisterScreen()),
            );
          },
        )
      ],
    );
  }
}
