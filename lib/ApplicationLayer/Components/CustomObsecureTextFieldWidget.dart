//Custom Text Field

import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class CustomObsecureTextFieldWidget extends StatefulWidget {
  final String hintText;
  final Icon icon;
  final String initialValue;
  bool isObsecure;
  final Function(String) onSaved;
  final Function(String) validator;

  CustomObsecureTextFieldWidget({
    Key? key,
    required this.hintText,
    required this.icon,
    required this.initialValue,
    required this.isObsecure,
    required this.validator,
    required this.onSaved,
  }) : super(key: key);

  @override
  _CustomObsecureTextFieldWidgetState createState() =>
      _CustomObsecureTextFieldWidgetState();
}

class _CustomObsecureTextFieldWidgetState
    extends State<CustomObsecureTextFieldWidget> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      //Yukarıda TextField için tanımladığım fonksiyonu kullandım
      initialValue: this.widget.initialValue,
      style: kNormalText,
      obscureText:
          this.widget.isObsecure == null ? false : this.widget.isObsecure,
      textAlign: TextAlign.start,
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.grey[800],
        enabledBorder: baseBorder,
        errorBorder: baseBorder,
        focusedBorder: baseBorder,
        focusedErrorBorder: baseBorder,
        counterStyle: TextStyle(color: Colors.white),
        hintText: this.widget.hintText,
        hintStyle: kHintText,
        border: InputBorder.none,
        prefixIcon: widget.icon,
        suffixIcon: IconButton(
          splashColor: Colors.transparent,
          icon: Icon(
            this.widget.isObsecure
                ? CupertinoIcons.eye
                : CupertinoIcons.eye_slash,
            color: Colors.white,
          ),
          onPressed: () {
            setState(() {
              this.widget.isObsecure = !this.widget.isObsecure;
            });
          },
        ),
      ),
      validator: (_) => this.widget.validator(_!),
      onSaved: (_) => this.widget.onSaved(_!),
    );
  }

  OutlineInputBorder get baseBorder {
    return OutlineInputBorder(borderRadius: BorderRadius.circular(30));
  }
}
