import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:flutter/material.dart';

class CustomBottomButtonWidget extends StatefulWidget {
  final String text;

  final Function() onTap;
  final Color? color;
  final double? width;
  //Something like Navigator, run after onTap function which true or false return
  final Function()? onSuccess;
  final Function()? onError;

  const CustomBottomButtonWidget({
    Key? key,
    required this.text,
    required this.onTap,
    this.onSuccess,
    this.onError,
    this.color,
    this.width,
  }) : super(key: key);

  @override
  _CustomBottomButtonWidgetState createState() =>
      _CustomBottomButtonWidgetState();
}

class _CustomBottomButtonWidgetState extends State<CustomBottomButtonWidget> {
  bool onProcess = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        if (onProcess) return;
        setState(() {
          onProcess = true;
        });
        var response = await widget.onTap();

        setState(() {
          onProcess = false;
        });

        if (response && widget.onSuccess != null) {
          widget.onSuccess!();
        }

        if (!response && widget.onError != null) {
          widget.onError!();
        }
      },
      child: onProcess
          ? CircularProgressIndicator()
          : Container(
              decoration: BoxDecoration(
                color: widget.color == null ? Color(0xff3fd490) : widget.color,
                borderRadius: BorderRadius.circular(30),
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 15.0),
                child: Text(
                  widget.text,
                  textAlign: TextAlign.center,
                  style: kNormalText,
                ),
              ),
              width: widget.width == null ? double.infinity : widget.width),
    );
  }
}
