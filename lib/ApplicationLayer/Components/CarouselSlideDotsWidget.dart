//This is Carousel Slider bottom page dots widgets
import 'package:cineprize/Models/Slider.dart' as SliderModel;
import 'package:flutter/material.dart';

class CarouselSlideDots extends StatelessWidget {
  const CarouselSlideDots({
    Key? key,
    required this.sliders,
    required this.width,
    required int current,
  })  : _current = current,
        super(key: key);

  final double width;
  final int _current;
  final List<SliderModel.Slider> sliders;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 0,
      left: width / 4,
      right: width / 4,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: this.sliders.map((slider) {
          int index = this.sliders.indexOf(slider);
          return Container(
            width: 8.0,
            height: 8.0,
            margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 5.0),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: _current == index ? Colors.white : Colors.grey,
            ),
          );
        }).toList(),
      ),
    );
  }
}
