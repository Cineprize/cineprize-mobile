import 'dart:ui';

import 'package:cineprize/ApplicationLayer/Auth/AuthManagement/LoginNavigator.dart';
import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Home/Bodies/Channels/ChannelsStack.dart';
import 'package:cineprize/ApplicationLayer/Tools/Image/ImageTools.dart';
import 'package:cineprize/Models/Category.dart';
import 'package:flutter/material.dart';
import 'package:skeleton_text/skeleton_text.dart';

Widget buildSquareCard(BuildContext context, {required Category category}) {
  return GestureDetector(
    onTap: () => Navigator.push(
        loginNavigatorContext!,
        MaterialPageRoute(
            builder: (context) => ChannelStack(category: category))),
    child: Card(
      elevation: 0,
      color: Colors.transparent,
      child: AspectRatio(
        aspectRatio: 1,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(17),
          child: Container(
            decoration: BoxDecoration(color: Colors.black.withOpacity(0)),
            child: Stack(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: ImageTools.getImage(
                    imageURL: category.imageURL,
                    gifPlaceholder: gifPlaceholder,
                  ),
                ),
                Container(
                  color: Colors.black.withOpacity(0.7),
                ),
                Center(
                    child: Text(
                  category.name,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 25),
                )),
              ],
            ),
          ),
        ),
      ),
    ),
  );
}

Widget buildSquareCardSkeleton(BuildContext context) {
  double width = MediaQuery.of(context).size.width;
  return Card(
    elevation: 0,
    color: Colors.transparent,
    child: AspectRatio(
      aspectRatio: 1,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(17),
        child: Container(
          decoration: BoxDecoration(color: Colors.black.withOpacity(0)),
          child: Stack(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: SkeletonAnimation(
                  shimmerColor: shimmerColor,
                  child: Container(
                      width: width,
                      decoration: BoxDecoration(color: skeletonColor)),
                ),
              ),
              Container(
                color: Colors.black.withOpacity(0.7),
              ),
            ],
          ),
        ),
      ),
    ),
  );
}
