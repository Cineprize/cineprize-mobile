import 'package:flutter/material.dart';

class RowText extends StatefulWidget {
  final String text;
  final Widget editableText;

  const RowText({Key? key, required this.text, required this.editableText})
      : super(key: key);

  @override
  _RowTextState createState() => _RowTextState();
}

class _RowTextState extends State<RowText> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(children: [
          Padding(
              padding: const EdgeInsets.symmetric(vertical: 15.0),
              child: NormalText(text: '${widget.text}')),
          Expanded(child: widget.editableText)
        ]),
        Divider(color: Colors.white24),
      ],
    );
  }
}

class NormalText extends StatelessWidget {
  final String text;

  const NormalText({Key? key, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(text, style: TextStyle(fontSize: 17, color: Colors.white)));
  }
}
