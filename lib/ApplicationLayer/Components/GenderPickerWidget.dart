//Gender Picker

import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:flutter/material.dart';

class GenderPickerWidget extends StatelessWidget {
  const GenderPickerWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(left: 15, top: 13, bottom: 13),
      decoration: BoxDecoration(
        color: Colors.grey[800],
        borderRadius: BorderRadius.circular(30),
      ),
      child: Row(
        children: [
          Icon(Icons.person),
          Padding(
            padding: const EdgeInsets.only(left: 10.0),
            child: Text(
              'Cinsiyet',
              style: kHintText,
            ),
          ),
          Flexible(
            child: DropdownButtonFormField<String>(
              decoration: InputDecoration.collapsed(hintText: null),
              items: <String>['Kadın', 'Erkek'].map((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
              onChanged: (_) {},
            ),
          ),
          SizedBox(
            width: 15,
          )
        ],
      ),
    );
  }
}
