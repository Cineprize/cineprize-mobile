import 'package:flutter/cupertino.dart';

import '../Constants.dart';

class ModLoadingElement extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Container(
      height: height * 0.1,
      margin: EdgeInsets.all(height * 0.01),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: 20,
            height: 20,
            child: loadingIndicator,
          ),
          SizedBox(
            width: 10,
          ),
          Center(
            child: Text(
              "Yükleniyor...",
              style: kNormalText,
            ),
          ),
        ],
      ),
    );
  }
}
