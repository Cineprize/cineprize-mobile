// Logo

import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:flutter/material.dart';

class LogoWidget extends StatelessWidget {
  const LogoWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Container(
        height: height / 7,
        child: Image(image: AssetImage(logo), fit: BoxFit.contain));
  }
}
