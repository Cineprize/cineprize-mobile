import 'package:cineprize/ApplicationLayer/Components/VideoCardWidget.dart';
import 'package:flutter/material.dart';
import 'package:html/dom.dart' as htmlParser;
import 'package:skeleton_text/skeleton_text.dart';

class CustomHorizontalVideoBar extends StatefulWidget {
  final ScrollController? scrollController;
  final double? height;
  final String barHeading;
  final List<Widget> children;

  const CustomHorizontalVideoBar(
      {Key? key,
      required this.barHeading,
      required this.children,
      this.height,
      this.scrollController})
      : super(key: key);

  @override
  _CustomHorizontalVideoBarState createState() =>
      _CustomHorizontalVideoBarState();
}

class _CustomHorizontalVideoBarState extends State<CustomHorizontalVideoBar> {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Padding(
      padding: const EdgeInsets.only(left: 15.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          HorizontalVideoBarHeading(barHeading: widget.barHeading),
          SizedBox(height: 5),
          LimitedBox(
            maxHeight: this.widget.height == null
                ? height * (0.22)
                : this.widget.height!,
            child: ListView(
              controller: this.widget.scrollController,
              physics: BouncingScrollPhysics(),
              cacheExtent: 100,
              addAutomaticKeepAlives: true,
              scrollDirection: Axis.horizontal,
              children: this.widget.children,
            ),
          ),
        ],
      ),
    );
  }
}

class HorizontalVideoBarHeading extends StatefulWidget {
  const HorizontalVideoBarHeading({Key? key, required this.barHeading})
      : super(key: key);

  final String barHeading;

  @override
  _HorizontalVideoBarHeadingState createState() =>
      _HorizontalVideoBarHeadingState();
}

class _HorizontalVideoBarHeadingState extends State<HorizontalVideoBarHeading> {
  @override
  Widget build(BuildContext context) {
    return RichText(
        text: TextSpan(children: <TextSpan>[
      TextSpan(
        text: htmlParser.DocumentFragment.html('&#9646;').text,
        style: TextStyle(color: Color(0xff3fd490), fontSize: 26),
      ),
      TextSpan(
          text: widget.barHeading,
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
    ]));
  }
}

// Skeleton Area

class CustomHorizontalVideoBarSkeleton extends StatefulWidget {
  final ScrollController scrollController;
  final double height;

  const CustomHorizontalVideoBarSkeleton(
      {Key? key, required this.height, required this.scrollController})
      : super(key: key);

  @override
  _CustomHorizontalVideoBarSkeletonState createState() =>
      _CustomHorizontalVideoBarSkeletonState();
}

class _CustomHorizontalVideoBarSkeletonState
    extends State<CustomHorizontalVideoBarSkeleton> {
  @override
  Widget build(BuildContext context) {
    //double height = MediaQuery.of(context).size.height;
    double height = widget.height;
    return Padding(
      padding: const EdgeInsets.only(left: 15.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          HorizontalVideoBarHeadingSkeleton(),
          SizedBox(height: 5),
          LimitedBox(
            maxHeight: this.widget.height + 50,
            child: ListView(
              controller: this.widget.scrollController == null
                  ? null
                  : this.widget.scrollController,
              physics: BouncingScrollPhysics(),
              cacheExtent: 100,
              addAutomaticKeepAlives: true,
              scrollDirection: Axis.horizontal,
              children: buildVideoListSkeletonCard(context, 3, height),
            ),
          ),
        ],
      ),
    );
  }
}

class HorizontalVideoBarHeadingSkeleton extends StatefulWidget {
  const HorizontalVideoBarHeadingSkeleton({
    Key? key,
  }) : super(key: key);

  @override
  _HorizontalVideoBarHeadingSkeletonState createState() =>
      _HorizontalVideoBarHeadingSkeletonState();
}

class _HorizontalVideoBarHeadingSkeletonState
    extends State<HorizontalVideoBarHeadingSkeleton> {
  @override
  Widget build(BuildContext context) {
    double width2 = MediaQuery.of(context).size.width;

    return Row(children: [
      RichText(
        text: TextSpan(
          text: htmlParser.DocumentFragment.html('&#9646;').text,
          style: TextStyle(color: Color(0xff3fd490), fontSize: 30),
        ),
      ),
      SkeletonAnimation(
        child: Container(
          width: width2 / 3,
          height: 12,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              color: Colors.grey[300]),
        ),
      ),
    ]);
  }
}
