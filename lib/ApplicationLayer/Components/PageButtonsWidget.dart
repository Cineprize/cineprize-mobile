// Custom Page Buttons

import 'package:flutter/material.dart';

class PageButtonsWidget extends StatelessWidget {
  final text;
  final Color textColor;
  final Function() onPressed;

  const PageButtonsWidget(
      {Key? key,
      required this.text,
      required this.textColor,
      required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      onPressed: onPressed,
      child: Text(text, style: TextStyle(color: textColor, fontSize: 23)),
    );
  }
}
