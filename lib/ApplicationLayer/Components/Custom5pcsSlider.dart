import 'dart:io';

import 'package:cineprize/ApplicationLayer/Auth/AuthManagement/LoginNavigator.dart';
import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Home/Mods/DemoScreen.dart';
import 'package:cineprize/ApplicationLayer/Home/Mods/GiveawayScreen.dart';
import 'package:cineprize/ApplicationLayer/Home/Mods/GuessMainScreen.dart';
import 'package:cineprize/ApplicationLayer/Home/Mods/MissionScreen.dart';
import 'package:cineprize/ApplicationLayer/Home/Mods/SurveyScreen.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:cineprize/Tools/Ads/AdmobAds/AdHelper.dart';
import 'package:cineprize/Tools/Ads/AdmobAds/AdmobInitialize.dart';
import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class Custom5pcsSlider extends StatefulWidget {
  const Custom5pcsSlider({Key? key, required this.height}) : super(key: key);

  final double height;

  @override
  _Custom5pcsSliderState createState() => _Custom5pcsSliderState();
}

class _Custom5pcsSliderState extends State<Custom5pcsSlider> {
  List<Map<String, dynamic>> values = Platform.isAndroid
      ? [
          {'image': 'cekilis.png', 'page': GiveawayScreen()},
          {'image': 'anket.png', 'page': SurveyScreen()},
          {'image': 'tahmin.png', 'page': GuessMainScreen()},
          {'image': 'gorev.png', 'page': MissionScreen()},
          {'image': 'bonus.png', 'page': null},
          {'image': 'gundem.png', 'page': null},
          {'image': 'cark.png', 'page': null},
          {'image': 'yardim.png', 'page': null},
        ]
      : [
          {'image': 'anket_ios.png', 'page': SurveyScreen()},
          {'image': 'gorev_ios.png', 'page': MissionScreen()},
          {
            'image': 'tahmin_ios.png',
            'page': GuessMainScreen(),
            'validator': () {
              return UserManagement.include.user.canEarn;
            }
          },
          {'image': 'gundem_ios.png', 'page': null},
          {'image': 'cark_ios.png', 'page': null},
        ];

  List<Widget> widgetsList = [];

  RewardedAd? _rewardedAd;
  bool _isRewardedAdReady = false;
  RewardedAdLoadCallback? _adListener;

  @override
  void initState() {
    values.forEach((value) => {widgetsList.add(generateCard(value))});
    createRewardedAd();
    super.initState();
  }

  void initializeAd() {
    _adListener = RewardedAdLoadCallback(
      onAdLoaded: (RewardedAd ad) {
        setState(() {
          this._rewardedAd = ad;
          _isRewardedAdReady = true;
          _rewardedAd?.fullScreenContentCallback = FullScreenContentCallback(
            onAdDismissedFullScreenContent: (RewardedAd ad) {
              this._onAlertButtonsPressed(
                context: context,
                content:
                    'Mod sayfalarına erişim sağlayabilmek için videoları sonuna kadar izlemeniz gerekmektedir!',
                title: 'Uyarı!',
                alertType: AlertType.warning,
              );

              setState(() {
                _isRewardedAdReady = false;
              });
              ad.dispose();
              createRewardedAd();
            },
            onAdFailedToShowFullScreenContent: (RewardedAd ad, AdError error) {
              _onAlertButtonsPressed(
                context: context,
                content:
                    'Mod sayfalarına erişim sağlayabilmek için videoları sonuna kadar izlemeniz gerekmektedir!',
                title: 'Uyarı!',
                alertType: AlertType.warning,
              );
              setState(() {
                _isRewardedAdReady = false;
              });
              ad.dispose();
              createRewardedAd();
            },
          );
        });
      },
      onAdFailedToLoad: (LoadAdError error) {
        setState(() {
          _isRewardedAdReady = false;
        });
        createRewardedAd();
      },
    );
  }

  viewVideoAd(var page) {
    _rewardedAd!.show(onUserEarnedReward: (RewardedAd ad, RewardItem reward) {
      setState(() {
        _isRewardedAdReady = false;
      });

      ad.dispose();
      Navigator.push(
        loginNavigatorContext!,
        MaterialPageRoute(
          builder: (context) => page ?? SurveyScreen(),
        ),
      );
      createRewardedAd();
    });
  }

  void createRewardedAd() {
    initializeAd();
    AdmobInitialize.instance.rewardedAd(listener: _adListener!);
  }

  @override
  void dispose() {
    _rewardedAd!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 15.0),
      child: SizedBox(
        height: widget.height / 5.5,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: widgetsList,
        ),
      ),
    );
  }

  Widget generateCard(Map<String, dynamic> element) => InkWell(
      child: Padding(
          padding: const EdgeInsets.only(right: 8.0),
          child: Image(
              image: AssetImage('assets/images/Cards/${element['image']}'),
              height: widget.height / 12)),
      onTap: () => {
            if (element['page'] == null)
              {
                this._onAlertButtonsPressed(
                    context: context,
                    alertType: AlertType.info,
                    title: "Bilgi!",
                    content: "Çok yakında hizmetinizde!")
              }
            else
              {
                if (element['validator'] != null &&
                    element['validator']() == false)
                  {
                    this._onAlertButtonsPressed(
                        context: context,
                        alertType: AlertType.warning,
                        title: "Uyarı!",
                        content:
                            "Bu modu kullanabilmek için uygulama ayarlarından hesap kazancınızı aktif hale getirmeniz gerekmektedir.")
                  }
                else
                  {
                    this.viewVideoAd(element['page']),
                  },
              }
          });

  void _onAlertButtonsPressed({
    required context,
    AlertType alertType = AlertType.info,
    required String title,
    required String content,
  }) {
    Alert(
      context: context,
      style: AlertStyle(
        backgroundColor: kPopUpBackgroundColor,
        animationType: AnimationType.grow,
        isCloseButton: false,
        alertAlignment: Alignment.center,
        isOverlayTapDismiss: true,
        isButtonVisible: false,
        descStyle: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
        descTextAlign: TextAlign.center,
        animationDuration: Duration(milliseconds: 400),
        titleStyle: TextStyle(
            fontWeight: FontWeight.bold, color: Colors.white, fontSize: 25),
        alertBorder:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
      ),
      type: alertType,
      title: title,
      desc: content,
    ).show();
  }
}
