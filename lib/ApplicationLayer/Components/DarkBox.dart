import 'package:cineprize/ApplicationLayer/Auth/AuthManagement/LoginNavigator.dart';
import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:cineprize/ApplicationLayer/Home/Bodies/Channels/ChannelsStack.dart';
import 'package:cineprize/ApplicationLayer/Tools/Image/ImageTools.dart';
import 'package:cineprize/Models/Category.dart';
import 'package:flutter/material.dart';
import 'package:skeleton_text/skeleton_text.dart';

class DarkBox extends StatelessWidget {
  DarkBox({
    Key? key,
    required this.category,
  }) : super(key: key);

  final Category category;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
          loginNavigatorContext!,
          MaterialPageRoute(
              builder: (context) => ChannelStack(category: this.category))),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(17),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.black.withOpacity(0),
          ),
          child: Stack(
            children: [
              ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: ImageTools.getImage(
                      imageURL: this.category.imageURL,
                      gifPlaceholder: gifPlaceholder)),
              Container(color: Colors.black.withOpacity(0.7)),
              Center(
                  child: Text(
                this.category.name == null ? '' : this.category.name,
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 30),
              )),
            ],
          ),
        ),
      ),
    );
  }
}

class DarkBoxSkeleton extends StatelessWidget {
  DarkBoxSkeleton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width2 = MediaQuery.of(context).size.width;

    return ClipRRect(
      borderRadius: BorderRadius.circular(17),
      child: Container(
        decoration: BoxDecoration(color: Colors.black.withOpacity(0)),
        child: Stack(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: SkeletonAnimation(
                  shimmerColor: shimmerColor,
                  child: Container(
                    width: width2,
                    decoration: BoxDecoration(color: skeletonColor),
                  )),
            ),
            Container(color: Colors.black.withOpacity(0.7)),
            Center(
              child: SkeletonAnimation(
                shimmerColor: shimmerColor,
                child: Container(
                  width: width2 / 4,
                  height: 13,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5.0),
                      color: skeletonTextColor),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
