import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:flutter/material.dart';

class DropdownWidget extends StatefulWidget {
  final String title;
  final List<String> items;
  final ValueChanged<String> itemCallBack;
  final String currentItem;
  final String hintText;

  DropdownWidget({
    required this.title,
    required this.items,
    required this.itemCallBack,
    required this.currentItem,
    required this.hintText,
  });

  @override
  State<StatefulWidget> createState() => _DropdownState(currentItem);
}

class _DropdownState extends State<DropdownWidget> {
  List<DropdownMenuItem<String>> dropDownItems = [];
  String currentItem;

  _DropdownState(this.currentItem);

  @override
  void initState() {
    super.initState();
    for (String item in widget.items) {
      dropDownItems.add(DropdownMenuItem(
        value: item,
        child: Text(item, style: kNormalText),
      ));
    }
  }

  @override
  void didUpdateWidget(DropdownWidget oldWidget) {
    if (this.currentItem != widget.currentItem) {
      setState(() {
        this.currentItem = widget.currentItem;
      });
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: DropdownButtonHideUnderline(
        child: DropdownButton(
          dropdownColor: kAppBlack,
          icon: Icon(
            Icons.arrow_drop_down,
            color: Colors.white,
          ),
          value: currentItem,
          isExpanded: true,
          items: dropDownItems,
          onChanged: (selectedItem) => setState(() {
            currentItem = selectedItem.toString();
            widget.itemCallBack(currentItem);
          }),
        ),
      ),
    );
  }
}
