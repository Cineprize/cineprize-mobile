import 'package:flutter/material.dart';

class UserTools {
  static ImageProvider getProfilePhoto(
      {required String imageURL, double? height}) {
    return Image.network(imageURL, height: height).image;
  }
}
