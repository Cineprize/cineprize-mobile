import 'package:cached_network_image/cached_network_image.dart';
import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:flutter/material.dart';
import 'package:skeleton_text/skeleton_text.dart';

class ImageTools {
  static CachedNetworkImage getImage(
      {required String imageURL, String? gifPlaceholder}) {
    return CachedNetworkImage(
      imageUrl: imageURL,
      imageBuilder: (context, imageProvider) => Container(
        decoration: BoxDecoration(
            image: DecorationImage(image: imageProvider, fit: BoxFit.fill)),
      ),
      placeholder: (context, url) => Container(
        child: gifPlaceholder == null
            ? GlowingOverscrollIndicator(
                color: kSplashScreenBackgroundColor,
                axisDirection: AxisDirection.up,
                child: Container(color: kSplashScreenBackgroundColor))
            : SkeletonAnimation(
                shimmerColor: shimmerColor,
                child: Container(color: skeletonColor)),
      ),
      errorWidget: (context, url, error) => Icon(Icons.error),
    );
  }
}
