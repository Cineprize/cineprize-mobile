import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class MyInfoDialog {
  final BuildContext context;
  final String title;
  final String description;
  final String buttonText;
  final AlertType alertType;
  final Function()? func;
  final bool closable;

  MyInfoDialog(
    this.context, {
    required this.title,
    required this.description,
    required this.buttonText,
    required this.alertType,
    this.closable = true,
    this.func,
  }) {
    showDialog();
  }

  Future showDialog() {
    return Alert(
      context: context,
      style: AlertStyle(
        backgroundColor: kPopUpBackgroundColor,
        animationType: AnimationType.grow,
        isCloseButton: false,
        isOverlayTapDismiss: false,
        descStyle: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
        descTextAlign: TextAlign.center,
        animationDuration: Duration(milliseconds: 400),
        titleStyle: TextStyle(
            fontWeight: FontWeight.bold, color: Colors.white, fontSize: 25),
        alertBorder:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
      ),
      type: alertType,
      title: title,
      desc: description,
      buttons: [
        DialogButton(
          child: Text(buttonText,
              style: TextStyle(color: Colors.white, fontSize: 18)),
          onPressed: () async {
            if (closable) Navigator.of(context, rootNavigator: true).pop(true);
            if (func != null) func!();
          },
          color: Color.fromRGBO(0, 179, 134, 1.0),
        ),
      ],
    ).show();
  }
}

class MyVideoAdDialog {
  final BuildContext context;
  final String title;
  final String description;
  final String firstButtonText;
  final String secondButtonText;
  final AlertType alertType;
  final Function() firstButtonFunction;

  MyVideoAdDialog(
    this.context, {
    required this.title,
    required this.description,
    required this.firstButtonText,
    required this.secondButtonText,
    required this.alertType,
    required this.firstButtonFunction,
  }) {
    showDialog();
  }

  Future showDialog() {
    return Alert(
      context: context,
      style: AlertStyle(
        backgroundColor: kPopUpBackgroundColor,
        animationType: AnimationType.grow,
        isCloseButton: false,
        isOverlayTapDismiss: false,
        descStyle: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
        descTextAlign: TextAlign.center,
        animationDuration: Duration(milliseconds: 400),
        titleStyle: TextStyle(
            fontWeight: FontWeight.bold, color: Colors.white, fontSize: 25),
        alertBorder: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
      ),
      type: alertType,
      title: title,
      desc: description,
      buttons: [
        DialogButton(
          child: Text(firstButtonText,
              style: TextStyle(color: Colors.white, fontSize: 18)),
          onPressed: firstButtonFunction,
          color: Color.fromRGBO(0, 179, 134, 1.0),
        ),
        DialogButton(
          child: Text(secondButtonText,
              style: TextStyle(color: Colors.white, fontSize: 18)),
          onPressed: () async {
            Navigator.of(context, rootNavigator: true).pop(true);
          },
          color: Color.fromRGBO(0, 179, 134, 1.0),
        ),
      ],
    ).show();
  }
}
