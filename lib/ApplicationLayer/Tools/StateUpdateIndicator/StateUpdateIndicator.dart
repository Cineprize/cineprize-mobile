import 'package:cineprize/ServiceLayer/Management/StateManagement.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class StateUpdateIndicator {
  static Widget stateControl(
      {required BuildContext context, required Widget child}) {
    final StateManagement stateManagement =
        Provider.of<StateManagement>(context);

    switch (stateManagement.state) {
      case ViewStateEnum.Idle:
        return child;
      default:
        // case ViewStateEnum.Busy:
        return Center(child: CircularProgressIndicator());
    }
  }
}
