import 'package:cineprize/ApplicationLayer/Constants.dart';
import 'package:flutter/material.dart';

import 'Tools/Image/ImageTools.dart';

class CategoryBox extends StatefulWidget {
  const CategoryBox({Key? key, required this.imageURL, required this.title})
      : super(key: key);

  final String imageURL;
  final String title;

  @override
  _CategoryBoxState createState() => _CategoryBoxState();
}

class _CategoryBoxState extends State<CategoryBox> {
  bool isTapped = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => {
        setState(() => {isTapped = !isTapped})
      },
      child: ClipRRect(
        borderRadius: BorderRadius.circular(15),
        child: Container(
          child: Stack(
            children: [
              // ColorFiltered(
              //     colorFilter: isTapped
              //         ? ColorFilter.mode(
              //             Colors.transparent,
              //             BlendMode.multiply,
              //           )
              //         : ColorFilter.mode(
              //             Colors.grey,
              //             BlendMode.saturation,
              //           ),
              //     child: Container(
              //       decoration: BoxDecoration(
              //           image: DecorationImage(
              //               image: AssetImage(
              //                   'assets/images/Categories/sport.png'))),
              //     )),
              ColorFiltered(
                colorFilter: isTapped
                    ? ColorFilter.mode(Colors.transparent, BlendMode.multiply)
                    : ColorFilter.mode(Colors.grey, BlendMode.saturation),
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: ImageTools.getImage(
                        imageURL: widget.imageURL,
                        gifPlaceholder: gifPlaceholder)),
              ),
              Align(
                  alignment: Alignment(0, 0.7),
                  child: Text(widget.title,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.bold)))
            ],
          ),
          //color: Colors.teal[100],
        ),
      ),
    );
  }
}
