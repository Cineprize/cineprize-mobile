class Giveaway {
  String id;
  String title;
  String description;
  String imageURL;
  int price;
  DateTime endDate;
  bool attendable;

  Giveaway({
    required this.id,
    required this.title,
    required this.description,
    required this.price,
    required this.imageURL,
    required this.endDate,
    required this.attendable,
  });

  Giveaway.fromMap(Map<String, dynamic> map)
      : this.id = map['id'].toString(),
        this.title = map['title'].toString(),
        this.description = map['description'].toString(),
        this.price = map['price'],
        this.imageURL = map['imageURL'].toString(),
        this.endDate = DateTime.parse(map['end_date'].toString()),
        this.attendable = map['attendable'];

  Map<String, dynamic> toMap() {
    return {
      'id': this.id,
      'title': this.title,
      'description': this.description,
      'price': this.price,
      'imageURL': this.imageURL,
      'endDate': this.endDate,
      'attendable': this.attendable,
    };
  }
}
