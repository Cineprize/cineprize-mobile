class Response {
  bool success;
  String message = '';
  dynamic data;

  Response({
    this.success = true,
    this.message = '',
    this.data,
  });

  Response.fromMap(Map<String, dynamic> map)
      : this.success = map['success'],
        this.message = map['message'].toString(),
        this.data = map['data'].toString();

  Map<String, dynamic> toMap() {
    return {
      'success': this.success,
      'message': this.message,
      'data': this.data,
    };
  }

  @override
  String toString() {
    return 'Response {success: $success, message: $message, data: $data}';
  }
}
