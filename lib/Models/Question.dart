class Question {
  String id;
  String title;
  String answerOne;
  String answerTwo;
  bool correctAnswer;
  int durationSecond;

  Question({
    required this.id,
    required this.title,
    required this.correctAnswer,
    required this.answerOne,
    required this.answerTwo,
    required this.durationSecond,
  });

  Question.fromMap(Map<String, dynamic> map)
      : this.id = map['id'].toString(),
        this.title = map['text'].toString(),
        this.correctAnswer = map['reply'] == 1 ? true : false,
        this.answerOne = map['answer_one'].toString(),
        this.answerTwo = map['answer_two'].toString(),
        this.durationSecond = (int.parse(map['showTime'].toString())).toInt();

  Map<String, dynamic> toMap() {
    return {
      'id': this.id,
      'title': this.title,
      'correctAnswer': this.correctAnswer,
      'answerOne': this.answerOne,
      'answerTwo': this.answerTwo,
      'durationSecond': this.durationSecond
    };
  }

  @override
  String toString() {
    return 'Question{id: $id, title: $title, correctAnswer: $correctAnswer, durationSecond: $durationSecond}';
  }
}
