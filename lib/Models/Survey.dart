class Survey {
  String? uid;
  String? content;
  List<dynamic>? options;

  Survey({
    this.uid,
    this.content,
    this.options,
  });

  Survey.fromMap(Map<String, dynamic> map, String uid)
      : this.uid = uid,
        this.content = map['content'],
        this.options = map['options'];

  @override
  String toString() {
    return 'Survey{uid: $uid, content: $content, options: $options}';
  }
}
