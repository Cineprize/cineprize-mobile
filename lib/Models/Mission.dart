class Mission {
  String id;
  String text;
  String url;
  String image;
  int coin;
  int gold;
  bool clicked;
  bool completed;

  Mission({
    required this.id,
    required this.text,
    required this.image,
    required this.url,
    required this.coin,
    required this.gold,
    required this.clicked,
    required this.completed,
  });

  Mission.fromMap(Map<String, dynamic> map)
      : this.id = map['id'].toString(),
        this.text = map['text'].toString(),
        this.url = map['url'].toString(),
        this.image = map['mobile_image'].toString(),
        this.coin = int.parse(map['coin'].toString()),
        this.gold = int.parse(map['gold'].toString()),
        this.clicked = map['clicked'],
        this.completed = map['completed'];

  Map<String, dynamic> toMap() {
    return {
      'id': this.id,
      'text': this.text,
      'url': this.url,
      'image': this.image,
      'coin': this.coin,
      'gold': this.gold,
      'clicked': this.clicked,
      'completed': this.completed,
    };
  }
}
