import 'dart:convert';

import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/material.dart';

class User {
  String? uid;
  String? name;
  String? lastName;
  String? description;
  late String email;
  late String userName;
  String? gender;
  String? refer;
  String? province;
  String? district;
  String? address;
  String? phoneNumber;
  String? imageURL;
  String? backgroundImage;
  late String role;
  late bool twitterFollow;
  late bool instagramFollow;
  late bool telegramFollow;
  late bool isLocked;
  late bool canEarn;
  late int coin;
  late int gold;
  late int usedGold;
  late int answerCount;
  DateTime? birthDate;

  List<Reference>? references;
  String? errorMessage;
  late String token;

  User.init();

  User({
    this.uid,
    this.name,
    this.lastName,
    this.description,
    required this.email,
    this.gender = "",
    this.refer,
    this.province = "",
    this.district = "",
    this.address = "",
    this.imageURL = "",
    this.backgroundImage = "",
    this.phoneNumber,
    required this.coin,
    required this.gold,
    required this.usedGold,
    required this.answerCount,
    required this.userName,
    this.birthDate,
    this.references,
    required this.twitterFollow,
    required this.instagramFollow,
    required this.telegramFollow,
    required this.isLocked,
    required this.canEarn,
    required this.role,
    required this.token,
  });

  User.fromMap(Map<String, dynamic> map)
      : this.uid = map['id'].toString(),
        this.token = map['token'] == null
            ? UserManagement.include.user.token.toString()
            : map['token'].toString(),
        this.name = map['name'] ?? "",
        this.lastName = map['lastName'] ?? "",
        this.description = map['description'] ?? "",
        this.email = map['email'].toString(),
        this.userName = map['userName'].toString(),
        this.coin = map['coin'],
        this.gold = map['gold'],
        this.usedGold = map['usedGold'],
        this.gender = map['gender'].toString(),
        this.refer = map['refer'] == null ? '' : map['refer'].toString(),
        this.province =
            map['province'] == null ? '' : map['province'].toString(),
        this.district =
            map['district'] == null ? '' : map['district'].toString(),
        this.address = map['address'] == null ? '' : map['address'].toString(),
        this.imageURL = map['profile_photo_url'].toString(),
        this.backgroundImage = map['backgroundImage'].toString(),
        this.phoneNumber =
            map['phoneNumber'] == null ? null : map['phoneNumber'].toString(),
        this.twitterFollow = map['twitterFollow'] == 1 ? true : false,
        this.instagramFollow = map['instagramFollow'] == 1 ? true : false,
        this.telegramFollow = map['telegramFollow'] == 1 ? true : false,
        this.isLocked = map['isLocked'] == 1 ? true : false,
        this.canEarn = map['canEarn'] == 1 ? true : false,
        this.birthDate = map['birthDate'] != null
            ? DateTime.parse(map['birthDate'].toString())
            : null,
        this.references = map['references'] != null
            ? List<Reference>.from(map['references']
                .toString()
                .split(',')
                .map((x) => Reference(userName: x)))
            : [],
        this.role = map['role'].toString(),
        this.answerCount = map['answers_count'] ?? 0;

  User.error({required this.errorMessage});

  Map<String, dynamic> toMap() {
    return {
      'uid': this.uid,
      'token': this.token,
      'name': this.name,
      'lastName': this.lastName,
      'description': this.description,
      'email': this.email,
      'userName': this.userName,
      'coin': this.coin,
      'gold': this.gold,
      'usedGold': this.usedGold,
      'answerCount': this.answerCount,
      'gender': this.gender,
      'phoneNumber': this.phoneNumber,
      'refer': this.refer,
      'province': this.province,
      'district': this.district,
      'address': this.address,
      'imageURL': this.imageURL,
      'birthDate': this.birthDate,
      'twitterFollow': this.twitterFollow,
      'instagramFollow': this.instagramFollow,
      'telegramFollow': this.telegramFollow,
      'isLocked': this.isLocked,
      'canEarn': this.canEarn,
      'role': this.role,
      'references': this.references != null
          ? List<Reference>.from(references!.map((x) => x.toMap()))
          : null,
    };

    //TODO:: check references
  }

  @override
  String toString() {
    return 'User{uid: $uid, userName: $userName, canEarn: $canEarn gold: $gold, coin: $coin, usedGold: $usedGold, birthDate: $birthDate, isLocked: $isLocked, refer: $refer, twitterFollow: $twitterFollow, instagramFollow: $instagramFollow, telegramFollow: $telegramFollow, answerCount: $answerCount}';
  }
}

class Reference {
  Reference({required this.userName});

  String userName;

  factory Reference.fromJson(String str) => Reference.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Reference.fromMap(username) => Reference(userName: username);

  Map<String, dynamic> toMap() => {'userName': userName};
}
