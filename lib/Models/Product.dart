import 'dart:io';

class Product {
  String uid;
  String name;
  String description;
  int count;
  int price;
  String imageURL;
  bool haveLine;

  Product({
    required this.uid,
    required this.name,
    required this.description,
    required this.count,
    required this.price,
    required this.imageURL,
    required this.haveLine,
  });

  Product.fromMap(Map<String, dynamic> map, String uid)
      : this.uid = uid,
        this.name = Platform.isAndroid
            ? map['name']
            : map['nameForIOS'] == null
                ? map['name']
                : map['nameForIOS'],
        this.description = Platform.isAndroid
            ? map['description']
            : map['descriptionForIOS'] == null
                ? map['description']
                : map['descriptionForIOS'],
        this.count = map['count'],
        this.price = Platform.isAndroid
            ? map['price']
            : map['priceIOS'] == 0 || map['priceIOS'] == null
                ? map['price']
                : map['priceIOS'],
        this.imageURL =
            Platform.isAndroid ? map['mobile_image'] : map['ImageForIOS'],
        this.haveLine = map['haveLine'] == 1;

  Map<String, dynamic> toMap() {
    return {
      'uid': this.uid,
      'name': this.name,
      'description': this.description,
      'count': this.count,
      'price': this.price,
      'imageURL': this.imageURL,
      'haveLine': this.haveLine,
    };
  }
}
