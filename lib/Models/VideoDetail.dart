import 'package:cineprize/Models/Comment.dart';
import 'package:cineprize/Models/Question.dart';

class VideoDetail {
  late List<Comment> lastComments;
  late int commentCount;
  late int adPerQuestion;
  late bool canViewAd;
  late bool liked;
  late bool disliked;
  late bool isWinnable;
  late int view;
  late List<Question> questions;
  late String seasonButtonMessage;
  late String seasonButtonText;

  VideoDetail();

  VideoDetail.fromMap(Map<String, dynamic> map, List<Question> _questions,
      List<Comment> _comments)
      : this.lastComments = _comments,
        this.commentCount = map['video']['comments_count'],
        this.adPerQuestion = map['adPerQuestion'],
        this.canViewAd = map['canViewAd'],
        this.liked = map['like'],
        this.disliked = map['dislike'],
        this.isWinnable = map['video']['isWinnable'],
        this.view = map['view'] == null ? 0 : map['view']['price'],
        this.seasonButtonMessage = map['seasonButtonMessage'],
        this.seasonButtonText = map['seasonButtonText'],
        this.questions = _questions;

  Map<String, dynamic> toMap() {
    return {
      'lastComments': this.lastComments,
      'commentCount': this.commentCount,
      'adPerQuestion': this.adPerQuestion,
      'canViewAd': this.canViewAd,
      'liked': this.liked,
      'disliked': this.disliked,
      'isWinnable': this.isWinnable,
      'view': this.view,
      'seasonButtonMessage': this.seasonButtonMessage,
      'seasonButtonText': this.seasonButtonText,
      'questions': this.questions,
    };
  }
}
