class Slider {
  String? title;
  String? imageURL;
  String? pageName;

  Slider.fromMap(Map<String, dynamic> map)
      : this.title = map['title'] ?? "",
        this.imageURL = map['mobile_image'] ?? "",
        this.pageName = map['pageName'] ?? "";

  Map<String, dynamic> toMap() {
    return {
      'title': this.title,
      'imageURL': this.imageURL,
      'pageName': this.pageName,
    };
  }

  @override
  String toString() {
    return 'Slider{title: $title, imageURL: $imageURL, pageName: $pageName}';
  }
}
