class Category {
  String uid;
  String name;
  String imageURL;
  bool isHome;

  Category({
    required this.uid,
    required this.name,
    required this.imageURL,
    required this.isHome,
  });

  Category.fromMap(Map<String, dynamic> map)
      : this.uid = map['id'].toString(),
        this.name = map['name'].toString(),
        this.imageURL = map['mobile_image'].toString(),
        this.isHome = map['isHome'] == 1 ? true : false;

  Map<String, dynamic> toMap() {
    return {
      'uid': this.uid,
      'name': this.name,
      'imageURL': this.imageURL,
      'isHome': this.isHome,
    };
  }

  @override
  String toString() {
    return 'Category {uid: $uid, name: $name, imageURL: $imageURL, isHome: $isHome}';
  }
}
