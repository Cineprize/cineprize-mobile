import 'package:cineprize/Models/Product.dart';

class Sale {
  String id;
  String product_id;
  String orderCode;
  String price;
  DateTime created_at;
  Product product;

  Sale.fromMap(Map<String, dynamic> map)
      : this.id = map['id'].toString(),
        this.product_id = map['product_id'].toString(),
        this.orderCode = map['orderCode'],
        this.price = map['price'].toString(),
        this.created_at = DateTime.parse(map['created_at']),
        this.product = Product.fromMap(map['product'], map['product_id'].toString());
}
