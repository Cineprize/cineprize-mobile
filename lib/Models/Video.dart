import 'package:cineprize/Models/Category.dart';
import 'package:flutter/cupertino.dart';

import 'User.dart';

class Video {
  late String uid;
  late String title;
  late String description;
  late String imageURL;
  late String vimeoID;
  late User user;
  late Category category;
  late bool isLiked = false;
  late bool isDisliked = false;
  late bool isActive = true;
  late bool isWinnable = true;
  String? errorMessage = '';

  Video.error({required this.errorMessage});

  Video({
    required this.uid,
    required this.title,
    required this.description,
    required this.vimeoID,
    required this.user,
    required this.category,
    required this.imageURL,
    required this.isLiked,
    required this.isDisliked,
    required this.isActive,
    required this.isWinnable,
  });

  Video.fromMap(Map<String, dynamic> map)
      : this.uid = map['id'].toString(),
        this.title = map['title'].toString(),
        this.user = User.fromMap(map['user']),
        this.description = map['description'].toString(),
        this.vimeoID = map['vimeo_id'].toString(),
        this.category = Category.fromMap(map['category']),
        this.imageURL = map['mobile_image'].toString(),
        this.isActive = map['isActive'] == 1 ? true : false;

  Map<String, dynamic> toMap() {
    return {
      'uid': this.uid,
      'title': this.title,
      'description': this.description,
      'vimeoID': this.vimeoID,
      'category': this.category,
      'imageURL': this.imageURL,
      'isActive': this.isActive,
      'isWinnable': this.isWinnable,
    };
  }
}
