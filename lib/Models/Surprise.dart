class Surprise {
  String uid;
  String name;
  String description;
  String user_id;
  String userName;
  String currentPrice;
  String imageURL;
  bool haveLine;

  Surprise({
    required this.uid,
    required this.name,
    required this.description,
    required this.user_id,
    required this.userName,
    required this.currentPrice,
    required this.imageURL,
    required this.haveLine,
  });

  Surprise.fromMap(Map<String, dynamic> map, String uid)
      : this.uid = uid,
        this.name = map['name'],
        this.description = map['description'],
        this.userName = map['userName'],
        this.user_id = map['user_id'].toString(),
        this.currentPrice = map['currentPrice'].toString(),
        this.imageURL = map['mobile_image'],
        this.haveLine = map['haveLine'] == 1;

  Map<String, dynamic> toMap() {
    return {
      'uid': this.uid,
      'name': this.name,
      'description': this.description,
      'user_id': this.user_id,
      'userName': this.userName,
      'currentPrice': this.currentPrice,
      'imageURL': this.imageURL,
      'haveLine': this.haveLine,
    };
  }
}
