import 'package:cineprize/Models/User.dart';

class Comment {
  String id;
  User author;
  String content = "";
  int likeCount = 0;
  bool isLiked = false;
  /* String? errorMessage; */

  Comment({
    required this.id,
    required this.author,
    required this.content,
    required this.likeCount,
    required this.isLiked,
  });

  /* Comment.error({this.errorMessage}); */

  Comment.fromMap(Map<String, dynamic> map)
      : this.id = map['id'].toString(),
        this.author = User.fromMap(map['user']),
        this.content = map['text'].toString(),
        this.likeCount = map['likeCount'],
        this.isLiked = map['isLiked'];

  Map<String, dynamic> toMap() {
    return {
      'id': this.id,
      'author': this.author,
      'content': this.content,
      'likeCount': this.likeCount,
      'isLiked': this.isLiked,
    };
  }

  @override
  String toString() {
    return 'Comment{id: $id, author: $author, likeCount: $likeCount, content: $content, isLiked: $isLiked}';
  }
}
