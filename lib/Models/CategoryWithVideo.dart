import 'dart:convert';

import 'package:cineprize/Models/Video.dart';

List<CategoryWithVideo> categoryWithVideoFromJson(String str) =>
    List<CategoryWithVideo>.from(
        json.decode(str).map((x) => CategoryWithVideo.fromMap(x)));

String categoryWithVideoToJson(List<CategoryWithVideo> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

class CategoryWithVideo {
  CategoryWithVideo({
    required this.id,
    required this.name,
    required this.imageURL,
    required this.isActive,
    required this.isHome,
    required this.queue,
    this.createdAt,
    required this.videos,
  });

  int id;
  String name;
  String imageURL;
  int isActive;
  int isHome;
  int queue;
  dynamic createdAt;
  dynamic updatedAt;
  List<Video> videos;

  factory CategoryWithVideo.fromMap(Map<String, dynamic> json) =>
      CategoryWithVideo(
        id: json['id'],
        name: json['name'],
        imageURL: json['image'],
        isActive: json['isActive'],
        isHome: json['isHome'],
        queue: json['queue'],
        createdAt: json['created_at'],
        videos: List<Video>.from(json['videos'].map((x) => Video.fromMap(x))),
      );

  Map<String, dynamic> toMap() => {
        'id': id,
        'name': name,
        'imageURL': imageURL,
        'isActive': isActive,
        'isHome': isHome,
        'queue': queue,
        'created_at': createdAt,
        'videos': List<dynamic>.from(videos.map((x) => x.toMap())),
      };
}
