import 'dart:io';

import 'package:cineprize/DataAccessLayer/DataAccessLayerVariable.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:package_info/package_info.dart';

class ErrorLogManagement {
  static logged(
      {required String scope,
      required String message,
      String userId = ""}) async {
    final url = Uri.parse('$baseApiUrl/error-logged');
    String? platform, version, flutter, sdk, company, model;
    if (Platform.isAndroid) {
      platform = 'Android';
      var androidInfo = await DeviceInfoPlugin().androidInfo;
      version = androidInfo.version.release;
      sdk = androidInfo.version.sdkInt.toString();
      company = androidInfo.manufacturer;
      model = androidInfo.model;
    } else if (Platform.isIOS) {
      platform = 'IOS';
      var iosInfo = await DeviceInfoPlugin().iosInfo;
      platform = iosInfo.systemName;
      version = iosInfo.systemVersion;
      company = 'Apple';
      model = iosInfo.model;
    }

    PackageInfo packageInfo = await PackageInfo.fromPlatform();

    String appVersion = packageInfo.version;
    String appBuildNumber = packageInfo.buildNumber;

    http.post(url, body: {
      'scope': scope,
      'message': message,
      'user_id': userId,
      'platform': platform,
      'version': version,
      'sdk': sdk,
      'company': company,
      'model': model,
      'app_version': appVersion,
      'app_build_number': appBuildNumber,
    });
  }
}
