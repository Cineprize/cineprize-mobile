import 'package:cineprize/Models/User.dart';
import 'package:flutter/material.dart';

// Auth olan kullanıcının bilgilerinin kontrol edilebilmesi için kullanılıyor.

class UserManagement with ChangeNotifier {
  static final UserManagement include = new UserManagement();

  User _user = User.init();

  User get user => _user;

  set user(User value) {
    this._user = value;
  }
}
