import 'package:flutter/material.dart';

// Durum yönetimini sağlamak için provider yapısını kullandım.
// Idle: Ekran müsait, Busy meşgul

enum ViewStateEnum { Idle, Busy }

class StateManagement with ChangeNotifier {
  static final StateManagement include = new StateManagement();

  ViewStateEnum _state = ViewStateEnum.Idle;

  ViewStateEnum get state => this._state;

  set state(ViewStateEnum value) {
    this._state = value;
    notifyListeners();
  }
}
