import 'package:cineprize/AbstractLayer/Auth/PhoneVerificationBase.dart';
import 'package:cineprize/DataAccessLayer/Auth/Verification/PhoneVerificationRest.dart';
import 'package:cineprize/Models/Response.dart';
import 'package:cineprize/ServiceLayer/Management/ErrorLogManagement.dart';
import 'package:flutter/material.dart';

class PhoneVerificationService implements PhoneVerificationBase {
  static final PhoneVerificationService include =
      new PhoneVerificationService();
  final PhoneVerificationRest _phoneVerificationRest =
      new PhoneVerificationRest();

  @override
  Future<Response> sendCode({required String phone}) async {
    try {
      Response response = await _phoneVerificationRest.sendCode(phone: phone);

      return response;
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'PhoneVerificationService > sendCode',
          message: e.toString(),
          userId: "");

      return Response(success: false, message: "Bir hata oluştu");
    }
  }

  @override
  Future<Response> verifyCode(
      {required String phone, required String code}) async {
    try {
      Response response =
          await _phoneVerificationRest.verifyCode(phone: phone, code: code);
      return response;
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'PhoneVerificationService > verifyCode',
          message: e.toString(),
          userId: "");

      return Response(success: false, message: "Bir hata oluştu");
    }
  }

  @override
  Future<Response> checkCode() async {
    try {
      Response response = await _phoneVerificationRest.checkCode();
      return response;
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'PhoneVerificationService > checkCode',
          message: e.toString(),
          userId: "");

      return Response(success: false, message: "Bir hata oluştu");
    }
  }
}
