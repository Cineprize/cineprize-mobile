import 'package:cineprize/AbstractLayer/Auth/CurrentUser/CurrentUserBase.dart';
import 'package:cineprize/DataAccessLayer/Auth/Rest/AuthRest.dart';
import 'package:cineprize/Models/User.dart';
import 'package:cineprize/ServiceLayer/Auth/Rest/AuthRestSharedPreferencesVariables.dart';
import 'package:cineprize/ServiceLayer/Management/ErrorLogManagement.dart';
import 'package:cineprize/ServiceLayer/Management/StateManagement.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

// Auth olan kullanıcının bilgilerinin kontrol ve yönetiminin yaptıldığı class.
class CurrentUserService with ChangeNotifier implements CurrentUserBase {
  static final CurrentUserService include = new CurrentUserService();

  final AuthRest _authRest = new AuthRest();

  CurrentUserService() {
    // Bu sınıf ilk çalıştığında ApplicationLayer ile işlem yapabilmek için bu methodları
    // çalıştırarak gerekli nesnelerin içini doldurmuş olacam.
    getCurrentUser();
  }

  // Aktif kullanıcıyı getirir.
  @override
  Future<User> getCurrentUser() async {
    try {
      StateManagement.include.state = ViewStateEnum.Busy;

      final SharedPreferences prefs = await SharedPreferences.getInstance();
      final String? email = prefs.getString(auth_email);
      final String? password = prefs.getString(auth_password);

      User? user;
      if (email != null && password != null) {
        user = await _authRest.login(id: email, password: password);
      }

      return UserManagement.include.user = user ?? User.init();
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'CurrentUserService > getCurrentUser',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());

      return UserManagement.include.user;
    } finally {
      StateManagement.include.state = ViewStateEnum.Idle;
    }
  }
}
