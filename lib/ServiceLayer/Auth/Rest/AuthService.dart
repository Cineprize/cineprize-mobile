import 'package:cineprize/AbstractLayer/Auth/AuthBase.dart';
import 'package:cineprize/DataAccessLayer/Auth/Rest/AuthRest.dart';
import 'package:cineprize/Models/Response.dart';
import 'package:cineprize/Models/User.dart';
import 'package:cineprize/ServiceLayer/Auth/Rest/AuthRestSharedPreferencesVariables.dart';
import 'package:cineprize/ServiceLayer/Management/ErrorLogManagement.dart';
import 'package:cineprize/ServiceLayer/Management/StateManagement.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthService implements AuthBase {
  static final AuthService include = new AuthService();
  final AuthRest _authRest = new AuthRest();

  @override
  Future<void> logout() async {
    try {
      StateManagement.include.state = ViewStateEnum.Busy;

      await _authRest.logout();

      UserManagement.include.user = User.init();
      final SharedPreferences prefs = await SharedPreferences.getInstance();

      prefs.remove(auth_email);
      prefs.remove(auth_password);
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'AuthLogoutService > logout',
          message: e.toString(),
          userId: "");
    } finally {
      StateManagement.include.state = ViewStateEnum.Idle;
    }
  }

  @override
  Future<Response> forgetPassword({required String email}) async {
    try {
      StateManagement.include.state = ViewStateEnum.Busy;

      return await _authRest.forgetPassword(email: email);
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'AuthForgetPasswordService > logout',
          message: e.toString(),
          userId: "");
      return Response(
          success: false,
          message:
              'Şifre sıfırlama işlemi sırasında bir hata oluştu! Lütfen daha sonra tekrar deneyin');
    } finally {
      StateManagement.include.state = ViewStateEnum.Idle;
    }
  }

  @override
  Future<User> login({required String id, required String password}) async {
    try {
      StateManagement.include.state = ViewStateEnum.Busy;
      User user = await _authRest.login(id: id, password: password);
      if (user.errorMessage == null && user.uid != null) {
        UserManagement.include.user = user;

        final SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString(auth_email, id);
        prefs.setString(auth_password, password);
      }
      return user;
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'AuthLoginService > login', message: e.toString(), userId: "");
      // UserManagement.include.user =
      return User.error(errorMessage: e.toString());
    } finally {
      StateManagement.include.state = ViewStateEnum.Idle;
    }
  }

  @override
  Future<User> register(
      {required String userName,
      required String email,
      required String password}) async {
    try {
      StateManagement.include.state = ViewStateEnum.Busy;

      User user = await _authRest.register(
          userName: userName, email: email, password: password);

      if (user.errorMessage == null && user.uid != null) {
        UserManagement.include.user = user;
      }
      return user;
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'AuthRegisterService > register',
          message: e.toString(),
          userId: "");
      // UserManagement.include.user =
      return User.error(
          errorMessage:
              'Bilinmeyen bir hata oluştu. Lütfen daha sonra tekrar deneyiniz.');
    } finally {
      StateManagement.include.state = ViewStateEnum.Idle;
    }
  }

  @override
  Future<User> registerDetail(
      {required DateTime birthDate,
      required String gender,
      required String refer,
      required String canEarn,
      required String userName,
      required String password}) async {
    try {
      StateManagement.include.state = ViewStateEnum.Busy;

      User user = await _authRest.registerDetail(
        birthDate: birthDate,
        gender: gender,
        canEarn: canEarn,
        refer: refer,
        userName: userName,
        password: password,
      );

      if (user.errorMessage == null && user.uid != null) {
        UserManagement.include.user = user;
        final SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString(auth_email, user.email);
        prefs.setString(auth_password, password);
      }

      return user;
    } catch (e) {
      ErrorLogManagement.logged(
        scope: 'AuthRegisterService > registerDetail',
        message: e.toString(),
        userId: UserManagement.include.user.uid.toString(),
      );

      //UserManagement.include.user =
      return User.error(
          errorMessage:
              'Bilinmeyen bir hata oluştu. Lütfen daha sonra tekrar deneyiniz.');
    } finally {
      StateManagement.include.state = ViewStateEnum.Idle;
    }
  }
}
