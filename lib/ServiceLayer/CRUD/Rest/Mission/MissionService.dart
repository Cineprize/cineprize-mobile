import 'package:cineprize/AbstractLayer/CRUD/Mission/MissionBase.dart';
import 'package:cineprize/DataAccessLayer/CRUD/Rest/Mission/MissionRest.dart';
import 'package:cineprize/Models/Mission.dart';
import 'package:cineprize/Models/Response.dart';
import 'package:cineprize/ServiceLayer/Management/ErrorLogManagement.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';

class MissionService implements MissionBase {
  static final MissionService include = new MissionService();

  final MissionRest _missionRest = new MissionRest();

  @override
  Future<Response> read() async {
    try {
      Response response = await _missionRest.read();
      return response;
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'MissionService > read',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());
      print('Error: ' + e.toString());
      return Response(success: false, message: "Bir hata oluştu");
    }
  }

  @override
  Future<Response> click({required String missionId}) async {
    try {
      Response response = await _missionRest.click(missionId: missionId);
      return response;
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'MissionService > click',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());
      print('Error: ' + e.toString());
      return Response(success: false, message: "Bir hata oluştu");
    }
  }
}
