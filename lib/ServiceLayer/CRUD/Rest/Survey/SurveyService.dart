import 'package:cineprize/AbstractLayer/CRUD/Survey/SurveyBase.dart';
import 'package:cineprize/DataAccessLayer/CRUD/Rest/Survey/SurveyRest.dart';
import 'package:cineprize/ServiceLayer/Management/ErrorLogManagement.dart';
import 'package:cineprize/ServiceLayer/Management/StateManagement.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/material.dart';

class SurveyService implements SurveyBase {
  static final SurveyService include = new SurveyService();
  final SurveyRest _surveyRest = new SurveyRest();

  @override
  Future<Map<String, dynamic>> get({required authUser}) async {
    try {
      return await _surveyRest.get(authUser: authUser);
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'SurveyService > get',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());

      return {};
    }
  }

  @override
  Future<Map<String, dynamic>> choose(
      {required String survey_id,
      required String survey_option_id,
      required authUser}) async {
    try {
      Map<String, dynamic> result = await _surveyRest.choose(
          survey_id: survey_id,
          survey_option_id: survey_option_id,
          authUser: authUser);
      if (result['success']) {
        authUser.coin = result['data']['coin'];
        authUser.gold = result['data']['gold'];
      }

      return result;
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'SurveyService > choose',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());

      return {};
    } finally {
      StateManagement.include.state = ViewStateEnum.Idle;
    }
  }
}
