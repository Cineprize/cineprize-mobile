import 'dart:io';

import 'package:cineprize/DataAccessLayer/CRUD/Rest/AppStatus/AppStatusRest.dart';
import 'package:cineprize/Models/Response.dart';
import 'package:cineprize/ServiceLayer/Management/ErrorLogManagement.dart';
import 'package:package_info/package_info.dart';

class AppStatusService {
  static final AppStatusService include = new AppStatusService();
  final AppStatusRest _appStatusRest = new AppStatusRest();
  late Response _response;

  @override
  Future<Response> control() async {
    try {
      PackageInfo packageInfo = await PackageInfo.fromPlatform();

      String? platform;

      if (Platform.isAndroid)
        platform = 'Android';
      else if (Platform.isIOS) platform = 'IOS';

      final String appBuildNumber = packageInfo.buildNumber;

      _response = await _appStatusRest.control(
          platform: platform ?? "Android", app_build_number: appBuildNumber);

      return _response;
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'AppStatusService > control',
          message: e.toString(),
          userId: "");
      return Response(success: false, message: "Bir hata oluştu");
    }
  }
}
