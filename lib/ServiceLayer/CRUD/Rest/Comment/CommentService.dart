import 'package:cineprize/AbstractLayer/CRUD/Comment/CommentBase.dart';
import 'package:cineprize/DataAccessLayer/CRUD/Rest/Comment/CommentRest.dart';
import 'package:cineprize/Models/Response.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/cupertino.dart';

class CommentService implements CommentBase {
  static final CommentService include = new CommentService();
  final CommentRest _commentRest = new CommentRest();

  @override
  Future<Response> add(
      {required String content, required String videoID}) async {
    try {
      return await _commentRest.add(
        content: content,
        videoID: videoID,
      );
    } catch (e) {
      return Response(
          success: false,
          message:
              'Yorum yapma işlemi sırasında bir hata oluştu! Lütfen daha sonra tekrar deneyin');
    }
  }

  @override
  Future<Response> delete({required String commentID}) async {
    try {
      return await _commentRest.delete(commentID: commentID);
    } catch (e) {
      return Response(
          success: false,
          message:
              'Silme işlemi sırasında bir hata oluştu! Lütfen daha sonra tekrar deneyin');
    }
  }

  @override
  Future<Response> like({required String commentID}) async {
    try {
      return await _commentRest.like(commentID: commentID);
    } catch (e) {
      return Response(
          success: false,
          message:
              'Beğenme işlemi sırasında bir hata oluştu! Lütfen daha sonra tekrar deneyin');
    }
  }

  @override
  Future<Response> complain({required String commentID}) async {
    try {
      return await _commentRest.complain(commentID: commentID);
    } catch (e) {
      return Response(
          success: false,
          message:
              'Şikayet işlemi sırasında bir hata oluştu! Lütfen daha sonra tekrar deneyin');
    }
  }

  @override
  Future<Response> ban({required String commentID}) async {
    try {
      if (UserManagement.include.user.role != 'admin')
        return Response(
            success: false,
            message: 'Yorum banlama yetkisine sahip değilsiniz!');
      return await _commentRest.ban(commentID: commentID);
    } catch (e) {
      return Response(
          success: false,
          message:
              'Banlama işlemi sırasında bir hata oluştu! Lütfen daha sonra tekrar deneyin');
    }
  }

  @override
  Future<Response> read({required String videoID, int page = 0}) async {
    try {
      return await _commentRest.read(
        videoID: videoID,
        page: page,
      );
    } catch (e) {
      return Response(
          success: false,
          message: 'Yorumları yükleme sırasında bir hata oluştu!');
    }
  }

  @override
  Future<Response> block({required String commentID}) async {
    try {
      return await _commentRest.block(commentID: commentID);
    } catch (e) {
      return Response(
          success: false,
          message: 'Kullanıcı banlama sırasında bir hata oluştu!');
    }
  }
}
