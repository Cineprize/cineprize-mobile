import 'package:cineprize/AbstractLayer/CRUD/Category/CategoryBase.dart';
import 'package:cineprize/DataAccessLayer/CRUD/Rest/Category/CategoryRest.dart';
import 'package:cineprize/Models/Response.dart';
import 'package:cineprize/ServiceLayer/Management/ErrorLogManagement.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';

class CategoryService implements CategoryBase {
  static final CategoryService include = new CategoryService();
  final CategoryRest _categoryRest = new CategoryRest();

  @override
  Future<Response> read() async {
    try {
      return await _categoryRest.read();
    } catch (e) {
      ErrorLogManagement.logged(scope: 'CategoryReadService > read', message: e.toString(), userId: UserManagement.include.user.uid.toString());
      return Response(success: false, message: 'Kategoriler listelenirken bir hata oluştu!', data: null);
    }
  }
}
