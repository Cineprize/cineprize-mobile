import 'package:cineprize/AbstractLayer/CRUD/FollowWin/FollowWinBase.dart';
import 'package:cineprize/DataAccessLayer/CRUD/Rest/FollowWin/FollowWinRest.dart';
import 'package:cineprize/Models/User.dart';
import 'package:cineprize/ServiceLayer/Management/ErrorLogManagement.dart';
import 'package:cineprize/ServiceLayer/Management/StateManagement.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/material.dart';

class FollowWinService implements FollowWinBase {
  static final FollowWinService include = new FollowWinService();
  final FollowWinRest _followWinRest = new FollowWinRest();

  @override
  Future<Map<String, dynamic>> follow(
      {required String type, required User authUser}) async {
    try {
      Map<String, dynamic> result =
          await _followWinRest.follow(type: type, authUser: authUser);

      if (result['success']) {
        User freshUser = result['data'];
        authUser.coin = freshUser.coin;
        authUser.gold = freshUser.gold;
        authUser.instagramFollow = freshUser.instagramFollow;
        authUser.twitterFollow = freshUser.twitterFollow;
        authUser.telegramFollow = freshUser.telegramFollow;
      }

      return result;
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'FollowWinService > follow',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());
      return {};
    } finally {
      StateManagement.include.state = ViewStateEnum.Idle;
    }
  }
}
