import 'package:cineprize/AbstractLayer/CRUD/Question/QuestionReplyBase.dart';
import 'package:cineprize/DataAccessLayer/CRUD/Rest/Question/QuestionReplyRest.dart';
import 'package:cineprize/ServiceLayer/Management/ErrorLogManagement.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/cupertino.dart';

class QuestionReplyService implements QuestionReplyBase {
  static final QuestionReplyService include = new QuestionReplyService();
  final QuestionReplyRest _questionReplyRest = new QuestionReplyRest();

  @override
  Future<Map<String, dynamic>> reply(
      {required String questionID, required String answer}) async {
    try {
      return await _questionReplyRest.reply(
          questionID: questionID, answer: answer);
    } catch (e) {
      ErrorLogManagement.logged(
        scope: 'QuestionReplyService > reply',
        message: e.toString(),
        userId: UserManagement.include.user.uid.toString(),
      );
      return {'success': false, 'message': 'Bir hata oluştu!'};
    }
  }
}
