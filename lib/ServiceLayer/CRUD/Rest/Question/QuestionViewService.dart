import 'package:cineprize/AbstractLayer/CRUD/Question/QuestionViewBase.dart';
import 'package:cineprize/DataAccessLayer/CRUD/Rest/Question/QuestionViewRest.dart';
import 'package:cineprize/ServiceLayer/Management/ErrorLogManagement.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/cupertino.dart';

class QuestionViewService implements QuestionViewBase {
  static final QuestionViewService include = new QuestionViewService();
  final QuestionViewRest _questionViewRest = new QuestionViewRest();

  @override
  Future<bool> view(String question_id) async {
    try {
      return await _questionViewRest.view(question_id);
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'QuestionViewService > read',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());

      return false;
    }
  }
}
