import 'package:cineprize/AbstractLayer/CRUD/Guess/GuessBase.dart';
import 'package:cineprize/DataAccessLayer/CRUD/Rest/Guess/GuessRest.dart';
import 'package:cineprize/Models/Response.dart';
import 'package:cineprize/ServiceLayer/Management/ErrorLogManagement.dart';
import 'package:cineprize/ServiceLayer/Management/StateManagement.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/material.dart';

class GuessService implements GuessBase {
  static final GuessService include = new GuessService();
  final GuessRest _guessRest = new GuessRest();

  @override
  Future<Response> score({required authUser}) async {
    try {
      return await _guessRest.score(authUser: authUser);
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'GuessService > score',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());

      return Response(success: false, message: "Bir hata oluştu");
    }
  }

  @override
  Future<Map<String, dynamic>> status({required authUser}) async {
    try {
      return await _guessRest.status(authUser: authUser);
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'GuessService > status',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());

      return {};
    }
  }

  @override
  Future<Response> bet({required String bet, authUser}) async {
    try {
      Response response = await _guessRest.bet(bet: bet, authUser: authUser);
      if (response.success) {
        authUser.coin = response.data['coin'];
      }

      return response;
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'GuessService > bet',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());

      return Response(success: false, message: "Bir hata oluştu");
    } finally {
      StateManagement.include.state = ViewStateEnum.Idle;
    }
  }

  @override
  Future<Response> questions({authUser}) async {
    try {
      return await _guessRest.questions(authUser: authUser);
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'GuessService > questions',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());

      return Response(success: false, message: "Bir hata oluştu");
    }
  }

  @override
  Future<Response> completed(
      {required Map<String, bool> answers, authUser}) async {
    try {
      return await _guessRest.completed(answers: answers, authUser: authUser);
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'GuessService > completed',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());

      return Response(success: false, message: "Bir hata oluştu");
    }
  }
}
