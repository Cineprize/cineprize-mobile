import 'package:cineprize/AbstractLayer/CRUD/Giveaway/GiveawayReadBase.dart';
import 'package:cineprize/DataAccessLayer/CRUD/Rest/Giveaway/GiveawayReadRest.dart';
import 'package:cineprize/Models/Giveaway.dart';
import 'package:cineprize/ServiceLayer/Management/ErrorLogManagement.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';

class GiveawayReadService implements GiveawayReadBase {
  static final GiveawayReadService include = new GiveawayReadService();

  final GiveawayReadRest _giveawayReadRest = new GiveawayReadRest();

  @override
  Future<Giveaway> read() async {
    try {
      Giveaway giveaway = await _giveawayReadRest.read();
      return giveaway;
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'GiveawayReadService > read',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());

      //TODO :: check null type
      return Giveaway.fromMap({});
    }
  }
}
