import 'package:cineprize/AbstractLayer/CRUD/Giveaway/GiveawayJoinBase.dart';
import 'package:cineprize/DataAccessLayer/CRUD/Rest/Giveaway/GiveawayJoinRest.dart';
import 'package:cineprize/ServiceLayer/Management/ErrorLogManagement.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/material.dart';

class GiveawayJoinService implements GiveawayJoinBase {
  static final GiveawayJoinService include = new GiveawayJoinService();

  final GiveawayJoinRest _giveawayJoinRest = new GiveawayJoinRest();

  @override
  Future<Map<String, dynamic>> join(String giveawayID) async {
    try {
      var response = await _giveawayJoinRest.join(giveawayID);
      return response;
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'GiveawayJoinService > join',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());

      return {};
    }
  }
}
