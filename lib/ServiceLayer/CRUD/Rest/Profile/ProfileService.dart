import 'package:cineprize/AbstractLayer/CRUD/Profile/ProfileBase.dart';
import 'package:cineprize/DataAccessLayer/CRUD/Rest/Profile/ProfileRest.dart';
import 'package:cineprize/Models/User.dart' as UserModel;
import 'package:cineprize/ServiceLayer/Management/ErrorLogManagement.dart';
import 'package:cineprize/ServiceLayer/Management/StateManagement.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/material.dart';

class ProfileService implements ProfileBase {
  static final ProfileService include = new ProfileService();
  final ProfileRest _profileRest = new ProfileRest();

  @override
  Future<UserModel.User> update({
    required Map<String, dynamic> data,
    bool isSetState = true,
  }) async {
    try {
      var response = await _profileRest.update(data: data);

      if (response.errorMessage == null) {
        UserManagement.include.user = response;
      }

      return response;
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'ProfileUpdateService > update',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());
      //UserManagement.include.user =
      return UserModel.User.error(errorMessage: e.toString());
    } finally {
      if (isSetState) StateManagement.include.state = ViewStateEnum.Idle;
    }
  }
}
