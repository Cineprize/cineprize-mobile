import 'package:cineprize/AbstractLayer/CRUD/Coupon/EnterCouponBase.dart';
import 'package:cineprize/DataAccessLayer/CRUD/Rest/Coupon/EnterCouponRest.dart';
import 'package:cineprize/Models/User.dart';
import 'package:cineprize/ServiceLayer/Management/ErrorLogManagement.dart';
import 'package:cineprize/ServiceLayer/Management/StateManagement.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/material.dart';

class EnterCouponService implements EnterCouponBase {
  static final EnterCouponService include = EnterCouponService();
  final EnterCouponRest _enterCouponRest = EnterCouponRest();

  @override
  Future<Map<String, dynamic>> enter(
      {required String code, required User authUser}) async {
    try {
      StateManagement.include.state = ViewStateEnum.Idle;
      Map<String, dynamic> result =
          await _enterCouponRest.enter(code: code, authUser: authUser);
      if (result['success']) {
        User newUser = result['data'] as User;
        authUser.gold = newUser.gold;
        authUser.coin = newUser.coin;
      }
      return result;
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'EnterCouponService > enter',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());

      return {};
    } finally {
      StateManagement.include.state = ViewStateEnum.Idle;
    }
  }
}
