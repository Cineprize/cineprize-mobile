import 'package:cineprize/AbstractLayer/CRUD/Shop/ProductReadBase.dart';
import 'package:cineprize/DataAccessLayer/CRUD/Rest/Shop/ProductReadRest.dart';
import 'package:cineprize/Models/Product.dart';
import 'package:cineprize/ServiceLayer/Management/ErrorLogManagement.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';

class ProductReadService implements ProductReadBase {
  static final ProductReadService include = new ProductReadService();
  final ProductReadRest _productReadRest = new ProductReadRest();

  @override
  Future<List<Product>> read() async {
    try {
      List<Product> products = await _productReadRest.read();
      return products;
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'ProductReadService > read',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());

      return [];
    }
  }
}
