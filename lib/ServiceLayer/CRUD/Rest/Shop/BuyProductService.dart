import 'package:cineprize/AbstractLayer/CRUD/Shop/BuyProductBase.dart';
import 'package:cineprize/DataAccessLayer/CRUD/Rest/Shop/BuyProductRest.dart';
import 'package:cineprize/Models/Product.dart';
import 'package:cineprize/Models/User.dart';
import 'package:cineprize/ServiceLayer/Management/ErrorLogManagement.dart';
import 'package:cineprize/ServiceLayer/Management/StateManagement.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/material.dart';

class BuyProductService implements BuyProductBase {
  static final BuyProductService include = new BuyProductService();
  final BuyProductRest _buyProductRest = new BuyProductRest();

  @override
  Future<Map<String, dynamic>> buy(
      {required Product product, required User authUser}) async {
    try {
      Map<String, dynamic> result =
          await _buyProductRest.buy(product: product, authUser: authUser);
      if (result['success']) {
        authUser.coin -= product.price;
        authUser.isLocked = false;
      }
      return result;
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'BuyProductService > buy',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());

      return {};
    } finally {
      StateManagement.include.state = ViewStateEnum.Idle;
    }
  }
}
