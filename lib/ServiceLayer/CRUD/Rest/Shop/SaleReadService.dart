import 'package:cineprize/AbstractLayer/CRUD/Shop/SaleReadBase.dart';
import 'package:cineprize/DataAccessLayer/CRUD/Rest/Shop/SaleReadRest.dart';
import 'package:cineprize/ServiceLayer/Management/ErrorLogManagement.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';

class SaleReadService implements SaleReadBase {
  static final SaleReadService include = new SaleReadService();
  final SaleReadRest _saleReadRest = new SaleReadRest();

  @override
  Future<Map<String, dynamic>> read() async {
    try {
      return await _saleReadRest.read();
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'SaleReadService > read',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());

      return {};
    }
  }
}
