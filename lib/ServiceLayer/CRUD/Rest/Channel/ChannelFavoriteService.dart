import 'package:cineprize/AbstractLayer/CRUD/Channel/ChannelFavoriteBase.dart';
import 'package:cineprize/DataAccessLayer/CRUD/Rest/Channel/ChannelFavoriteRest.dart';
import 'package:cineprize/Models/User.dart';
import 'package:cineprize/ServiceLayer/Management/ErrorLogManagement.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';

class ChannelFavoriteService implements ChannelFavoriteBase {
  static final ChannelFavoriteService include = new ChannelFavoriteService();
  final ChannelFavoriteRest _channelFavoriteRest = new ChannelFavoriteRest();

  @override
  Future<bool> isFavorite({required String channel_id}) async {
    try {
      return await _channelFavoriteRest.isFavorite(channel_id: channel_id);
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'ChannelFavoriteService > isFavorite',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());

      return false;
    }
  }

  @override
  Future<Map<String, dynamic>> addOrRemove({required String channel_id}) async {
    try {
      return await _channelFavoriteRest.addOrRemove(channel_id: channel_id);
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'ChannelFavoriteService > addOrRemove',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());

      return {'success': false, 'message': 'Bir hata oluştu'};
    }
  }
}
