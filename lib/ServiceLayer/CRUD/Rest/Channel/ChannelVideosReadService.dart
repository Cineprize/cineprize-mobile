import 'package:cineprize/AbstractLayer/CRUD/Channel/ChannelVideosReadBase.dart';
import 'package:cineprize/DataAccessLayer/CRUD/Rest/Channel/ChannelVideosReadRest.dart';
import 'package:cineprize/Models/Video.dart';
import 'package:cineprize/ServiceLayer/Management/ErrorLogManagement.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';

class ChannelVideosReadService implements ChannelVideosReadBase {
  static final ChannelVideosReadService include =
      new ChannelVideosReadService();
  final ChannelVideosReadRest _channelVideosReadRest =
      new ChannelVideosReadRest();

  @override
  Future<List<Video>> lastVideos({required String channel_id}) async {
    try {
      List<Video> data =
          await _channelVideosReadRest.lastVideos(channel_id: channel_id);
      return data;
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'ChannelVideosReadService > lastVideos',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());

      return [];
    }
  }

  @override
  Future<List<Video>> videos(
      {required String channel_id, String page = "0"}) async {
    try {
      List<Video> data = await _channelVideosReadRest.videos(
          channel_id: channel_id, page: page);
      return data;
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'ChannelVideosReadService > videos',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());

      return [];
    }
  }
}
