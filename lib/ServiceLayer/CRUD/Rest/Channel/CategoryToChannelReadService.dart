import 'package:cineprize/AbstractLayer/CRUD/Channel/CategoryToChannelReadBase.dart';
import 'package:cineprize/DataAccessLayer/CRUD/Rest/Channel/CategoryToChannelReadRest.dart';
import 'package:cineprize/Models/User.dart';
import 'package:cineprize/ServiceLayer/Management/ErrorLogManagement.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/material.dart';

class CategoryToChannelReadService implements CategoryToChannelReadBase {
  static final CategoryToChannelReadService include =
      new CategoryToChannelReadService();
  final CategoryToChannelReadRest _categoryToChannelReadRest =
      new CategoryToChannelReadRest();

  @override
  Future<List<User>> read({required String category_id}) async {
    try {
      List<User> channels =
          await _categoryToChannelReadRest.read(category_id: category_id);
      return channels;
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'CategoryToChannelReadService > read',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());

      return [];
    }
  }
}
