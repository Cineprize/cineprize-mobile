import 'package:cineprize/AbstractLayer/CRUD/Surprise/BuySurpriseBase.dart';
import 'package:cineprize/DataAccessLayer/CRUD/Rest/Surprise/BuySurpriseRest.dart';
import 'package:cineprize/ServiceLayer/Management/ErrorLogManagement.dart';
import 'package:cineprize/ServiceLayer/Management/StateManagement.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';
import 'package:flutter/material.dart';

class BuySurpriseService implements BuySurpriseBase {
  static final BuySurpriseService include = new BuySurpriseService();
  final BuySurpriseRest _buySurpriseRest = new BuySurpriseRest();

  @override
  Future<Map<String, dynamic>> buy(
      {required String surprise_id,
      required int bid,
      required authUser}) async {
    try {
      final int coin = authUser.gold - bid;

      if (coin < 0) {
        return {
          'message': 'Hesabındaki CineGold yeterli değil.',
          'success': false
        };
      }

      Map<String, dynamic> result = await _buySurpriseRest.buy(
          surprise_id: surprise_id, bid: bid, authUser: authUser);
      if (result['success']) {
        authUser.gold -= bid;
        authUser.usedGold += bid;
      }
      return result;
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'BuySurpriseService > buy',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());

      return {};
    } finally {
      StateManagement.include.state = ViewStateEnum.Idle;
    }
  }
}
