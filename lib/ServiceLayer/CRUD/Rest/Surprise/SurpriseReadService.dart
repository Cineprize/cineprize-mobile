import 'package:cineprize/AbstractLayer/CRUD/Surprise/SurpriseReadBase.dart';
import 'package:cineprize/DataAccessLayer/CRUD/Rest/Surprise/SurpriseReadRest.dart';
import 'package:cineprize/Models/Surprise.dart';
import 'package:cineprize/ServiceLayer/Management/ErrorLogManagement.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';

class SurpriseReadService implements SurpriseReadBase {
  static final SurpriseReadService include = new SurpriseReadService();
  final SurpriseReadRest _surpriseReadRest = new SurpriseReadRest();

  @override
  Future<List<Surprise>> read() async {
    try {
      List<Surprise> surprise = await _surpriseReadRest.read();
      return surprise;
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'SurpriseReadService > read',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());

      return [];
    }
  }
}
