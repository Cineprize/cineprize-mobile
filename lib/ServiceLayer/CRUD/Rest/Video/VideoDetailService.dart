import 'package:cineprize/AbstractLayer/CRUD/Video/VideoDetailBase.dart';
import 'package:cineprize/DataAccessLayer/CRUD/Rest/Video/VideoDetailRest.dart';
import 'package:cineprize/Models/VideoDetail.dart';
import 'package:cineprize/ServiceLayer/Management/ErrorLogManagement.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';

class VideoDetailService implements VideoDetailBase {
  static final VideoDetailService include = new VideoDetailService();

  final VideoDetailRest _videoDetailRest = new VideoDetailRest();

  @override
  Future<VideoDetail> get({required String videoId}) async {
    try {
      VideoDetail _videoDetail = await _videoDetailRest.get(videoId: videoId);
      return _videoDetail;
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'VideoDetail > get',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());
      return VideoDetail();
    }
  }
}
