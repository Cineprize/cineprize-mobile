import 'package:cineprize/AbstractLayer/CRUD/Video/TrendReadBase.dart';
import 'package:cineprize/DataAccessLayer/CRUD/Rest/Video/TrendReadRest.dart';
import 'package:cineprize/Models/Video.dart';
import 'package:cineprize/ServiceLayer/Management/ErrorLogManagement.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';

class TrendReadService implements TrendReadBase {
  static final TrendReadService include = new TrendReadService();

  final TrendReadRest _trendReadRest = new TrendReadRest();

  @override
  Future<List<Video>> read() async {
    try {
      List<Video> trends = await _trendReadRest.read();
      return trends;
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'TrendReadService > read',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());

      return [];
    }
  }
}
