import 'package:cineprize/AbstractLayer/CRUD/Video/VideoReadBase.dart';
import 'package:cineprize/CommonLayer/Video/VideoQueryType.dart';
import 'package:cineprize/DataAccessLayer/CRUD/Rest/Video/VideoReadRest.dart';
import 'package:cineprize/Models/CategoryWithVideo.dart';
import 'package:cineprize/Models/Video.dart';
import 'package:cineprize/ServiceLayer/Management/ErrorLogManagement.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';

class VideoReadService implements VideoReadBase {
  static final VideoReadService include = new VideoReadService();
  final VideoReadRest _videoReadRest = new VideoReadRest();

  @override
  Future<List<Video>> read({VideoQueryType? videoQueryType}) async {
    try {
      List<Video> videos =
          await _videoReadRest.read(videoQueryType: videoQueryType);
      return videos;
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'VideoReadService > read',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());

      return [];
    }
  }

  @override
  Future<List<CategoryWithVideo>> categoryWithVideoHome() async {
    try {
      List<CategoryWithVideo> data =
          await _videoReadRest.categoryWithVideoHome();
      return data;
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'VideoReadService > categoryWithVideoHome',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());

      return [];
    }
  }

  @override
  Future<List<CategoryWithVideo>> categoryWithVideoHomeOther() async {
    try {
      List<CategoryWithVideo> data =
          await _videoReadRest.categoryWithVideoHomeOther();
      return data;
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'VideoReadService > categoryWithVideoHomeOthers',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());

      return [];
    }
  }
}
