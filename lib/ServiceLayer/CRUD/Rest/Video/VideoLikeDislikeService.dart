import 'package:cineprize/AbstractLayer/CRUD/Video/VideoLikeDislikeBase.dart';
import 'package:cineprize/DataAccessLayer/CRUD/Rest/Video/VideoLikeDislikeRest.dart';
import 'package:cineprize/Models/Video.dart';
import 'package:cineprize/ServiceLayer/Management/ErrorLogManagement.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';

class VideoLikeDislikeService implements VideoLikeDislikeBase {
  static final VideoLikeDislikeService include = new VideoLikeDislikeService();
  final VideoLikeDislikeRest _videoLikeDislikeRest = new VideoLikeDislikeRest();

  @override
  Future<bool> dislike({required String videoID}) async {
    try {
      return await _videoLikeDislikeRest.dislike(videoID: videoID);
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'VideoLikeDislikeService > dislike',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());

      return false;
    }
  }

  @override
  Future<bool> like({required String videoID}) async {
    try {
      return await _videoLikeDislikeRest.like(videoID: videoID);
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'VideoLikeDislikeService > like',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());
      //TODO:: return only boolean
      return false;
    }
  }
}
