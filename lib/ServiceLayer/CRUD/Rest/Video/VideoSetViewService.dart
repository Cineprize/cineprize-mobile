import 'package:cineprize/AbstractLayer/CRUD/Video/VideoSetViewBase.dart';
import 'package:cineprize/DataAccessLayer/CRUD/Rest/Video/VideoSetViewRest.dart';
import 'package:cineprize/ServiceLayer/Management/ErrorLogManagement.dart';
import 'package:cineprize/ServiceLayer/Management/UserManagement.dart';

class VideoSetViewService implements VideoSetViewBase {
  static final VideoSetViewService include = new VideoSetViewService();
  final VideoSetViewRest _videoSetViewRest = new VideoSetViewRest();

  @override
  Future<List<int>> set({required String videoID, required int maxCoin}) async {
    try {
      List<int> coinReturn = await _videoSetViewRest.set(
          videoID: videoID.toString(), maxCoin: maxCoin);
      return coinReturn;
    } catch (e) {
      ErrorLogManagement.logged(
          scope: 'VideoSetViewService > set',
          message: e.toString(),
          userId: UserManagement.include.user.uid.toString());

      return [];
    }
  }
}
