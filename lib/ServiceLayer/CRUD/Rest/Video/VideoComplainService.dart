import 'package:cineprize/AbstractLayer/CRUD/Video/VideoComplainBase.dart';
import 'package:cineprize/DataAccessLayer/CRUD/Rest/Video/VideoComplainRest.dart';
import 'package:cineprize/Models/Response.dart';
import 'package:flutter/cupertino.dart';

class VideoComplainService implements VideoComplainBase {
  static final VideoComplainService include = new VideoComplainService();

  final VideoComplainRest _videoComplainRest = new VideoComplainRest();

  @override
  Future<Response> complain(
      {required String videoID, required String reason}) async {
    try {
      return await _videoComplainRest.complain(
          videoID: videoID, reason: reason);
    } catch (e) {
      return Response(
          success: false,
          message:
              'Şikayet işlemi sırasında bir hata oluştu! Lütfen daha sonra tekrar deneyin');
    }
  }
}
