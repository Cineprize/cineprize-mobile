import 'package:cineprize/AbstractLayer/CRUD/Slider/SliderReadBase.dart';
import 'package:cineprize/DataAccessLayer/CRUD/Rest/Slider/SliderReadRest.dart';
import 'package:cineprize/Models/Slider.dart';

class SliderReadService implements SliderReadBase {
  static final SliderReadService include = new SliderReadService();
  final SliderReadRest _sliderReadRest = new SliderReadRest();

  @override
  Future<List<Slider>> read() async {
    try {
      List<Slider> sliders = await _sliderReadRest.read();
      return sliders;
    } catch (e) {
      print('Hata SliderReadService catch ${e.toString()}');
      return [];
    }
  }
}
