enum VideoOrderByEnum { ViewCount, CommentCount, CreatedAt, None }

extension VideoOrderByExtension on VideoOrderByEnum {
  String get value {
    switch (this) {
      case VideoOrderByEnum.ViewCount:
        return 'viewCount';
      case VideoOrderByEnum.CommentCount:
        return 'commentCount';
      case VideoOrderByEnum.CreatedAt:
        return 'createdAt';
      default:
        return '';
    }
  }
}
