import 'package:cineprize/ApplicationLayer/Auth/AuthManagement/LoginNavigator.dart';
import 'package:cineprize/Tools/Provider/Providers.dart';
import 'package:cineprize/Tools/PushNotification/FCM.dart';
import 'package:cineprize/Tools/SharedPreferences/MySharedPreferences.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'Tools/Ads/AdmobAds/AdmobInitialize.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // TODO: IN APP PURCHASE İÇİN BURASI YORUMDAN ÇIKACAK.
  // import 'package:in_app_purchase/in_app_purchase.dart';
  // InAppPurchaseConnection.enablePendingPurchases();

  // Introdution görüntülenmesi gerekiyor mu ?
  introductionPageViewed =
      await MySharedPreferences.getInt(key: 'introductionPage') ?? 0;

  // Firebase'in başlatılması.
  await Firebase.initializeApp();
  AdmobInitialize.instance;
  // Firebase push notification.
  final FCM fcm = new FCM();
  await fcm.initialize();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Colors.transparent));
    return Providers(
      child: MaterialApp(
          title: 'CinePrize',
          debugShowCheckedModeBanner: false,
          home: LoginNavigator(),
          theme: ThemeData(
            canvasColor: Color(0xff404140),
            appBarTheme: AppBarTheme(color: Color(0xff191d21)),
            unselectedWidgetColor: Colors.white,
            iconTheme: IconThemeData(color: Colors.white, size: 20),
            visualDensity: VisualDensity.adaptivePlatformDensity,
            scaffoldBackgroundColor: Colors.black,
            platform: Theme.of(context).platform,
          )),
    ).include();
  }
}
